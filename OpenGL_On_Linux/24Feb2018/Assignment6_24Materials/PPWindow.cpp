//GLXContextWithFrameBufferConfigs.cpp

//Headers
#include<iostream>
#include<stdio.h> // For printf()
#include<stdlib.h> // for exit()
#include<memory.h> //for memset()

//header file for Xserver
#include<X11/Xlib.h> //analogous to Windows.h
#include<X11/Xutil.h> //For visuals
#include<X11/XKBlib.h> //XkbKeycodeTOKeysym()
#include<X11/keysym.h> // for keysym

#include<GL/glew.h> // *Include glew.h before gl.h
#include<GL/gl.h> 
#include<GL/glx.h> //for glx functions

#include "vmath.h"

#include "Sphere.h"

using namespace vmath;

enum
{
	VDG_ATTRIBUTE_VERTEX = 0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE0,
};



#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//global variable declaration
FILE *gpFile = NULL;


Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
typedef GLXContext (*glXCreateContextAttribsARBProc)(Display*, GLXFBConfig, GLXContext, Bool, const int*);
glXCreateContextAttribsARBProc glXCreateContextAttribsARB = NULL;
GLXFBConfig gGLXFBConfig;
GLXContext gGLXContext; //parallel to HGLRC

bool gbFullscreen = false;

float width, height;
float width1, height1;

GLfloat red_x = 0.0f;
GLfloat red_y = 0.0f;
GLfloat red_z = 0.0f;

GLfloat green_x = 0.0f;
GLfloat green_y = 0.0f;
GLfloat green_z = 0.0f;

GLfloat blue_x = 0.0f;
GLfloat blue_y = 0.0f;
GLfloat blue_z = 0.0f;

GLfloat Xcenter = 0.0f;
GLfloat Ycenter = 0.0f;
GLfloat Zcenter = 0.0f;

GLfloat angleCircle = 0.0f;

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];

GLuint gNumVertices;
GLuint gNumElements;

GLuint gVao_sphere;
GLuint gVbo_sphere_position;
GLuint gVbo_sphere_normal;
GLuint gVbo_sphere_element;

GLuint gMVPUniform;

GLuint model_matrix_uniform, view_matrix_uniform, projection_matrix_uniform, rotation_matrix_uniform;

GLuint L_KeyPressed_uniform;

GLuint La0_uniform;
GLuint Ld0_uniform;
GLuint Ls0_uniform;
GLuint light0_position_uniform;

GLuint La1_uniform;
GLuint Ld1_uniform;
GLuint Ls1_uniform;
GLuint light1_position_uniform;

GLuint La2_uniform;
GLuint Ld2_uniform;
GLuint Ls2_uniform;
GLuint light2_position_uniform;

GLuint Ka_uniform;
GLuint Kd_uniform;
GLuint Ks_uniform;
GLuint material_shininess_uniform;


mat4 gPerspectiveProjectionMatrix;

bool gbAnimate = true;
bool gbLight = false;

bool Xkey = false;
bool Ykey = false;
bool Zkey = false;

GLfloat light0_Ambient[] = { 0.0f,  0.0f,  0.0f, 1.0f };
GLfloat light0_Diffuse[] = { 1.0f,  1.0f,  1.0f, 0.0f }; //light color - white
GLfloat light0_Specular[] = { 1.0f,  1.0f,  1.0f, 1.0f };
GLfloat light0_Position[] = { 200.0f,  100.0f,  100.0f, 1.0f };

GLfloat light1_Ambient[] = { 0.0f,  0.0f,  0.0f, 1.0f };
GLfloat light1_Diffuse[] = { 1.0f,  1.0f,  1.0f, 0.0f }; //light color - white
GLfloat light1_Specular[] = { 1.0f,  1.0f,  1.0f, 1.0f };
GLfloat light1_Position[] = { 200.0f,  100.0f,  100.0f, 1.0f };

GLfloat light2_Ambient[] = { 0.0f,  0.0f,  0.0f, 1.0f };
GLfloat light2_Diffuse[] = { 1.0f,  1.0f,  1.0f, 0.0f }; //light color - white
GLfloat light2_Specular[] = { 1.0f,  1.0f,  1.0f, 1.0f };
GLfloat light2_Position[] = { 200.0f,  100.0f,  100.0f, 1.0f };

GLfloat light_model_ambient[] = { 0.2f, 0.2f, 0.2f, 0.0f };
GLfloat light_model_local_viewer[] = { 0.0f };

//First sphere on first column, emerald
GLfloat material1_ambient[] = { 0.0215f, 0.1745f, 0.0215f, 1.0f };
GLfloat material1_diffuse[] = { 0.07568f, 0.61424f, 0.07568f, 1.0f };
GLfloat material1_specular[] = { 0.633f, 0.727811f, 0.633f, 1.0f };
GLfloat material1_shininess[] = { 0.6f * 128 };

//Second sphere on first column, jade
GLfloat material2_ambient[] = { 0.135f, 0.2225f, 0.1575f, 1.0f };
GLfloat material2_diffuse[] = { 0.54f, 0.89f, 0.63f, 1.0f };
GLfloat material2_specular[] = { 0.316228f, 0.316228f, 0.316228f, 1.0f };
GLfloat material2_shininess[] = { 0.1f * 128 };

//Third sphere on first column, obsidian
GLfloat material3_ambient[] = { 0.05375f, 0.05f, 0.06625f, 1.0f };
GLfloat material3_diffuse[] = { 0.18275f, 0.17f, 0.22525f, 1.0f };
GLfloat material3_specular[] = { 0.332741f, 0.328634f, 0.346435f, 1.0f };
GLfloat material3_shininess[] = { 0.3f * 128 };

//Fourth sphere on first column, pearl
GLfloat material4_ambient[] = { 0.25f, 0.20725f, 0.20725f, 1.0f };
GLfloat material4_diffuse[] = { 1.0f, 0.829f, 0.829f, 1.0f };
GLfloat material4_specular[] = { 0.296648f, 0.296648f, 0.296648f, 1.0f };
GLfloat material4_shininess[] = { 0.088f * 128 };

//Fifth sphere on first column, ruby
GLfloat material5_ambient[] = { 0.1745f, 0.01175f, 0.01175f, 1.0f };
GLfloat material5_diffuse[] = { 0.61424f, 0.04136f, 0.04136f, 1.0f };
GLfloat material5_specular[] = { 0.727811f, 0.626959f, 0.626959f, 1.0f };
GLfloat material5_shininess[] = { 0.6f * 128 };

//Sixth sphere on first column, turquoise
GLfloat material6_ambient[] = { 0.1f, 0.18725f, 0.1745f, 1.0f };
GLfloat material6_diffuse[] = { 0.396f, 0.74151f, 0.69102f, 1.0f };
GLfloat material6_specular[] = { 0.297254f, 0.30829f, 0.306678f, 1.0f };
GLfloat material6_shininess[] = { 1.0f };

//First sphere on second column, brass
GLfloat material7_ambient[] = { 0.329412f, 0.223529f, 0.027451f, 1.0f };
GLfloat material7_diffuse[] = { 0.780392f, 0.568627f, 0.113725f, 1.0f };
GLfloat material7_specular[] = { 0.992157f, 0.941176f, 0.807843f, 1.0f };
GLfloat material7_shininess[] = { 0.21794872f * 128 };

//Second sphere on second column, bronze
GLfloat material8_ambient[] = { 0.2125f, 0.1275f, 0.054f, 1.0f };
GLfloat material8_diffuse[] = { 0.714f, 0.4284f, 0.18144f, 1.0f };
GLfloat material8_specular[] = { 0.393548f, 0.211906f, 0.166721f, 1.0f };
GLfloat material8_shininess[] = { 0.2f * 128 };

//Third sphere on second column, chrome
GLfloat material9_ambient[] = { 0.25f, 0.25f, 0.25f, 1.0f };
GLfloat material9_diffuse[] = { 0.4f, 0.4f, 0.4f, 1.0f };
GLfloat material9_specular[] = { 0.774597f, 0.774597f, 0.774597f, 1.0f };
GLfloat material9_shininess[] = { 0.6f * 128 };

//Fourth sphere on second column, copper
GLfloat material10_ambient[] = { 0.19125f, 0.0735f, 0.0225f, 1.0f };
GLfloat material10_diffuse[] = { 0.7038f, 0.27048f, 0.0828f, 1.0f };
GLfloat material10_specular[] = { 0.276777f, 0.137622f, 0.086014f, 1.0f };
GLfloat material10_shininess[] = { 0.1f * 128 };

//Fifth sphere on second column, gold
GLfloat material11_ambient[] = { 0.24725f, 0.1995f, 0.07455f, 1.0f };
GLfloat material11_diffuse[] = { 0.75164f, 0.60648f, 0.22648f, 1.0f };
GLfloat material11_specular[] = { 0.628281f, 0.555802f, 0.366065f, 1.0f };
GLfloat material11_shininess[] = { 0.4f * 128 };

//Sixth sphere on second column, silver
GLfloat material12_ambient[] = { 0.19225f, 0.19225f, 0.19225f, 1.0f };
GLfloat material12_diffuse[] = { 0.50754f, 0.50754f, 0.50754f, 1.0f };
GLfloat material12_specular[] = { 0.508273f, 0.508273f, 0.508273f, 1.0f };
GLfloat material12_shininess[] = { 0.4f * 128 };

//First sphere on third column, black
GLfloat material13_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat material13_diffuse[] = { 0.01f, 0.01f, 0.01f, 1.0f };
GLfloat material13_specular[] = { 0.50f, 0.50f, 0.50f, 1.0f };
GLfloat material13_shininess[] = { 0.25f * 128 };

//Second sphere on third column, cyan
GLfloat material14_ambient[] = { 0.0f, 0.1f, 0.06f, 1.0f };
GLfloat material14_diffuse[] = { 0.0f, 0.50980392f, 0.50980392f, 1.0f };
GLfloat material14_specular[] = { 0.50196078f, 0.50196078f, 0.50196078f, 1.0f };
GLfloat material14_shininess[] = { 0.25f * 128 };

//Third sphere on third column, green
GLfloat material15_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat material15_diffuse[] = { 0.1f, 0.35f, 0.1f, 1.0f };
GLfloat material15_specular[] = { 0.45f, 0.55f, 0.45f, 1.0f };
GLfloat material15_shininess[] = { 0.25f * 128 };

//Fourth sphere on third column, red
GLfloat material16_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat material16_diffuse[] = { 0.5f, 0.0f, 0.0f, 1.0f };
GLfloat material16_specular[] = { 0.7f, 0.6f, 0.6f, 1.0f };
GLfloat material16_shininess[] = { 0.25f * 128 };

//Fifth sphere on third column, white
GLfloat material17_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat material17_diffuse[] = { 0.55f, 0.55f, 0.55f, 1.0f };
GLfloat material17_specular[] = { 0.70f, 0.70f, 0.70f, 1.0f };
GLfloat material17_shininess[] = { 0.25f * 128 };

//Sixth sphere on third column, yellow  plastic
GLfloat material18_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat material18_diffuse[] = { 0.5f, 0.5f, 0.0f, 1.0f };
GLfloat material18_specular[] = { 0.60f, 0.60f, 0.50f, 1.0f };
GLfloat material18_shininess[] = { 0.25f * 128 };

//First sphere on fourth column, black
GLfloat material19_ambient[] = { 0.02f, 0.02f, 0.02f, 1.0f };
GLfloat material19_diffuse[] = { 0.01f, 0.01f, 0.01f, 1.0f };
GLfloat material19_specular[] = { 0.4f, 0.4f, 0.4f, 1.0f };
GLfloat material19_shininess[] = { 0.078125f * 128 };

//Second sphere on fourth column, cyan
GLfloat material20_ambient[] = { 0.0f, 0.05f, 0.05f, 1.0f };
GLfloat material20_diffuse[] = { 0.4f, 0.5f, 0.5f, 1.0f };
GLfloat material20_specular[] = { 0.04f, 0.7f, 0.7f, 1.0f };
GLfloat material20_shininess[] = { 0.078125f * 128 };

//Third sphere on fourth column, green
GLfloat material21_ambient[] = { 0.0f, 0.05f, 0.0f, 1.0f };
GLfloat material21_diffuse[] = { 0.4f, 0.5f, 0.4f, 1.0f };
GLfloat material21_specular[] = { 0.04f, 0.7f, 0.04f, 1.0f };
GLfloat material21_shininess[] = { 0.078125f * 128 };

//Fourth sphere on fourth column, red
GLfloat material22_ambient[] = { 0.05f, 0.0f, 0.0f, 1.0f };
GLfloat material22_diffuse[] = { 0.5f, 0.4f, 0.4f, 1.0f };
GLfloat material22_specular[] = { 0.7f, 0.04f, 0.04f, 1.0f };
GLfloat material22_shininess[] = { 0.078125f * 128 };

//Fifth sphere on fourth column, white
GLfloat material23_ambient[] = { 0.05f, 0.05f, 0.05f, 1.0f };
GLfloat material23_diffuse[] = { 0.5f, 0.5f, 0.5f, 1.0f };
GLfloat material23_specular[] = { 0.7f, 0.7f, 0.7f, 1.0f };
GLfloat material23_shininess[] = { 0.078125f * 128 };

//Fifth sphere on fourth column, yellow rubber
GLfloat material24_ambient[] = { 0.05f, 0.05f, 0.0f, 1.0f };
GLfloat material24_diffuse[] = { 0.5f, 0.5f, 0.4f, 1.0f };
GLfloat material24_specular[] = { 0.7f, 0.7f, 0.04f, 1.0f };
GLfloat material24_shininess[] = { 0.078125f * 128 };


//Entry point function
int main(int argc, char *argv[])
{
	//function prototype
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void initialize(void);
	void resize(int,int);
	void display();
	void uninitialize();
	void update(void);

	//variable declaration
	static bool bIsAKeyPressed = false;
	static bool bIsLKeyPressed = false;


	//Create Window
	CreateWindow();

	//Initialize
	initialize();

	//Message loop

	//variable declaration
	XEvent event;
	KeySym keysym;
	int winWidth;
	int winHeight;
	bool bDone = false;

	while (bDone == false)
	{
		while (XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,&event); //Parallel to GetMessage()
			switch (event.type)  //parallel to iMsg
			{
			case MapNotify:
				break;
			case KeyPress:
				keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
				switch (keysym)
				{
				case XK_Escape:
					bDone = true;
					break;
				case XK_F:
				case XK_f:
					if (gbFullscreen == false)
					{
						ToggleFullscreen();
						gbFullscreen = true;
					}
					else
					{
						ToggleFullscreen();
						gbFullscreen = false;
					}
					break;
				case XK_A:
				case XK_a:
					if (bIsAKeyPressed == false)
					{
						gbAnimate = true;
						bIsAKeyPressed = true;
					}
					else
					{
						gbAnimate = false;
						bIsAKeyPressed = false;
					}
					break;
				case XK_L:
				case XK_l:
					if (bIsLKeyPressed == false)
					{
						gbLight = true;
						bIsLKeyPressed = true;
					}
					else
					{
						gbLight = false;
						bIsLKeyPressed = false;
					}
					break;
				case XK_X:
				case XK_x:
					{
						Xkey = true;
						Ykey = false;
						Zkey = false;
					}
					break;
				case XK_Y:
				case XK_y:
					{
						Xkey = false;
						Ykey = true;
						Zkey = false;
					}
					break;
				case XK_Z:
				case XK_z:
					{
						Xkey = false;
						Ykey = false;
						Zkey = true;
					}
					break;
				default:
					break;
				}
				break;
			case ButtonPress:
				switch (event.xbutton.button)
				{
				case 1: //Left button
					break;
				case 2: //Middle button
					break;
				case 3: //Right button
					break;
				default:
					break;

				}
				break;
			case MotionNotify: //parallel to WM_MOUSEMOVE
				break;
			case ConfigureNotify: //parallel to WM_SIZE
				winWidth = event.xconfigure.width;
				winHeight = event.xconfigure.height;
				resize(winWidth, winHeight);
				break;
			case Expose: //parallel to WM_PAINT
				break;
			case DestroyNotify:
				break;
			case 33: //Close button, system menu ->close
				bDone = true;
				break;
			default:
				break;
			}
		}
		display();
		if (Xkey == true || Ykey == true || Zkey == true)
			update();
	}
	uninitialize();
	return(0);
}


void CreateWindow(void)
{
	//function prototype
	void uninitialize(void);

	//variable declaration
	XSetWindowAttributes winAttribs;
	GLXFBConfig *pGLXFBConfigs = NULL;
	GLXFBConfig bestGLXFBConfig;
	XVisualInfo *pTempXVisualInfo = NULL;
	int iNumFBConfigs = 0;
	int styleMask;
	int i;

	static int frameBufferAttributes[] = {
		GLX_X_RENDERABLE, True,
		GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
		GLX_RENDER_TYPE, GLX_RGBA_BIT,
		GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR,
		GLX_RED_SIZE, 8,
		GLX_GREEN_SIZE, 8,
		GLX_BLUE_SIZE, 8,
		GLX_ALPHA_SIZE, 8,
		GLX_DEPTH_SIZE, 24,
		GLX_STENCIL_SIZE,8,
		GLX_DOUBLEBUFFER, True,
		//GLX_SAMPLE_BUFFERS,1,
		//GLX_SAMPLES,4,
	None }; //Array must be terminated by 0

	//code
	gpDisplay = XOpenDisplay(NULL);
	if (gpDisplay == NULL)
	{
		printf("ERROR : Unable to obtain X Display. \n");
		uninitialize();
		exit(1);
	}

	//get a new framebuffer config that meets our attrib requirements
	pGLXFBConfigs = glXChooseFBConfig(gpDisplay, DefaultScreen(gpDisplay), frameBufferAttributes, &iNumFBConfigs);
	if (pGLXFBConfigs == NULL)
	{
		printf("Failed to get valid Framebuffer config, exiting now.....\n");
		uninitialize();
		exit(1);
	}
	printf("%d matching FB configs found. \n", iNumFBConfigs);

	//pick that FB config/visual with the most samples per pixel
	int bestFramebufferConfig = -1, worstFramebufferConfig = -1, bestNumberOfSamples = -1, worstNumberOfSamples = 999;
	for (i = 0; i < iNumFBConfigs; i++)
	{
		pTempXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, pGLXFBConfigs[1]);
		if (pTempXVisualInfo)
		{
			int sampleBuffer, samples;
			glXGetFBConfigAttrib(gpDisplay, pGLXFBConfigs[i], GLX_SAMPLE_BUFFERS, &sampleBuffer);
			glXGetFBConfigAttrib(gpDisplay, pGLXFBConfigs[i], GLX_SAMPLES, &samples);
			printf("Matching FrameBuffer Config = %d :Visual ID=0x%lu: SAMPLE_BUFFERS = %d : SAMPLES = %d \n", i, pTempXVisualInfo->visualid, sampleBuffer, samples);
			if (bestFramebufferConfig < 0 || sampleBuffer && samples > bestNumberOfSamples)
			{
				bestFramebufferConfig = i;
				bestNumberOfSamples = samples;

			}
			if (worstFramebufferConfig < 0 || !sampleBuffer || samples < worstNumberOfSamples)
			{
				worstFramebufferConfig = i;
				worstNumberOfSamples = samples;

			}
		}
		XFree(pTempXVisualInfo);
	}
	bestGLXFBConfig = pGLXFBConfigs[bestFramebufferConfig];

	//set global GLXFBConfig
	gGLXFBConfig = bestGLXFBConfig;

	//be sure to free FBConfig list allocated by glXChooseFBConfig()
	XFree(pGLXFBConfigs);

	gpXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, bestGLXFBConfig);
	printf("Chosen Visual ID = 0x%lu \n", gpXVisualInfo->visualid);

	//setting window's attributes
	winAttribs.border_pixel = 0;
	winAttribs.background_pixmap = 0;
	winAttribs.colormap = XCreateColormap(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen),//you can give defaultscreen as well
		gpXVisualInfo->visual,
		AllocNone); // For 'movable' memory allocation

	winAttribs.event_mask = StructureNotifyMask | KeyPressMask | ButtonPressMask | ExposureMask | VisibilityChangeMask | PointerMotionMask;
	styleMask = CWBorderPixel | CWEventMask | CWColormap;
	gColormap = winAttribs.colormap;

	gWindow = XCreateWindow(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen),
		0,
		0,
		WIN_WIDTH,
		WIN_HEIGHT,
		0, //border width
		gpXVisualInfo->depth, // defpth of visual (depth for colormap)
		InputOutput, //class (type) of your window
		gpXVisualInfo->visual,
		styleMask,
		&winAttribs);

	if (!gWindow)
	{
		printf("Failure In Window Creation. \n");
		uninitialize();
		exit(1);
	}

	XStoreName(gpDisplay, gWindow, "OpenGL Window");

	Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_WINDOW_STATE", True);
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);

	XMapWindow(gpDisplay, gWindow);

}

void ToggleFullscreen(void)
{
	//code
	Atom wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False); // Normal Window state

	XEvent event;
	memset(&event, 0, sizeof(XEvent));

	event.type = ClientMessage;
	event.xclient.window = gWindow;
	event.xclient.message_type = wm_state;
	event.xclient.format = 32; //32-bit
	event.xclient.data.l[0] = gbFullscreen ? 0 : 1;

	Atom fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	event.xclient.data.l[1] = fullscreen;

	//parallel to SendMessage()
	XSendEvent(gpDisplay,
		RootWindow(gpDisplay, gpXVisualInfo->screen),
		False, // do not send this message to sibling windows
		StructureNotifyMask, //resizing mask (event_mask)
		&event);

}

void initialize(void)
{
	//function declaration
	void uninitialize(void);
	void resize(int, int);

	//code
	//create a new GL context 4.5 for rendering
	glXCreateContextAttribsARB = (glXCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte *)"glXCreateContextAttribsARB");

	GLint attribs[] = {
		GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
		GLX_CONTEXT_MINOR_VERSION_ARB, 5,
		GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB,
		0 }; //array must be terminated by 0

	gGLXContext = glXCreateContextAttribsARB(gpDisplay, gGLXFBConfig, 0, True, attribs);

	if (!gGLXContext) // fallback to safe old style 2.x context
	{
		//When a context version below 3.0 is requested, implementations will return
		//the newest context version compatible with OpenGL versions less than version 3.0.

		GLint attribs[] = {
			GLX_CONTEXT_MAJOR_VERSION_ARB, 1,
			GLX_CONTEXT_MINOR_VERSION_ARB, 0,
			0 }; // array must be terminated by 0
		printf("Failed to create GLX 4.5 context , hence using Old-style GLX context \n");
		gGLXContext = glXCreateContextAttribsARB(gpDisplay, gGLXFBConfig, 0, True, attribs);
	}
	else //successfully created 4.5 context
	{
		printf("OpenGL Context 4.5 is created \n");

	}

	if (!glXIsDirect(gpDisplay, gGLXContext))
	{
		printf("Indirect GLX rendering context obtained \n");

	}
	else
	{
		printf("Direct GLX rendering Context obtained \n");
	}

	glXMakeCurrent(gpDisplay, gWindow, gGLXContext);

	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		glXDestroyContext(gpDisplay, gGLXContext);
	}

//*** Vertex Shader ***
	// Create Shader
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//Provide source code to shader
	const GLchar *vertexShaderSourceCode =
		"#version 430 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_rotation_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform vec4 u_light0_position;" \
		"uniform vec4 u_light1_position;" \
		"uniform vec4 u_light2_position;" \
		"uniform int u_lighting_enabled;" \
		"out vec3 transformed_normals;" \
		"out vec3 light0_direction;" \
		"out vec3 light1_direction;" \
		"out vec3 light2_direction;" \
		"out vec3 viewer_vector;" \
		"void main(void)" \
		"{" \
		"if (u_lighting_enabled == 1)" \
		"{" \
		"vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;" \
		"transformed_normals =  mat3(u_view_matrix * u_model_matrix * u_rotation_matrix) * vNormal;" \
		"light0_direction = vec3(u_light0_position) - eye_coordinates.xyz;" \
		"light1_direction = vec3(u_light1_position) - eye_coordinates.xyz;" \
		"light2_direction = vec3(u_light2_position) - eye_coordinates.xyz;" \
		"viewer_vector = -eye_coordinates.xyz;" \
		"}" \
		"gl_Position = u_projection_matrix  * u_view_matrix * u_model_matrix * u_rotation_matrix * vPosition;" \
		"}";

	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gVertexShaderObject);
	GLint iInfoLogLength = 0;
	GLint iShaderCompliedStatus = 0;
	char szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompliedStatus);
	if (iShaderCompliedStatus == GL_FALSE)
		{
			char *szInfoLog = NULL;
			glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength); 
			if (iInfoLogLength > 0)
			{
				szInfoLog = (char *)malloc(iInfoLogLength);
				if (szInfoLog != NULL)
				{
					GLsizei written;
					glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
					fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
					free(szInfoLog);
					uninitialize();
					exit(0);
				}
			}
		}
	

	//*** Fragment Shader ***
	// Create Shader
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//Provide source code to shader
	const GLchar *fragmentShaderSourceCode =
		"#version 430 core" \
		"\n" \
		"in vec3 transformed_normals; " \
		"in vec3 light0_direction; " \
		"in vec3 light1_direction; " \
		"in vec3 light2_direction; " \
		"in vec3 viewer_vector; " \
		"out vec4 FragColor;" \
		"uniform vec3 u_La0;" \
		"uniform vec3 u_Ld0;" \
		"uniform vec3 u_Ls0;" \
		"uniform vec3 u_La1;" \
		"uniform vec3 u_Ld1;" \
		"uniform vec3 u_Ls1;" \
		"uniform vec3 u_La2;" \
		"uniform vec3 u_Ld2;" \
		"uniform vec3 u_Ls2;" \
		"uniform vec3 u_Ka;" \
		"uniform vec3 u_Kd;" \
		"uniform vec3 u_Ks;" \
		"uniform float u_material_shininess;" \
		"uniform int u_lighting_enabled;" \
		"void main(void)" \
		"{" \
		"vec3 light0;" \
		"vec3 light1;" \
		"vec3 light2;" \
		"vec3 phong_ads_color;" \
		"if (u_lighting_enabled == 1)" \
		"{" \
		"vec3 normalized_transformed_normals = normalize(transformed_normals);" \
		"vec3 normalized_light0_direction = normalize(light0_direction);" \
		"vec3 normalized_light1_direction = normalize(light1_direction);" \
		"vec3 normalized_light2_direction = normalize(light2_direction);" \
		"vec3 normalized_viewer_vector = normalize(viewer_vector);" \
		"float tn_dot_ld0 = max(dot(normalized_transformed_normals, normalized_light0_direction), 0.0);" \
		"vec3 ambient0 = u_La0 * u_Ka;" \
		"vec3 diffuse0 = u_Ld0 * u_Kd * tn_dot_ld0;" \
		"vec3 reflection_vector0 = reflect(-normalized_light0_direction,normalized_transformed_normals);" \
		"vec3 specular0 = u_Ls0 * u_Ks * pow(max(dot(reflection_vector0, normalized_viewer_vector), 0.0), u_material_shininess);" \
		"float tn_dot_ld1 = max(dot(normalized_transformed_normals, normalized_light1_direction), 0.0);" \
		"vec3 ambient1 = u_La1 * u_Ka;" \
		"vec3 diffuse1 = u_Ld1 * u_Kd * tn_dot_ld1;" \
		"vec3 reflection_vector1 = reflect(-normalized_light1_direction,normalized_transformed_normals);" \
		"vec3 specular1 = u_Ls1 * u_Ks * pow(max(dot(reflection_vector1, normalized_viewer_vector), 0.0), u_material_shininess);" \
		"float tn_dot_ld2 = max(dot(normalized_transformed_normals, normalized_light2_direction), 0.0);" \
		"vec3 ambient2 = u_La2 * u_Ka;" \
		"vec3 diffuse2 = u_Ld2 * u_Kd * tn_dot_ld2;" \
		"vec3 reflection_vector2 = reflect(-normalized_light2_direction,normalized_transformed_normals);" \
		"vec3 specular2 = u_Ls2 * u_Ks * pow(max(dot(reflection_vector2, normalized_viewer_vector), 0.0), u_material_shininess);" \
		"light0 = ambient0 + diffuse0 + specular0;" \
		"light1 = ambient1 + diffuse1 + specular1;" \
		"light2 = ambient2 + diffuse2 + specular2;" \
		"phong_ads_color = light0;" \
		"}" \
		"else" \
		"{" \
		"phong_ads_color =  vec3(1.0,  1.0,  1.0);" \
		"}" \
		"FragColor = vec4(phong_ads_color, 1.0); " \
		"}";

	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gFragmentShaderObject);
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompliedStatus);
	if (iShaderCompliedStatus == GL_FALSE)
	{
		char *szInfoLog = NULL;
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	//Shader program
	//create
	gShaderProgramObject = glCreateProgram();

	//attach vertex shader to shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	//attach fragment shader to shader program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	//Pre-link binding of shader program object with vertex shader position attribute
	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");

	//Pre-link binding of shader program object with vertex shader Normal attribute
	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");

	//Link shader
	glLinkProgram(gShaderProgramObject);
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		char *szInfoLog = NULL;
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	} 

	//get MVP uniform location
	model_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
	view_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
	projection_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");
	rotation_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_rotation_matrix");

	L_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObject, "u_lighting_enabled");

	La0_uniform = glGetUniformLocation(gShaderProgramObject, "u_La0");
	Ld0_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld0");
	Ls0_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls0");
	light0_position_uniform = glGetUniformLocation(gShaderProgramObject, "u_light0_position");

	La1_uniform = glGetUniformLocation(gShaderProgramObject, "u_La1");
	Ld1_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld1");
	Ls1_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls1");
	light1_position_uniform = glGetUniformLocation(gShaderProgramObject, "u_light1_position");

	La2_uniform = glGetUniformLocation(gShaderProgramObject, "u_La2");
	Ld2_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld2");
	Ls2_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls2");
	light2_position_uniform = glGetUniformLocation(gShaderProgramObject, "u_light2_position");

	Ka_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ka");
	Kd_uniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");
	Ks_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ks");
	material_shininess_uniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");

	
	// *** vertices, Colors, Shader Attribs, vbo, vao initializations
	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	
	//Vao sphere
	glGenVertexArrays(1, &gVao_sphere);  // recording of casatte
	glBindVertexArray(gVao_sphere);

	//vbo position
	glGenBuffers(1, &gVbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
	glBindBuffer(GL_ARRAY_BUFFER, 0); // passing zero is unbinding

									  //vbo normal
	glGenBuffers(1, &gVbo_sphere_normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0); // passing zero is unbinding

	//vbo element
	glGenBuffers(1, &gVbo_sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0); // passing zero is unbinding

	glBindVertexArray(0);

	
	glClearDepth(1.0f); // range is 0 to 1
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	//glEnable(GL_CULL_FACE);
	
	glClearColor(0.25f, 0.25f, 0.25f, 0.0f);  //background color

	//set perspective matrix to identity matrix
	gPerspectiveProjectionMatrix = mat4::identity();

	gbLight = false;


	//resize
	resize(WIN_WIDTH, WIN_HEIGHT);

}

void resize(int width, int height)
{
	//code
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	width1 = width;
	height1 = height;

	
	//Perspective projection matrix
	gPerspectiveProjectionMatrix = perspective(45.0f, (4.0/3.0) * (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Strt using OpenGL program object
	glUseProgram(gShaderProgramObject);



	if (gbLight == true)
	{
		glUniform1i(L_KeyPressed_uniform, 1);

		glUniform3fv(La0_uniform, 1, light0_Ambient);
		glUniform3fv(Ld0_uniform, 1, light0_Diffuse);
		glUniform3fv(Ls0_uniform, 1, light0_Specular);
		glUniform4fv(light0_position_uniform, 1, light0_Position);

		glUniform3fv(La1_uniform, 1, light1_Ambient);
		glUniform3fv(Ld1_uniform, 1, light1_Diffuse);
		glUniform3fv(Ls1_uniform, 1, light1_Specular);
		glUniform4fv(light1_position_uniform, 1, light1_Position);

		glUniform3fv(La2_uniform, 1, light2_Ambient);
		glUniform3fv(Ld2_uniform, 1, light2_Diffuse);
		glUniform3fv(Ls2_uniform, 1, light2_Specular);
		glUniform4fv(light2_position_uniform, 1, light2_Position);

		glUniform3fv(Ka_uniform, 1, material1_ambient);
		glUniform3fv(Kd_uniform, 1, material1_diffuse);
		glUniform3fv(Ks_uniform, 1, material1_specular);
		glUniform1fv(material_shininess_uniform, 1, material1_shininess);
		
		if (Xkey == true)
		{
			//Rotate RED light to Y-direction
			light0_Position[0] = red_x;
			light0_Position[1] = red_y;
			light0_Position[2] = red_z;
		}

		if (Ykey == true)
		{

			//Rotate Blue light to Z-direction
			light0_Position[0] = blue_x;
			light0_Position[1] = blue_y;
			light0_Position[2] = blue_z;
		}

		if (Zkey == true)
		{
			//Rotate GREEN light to X-direction
			light0_Position[0] = green_x;
			light0_Position[1] = green_y;
			light0_Position[2] = green_z;
		}
	}
	else
	{
		glUniform1i(L_KeyPressed_uniform, 0);
	}

	//glViewport(viewportX, viewportY, (GLsizei)width1/4, (GLsizei)height1/4);

	// OpenGL Drawing


	//Set model view and modelviewprojection matrices to identity
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 RotationMatrix = mat4::identity();

	

	//****drawing*******

	//Translate model view matrix
	modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);

	RotationMatrix = rotate(0.0f, 0.0f, 0.0f);

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(rotation_matrix_uniform, 1, GL_FALSE, RotationMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	//width1 = width / 4;
	//height1 = height / 6;
	//viewportX = 0;
	//viewportY = 0;
//	resize(width, height);

	//*** bind vao ***
	glBindVertexArray(gVao_sphere);

	// 1st sphere
	glUniform3fv(Ka_uniform, 1, material1_ambient);
	glUniform3fv(Kd_uniform, 1, material1_diffuse);
	glUniform3fv(Ks_uniform, 1, material1_specular);
	glUniform1fv(material_shininess_uniform, 1, material1_shininess);

	glViewport(0, 5 * (GLsizei)height1 / 6, (GLsizei)width1 / 4, (GLsizei)height1 / 6);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	//2nd sphere
	glUniform3fv(Ka_uniform, 1, material2_ambient);
	glUniform3fv(Kd_uniform, 1, material2_diffuse);
	glUniform3fv(Ks_uniform, 1, material2_specular);
	glUniform1fv(material_shininess_uniform, 1, material2_shininess);

	glViewport(0, 4 * (GLsizei)height1 / 6, (GLsizei)width1 / 4, (GLsizei)height1 / 6);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
		
	//3rd sphere
	glUniform3fv(Ka_uniform, 1, material3_ambient);
	glUniform3fv(Kd_uniform, 1, material3_diffuse);
	glUniform3fv(Ks_uniform, 1, material3_specular);
	glUniform1fv(material_shininess_uniform, 1, material3_shininess);

	glViewport(0, 3 * (GLsizei)height1 / 6, (GLsizei)width1 / 4, (GLsizei)height1 / 6);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	//4th sphere
	glUniform3fv(Ka_uniform, 1, material4_ambient);
	glUniform3fv(Kd_uniform, 1, material4_diffuse);
	glUniform3fv(Ks_uniform, 1, material4_specular);
	glUniform1fv(material_shininess_uniform, 1, material4_shininess);

	glViewport(0, 2 * (GLsizei)height1 / 6, (GLsizei)width1 / 4, (GLsizei)height1 / 6);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	//5th sphere
	glUniform3fv(Ka_uniform, 1, material5_ambient);
	glUniform3fv(Kd_uniform, 1, material5_diffuse);
	glUniform3fv(Ks_uniform, 1, material5_specular);
	glUniform1fv(material_shininess_uniform, 1, material5_shininess);

	glViewport(0, 1 * (GLsizei)height1 / 6, (GLsizei)width1 / 4, (GLsizei)height1 / 6);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	//6th sphere
	glUniform3fv(Ka_uniform, 1, material6_ambient);
	glUniform3fv(Kd_uniform, 1, material6_diffuse);
	glUniform3fv(Ks_uniform, 1, material6_specular);
	glUniform1fv(material_shininess_uniform, 1, material6_shininess);

	glViewport(0, 0 * (GLsizei)height1 / 6, (GLsizei)width1 / 4, (GLsizei)height1 / 6);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	//7th sphere
	glUniform3fv(Ka_uniform, 1, material7_ambient);
	glUniform3fv(Kd_uniform, 1, material7_diffuse);
	glUniform3fv(Ks_uniform, 1, material7_specular);
	glUniform1fv(material_shininess_uniform, 1, material7_shininess);

	glViewport( 1 * (GLsizei)width1 / 4, 5 * (GLsizei)height1 / 6, (GLsizei)width1 / 4, (GLsizei)height1 / 6);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	//8th sphere
	glUniform3fv(Ka_uniform, 1, material8_ambient);
	glUniform3fv(Kd_uniform, 1, material8_diffuse);
	glUniform3fv(Ks_uniform, 1, material8_specular);
	glUniform1fv(material_shininess_uniform, 1, material8_shininess);

	glViewport(1 * (GLsizei)width1 / 4, 4 * (GLsizei)height1 / 6, (GLsizei)width1 / 4, (GLsizei)height1 / 6);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	//9th sphere
	glUniform3fv(Ka_uniform, 1, material9_ambient);
	glUniform3fv(Kd_uniform, 1, material9_diffuse);
	glUniform3fv(Ks_uniform, 1, material9_specular);
	glUniform1fv(material_shininess_uniform, 1, material9_shininess);

	glViewport(1 * (GLsizei)width1 / 4, 3 * (GLsizei)height1 / 6, (GLsizei)width1 / 4, (GLsizei)height1 / 6);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	//10th sphere
	glUniform3fv(Ka_uniform, 1, material10_ambient);
	glUniform3fv(Kd_uniform, 1, material10_diffuse);
	glUniform3fv(Ks_uniform, 1, material10_specular);
	glUniform1fv(material_shininess_uniform, 1, material10_shininess);

	glViewport(1 * (GLsizei)width1 / 4, 2 * (GLsizei)height1 / 6, (GLsizei)width1 / 4, (GLsizei)height1 / 6);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	//11th sphere
	glUniform3fv(Ka_uniform, 1, material11_ambient);
	glUniform3fv(Kd_uniform, 1, material11_diffuse);
	glUniform3fv(Ks_uniform, 1, material11_specular);
	glUniform1fv(material_shininess_uniform, 1, material11_shininess);

	glViewport(1 * (GLsizei)width1 / 4, 1 * (GLsizei)height1 / 6, (GLsizei)width1 / 4, (GLsizei)height1 / 6);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	//12th sphere
	glUniform3fv(Ka_uniform, 1, material12_ambient);
	glUniform3fv(Kd_uniform, 1, material12_diffuse);
	glUniform3fv(Ks_uniform, 1, material12_specular);
	glUniform1fv(material_shininess_uniform, 1, material12_shininess);

	glViewport(1 * (GLsizei)width1 / 4, 0 * (GLsizei)height1 / 6, (GLsizei)width1 / 4, (GLsizei)height1 / 6);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	//13th sphere
	glUniform3fv(Ka_uniform, 1, material13_ambient);
	glUniform3fv(Kd_uniform, 1, material13_diffuse);
	glUniform3fv(Ks_uniform, 1, material13_specular);
	glUniform1fv(material_shininess_uniform, 1, material13_shininess);

	glViewport(2 * (GLsizei)width1 / 4, 5 * (GLsizei)height1 / 6, (GLsizei)width1 / 4, (GLsizei)height1 / 6);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	//14th sphere
	glUniform3fv(Ka_uniform, 1, material14_ambient);
	glUniform3fv(Kd_uniform, 1, material14_diffuse);
	glUniform3fv(Ks_uniform, 1, material14_specular);
	glUniform1fv(material_shininess_uniform, 1, material14_shininess);

	glViewport(2 * (GLsizei)width1 / 4, 4 * (GLsizei)height1 / 6, (GLsizei)width1 / 4, (GLsizei)height1 / 6);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	//15th sphere
	glUniform3fv(Ka_uniform, 1, material15_ambient);
	glUniform3fv(Kd_uniform, 1, material15_diffuse);
	glUniform3fv(Ks_uniform, 1, material15_specular);
	glUniform1fv(material_shininess_uniform, 1, material15_shininess);

	glViewport(2 * (GLsizei)width1 / 4, 3 * (GLsizei)height1 / 6, (GLsizei)width1 / 4, (GLsizei)height1 / 6);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);


	//16th sphere
	glUniform3fv(Ka_uniform, 1, material16_ambient);
	glUniform3fv(Kd_uniform, 1, material16_diffuse);
	glUniform3fv(Ks_uniform, 1, material16_specular);
	glUniform1fv(material_shininess_uniform, 1, material16_shininess);

	glViewport(2 * (GLsizei)width1 / 4, 2 * (GLsizei)height1 / 6, (GLsizei)width1 / 4, (GLsizei)height1 / 6);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	//17th sphere
	glUniform3fv(Ka_uniform, 1, material17_ambient);
	glUniform3fv(Kd_uniform, 1, material17_diffuse);
	glUniform3fv(Ks_uniform, 1, material17_specular);
	glUniform1fv(material_shininess_uniform, 1, material17_shininess);

	glViewport(2 * (GLsizei)width1 / 4, 1 * (GLsizei)height1 / 6, (GLsizei)width1 / 4, (GLsizei)height1 / 6);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	//18th sphere
	glUniform3fv(Ka_uniform, 1, material18_ambient);
	glUniform3fv(Kd_uniform, 1, material18_diffuse);
	glUniform3fv(Ks_uniform, 1, material18_specular);
	glUniform1fv(material_shininess_uniform, 1, material18_shininess);

	glViewport(2 * (GLsizei)width1 / 4, 0 * (GLsizei)height1 / 6, (GLsizei)width1 / 4, (GLsizei)height1 / 6);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	//19th sphere
	glUniform3fv(Ka_uniform, 1, material19_ambient);
	glUniform3fv(Kd_uniform, 1, material19_diffuse);
	glUniform3fv(Ks_uniform, 1, material19_specular);
	glUniform1fv(material_shininess_uniform, 1, material19_shininess);

	glViewport(3 * (GLsizei)width1 / 4, 5 * (GLsizei)height1 / 6, (GLsizei)width1 / 4, (GLsizei)height1 / 6);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	//20th sphere
	glUniform3fv(Ka_uniform, 1, material20_ambient);
	glUniform3fv(Kd_uniform, 1, material20_diffuse);
	glUniform3fv(Ks_uniform, 1, material20_specular);
	glUniform1fv(material_shininess_uniform, 1, material20_shininess);

	glViewport(3 * (GLsizei)width1 / 4, 4 * (GLsizei)height1 / 6, (GLsizei)width1 / 4, (GLsizei)height1 / 6);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	//21st sphere
	glUniform3fv(Ka_uniform, 1, material21_ambient);
	glUniform3fv(Kd_uniform, 1, material21_diffuse);
	glUniform3fv(Ks_uniform, 1, material21_specular);
	glUniform1fv(material_shininess_uniform, 1, material21_shininess);

	glViewport(3 * (GLsizei)width1 / 4, 3 * (GLsizei)height1 / 6, (GLsizei)width1 / 4, (GLsizei)height1 / 6);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	//22nd sphere
	glUniform3fv(Ka_uniform, 1, material22_ambient);
	glUniform3fv(Kd_uniform, 1, material22_diffuse);
	glUniform3fv(Ks_uniform, 1, material22_specular);
	glUniform1fv(material_shininess_uniform, 1, material22_shininess);

	glViewport(3 * (GLsizei)width1 / 4, 2 * (GLsizei)height1 / 6, (GLsizei)width1 / 4, (GLsizei)height1 / 6);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	//23rd sphere
	glUniform3fv(Ka_uniform, 1, material23_ambient);
	glUniform3fv(Kd_uniform, 1, material23_diffuse);
	glUniform3fv(Ks_uniform, 1, material23_specular);
	glUniform1fv(material_shininess_uniform, 1, material23_shininess);

	glViewport(3 * (GLsizei)width1 / 4, 1 * (GLsizei)height1 / 6, (GLsizei)width1 / 4, (GLsizei)height1 / 6);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	//24rd sphere
	glUniform3fv(Ka_uniform, 1, material24_ambient);
	glUniform3fv(Kd_uniform, 1, material24_diffuse);
	glUniform3fv(Ks_uniform, 1, material24_specular);
	glUniform1fv(material_shininess_uniform, 1, material24_shininess);

	glViewport(3 * (GLsizei)width1 / 4, 0 * (GLsizei)height1 / 6, (GLsizei)width1 / 4, (GLsizei)height1 / 6);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	//*** unbind vao ***
	glBindVertexArray(0);

	//Stop using OpenGL program object
	glUseProgram(0);


	glXSwapBuffers(gpDisplay, gWindow);
}

void uninitialize(void)
{
	//code
	//Releasing OpenGL related and Xwindow related objects
	GLXContext currentContext = glXGetCurrentContext();
	if (currentContext != NULL && currentContext == gGLXContext)
	{
		glXMakeCurrent(gpDisplay, 0, 0);

	}
	if (gGLXContext)
	{
		glXDestroyContext(gpDisplay, gGLXContext);
	}
	if (gWindow)
	{
		XDestroyWindow(gpDisplay, gWindow);
	}
	if (gColormap)
	{
		XFreeColormap(gpDisplay, gColormap);
	}
	if (gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;

	}
	if (gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}

	if (gVao_sphere)
	{
		glDeleteVertexArrays(1, &gVao_sphere);
		gVao_sphere = 0;
	}

	
	// Destroy vbo_position
	if (gVbo_sphere_position)
	{
		glDeleteVertexArrays(1, &gVbo_sphere_position);
		gVbo_sphere_position = 0;
	}

	// Destroy vbo_normal
	if (gVbo_sphere_normal)
	{
		glDeleteVertexArrays(1, &gVbo_sphere_normal);
		gVbo_sphere_normal = 0;
	}

	// Destroy vbo_element
	if (gVbo_sphere_element)
	{
		glDeleteVertexArrays(1, &gVbo_sphere_element);
		gVbo_sphere_element = 0;
	}

	//Detach vertex shader from shader program object
	glDetachShader(gShaderProgramObject, gVertexShaderObject);

	//Detach fragment shader from shader program object
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	
	
	// Delete vertex shader object
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;

	// Delete fragment shader object
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	// Delete shader program object
	glDeleteShader(gShaderProgramObject);
	gShaderProgramObject = 0;

	//unlink shader program
	glUseProgram(0);

}

void update(void)
{
	// Circle equalation  Circle_X= Xcenter + rcosQ , Circle_X = Ycenter + rsinQ, Q = 0 to 360
	
	angleCircle = angleCircle - 0.005f;
	if (angleCircle >= 360.0f)
		angleCircle = 0.0f;

		red_x = Xcenter + 100.0f * cos(angleCircle);
		red_y = Ycenter;
		red_z = Zcenter + 100.0f * sin(angleCircle);
	
		green_x = Xcenter + 100.0f * sin(angleCircle);
		green_y = Ycenter + 100.0f * cos(angleCircle);
		green_z = Zcenter;
	
		blue_x = Xcenter;
		blue_y = Ycenter + 100.0f * sin(angleCircle);
		blue_z = Zcenter + 100.0f * cos(angleCircle);
	
	
}
