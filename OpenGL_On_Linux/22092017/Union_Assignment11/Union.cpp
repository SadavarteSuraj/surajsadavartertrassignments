#include <stdio.h>

int main ()

{

int choice;
int inputI;
char inputC;

//Declare union
union test
{
  int i;
  char c;
};

  // A union variable t

 union test t;

printf("This program demonstate use of union to store two variables at same memory location.\n \n ");

printf(" Enter any Interger :\n");
scanf("%d", &inputI);


printf(" Enter any Character :\n");
scanf(" %c", &inputC); //First blank space is to ignore new line character from previuos printf


printf(" Enter your choice : 1.Interger , 2.Character :");
scanf("%d", &choice);

if (choice == 1)
	{
		t.c = inputC; 
		t.i = inputI; 
printf("\n Interger value is : %d \n", t.i);
printf("Character value (garbage) : %d \n", t.c);

    	}
else if (choice == 2)
	{
		t.i = inputI; 
		t.c = inputC;
printf("Character value is : %c \n", t.c);
printf("Interger value (garbage) : %d \n", t.i);
	}
else
{
printf(" Wrong choice \n");
}


return 0;
}
