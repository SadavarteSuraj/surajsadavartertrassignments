// Linux XWindows

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glx.h>

//Namespace
using namespace std;

//Global variable declaration
bool bFullscreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;

GLfloat angleTri = 0.0f;
GLfloat angleRect = 0.0f;

GLXContext gGLXContext;

char ascii[32];

GLUquadric *quadric = NULL;

static int year = 0;
static int day = 0;

//Function declaration
void drawPyramid(void);
void drawCube(void);
void update(void);

// Entry point function
int main(void)
{
	//Function prototype
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void initialize(void);
	void display(void);
	void resize(int, int);
	void uninitialize();
	
	//Variable declaration
	int winWidth = giWindowWidth;
	int winHeight = giWindowHeight;

	bool bDone = false;
	
	//code
	CreateWindow();
	
	//initialize
	initialize();

	//Message Loop
	XEvent event;
	KeySym keysym;
	
	while(bDone==false)
	{
		
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay, &event);
			switch(event.type)
			{
			case MapNotify:
			break;
			case KeyPress:
			keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
				switch(keysym)
				{
				
				case XK_Escape:
					bDone = true;
					break;
				case XK_F:
				case XK_f:
				if (bFullscreen == false)
					{
					ToggleFullscreen();
					bFullscreen = true;
					}
				else
					{
					ToggleFullscreen();
					bFullscreen = false;
					}
				break;
				
				
				}
				XLookupString(&event.xkey, ascii, sizeof(ascii), NULL, NULL);
				switch (ascii[0])
					{
				case 'D':
					day = (day + 6) % 360;
					break;
				case 'd':
					day = (day - 6) % 360;
					break;
				case 'Y':
					year = (year + 3) % 360;
					break;
				case 'y':
					year = (year - 3) % 360;
					break;
				default:
					break;
					}
				break;
				case ButtonPress:
				switch(event.xbutton.button)
					{
					case 1:
						break;
					case 2:
						break;
					case 3:
						break;
					default:
						break;
					
					}
				break;
				
				case MotionNotify:
					break;
				case ConfigureNotify:
					winWidth = event.xconfigure.width;
					winHeight = event.xconfigure.height;
					resize(winWidth, winHeight);
					break;
				case Expose:
					break;
				case DestroyNotify:
					break;
				case 33:
					uninitialize();
					exit(0);
				default:
				break;
				
			
					
			}	
		}
		display();
	}
	uninitialize();
	return(0);
	
}

void CreateWindow(void)
{
	//Function prototypes
	void uninitialize(void);
	
	//Variable declaration
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;
	
	static int frameBufferAttributes[]= 
	{
		//GLX_X_RENDERABLE, True,
		GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR,
		//GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,	
		//GLX_RENDER_TYPE, GLX_RGBA_BIT,
		GLX_RGBA,
		GLX_RED_SIZE, 8,
		GLX_GREEN_SIZE, 8,
		GLX_BLUE_SIZE, 8,
		GLX_ALPHA_SIZE, 8,
		GLX_DOUBLEBUFFER, True,
		GLX_DEPTH_SIZE, 24,
		None
	};

	//Code
	
	gpDisplay = XOpenDisplay(NULL);
	if (gpDisplay == NULL)
	{
		printf("ERROR : Unable to open X Display \n Exiting now...\n");
		uninitialize();
		exit(1);
	}
	
	defaultScreen = XDefaultScreen(gpDisplay);
	
	
	gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);

	winAttribs.border_pixmap = 0;
	winAttribs.border_pixel = 0;
	winAttribs.background_pixmap = 0;
	winAttribs.colormap = XCreateColormap(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen), gpXVisualInfo->visual, AllocNone);
	gColormap = winAttribs.colormap;

	winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);

	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask |PointerMotionMask |StructureNotifyMask;

	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow = XCreateWindow(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen), 0, 0, giWindowWidth, giWindowHeight, 0, gpXVisualInfo->depth, InputOutput, gpXVisualInfo->visual, styleMask, &winAttribs);

	if (!gWindow)
	{
		printf("Error : Fail to create Main Window. \n Exiting now....\n");
		uninitialize();
		exit(1);
	}

	XStoreName(gpDisplay, gWindow, "First OpenGL XWindow");

	Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);

	XMapWindow(gpDisplay, gWindow);

}

void ToggleFullscreen(void)
	{
		//Variable declaration
		Atom wm_state;
		Atom fullscreen;
		XEvent xev = {0};

		//code
		wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
		memset(&xev, 0, sizeof(xev));

		xev.type = ClientMessage;
		xev.xclient.window = gWindow;
		xev.xclient.message_type = wm_state;
		xev.xclient.format = 32;
		xev.xclient.data.l[0] = bFullscreen ? 0:1;

		fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
		xev.xclient.data.l[1] = fullscreen;

		XSendEvent(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen), False, StructureNotifyMask, &xev);
	}

void initialize()
	{
		//function declaration
		void resize(int, int);

		//code
		gGLXContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE);

		glXMakeCurrent(gpDisplay, gWindow, gGLXContext);

		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

		glClearDepth(1.0f);
		
		glEnable(GL_DEPTH_TEST);
	
		glDepthFunc(GL_LEQUAL);

		resize(giWindowWidth, giWindowHeight);
	}


void display(void)
	{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// View transformation
	gluLookAt(0.0f, 0.0f, 5.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);

	// Large sphere like SUN
	// Model transformation
	glPushMatrix();

	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	glColor3f(1.0f, 1.0f, 0.0f);

	gluSphere(quadric, 0.75, 30, 30); // 3rd parameter slices like longitude and 4th parameter is stacks like latitudes. More the subdivision, more the circular sphere.

	glPopMatrix();

	//Small sphere like Earth
	glPushMatrix();
	glRotatef((GLfloat)year, 0.0f, 1.0f, 0.0f);

	glTranslatef(1.5f, 0.0f, 0.0f);

	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);

	glRotatef((GLfloat)day, 0.0f, 0.0f, 1.0f);

	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	quadric = gluNewQuadric();
	glColor3f(0.4f, 0.9f, 1.0f);
	gluSphere(quadric, 0.2, 20, 20); 

	glPopMatrix();

	glXSwapBuffers(gpDisplay, gWindow);
	}


void resize(int width, int height)
	{
	//code
	if(height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width/(GLfloat)height, 0.1f, 100.0f); 
}


void uninitialize(void)
	{
		GLXContext currentGLXContext;
		currentGLXContext = glXGetCurrentContext();
		
		if(currentGLXContext != NULL && currentGLXContext == gGLXContext)
		{
			glXMakeCurrent(gpDisplay, 0, 0);
		}

		if(gGLXContext)
		{
			glXDestroyContext(gpDisplay, gGLXContext);
		}

		if (gWindow)
		{
			XDestroyWindow(gpDisplay, gWindow);
		}
		
		if (gColormap)
		{
			XFreeColormap(gpDisplay, gColormap);
		}

		if (gpXVisualInfo)
		{
			free(gpXVisualInfo);
			gpXVisualInfo = NULL;
		}

		if (gpDisplay)
		{
			XCloseDisplay(gpDisplay);
			gpDisplay = NULL;
		}
	}

void drawPyramid(void)
{
	glTranslatef(-2.0f, 0.0f, -7.0f);
	glRotatef(angleTri, 0.0f, 1.0f, 0.0f);
	glBegin(GL_TRIANGLES);

	//Front face
	glColor3f(1.0f, 0.0f, 0.0f);     // RED
	glVertex3f(0.0f, 1.0f, 0.0f);    // APEX point A
	glColor3f(0.0f, 1.0f, 0.0f);     //Green
	glVertex3f(-1.0f, -1.0f, 1.0f);  // Point P
	glColor3f(0.0f, 0.0f, 1.0f);     // Blue
	glVertex3f(1.0f, -1.0f, 1.0f);   //Point Q

	//Right face
	glColor3f(1.0f, 0.0f, 0.0f);    // RED
	glVertex3f(0.0f, 1.0f, 0.0f);  // APEX point A
	glColor3f(0.0f, 0.0f, 1.0f);   // Blue
	glVertex3f(1.0f, -1.0f, 1.0f);  // Point Q
	glColor3f(0.0f, 1.0f, 0.0f);   // Green
	glVertex3f(1.0f, -1.0f, -1.0f); // Point R

	//Back face
	glColor3f(1.0f, 0.0f, 0.0f);  // RED
	glVertex3f(0.0f, 1.0f, 0.0f);  // APEX point A
	glColor3f(0.0f, 1.0f, 0.0f);  // Green
	glVertex3f(1.0f, -1.0f, -1.0f);  // Point R
	glColor3f(0.0f, 0.0f, 1.0f); // Blue
	glVertex3f(-1.0f, -1.0f, -1.0f); // Point S

	//Left face
	glColor3f(1.0f, 0.0f, 0.0f);  // RED
	glVertex3f(0.0f, 1.0f, 0.0f); // APEX point A
	glColor3f(0.0f, 0.0f, 1.0f);  // Blue
	glVertex3f(-1.0f, -1.0f, -1.0f); // Point S
	glColor3f(0.0f, 1.0f, 0.0f);  // Green
	glVertex3f(-1.0f, -1.0f, 1.0f); // Point R


	glEnd();
}

void drawCube(void)
{
	glTranslatef(2.0f, 0.0f, -7.0f);
	glScalef(0.75f, 0.75f, 0.75f);
	glRotatef(angleRect, 1.0f, 1.0f, 1.0f);

	glBegin(GL_QUADS);

	//Front face
	glColor3f(1.0f, 0.0f, 0.0f); // Red Color
	glVertex3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	//Back face
	glColor3f(0.0f, 1.0f, 0.0f); // Green Color
	glVertex3f(1.0f, 1.0f, -1.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	//Top face
	glColor3f(0.0f, 0.0f, 1.0f); // Blue Color
	glVertex3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	//Bottom Face
	glColor3f(0.0f, 1.0f, 1.0f); // Cyan Color
	glVertex3f(1.0f, -1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	//Right face
	glColor3f(1.0f, 0.0f, 1.0f); // Magneta Color
	glVertex3f(1.0f, 1.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);


	//Left face
	glColor3f(1.0f, 1.0f, 0.0f); // Yellow Color
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);


	glEnd();
}

void update(void)
{
	angleTri = angleTri + 0.02f;
	if (angleTri >= 360.0f)
		angleTri = 0.0f;

	angleRect = angleRect - 0.02f;
	if (angleRect <= -360.0f)
		angleRect = 0.0f;

}































