// Linux XWindows

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glx.h>

#include <SOIL/SOIL.h>

//Namespace
using namespace std;

//Global variable declaration
bool bFullscreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;

GLuint Texture_Smiley; // texture object for Smiley texture



GLXContext gGLXContext;

//Function declaration
void drawRectangle(void);


// Entry point function
int main(void)
{
	//Function prototype
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void initialize(void);
	void display(void);
	void resize(int, int);
	void uninitialize();
	
	//Variable declaration
	int winWidth = giWindowWidth;
	int winHeight = giWindowHeight;

	bool bDone = false;
	
	//code
	CreateWindow();
	
	//initialize
	initialize();

	//Message Loop
	XEvent event;
	KeySym keysym;
	
	while(bDone==false)
	{
		
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay, &event);
			switch(event.type)
			{
			case MapNotify:
			break;
			case KeyPress:
			keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
				switch(keysym)
				{
				case XK_Escape:
					bDone = true;
					break;
				case XK_F:
				case XK_f:
				if (bFullscreen == false)
					{
					ToggleFullscreen();
					bFullscreen = true;
					}
				else
					{
					ToggleFullscreen();
					bFullscreen = false;
					}
				break;
				default:
					break;
				}
				break;
				case ButtonPress:
				switch(event.xbutton.button)
					{
					case 1:
						break;
					case 2:
						break;
					case 3:
						break;
					default:
						break;
					
					}
				break;
				
				case MotionNotify:
					break;
				case ConfigureNotify:
					winWidth = event.xconfigure.width;
					winHeight = event.xconfigure.height;
					resize(winWidth, winHeight);
					break;
				case Expose:
					break;
				case DestroyNotify:
					break;
				case 33:
					uninitialize();
					exit(0);
				default:
				break;
				
			
					
			}	
		}
		display();
	}
	uninitialize();
	return(0);
	
}

void CreateWindow(void)
{
	//Function prototypes
	void uninitialize(void);
	
	//Variable declaration
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;
	
	static int frameBufferAttributes[]= 
	{
		GLX_RGBA,
		GLX_RED_SIZE, 8,
		GLX_GREEN_SIZE, 8,
		GLX_BLUE_SIZE, 8,
		GLX_ALPHA_SIZE, 8,
		GLX_DOUBLEBUFFER, True,
		GLX_DEPTH_SIZE, 24,
		None
	};

	//Code
	
	gpDisplay = XOpenDisplay(NULL);
	if (gpDisplay == NULL)
	{
		printf("ERROR : Unable to open X Display \n Exiting now...\n");
		uninitialize();
		exit(1);
	}
	
	defaultScreen = XDefaultScreen(gpDisplay);
	
	
	gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);

	winAttribs.border_pixmap = 0;
	winAttribs.border_pixel = 0;
	winAttribs.background_pixmap = 0;
	winAttribs.colormap = XCreateColormap(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen), gpXVisualInfo->visual, AllocNone);
	gColormap = winAttribs.colormap;

	winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);

	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask |PointerMotionMask |StructureNotifyMask;

	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow = XCreateWindow(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen), 0, 0, giWindowWidth, giWindowHeight, 0, gpXVisualInfo->depth, InputOutput, gpXVisualInfo->visual, styleMask, &winAttribs);

	if (!gWindow)
	{
		printf("Error : Fail to create Main Window. \n Exiting now....\n");
		uninitialize();
		exit(1);
	}

	XStoreName(gpDisplay, gWindow, "First OpenGL XWindow");

	Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);

	XMapWindow(gpDisplay, gWindow);

}

void ToggleFullscreen(void)
	{
		//Variable declaration
		Atom wm_state;
		Atom fullscreen;
		XEvent xev = {0};

		//code
		wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
		memset(&xev, 0, sizeof(xev));

		xev.type = ClientMessage;
		xev.xclient.window = gWindow;
		xev.xclient.message_type = wm_state;
		xev.xclient.format = 32;
		xev.xclient.data.l[0] = bFullscreen ? 0:1;

		fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
		xev.xclient.data.l[1] = fullscreen;

		XSendEvent(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen), False, StructureNotifyMask, &xev);
	}

void initialize()
	{
		//function declaration
		void resize(int, int);
		//int LoadGLTextures(GLuint *, TCHAR[]);
		int LoadGLTextures(GLuint *texture, const char *path);

		//code
		gGLXContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE);

		glXMakeCurrent(gpDisplay, gWindow, gGLXContext);

		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glClearDepth(1.0f); // range is 0 to 1
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL);
		glShadeModel(GL_SMOOTH);
		glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

		//texture code
		glEnable(GL_TEXTURE_2D); // Enable texture mappinng

		LoadGLTextures(&Texture_Smiley, "Smiley.bmp");

		resize(giWindowWidth, giWindowHeight);
	}


int LoadGLTextures(GLuint *texture, const char *path)
{
	
	int width, height;
	unsigned char *imagedata = NULL;

	

	//code
	
	//hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), imageresourceId, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);

	imagedata = SOIL_load_image(path, &width, &height, 0, SOIL_LOAD_RGB);

	
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4); //pixel storage mode (word alignment/4 bytes)
		glGenTextures(1, texture);  //1 image
		glBindTexture(GL_TEXTURE_2D, *texture); //bind texture
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

		//Generate mipmapped texture (3 bytes, width, height & data from bmp)
		//gluBuild2DMipmaps(GL_TEXTURE_2D, 3, bmp.bmWidth, bmp.bmHeight, GL_BGR_EXT, GL_UNSIGNED_BYTE, bmp.bmBits);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, width, height, GL_RGB, GL_UNSIGNED_BYTE, (GLvoid*)imagedata);

		//DeleteObject(hBitmap); //delete unwanted bitmap handle
		SOIL_free_image_data(imagedata);
	
	
}

void display(void)
	{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glBindTexture(GL_TEXTURE_2D, Texture_Smiley); //bind texture
	drawRectangle();

	glXSwapBuffers(gpDisplay, gWindow);
	}


void resize(int width, int height)
	{
	//code
	if(height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width/(GLfloat)height, 0.1f, 100.0f); 
}


void uninitialize(void)
	{
		GLXContext currentGLXContext;
		currentGLXContext = glXGetCurrentContext();
	//code
	if (Texture_Smiley)
	{
		glDeleteTextures(1, &Texture_Smiley);
		Texture_Smiley = 0;
	}
		
		if(currentGLXContext != NULL && currentGLXContext == gGLXContext)
		{
			glXMakeCurrent(gpDisplay, 0, 0);
		}

		if(gGLXContext)
		{
			glXDestroyContext(gpDisplay, gGLXContext);
		}

		if (gWindow)
		{
			XDestroyWindow(gpDisplay, gWindow);
		}
		
		if (gColormap)
		{
			XFreeColormap(gpDisplay, gColormap);
		}

		if (gpXVisualInfo)
		{
			free(gpXVisualInfo);
			gpXVisualInfo = NULL;
		}

		if (gpDisplay)
		{
			XCloseDisplay(gpDisplay);
			gpDisplay = NULL;
		}
	}





void drawRectangle(void)
{
	glTranslatef(0.0f, 0.0f, -3.0f);
	
	glBindTexture(GL_TEXTURE_2D, Texture_Smiley);
	glBegin(GL_QUADS);

	// SOIL texture coordinates 
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 0.0f);    // Top right

	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 0.0f);    // Top left

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 0.0f);    // Bottom right

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 0.0f);    // Bottom left
	
	glEnd();
}


































