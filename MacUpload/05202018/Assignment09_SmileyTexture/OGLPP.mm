// ======= 3D Texture =========

//Headers
#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import <QuartzCore/CVDisplayLink.h>

#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

#import "vmath.h"

enum
{
	VDG_ATTRIBUTE_VERTEX = 0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE0,
};

//'C' style global function declarations
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef,  const CVTimeStamp *, const CVTimeStamp *, CVOptionFlags, CVOptionFlags *,  void *);


//global variables
FILE *gpFile=NULL;

GLfloat anglePyramid = 0.0f;
GLfloat angleCube = 0.0f;

// interface declarations
@interface AppDelegate:  NSObject <NSApplicationDelegate,NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

// Entry-point function
int main(int argc,  const char * argv[])
{
	//code
	NSAutoreleasePool *pPool =[[NSAutoreleasePool alloc]init];
	NSApp = [NSApplication sharedApplication];
	[NSApp setDelegate : [[AppDelegate alloc]init]];
	[NSApp run];
	[pPool release];
	return(0);
}

//interface implementations
@implementation AppDelegate
{
	@private
	NSWindow *window;
	GLView *glView;
}

-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	//code

	//log file
	NSBundle *mainBundle=[NSBundle mainBundle];
	NSString *appDirName = [mainBundle bundlePath];
	NSString *parentDirPath = [appDirName stringByDeletingLastPathComponent];
	NSString  *logFileNameWithPath = [NSString stringWithFormat:@"%@/Log.txt", parentDirPath];
	const char *pszlogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
	gpFile = fopen(pszlogFileNameWithPath, "w");
	if(gpFile == NULL)
	{
		printf("Can not create log file. \n Exiting .....\n");
		[self release];
		[NSApp terminate:self];
	}
	fprintf(gpFile, "Program is started successfully\n");

	//Window
	NSRect win_rect;
	win_rect = NSMakeRect(0.0, 0.0,  800.0, 600.0);

	//create simple window
	window = [[NSWindow alloc] initWithContentRect:win_rect
								styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
								backing:NSBackingStoreBuffered
								defer:NO];
	[window setTitle:@"macOS OpenGL Window"];
	[window center];

	glView = [[GLView alloc]initWithFrame:win_rect];

	[window setContentView:glView];
	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];
}

-(void)applicationWillTerminate:(NSNotification *)notification
{
	//code
	fprintf(gpFile, "Program is terminated successfully\n");

	if(gpFile)
	{
		fclose(gpFile);
		gpFile = NULL;
	}

}

-(void)windowWillClose:(NSNotification *)notification
{
	//code
	[NSApp terminate:self];
}

-(void)dealloc
{
	//code
	[glView  release];
	[window release];
	[super dealloc];
}
@end

@implementation GLView
{
	@private
	CVDisplayLinkRef displayLink;

	GLuint gVertexShaderObject;
	GLuint gFragmentShaderObject;
	GLuint gShaderProgramObject;

 
	GLuint gVaoPyramid;
	GLuint gVbo_Pyramid_Position;
	GLuint gVbo_Pyramid_Texture;

	GLuint gVaoCube;
	GLuint gVbo_Cube_Position;
	GLuint gVbo_Cube_Texture;

	GLuint Texture_Stone; // Texture object for texture Stone
	GLuint Texture_Kundali; // Texture object for texture Kundali

	GLuint pyramid_texture;
	GLuint cube_texture;

	GLuint gTexture_sampler_uniform;

    GLuint gMVPUniform;
    
	vmath::mat4 gPerspectiveProjectionMatrix;

}

-(id)initWithFrame:(NSRect)frame;
{
	//code
	self=[super initWithFrame:frame];

	if(self)
	{
		[[self window]setContentView:self];
		NSOpenGLPixelFormatAttribute attrs[]=
		{
			//Must specify the 4.1 Core profile to use OpenGL 4.1
			NSOpenGLPFAOpenGLProfile,
			NSOpenGLProfileVersion4_1Core,
			// Specify the display ID to associate the GL context with (main display for now)
			 NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
			 NSOpenGLPFANoRecovery,
			 NSOpenGLPFAAccelerated,
			 NSOpenGLPFAColorSize,24,
			 NSOpenGLPFADepthSize, 24,
			 NSOpenGLPFAAlphaSize,8,
			 NSOpenGLPFADoubleBuffer,
			 0}; //last 0 is must

			 NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc ]initWithAttributes:attrs] autorelease];

			 if(pixelFormat == nil)
			 {
				fprintf(gpFile, "No valid OpenGL pixel format is available. Exiting  ......");
				[self release];
				[NSApp terminate:self];
			 }

			 NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
			 
			 [self setPixelFormat:pixelFormat];

			 [self setOpenGLContext:glContext]; // it automatically releases older context if present, and sets the newer one
			 
	}
	return(self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
	//code
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];

	[self drawView];
    
    // update();
    
	[pool release];

	return(kCVReturnSuccess);
}

-(void)prepareOpenGL
{
	//code
	//openGL info
	fprintf(gpFile,  "OpenGL Version : %s\n", glGetString(GL_VERSION));
	fprintf(gpFile,  "GLSL Version : %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	[[self openGLContext]makeCurrentContext];										 
	GLint swapInt = 1;
	[[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

	//*** Vertex Shader ***
	// Create Shader
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//Provide source code to shader
	const GLchar *vertexShaderSourceCode =
		"#version 410" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec2 vTexture0_Coord;" \
		"out vec2 out_vTexture0_Coord;" \
		"uniform mat4 u_mvp_matrix;" \
		"void main(void)" \
		"{"\
		"gl_Position  = u_mvp_matrix * vPosition;" \
		"out_vTexture0_Coord = vTexture0_Coord;" \
		"}";
    
    glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gVertexShaderObject);
	GLint iInfoLogLength = 0;
	GLint iShaderCompliedStatus = 0;
	char szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompliedStatus);
	if (iShaderCompliedStatus == GL_FALSE)
		{
			char *szInfoLog = nil;
			glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength); 
			if (iInfoLogLength > 0)
			{
				szInfoLog = (char *)malloc(iInfoLogLength);
				if (szInfoLog != NULL)
				{
					GLsizei written;
					glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
					fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
					free(szInfoLog);
					[self  release];
					[NSApp terminate:self];
				}
			}
		}
	

	//*** Fragment Shader ***
	// Create Shader
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//Provide source code to shader
	const GLchar *fragmentShaderSourceCode =
		"#version 410" \
		"\n" \
		"in vec2 out_vTexture0_Coord;" \
		"out vec4 FragColor;" \
		"uniform sampler2D u_texture0_sampler;"\
		"void main(void)" \
		"{" \
        "vec3 tex =  vec3(texture(u_texture0_sampler, out_vTexture0_Coord));" \
		"FragColor = vec4(tex, 1.0f); " \
		"}";

    glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gFragmentShaderObject);
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompliedStatus);
	if (iShaderCompliedStatus == GL_FALSE)
	{
		char *szInfoLog = NULL;
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				[self  release];
				[NSApp terminate:self];
			}
		}
	}

	//Shader program
	//create
	gShaderProgramObject = glCreateProgram();

	//attach vertex shader to shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	//attach fragment shader to shader program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	//Pre-link binding of shader program object with vertex shader position attribute
	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");
    
    //Pre-link binding of shader program object with vertex shader texture attribute
 glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_TEXTURE0, "vTexture0_Coord");

	//Link shader
	glLinkProgram(gShaderProgramObject);
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		char *szInfoLog = NULL;
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s\n", szInfoLog);
				free(szInfoLog);
				[self  release];
				[NSApp terminate:self];
			}
		}
	} 

	//get MVP uniform location
	gMVPUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");

	gTexture_sampler_uniform = glGetUniformLocation(gShaderProgramObject, "u_texture0_sampler");

	//load texture
	pyramid_texture = [self loadTextureFromBMPFile:"Smiley.bmp"];
	

    // *** vertices, Colors, Shader Attribs, vbo, vao initializations
    const GLfloat pyramidVertices[] =
    {
        1.0f, 1.0f, 0.0f,
        -1.0f, 1.0f, 0.0f,
        -1.0f, -1.0f, 0.0f,
        1.0f, -1.0f, 0.0f,
        
    };
    
	const GLfloat pyramidTexCoords[] =
	{
        1.0f, 1.0f,
        0.0f, 1.0f,
        0.0f, 0.0f,
        1.0f, 0.0f,
	};
    
   
    //Vao Pyramid
    glGenVertexArrays(1, &gVaoPyramid);  // recording of casatte
    glBindVertexArray(gVaoPyramid);
    
    glGenBuffers(1, &gVbo_Pyramid_Position);
    glBindBuffer(GL_ARRAY_BUFFER, gVbo_Pyramid_Position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
    glBindBuffer(GL_ARRAY_BUFFER, 0); // passing zero is unbinding
    
    //Vbo for texture
	glGenBuffers(1, &gVbo_Pyramid_Texture);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Pyramid_Texture);
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidTexCoords), pyramidTexCoords, GL_STATIC_DRAW);
	glVertexAttribPointer(VDG_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL); // 2 for  two  dimemtional texture s,t
	glEnableVertexAttribArray(VDG_ATTRIBUTE_TEXTURE0);
	glBindBuffer(GL_ARRAY_BUFFER, 0); // passing zero is unbinding
    
    glBindVertexArray(0);

	//===============
    
	glClearDepth(1.0f); // range is 0 to 1
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	//glEnable(GL_CULL_FACE);

	glEnable(GL_TEXTURE_2D);

	//set background  color
	glClearColor(0.0f,0.0f, 0.0f, 0.0f); //blue

	//set orthographics matrix to identity matrix
	gPerspectiveProjectionMatrix = vmath::mat4::identity();

	CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
	CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback, self);
	CGLContextObj cglContext = (CGLContextObj) [[self openGLContext]CGLContextObj];
	CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext,cglPixelFormat );
	CVDisplayLinkStart(displayLink);
}

-(GLuint)loadTextureFromBMPFile:(const char *)texFileName
{
	NSBundle *mainBundle = [NSBundle mainBundle];
	NSString *appDirName = [mainBundle bundlePath];
	NSString *parentDirPath = [appDirName stringByDeletingLastPathComponent];
	NSString *textureFileNameWithPath = [NSString stringWithFormat:@"%@/%s",parentDirPath, texFileName];

	NSImage *bmpImage = [[NSImage alloc]initWithContentsOfFile:textureFileNameWithPath];
	if(!bmpImage)
	{
		NSLog(@"can't  find %@", textureFileNameWithPath);
		return(0);
	}

	CGImageRef cgImage = [bmpImage CGImageForProposedRect:nil context:nil hints:nil];

	int w = (int)CGImageGetWidth(cgImage);
	int h = (int)CGImageGetHeight(cgImage);

	CFDataRef imageData = CGDataProviderCopyData(CGImageGetDataProvider(cgImage));
	void* pixels = (void *)CFDataGetBytePtr(imageData);

	GLuint bmpTexture;
	glGenTextures(1, &bmpTexture);

		glPixelStorei(GL_UNPACK_ALIGNMENT, 1); //pixel storage mode (word alignment/4 bytes)
		glBindTexture(GL_TEXTURE_2D, bmpTexture); //bind texture
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

		//Generate mipmapped texture (3 bytes, width, height & data from bmp)
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);

		//create mipmap for this texture for better image quality
		glGenerateMipmap(GL_TEXTURE_2D);

		CFRelease(imageData);
		return(bmpTexture);
}

-(void)reshape
{
	//code
	CGLLockContext((CGLContextObj)[[self  openGLContext]CGLContextObj]);

	NSRect rect = [self bounds];

	GLfloat width=rect.size.width;
	GLfloat height=rect.size.height;

	if(height==0)
		height=1;

	glViewport(0, 0,  (GLsizei)width, (GLsizei)height);

    //Perspective projection matrix
    gPerspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);


	CGLUnlockContext((CGLContextObj)[[self  openGLContext]CGLContextObj]);

}

-(void)drawRect:(NSRect)dirtyRect
{
	//code
		[self drawView];
    
}

-(void)drawView
{
	//code
	[[self openGLContext]makeCurrentContext];

	CGLLockContext((CGLContextObj)[[self  openGLContext]CGLContextObj]);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Strt using OpenGL program object
	glUseProgram(gShaderProgramObject);
	
	// OpenGL Drawing
	//Set model view and modelviewprojection matrices to identity
	vmath::mat4 modelViewMatrix = vmath::mat4::identity();
	vmath::mat4 modelViewProjectionMatrix =vmath::mat4::identity();
    
    //Declare Rotation Matrix to identity
    vmath::mat4 RotationMatrix = vmath::mat4::identity();
    
    //Translate model view matrix
    modelViewMatrix = vmath::translate(0.0f, 0.0f, -4.0f);
    
    //Rotation matrix for triangle
    RotationMatrix = vmath::rotate(anglePyramid, 0.0f, 1.0f, 0.0f);
    
    //Multiply model view matrix and Rotation Matrix
    modelViewMatrix = modelViewMatrix * RotationMatrix;
    
    //multiple the modelview and perspective matrix to get modelview projection matrix
    modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix; // ORDER IS IMPORTANT
    
    
    //pass above modelViewProjectionMatrix to the vertexshader in u_mvp_matrix shader variable
    // whose position value we already calculated in initWithFrame() by using glGetUniformLocation()
    glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    
	
	//bind with texture
	glBindTexture(GL_TEXTURE_2D,  pyramid_texture);

    //*** bind vao ***
    glBindVertexArray(gVaoPyramid);
    
    // *** draw eigther by glDrawTriangles() or glDrawArrays() or glDrawElements()
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4); // for pyramid

    //*** unbind vao ***
    glBindVertexArray(0);
    

    
    //Stop using OpenGL program object
    glUseProgram(0);

	CGLFlushDrawable((CGLContextObj)[[self  openGLContext]CGLContextObj]);
	CGLUnlockContext((CGLContextObj)[[self  openGLContext]CGLContextObj]);
    
}

-(BOOL)acceptsFirstResponder
{
	//code
	[[self window]makeFirstResponder : self];
	return(YES);
}

-(void)keyDown:(NSEvent *)theEvent
{
	//code
	int key = (int)[[theEvent characters]characterAtIndex:0];
	switch(key)
	{
		case 27: //Esc key
			[self release];
			[NSApp terminate:self];
			break;
		case 'F':
		case 'f':
        	[[self window]toggleFullScreen:self]; //repainting occurs automatically
			break;
		default:
			break;

	}
}

-(void)mouseDown:(NSEvent *)theEvent
{
	//code
	
}

-(void)mouseDragged:(NSEvent *)theEvent
{
	//code
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
	//code
	
}

-(void)dealloc
{

    // Destroy vao
    if (gVaoPyramid)
    {
        glDeleteVertexArrays(1, &gVaoPyramid);
        gVaoPyramid = 0;
    }
    
   
    
    // Destroy vbo_position
    if (gVbo_Pyramid_Position)
    {
        glDeleteVertexArrays(1, &gVbo_Pyramid_Position);
        gVbo_Pyramid_Position = 0;
    }
    
    
    
		// Destroy vbo_pyramid_Texture
	if (gVbo_Pyramid_Texture)
	{
		glDeleteVertexArrays(1, &gVbo_Pyramid_Texture);
		gVbo_Pyramid_Texture = 0;
	}


	//Detach vertex shader from shader program object
	glDetachShader(gShaderProgramObject, gVertexShaderObject);

	//Detach fragment shader from shader program object
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	
	
	// Delete vertex shader object
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;

	// Delete fragment shader object
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	// Delete shader program object
	glDeleteShader(gShaderProgramObject);
	gShaderProgramObject = 0;

	//unlink shader program
	glUseProgram(0);

	//code
	CVDisplayLinkStop(displayLink);
	CVDisplayLinkRelease(displayLink);

	[super dealloc];


}

void update()
{
    anglePyramid = anglePyramid + 0.2f;
    if (anglePyramid >= 360.0f)
        anglePyramid = 0.0f;
    
    angleCube = angleCube - 0.2f;
    if (angleCube <= -360.0f)
        angleCube = 0.0f;

    
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime,CVOptionFlags flagsIn,
								CVOptionFlags *pFlagsOut,void *pDisplayLinkContext )
{
	CVReturn result = [(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
	return(result);
}



