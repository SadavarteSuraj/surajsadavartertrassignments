// ======= PerFragment Light =========

//Headers
#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import <QuartzCore/CVDisplayLink.h>

#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

#import "vmath.h"

#import "Sphere.mm"

enum
{
	VDG_ATTRIBUTE_VERTEX = 0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE0,
};

//'C' style global function declarations
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef,  const CVTimeStamp *, const CVTimeStamp *, CVOptionFlags, CVOptionFlags *,  void *);


//global variables
FILE *gpFile=NULL;

GLfloat angleCube = 0.0f;

bool gbAnimate = false;
bool gbLight = false;

// interface declarations
@interface AppDelegate:  NSObject <NSApplicationDelegate,NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

// Entry-point function
int main(int argc,  const char * argv[])
{
	//code
	NSAutoreleasePool *pPool =[[NSAutoreleasePool alloc]init];
	NSApp = [NSApplication sharedApplication];
	[NSApp setDelegate : [[AppDelegate alloc]init]];
	[NSApp run];
	[pPool release];
	return(0);
}

//interface implementations
@implementation AppDelegate
{
	@private
	NSWindow *window;
	GLView *glView;
}

-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	//code

	//log file
	NSBundle *mainBundle=[NSBundle mainBundle];
	NSString *appDirName = [mainBundle bundlePath];
	NSString *parentDirPath = [appDirName stringByDeletingLastPathComponent];
	NSString  *logFileNameWithPath = [NSString stringWithFormat:@"%@/Log.txt", parentDirPath];
	const char *pszlogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
	gpFile = fopen(pszlogFileNameWithPath, "w");
	if(gpFile == NULL)
	{
		printf("Can not create log file. \n Exiting .....\n");
		[self release];
		[NSApp terminate:self];
	}
	fprintf(gpFile, "Program is started successfully\n");

	//Window
	NSRect win_rect;
	win_rect = NSMakeRect(0.0, 0.0,  800.0, 600.0);

	//create simple window
	window = [[NSWindow alloc] initWithContentRect:win_rect
								styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
								backing:NSBackingStoreBuffered
								defer:NO];
	[window setTitle:@"macOS OpenGL Window"];
	[window center];

	glView = [[GLView alloc]initWithFrame:win_rect];

	[window setContentView:glView];
	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];
}

-(void)applicationWillTerminate:(NSNotification *)notification
{
	//code
	fprintf(gpFile, "Program is terminated successfully\n");

	if(gpFile)
	{
		fclose(gpFile);
		gpFile = NULL;
	}

}

-(void)windowWillClose:(NSNotification *)notification
{
	//code
	[NSApp terminate:self];
}

-(void)dealloc
{
	//code
	[glView  release];
	[window release];
	[super dealloc];
}
@end

@implementation GLView
{
	@private
	CVDisplayLinkRef displayLink;

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];

GLuint gNumVertices;
GLuint gNumElements;

GLuint gVao_sphere;
GLuint gVbo_sphere_position;
GLuint gVbo_sphere_normal;
GLuint gVbo_sphere_element;

GLuint gMVPUniform;

GLuint model_matrix_uniform, view_matrix_uniform, projection_matrix_uniform;

GLuint L_KeyPressed_uniform;

GLuint La_uniform;
GLuint Ld_uniform;
GLuint Ls_uniform;
GLuint light_position_uniform;

GLuint Ka_uniform;
GLuint Kd_uniform;
GLuint Ks_uniform;
GLuint material_shininess_uniform;
    
vmath::mat4 gPerspectiveProjectionMatrix;
    
    GLfloat lightAmbient[4];
    GLfloat lightDiffuse[4];
    GLfloat lightSpecular[4];
    GLfloat lightPosition[4];

    GLfloat material_ambient[4];
    GLfloat material_diffuse[4];
    GLfloat material_specular[4];
    GLfloat material_shininess;
    
    
  
    
}

-(id)initWithFrame:(NSRect)frame;
{
	//code
	self=[super initWithFrame:frame];

	if(self)
	{
		[[self window]setContentView:self];
		NSOpenGLPixelFormatAttribute attrs[]=
		{
			//Must specify the 4.1 Core profile to use OpenGL 4.1
			NSOpenGLPFAOpenGLProfile,
			NSOpenGLProfileVersion4_1Core,
			// Specify the display ID to associate the GL context with (main display for now)
			 NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
			 NSOpenGLPFANoRecovery,
			 NSOpenGLPFAAccelerated,
			 NSOpenGLPFAColorSize,24,
			 NSOpenGLPFADepthSize, 24,
			 NSOpenGLPFAAlphaSize,8,
			 NSOpenGLPFADoubleBuffer,
			 0}; //last 0 is must

			 NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc ]initWithAttributes:attrs] autorelease];

			 if(pixelFormat == nil)
			 {
				fprintf(gpFile, "No valid OpenGL pixel format is available. Exiting  ......");
				[self release];
				[NSApp terminate:self];
			 }

			 NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
			 
			 [self setPixelFormat:pixelFormat];

			 [self setOpenGLContext:glContext]; // it automatically releases older context if present, and sets the newer one
			 
	}
	return(self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
	//code
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];

	[self drawView];
    
	if (gbAnimate == true)
     update();
    
	[pool release];

	return(kCVReturnSuccess);
}

-(void)prepareOpenGL
{
	//code
	//openGL info
	fprintf(gpFile,  "OpenGL Version : %s\n", glGetString(GL_VERSION));
	fprintf(gpFile,  "GLSL Version : %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	[[self openGLContext]makeCurrentContext];										 
	GLint swapInt = 1;
	[[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

	//*** Vertex Shader ***
	// Create Shader
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//Provide source code to shader
	const GLchar *vertexShaderSourceCode =
		"#version 410" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform vec4 u_light_position;" \
		"uniform int u_lighting_enabled;" \
		"out vec3 transformed_normals;" \
		"out vec3 light_direction;" \
		"out vec3 viewer_vector;" \
		"void main(void)" \
		"{" \
		"if (u_lighting_enabled == 1)" \
		"{" \
		"vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;" \
		"transformed_normals =  mat3(u_view_matrix * u_model_matrix) * vNormal;" \
		"light_direction = vec3(u_light_position) - eye_coordinates.xyz;" \
		"viewer_vector = -eye_coordinates.xyz;" \
		"}" \
		"gl_Position = u_projection_matrix  * u_view_matrix * u_model_matrix * vPosition;" \
		"}";
    
    glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gVertexShaderObject);
	GLint iInfoLogLength = 0;
	GLint iShaderCompliedStatus = 0;
	char szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompliedStatus);
	if (iShaderCompliedStatus == GL_FALSE)
		{
			char *szInfoLog = nil;
			glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength); 
			if (iInfoLogLength > 0)
			{
				szInfoLog = (char *)malloc(iInfoLogLength);
				if (szInfoLog != NULL)
				{
					GLsizei written;
					glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
					fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
					free(szInfoLog);
					[self  release];
					[NSApp terminate:self];
				}
			}
		}
	

	//*** Fragment Shader ***
	// Create Shader
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//Provide source code to shader
	const GLchar *fragmentShaderSourceCode =
		"#version 410" \
		"\n" \
		"in vec3 transformed_normals; " \
		"in vec3 light_direction; " \
		"in vec3 viewer_vector; " \
		"out vec4 FragColor;" \
		"uniform vec3 u_La;" \
		"uniform vec3 u_Ld;" \
		"uniform vec3 u_Ls;" \
		"uniform vec3 u_Ka;" \
		"uniform vec3 u_Kd;" \
		"uniform vec3 u_Ks;" \
		"uniform float u_material_shininess;" \
		"uniform int u_lighting_enabled;" \
		"void main(void)" \
		"{" \
		"vec3 phong_ads_color;" \
		"if (u_lighting_enabled == 1)" \
		"{" \
		"vec3 normalized_transformed_normals = normalize(transformed_normals);" \
		"vec3 normalized_light_direction = normalize(light_direction);" \
		"vec3 normalized_viewer_vector = normalize(viewer_vector);" \
		"float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction), 0.0);" \
		"vec3 ambient = u_La * u_Ka;" \
		"vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;" \
		"vec3 reflection_vector = reflect(-normalized_light_direction,normalized_transformed_normals);" \
		"vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);" \
		"phong_ads_color = ambient + diffuse + specular;" \
		"}" \
		"else" \
		"{" \
		"phong_ads_color =  vec3(1.0,  1.0,  1.0);" \
		"}" \
		"FragColor = vec4(phong_ads_color, 1.0); " \
		"}";
		
    glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gFragmentShaderObject);
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompliedStatus);
	if (iShaderCompliedStatus == GL_FALSE)
	{
		char *szInfoLog = NULL;
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				[self  release];
				[NSApp terminate:self];
			}
		}
	}

	//Shader program
	//create
	gShaderProgramObject = glCreateProgram();

	//attach vertex shader to shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	//attach fragment shader to shader program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	//Pre-link binding of shader program object with vertex shader position attribute
	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");
    
    //Pre-link binding of shader program object with vertex shader Normal attribute
    glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");

	//Link shader
	glLinkProgram(gShaderProgramObject);
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		char *szInfoLog = NULL;
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s\n", szInfoLog);
				free(szInfoLog);
				[self  release];
				[NSApp terminate:self];
			}
		}
	} 

    	//get MVP uniform location
	model_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
	view_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
	projection_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");

	L_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObject, "u_lighting_enabled");

	La_uniform = glGetUniformLocation(gShaderProgramObject, "u_La");
	Ld_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld");
	Ls_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls");
	light_position_uniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");

	Ka_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ka");
	Kd_uniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");
	Ks_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ks");
	material_shininess_uniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");

	// *** vertices, Colors, Shader Attribs, vbo, vao initializations
    
    Sphere *sph = [[Sphere alloc]init];
    
    
    [sph getSphereVertexData: sphere_vertices : sphere_normals : sphere_textures : sphere_elements];
    
   // getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
    gNumVertices = [sph getNumberOfSphereVertices];
    gNumElements = [sph getNumberOfSphereElements];
    
   // gNumVertices = getNumberOfSphereVertices();
   //  = getNumberOfSphereElements();
    
    
    
    
	//Vao sphere
	glGenVertexArrays(1, &gVao_sphere);  // recording of casatte
	glBindVertexArray(gVao_sphere);

	//vbo position
	glGenBuffers(1, &gVbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
	glBindBuffer(GL_ARRAY_BUFFER, 0); // passing zero is unbinding
	
	//vbo normal
	glGenBuffers(1, &gVbo_sphere_normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0); // passing zero is unbinding

	//vbo element
	glGenBuffers(1, &gVbo_sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0); // passing zero is unbinding

	glBindVertexArray(0);
    
    lightAmbient[0] = 0.0f;
    lightAmbient[1] = 0.0f;
    lightAmbient[2] = 0.0f;
    lightAmbient[3] = 1.0f;
    
    lightDiffuse[0] = 1.0f;
    lightDiffuse[1] = 1.0f;
    lightDiffuse[2] = 1.0f;
    lightDiffuse[3] = 1.0f;
    
    lightSpecular[0] = 1.0f;
    lightSpecular[1] = 1.0f;
    lightSpecular[2] = 1.0f;
    lightSpecular[3] = 1.0f;
    
    lightPosition[0] = 100.0f;
    lightPosition[1] = 100.0f;
    lightPosition[2] = 100.0f;
    lightPosition[3] = 1.0f;
    
    material_ambient[0] = 0.0f;
    material_ambient[1] = 0.0f;
    material_ambient[2] = 0.0f;
    material_ambient[3] = 1.0f;
    
    material_diffuse[0] = 1.0f;
    material_diffuse[1] = 1.0f;
    material_diffuse[2] = 1.0f;
    material_diffuse[3] = 1.0f;
    
    material_specular[0] = 1.0f;
    material_specular[1] = 1.0f;
    material_specular[2] = 1.0f;
    material_specular[3] = 1.0f;
    
    material_shininess = 50.0f;
    
    


	glClearDepth(1.0f); // range is 0 to 1
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	//glEnable(GL_CULL_FACE);

	//set background  color
	glClearColor(0.0f,0.0f, 0.0f, 0.0f); //black

	//set orthographics matrix to identity matrix
	gPerspectiveProjectionMatrix = vmath::mat4::identity();

	CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
	CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback, self);
	CGLContextObj cglContext = (CGLContextObj) [[self openGLContext]CGLContextObj];
	CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext,cglPixelFormat );
	CVDisplayLinkStart(displayLink);
}

-(void)reshape
{
	//code
	CGLLockContext((CGLContextObj)[[self  openGLContext]CGLContextObj]);

	NSRect rect = [self bounds];

	GLfloat width=rect.size.width;
	GLfloat height=rect.size.height;

	if(height==0)
		height=1;

	glViewport(0, 0,  (GLsizei)width, (GLsizei)height);

    //Perspective projection matrix
    gPerspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);


	CGLUnlockContext((CGLContextObj)[[self  openGLContext]CGLContextObj]);

}

-(void)drawRect:(NSRect)dirtyRect
{
	//code
		[self drawView];
    
}

-(void)drawView
{
	//code
	[[self openGLContext]makeCurrentContext];

	CGLLockContext((CGLContextObj)[[self  openGLContext]CGLContextObj]);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Strt using OpenGL program object
	glUseProgram(gShaderProgramObject);
    
	if (gbLight == true)
	{
		glUniform1i(L_KeyPressed_uniform, 1);

		glUniform3fv(La_uniform, 1, lightAmbient);
		glUniform3fv(Ld_uniform, 1, lightDiffuse);
		glUniform3fv(Ls_uniform, 1, lightSpecular);
		glUniform4fv(light_position_uniform, 1, lightPosition);

		glUniform3fv(Ka_uniform, 1, material_ambient);
		glUniform3fv(Kd_uniform, 1, material_diffuse);
		glUniform3fv(Ks_uniform, 1, material_specular);
		glUniform1f(material_shininess_uniform, material_shininess);

	}
	else
	{
		glUniform1i(L_KeyPressed_uniform, 0);
	}
	
    
	// OpenGL Drawing
	//Set model view and modelviewprojection matrices to identity
	vmath::mat4 modelMatrix = vmath::mat4::identity();
	vmath::mat4 viewMatrix =vmath::mat4::identity();
    
    //Declare Rotation Matrix to identity
    vmath::mat4 RotationMatrix = vmath::mat4::identity();
    
    
    
    //****Square drawing*******
    
 
    //Translate model view matrix
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    
    
    //Rotation matrix for Square
    RotationMatrix = vmath::rotate(angleCube, angleCube, angleCube);

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	//*** bind vao ***
	glBindVertexArray(gVao_sphere);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	
									 
	//*** unbind vao ***
	glBindVertexArray(0);

	
	//Stop using OpenGL program object
	glUseProgram(0);

	CGLFlushDrawable((CGLContextObj)[[self  openGLContext]CGLContextObj]);
	CGLUnlockContext((CGLContextObj)[[self  openGLContext]CGLContextObj]);
    
}

-(BOOL)acceptsFirstResponder
{
	//code
	[[self window]makeFirstResponder : self];
	return(YES);
}

-(void)keyDown:(NSEvent *)theEvent
{
    //variable declaration
    static bool bIsAKeyPressed = false;
    static bool bIsLKeyPressed = false;
    
    //code
	int key = (int)[[theEvent characters]characterAtIndex:0];
	switch(key)
	{
		case 27: //Esc key
			[self release];
			[NSApp terminate:self];
			break;
		case 'F':
		case 'f':
        	[[self window]toggleFullScreen:self]; //repainting occurs automatically
			break;
        case 'A':
        case 'a':
            if (bIsAKeyPressed == false)
            {
                gbAnimate = true;
                bIsAKeyPressed = true;
            }
            else
            {
                gbAnimate = false;
                bIsAKeyPressed = false;
            }
            break;
        case 'L':
        case 'l':
            if (bIsLKeyPressed == false)
            {
                gbLight = true;
                bIsLKeyPressed = true;
            }
            else
            {
                gbLight = false;
                bIsLKeyPressed = false;
            }
            break;
		default:
			break;

	}
}

-(void)mouseDown:(NSEvent *)theEvent
{
	//code
	
}

-(void)mouseDragged:(NSEvent *)theEvent
{
	//code
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
	//code
	
}

-(void)dealloc
{

    
	if (gVao_sphere)
	{
		glDeleteVertexArrays(1, &gVao_sphere);
		gVao_sphere = 0;
	}

	
	// Destroy vbo_position
	if (gVbo_sphere_position)
	{
		glDeleteVertexArrays(1, &gVbo_sphere_position);
		gVbo_sphere_position = 0;
	}

	// Destroy vbo_normal
	if (gVbo_sphere_normal)
	{
		glDeleteVertexArrays(1, &gVbo_sphere_normal);
		gVbo_sphere_normal = 0;
	}

	// Destroy vbo_element
	if (gVbo_sphere_element)
	{
		glDeleteVertexArrays(1, &gVbo_sphere_element);
		gVbo_sphere_element = 0;
	}

	//Detach vertex shader from shader program object
	glDetachShader(gShaderProgramObject, gVertexShaderObject);

	//Detach fragment shader from shader program object
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	
	
	// Delete vertex shader object
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;

	// Delete fragment shader object
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	// Delete shader program object
	glDeleteShader(gShaderProgramObject);
	gShaderProgramObject = 0;

	//unlink shader program
	glUseProgram(0);

	//code
	CVDisplayLinkStop(displayLink);
	CVDisplayLinkRelease(displayLink);

	[super dealloc];


}

void update()
{
    angleCube = angleCube - 0.2f;
    if (angleCube <= -360.0f)
        angleCube = 0.0f;

    
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime,CVOptionFlags flagsIn,
								CVOptionFlags *pFlagsOut,void *pDisplayLinkContext )
{
	CVReturn result = [(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
	return(result);
}



