#include<windows.h>
#include<gl/GL.h>
#include<gl/GLU.h>
#include "Header.h"

#define WIN_WIDTH 1920
#define WIN_HEIGHT 1080

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")

//Prototype of wndproc declared globally

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global varible declaration
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

GLUquadric *quadric = NULL;
GLfloat lookatz = 0.0f;
GLfloat angleMercury = -45.0f;
GLfloat angleVenus = 90.0f;
GLfloat angleEarth = 90.0f;
GLfloat angleMars = 90.0f;
GLfloat angleJupiter = 180.0f;
GLfloat angleSaturn = 10.0f;
GLfloat angleUranus = -180.0f;
GLfloat angleNeptune = 45.0f;
GLfloat anglePluto = -180.0f;


GLuint Texture_Sun;
GLuint Texture_Mercury;
GLuint	Texture_Venus;
GLuint	Texture_Earth;
GLuint	Texture_Mars;
GLuint	Texture_Jupiter;
GLuint	Texture_Saturn;
GLuint	Texture_Uranus;
GLuint	Texture_Neptune;
GLuint	Texture_Pluto;

GLfloat Saturnx = 0.0f;
GLfloat Saturny = 0.0f;
GLfloat Saturnz = 0.0f;

GLfloat Venusx = 0.0f;
GLfloat Venusy = 0.0f;
GLfloat Venusz = -100.0f;


//Function declaration
//void solarSystem(void);
//void update(void);
void Kundali(void);
void KundaliLines(void);
void KundaliSaturn(void);
void KundaliVenus(void);
void KundaliAnimation(void);

int LoadGLTextures(GLuint *, TCHAR[]);


DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

static int year = 0;
static int day = 0;

//main
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Function prototype
	void initialize(void);
	void uninitialize(void);
	void display();

	//Variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("RTROPENGL");
	bool bDone = false;

	//code
	//Initialize members of struct WNDCLASS
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;

	//Registering class
	RegisterClassEx(&wndclass);

	//Create Window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("MSOGLTemplate"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0,
		0,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	//Initialize
	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Messge loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
				display();
			}
		}
	}

	uninitialize();
	return((int)msg.wParam);
}

//WndProc
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//Function prototype
	//void display(void);
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
		/*case WM_PAINT:
		display();
		break;*/
		/*case WM_ERASEBKGND:
		return(0);*/
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x46: //For 'f' or 'F'
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;
		default:
			break;
		}
		break;
	case WM_CHAR:
		switch (LOWORD(wParam))
		{
		case 'D':
			day = (day + 6) % 360;
			break;
		case 'd':
			day = (day - 6) % 360;
			break;
		case 'Y':
			year = (year + 3) % 360;
			break;
		case 'y':
			year = (year - 3) % 360;
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));

}

void ToggleFullscreen(void)
{
	//Variable declaration
	MONITORINFO mi;

	//code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);

			}
		}
		ShowCursor(FALSE);

	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

}

void initialize(void)
{
	//function prototype
	void resize(int, int);

	//variable declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	//Initialization of structure PIXELFORMATESCRIPTOR
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;

	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	glClearDepth(1.0f); // range is 0 to 1
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	
	LoadGLTextures(&Texture_Sun, MAKEINTRESOURCE(IDBITMAP_SUN));
	LoadGLTextures(&Texture_Mercury, MAKEINTRESOURCE(IDBITMAP_MERCURY));
	LoadGLTextures(&Texture_Venus, MAKEINTRESOURCE(IDBITMAP_VENUS));
	LoadGLTextures(&Texture_Earth, MAKEINTRESOURCE(IDBITMAP_EARTH));
	LoadGLTextures(&Texture_Mars, MAKEINTRESOURCE(IDBITMAP_MARS));
	LoadGLTextures(&Texture_Jupiter, MAKEINTRESOURCE(IDBITMAP_JUPITER));
	LoadGLTextures(&Texture_Saturn, MAKEINTRESOURCE(IDBITMAP_SATURN));
	LoadGLTextures(&Texture_Uranus, MAKEINTRESOURCE(IDBITMAP_URANUS));
	LoadGLTextures(&Texture_Neptune, MAKEINTRESOURCE(IDBITMAP_NEPTUNE));
	LoadGLTextures(&Texture_Pluto, MAKEINTRESOURCE(IDBITMAP_PLUTO));

	glEnable(GL_TEXTURE_2D);

	resize(WIN_WIDTH, WIN_HEIGHT);

}

void display(void)
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// View transformation

	

	//gluLookAt(0.0f, 0.0f, lookatz, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f); // front view
	//KundaliSaturnAnimation();
	

	 //gluLookAt(0.0f, 90.0f, lookatz, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f); // top view

	//gluLookAt(0.0f, 0.0f, 18.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f); // Kundali View

	//update();
	//solarSystem();
	

	//glMatrixMode(GL_MODELVIEW);
	//glLoadIdentity();
	Kundali();

	SwapBuffers(ghdc);
}

int LoadGLTextures(GLuint *texture, TCHAR imageResourceId[])
{
	//variable declarations
	HBITMAP hBitmap;
	BITMAP bmp;
	int iStatus = FALSE;

	//code
	glGenTextures(1, texture); //1 image
	hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), imageResourceId, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);
	if (hBitmap) //if bitmap exists ( means hBitmap is not null )
	{
		iStatus = TRUE;
		GetObject(hBitmap, sizeof(bmp), &bmp);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4); //pixel storage mode (word alignment/4 bytes)
		glBindTexture(GL_TEXTURE_2D, *texture); //bind texture
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		//generate mipmapped texture (3 bytes, width, height & data from bmp)
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, bmp.bmWidth, bmp.bmHeight, GL_BGR_EXT, GL_UNSIGNED_BYTE, bmp.bmBits);

		DeleteObject(hBitmap); //delete unwanted bitmap handle
	}
	return(iStatus);
}

void resize(int width, int height)
{
	//code
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

}

void uninitialize(void)
{
	//UNINITIALIZE CODE
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;

}



void solarSystem(void)
{


	//Sun
	glBindTexture(GL_TEXTURE_2D, Texture_Sun);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluQuadricTexture(quadric, true);
	//glColor3f(1.0f, 0.0f, 0.0f);
	gluSphere(quadric, 1, 100, 100);



	//Mercury
	glBindTexture(GL_TEXTURE_2D, Texture_Mercury);
	glRotatef(angleMercury, 0.0f, 1.0f, 0.0f);
	glTranslatef(0.0, 0.0f, 2.0f); // merucy radius from Sun
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluQuadricTexture(quadric, true);
	//glColor3f(0.3f, 0.3f, 0.3f);
	gluSphere(quadric, 0.2f, 100, 100);

	//Venus
	glBindTexture(GL_TEXTURE_2D, Texture_Venus);
	glRotatef(angleVenus, 0.0f, 1.0f, 0.0f);
	glTranslatef(0.0, 0.0f, 4.0f); // Venus radius from Sun
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluQuadricTexture(quadric, true);
	//glColor3f(1.0f, 0.8f, 0.2f);
	gluSphere(quadric, 0.5f, 100, 100);

	//Earth
	glBindTexture(GL_TEXTURE_2D, Texture_Earth);
	glRotatef(angleEarth, 0.0f, 1.0f, 0.0f);
	glTranslatef(0.0, 0.0f, 7.0f); // Earth radius from Sun
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluQuadricTexture(quadric, true);
	//glColor3f(0.0f, 0.0f, 1.0f);
	gluSphere(quadric, 0.8f, 100, 100);
		

	//Mars
	glBindTexture(GL_TEXTURE_2D, Texture_Mars);
	glRotatef(angleMars, 0.0f, 1.0f, 0.0f);
	glTranslatef(0.0, 0.0f, 9.0f); // Mars radius from Sun
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluQuadricTexture(quadric, true);
	//glColor3f(1.0f, 0.6f, 0.0f);
	gluSphere(quadric, 0.5f, 100, 100);

	//Jupiter
	glBindTexture(GL_TEXTURE_2D, Texture_Jupiter);
	glRotatef(angleMars, 0.0f, 1.0f, 0.0f);
	glTranslatef(0.0, 0.0f, 12.0f); // Jupiter radius from Sun
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluQuadricTexture(quadric, true);
	//glColor3f(0.5f, 0.6f, 0.5f);
	gluSphere(quadric, 0.9f, 100, 100);


	//Saturn
	glBindTexture(GL_TEXTURE_2D, Texture_Saturn);
	glRotatef(angleSaturn, 0.0f, 1.0f, 0.0f);
	glTranslatef(0.0, 0.0f, 15.0f); // Saturn radius from Sun
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluQuadricTexture(quadric, true);
	//glColor3f(1.0f, 1.0f, 0.5f);
	gluSphere(quadric, 0.8f, 100, 100);

	glPushMatrix();

	// saturn ring
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	glRotatef(70.0f, -1.2f, -0.3f, 0.0f);
	glColor3f(0.5f, 0.5f, 0.5f);
	gluPartialDisk(quadric, 1.20, 1.30, 100, 1, 0, 360); //inner ring
	glColor3f(0.4f, 0.4f, 0.4f);
	gluPartialDisk(quadric, 1.30, 1.35, 100, 1, 0, 360); // middle ring 
	glColor3f(0.5f, 0.5f, 0.5f);
	gluPartialDisk(quadric, 1.35, 1.45, 100, 1, 0, 360); //outer ring

	glPopMatrix();

	//Uranus
	glBindTexture(GL_TEXTURE_2D, Texture_Uranus);
	glRotatef(angleUranus, 0.0f, 1.0f, 0.0f);
	glTranslatef(0.0, 0.0f, 18.0f); // Uranus radius from Sun
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluQuadricTexture(quadric, true);
	//glColor3f(0.5f, 0.5f, 1.0f);
	gluSphere(quadric, 0.6f, 100, 100);

	//Neptune
	glBindTexture(GL_TEXTURE_2D, Texture_Neptune);
	glRotatef(angleNeptune, 0.0f, 1.0f, 0.0f);
	glTranslatef(0.0, 0.0f, 20.0f); // Neptune radius from Sun
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluQuadricTexture(quadric, true);
	//glColor3f(0.0f, 0.3f, 1.0f);
	gluSphere(quadric, 0.5f, 100, 100);

	//Pluto
	glBindTexture(GL_TEXTURE_2D, Texture_Pluto);
	glRotatef(angleNeptune, 0.0f, 1.0f, 0.0f);
	glTranslatef(0.0, 0.0f, 22.0f); // Pluto radius from Sun
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluQuadricTexture(quadric, true);
	//glColor3f(1.0f, 0.6f, 0.2f);
	gluSphere(quadric, 0.3f, 100, 100);


}

void update(void)
{
	// Lookat transformation
	lookatz = lookatz + 0.003;
	if (lookatz >= 12)
		lookatz = 3;


	//mercury rotation
	angleMercury = angleMercury + 0.030;
	if (angleMercury >= 315)
		angleMercury = -45;

	//Venus rotation
	angleVenus = angleVenus + 0.0005;
	if (angleVenus >= 315)
		angleVenus = -45;

	//Earth rotation
	angleEarth = angleEarth + 0.0005;
	if (angleEarth >= 360)
		angleEarth = 0;

	//Mars rotation
	angleMars = angleMars + 0.0009;
	if (angleMars >= 360)
		angleMars = 0;

	// Saturn rotation
	angleSaturn = angleSaturn + 0.000005;
	if (angleSaturn >= 360)
		angleSaturn = 0;

	// Uranus rotation
	angleUranus = angleUranus + 0.000009;
	if (angleUranus >= 360)
		angleUranus = 0;

	// Neptune rotation
	angleNeptune = angleNeptune + 0.0000005;
	if (angleNeptune >= 360)
		angleNeptune = 0;

	// Pluto rotation
	anglePluto = anglePluto + 0.0000009;
	if (anglePluto >= 360)
		anglePluto = 0;

}

void Kundali(void)
{
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	KundaliLines();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	KundaliSaturn();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	KundaliVenus();

	KundaliAnimation();
}

void KundaliLines(void)
{
	glTranslatef(0.0f, 0.0f, -18.0f);
	glLineWidth(5);
	glBegin(GL_LINES);
	glColor3f(0.2f, 0.2f, 1.0f);

	glVertex3f(8.0f, 5.0f, 0.0f); //1
	glVertex3f(0.0f, 5.0f, 0.0f); //2

	glVertex3f(0.0f, 5.0f, 0.0f); //2
	glVertex3f(-8.0f, 5.0f, 0.0f); //3

	glVertex3f(-8.0, 5.0f, 0.0f); //3
	glVertex3f(-8.0f, 0.0f, 0.0f); //4

	glVertex3f(-8.0f, 0.0f, 0.0f); //4
	glVertex3f(-8.0f, -5.0f, 0.0f); //5

	glVertex3f(-8.0f, -5.0f, 0.0f); //5
	glVertex3f(0.0f, -5.0f, 0.0f); //6

	glVertex3f(0.0f, -5.0f, 0.0f); //6
	glVertex3f(8.0f, -5.0f, 0.0f); //7

	glVertex3f(8.0f, -5.0f, 0.0f); //7
	glVertex3f(8.0f, 0.0f, 0.0f); //8

	glVertex3f(8.0f, 0.0f, 0.0f); //8
	glVertex3f(8.0f, 5.0f, 0.0f); //1

	glVertex3f(8.0f, 0.0f, 0.0f); //8
	glVertex3f(0.0f, 5.0f, 0.0f); //2

	glVertex3f(0.0f, 5.0f, 0.0f); //2
	glVertex3f(-8.0f, 0.0f, 0.0f); //4

	glVertex3f(-8.0f, 0.0f, 0.0f); //4
	glVertex3f(0.0f, -5.0f, 0.0f); //6

	glVertex3f(0.0f, -5.0f, 0.0f); //6
	glVertex3f(8.0f, 0.0f, 0.0f); //8

	glVertex3f(8.0f, 5.0f, 0.0f); //1
	glVertex3f(-8.0f, -5.0f, 0.0f); //5

	glVertex3f(-8.0f, 5.0f, 0.0f); //3
	glVertex3f(8.0f, -5.0f, 0.0f); //7

	// Upper square of Kundali (ASTROMEDICOMP)
	glVertex3f(8.0f, 6.5f, 0.0f); //9
	glVertex3f(-8.0, 6.5f, 0.0f); //10

	glVertex3f(-8.0, 6.5f, 0.0f); //10
	glVertex3f(-8.0, 5.2f, 0.0f); //11

	glVertex3f(-8.0, 5.2f, 0.0f); //11
	glVertex3f(8.0, 5.2f, 0.0f); //12
	
	glVertex3f(8.0, 5.2f, 0.0f); //12
	glVertex3f(8.0f, 6.5f, 0.0f); //9


	glEnd();
}

void KundaliSaturn(void)
{
	
	//Saturn
	glBindTexture(GL_TEXTURE_2D, Texture_Saturn);
	//glRotatef(angleSaturn, 0.0f, 1.0f, 0.0f);
	//glTranslatef(3.3f, -3.1f, -15.0f); 
	glTranslatef(Saturnx, Saturny, Saturnz);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluQuadricTexture(quadric, true);
	//glColor3f(1.0f, 1.0f, 0.5f);
	gluSphere(quadric, 0.5f, 100, 100);
	glPushMatrix();

	// saturn ring
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	glRotatef(80.0f, -1.2f, -0.2f, 0.0f);
	glColor3f(0.5f, 0.5f, 0.5f);
	gluPartialDisk(quadric, 0.60, 0.70, 100, 1, 0, 360); //inner ring
	glColor3f(0.4f, 0.4f, 0.4f);
	gluPartialDisk(quadric, 0.70, 0.75, 100, 1, 0, 360); // middle ring 
	glColor3f(0.5f, 0.5f, 0.5f);
	gluPartialDisk(quadric, 0.75, 0.85, 100, 1, 0, 360); //outer ring

	glPopMatrix();
}


void KundaliVenus(void)
{
	//Venus
	glBindTexture(GL_TEXTURE_2D, Texture_Venus);
	//glRotatef(angleVenus, 0.0f, 1.0f, 0.0f);
	//glTranslatef(-3.0f, 0.0f, -15.0f); 
	glTranslatef(Venusx, Venusy, Venusz);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	gluQuadricTexture(quadric, true);
	//glColor3f(1.0f, 0.8f, 0.2f);
	gluSphere(quadric, 0.8f, 100, 100);

}

void KundaliAnimation(void)
{	
	if (Saturnz >= -15.0f)
	{
		Saturnz = Saturnz - 0.003;
		if (Saturnz <= -15.0f)
		{
			Saturnx = 3.3f;
			Saturny = -3.1f;
			Venusz = 0.0f;
		}
	}
	else if (Venusz >= -15.0f)
	{
		Venusz = Venusz - 0.003;
		if (Venusz <= -15.0f)
		{
			Venusx = -3.0f;
			Venusy = 0.0f;
		}
	}
	
}