#include<windows.h>
#include<gl/GL.h>
#include<gl/GLU.h>

#define WIN_WIDTH 1920
#define WIN_HEIGHT 1080

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")

//Prototype of wndproc declared globally

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global varible declaration
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

GLUquadric *quadric = NULL;
GLfloat lookatz = 0.0f;
GLfloat angleMercury = -45.0f;
GLfloat angleVenus = 90.0f;
GLfloat angleEarth = 90.0f;
GLfloat angleMars = 90.0f;
GLfloat angleJupiter = 180.0f;
GLfloat angleSaturn = 10.0f;
GLfloat angleUranus = -180.0f;
GLfloat angleNeptune = 45.0f;
GLfloat anglePluto = -180.0f;


//Function declaration
void solarSystem(void);
void update(void);


DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

static int year = 0;
static int day = 0;

//main
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Function prototype
	void initialize(void);
	void uninitialize(void);
	void display();

	//Variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("RTROPENGL");
	bool bDone = false;

	//code
	//Initialize members of struct WNDCLASS
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;

	//Registering class
	RegisterClassEx(&wndclass);

	//Create Window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("MSOGLTemplate"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0,
		0,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	//Initialize
	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Messge loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
				display();
			}
		}
	}

	uninitialize();
	return((int)msg.wParam);
}

//WndProc
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//Function prototype
	//void display(void);
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
		/*case WM_PAINT:
		display();
		break;*/
		/*case WM_ERASEBKGND:
		return(0);*/
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x46: //For 'f' or 'F'
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;
		default:
			break;
		}
		break;
	case WM_CHAR:
		switch (LOWORD(wParam))
		{
		case 'D':
			day = (day + 6) % 360;
			break;
		case 'd':
			day = (day - 6) % 360;
			break;
		case 'Y':
			year = (year + 3) % 360;
			break;
		case 'y':
			year = (year - 3) % 360;
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));

}

void ToggleFullscreen(void)
{
	//Variable declaration
	MONITORINFO mi;

	//code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);

			}
		}
		ShowCursor(FALSE);

	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

}

void initialize(void)
{
	//function prototype
	void resize(int, int);

	//variable declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	//Initialization of structure PIXELFORMATESCRIPTOR
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;

	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	glClearDepth(1.0f); // range is 0 to 1
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
		
	resize(WIN_WIDTH, WIN_HEIGHT);

}

void display(void)
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// View transformation
	
	gluLookAt(0.0f, 0.0f, lookatz, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f); // front view
	
	//gluLookAt(0.0f, 90.0f, lookatz, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f); // top view

	update();
	solarSystem();
			
	SwapBuffers(ghdc);
}

void resize(int width, int height)
{
	//code
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

}

void uninitialize(void)
{
	//UNINITIALIZE CODE
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;

}



void solarSystem(void)
{
	

	//Sun
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	glColor3f(1.0f, 0.0f, 0.0f);
	gluSphere(quadric, 1, 100, 100);

	

	//Mercury
	glRotatef(angleMercury, 0.0f, 1.0f, 0.0f);
	glTranslatef(0.0, 0.0f, 2.0f); // merucy radius from Sun
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	glColor3f(0.3f, 0.3f, 0.3f);
	gluSphere(quadric, 0.2f, 100, 100);

	//Venus
	glRotatef(angleVenus, 0.0f, 1.0f, 0.0f);
	glTranslatef(0.0, 0.0f, 4.0f); // Venus radius from Sun
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	glColor3f(1.0f, 0.8f, 0.2f);
	gluSphere(quadric, 0.5f, 100, 100);

	//Earth
	glRotatef(angleEarth, 0.0f, 1.0f, 0.0f);
	glTranslatef(0.0, 0.0f, 7.0f); // Earth radius from Sun
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	glColor3f(0.0f, 0.0f, 1.0f);
	gluSphere(quadric, 0.8f, 100, 100);

	//Mars
	glRotatef(angleMars, 0.0f, 1.0f, 0.0f);
	glTranslatef(0.0, 0.0f, 9.0f); // Mars radius from Sun
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	glColor3f(1.0f, 0.6f, 0.0f);
	gluSphere(quadric, 0.5f, 100, 100);

	//Jupiter
	glRotatef(angleMars, 0.0f, 1.0f, 0.0f);
	glTranslatef(0.0, 0.0f, 12.0f); // Jupiter radius from Sun
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	glColor3f(0.5f, 0.6f, 0.5f);
	gluSphere(quadric, 0.9f, 100, 100);

	
	
	
	//Saturn
	glRotatef(angleSaturn, 0.0f, 1.0f, 0.0f);
	glTranslatef(0.0, 0.0f, 15.0f); // Saturn radius from Sun
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	glColor3f(1.0f, 1.0f, 0.5f);
	gluSphere(quadric, 0.8f, 100, 100);

	glPushMatrix();
		
	// saturn ring
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	glRotatef(70.0f, -1.2f, -0.3f, 0.0f);
	glColor3f(0.5f, 0.5f, 0.5f);
	gluPartialDisk(quadric, 1.20, 1.30, 100, 1, 0, 360); //inner ring
	glColor3f(0.4f, 0.4f, 0.4f);
	gluPartialDisk(quadric, 1.30, 1.35, 100, 1, 0, 360); // middle ring 
	glColor3f(0.5f, 0.5f, 0.5f);
	gluPartialDisk(quadric, 1.35, 1.45, 100, 1, 0, 360); //outer ring

	glPopMatrix();
														 
	//Uranus
	glRotatef(angleUranus, 0.0f, 1.0f, 0.0f);
	glTranslatef(0.0, 0.0f, 18.0f); // Uranus radius from Sun
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	glColor3f(0.5f, 0.5f, 1.0f);
	gluSphere(quadric, 0.6f, 100, 100);

	//Neptune
	glRotatef(angleNeptune, 0.0f, 1.0f, 0.0f);
	glTranslatef(0.0, 0.0f, 20.0f); // Neptune radius from Sun
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	glColor3f(0.0f, 0.3f, 1.0f);
	gluSphere(quadric, 0.5f, 100, 100);

	//Pluto
	glRotatef(angleNeptune, 0.0f, 1.0f, 0.0f);
	glTranslatef(0.0, 0.0f, 22.0f); // Pluto radius from Sun
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	glColor3f(1.0f, 0.6f, 0.2f);
	gluSphere(quadric, 0.3f, 100, 100);
	

}

void update(void)
{
	// Lookat transformation
	lookatz = lookatz + 0.001;
	if (lookatz >= 50)
		lookatz = 50;
	
			
	//mercury rotation
	angleMercury = angleMercury + 0.030;
	if (angleMercury >= 315)
		angleMercury = -45;

	//Venus rotation
	angleVenus = angleVenus + 0.0005;
	if (angleVenus >= 315)
		angleVenus = -45;

	//Earth rotation
	angleEarth = angleEarth + 0.0005;
	if (angleEarth >= 360)
		angleEarth = 0;

	//Mars rotation
	angleMars = angleMars + 0.0009;
	if (angleMars >= 360)
		angleMars = 0;
	
	// Saturn rotation
	angleSaturn = angleSaturn + 0.000005;
	if (angleSaturn >= 360)
		angleSaturn = 0;

	// Uranus rotation
	angleUranus = angleUranus + 0.000009;
	if (angleUranus >= 360)
		angleUranus = 0;

	// Neptune rotation
	angleNeptune = angleNeptune + 0.0000005;
	if (angleNeptune >= 360)
		angleNeptune = 0;

	// Pluto rotation
	anglePluto = anglePluto + 0.0000009;
	if (anglePluto >= 360)
		anglePluto = 0;

}
