// DirectX three light

#include<windows.h>

#include<stdio.h>  // For file 

#include<d3d11.h>
#include<d3dcompiler.h>

#pragma warning (disable: 4838 )
#include "XNAMath\xnamath.h"

#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "D3dcompiler.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#include "Sphere.h"
#pragma comment(lib, "Sphere.lib")


//Prototype of wndproc declared globally

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global varible declaration
FILE *gpFile = NULL;
char gszLogFileName[] = "Log.txt";

HWND ghwnd = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

float gClearColor[4]; //RGBA
IDXGISwapChain *gpIDXGISwapChain = NULL;
ID3D11Device *gpID3D11Device = NULL;
ID3D11DeviceContext *gpID3D11DeviceContext = NULL;
ID3D11RenderTargetView *gpID3D11RenderTargetView = NULL;

ID3D11VertexShader *gpID3D11VertexShader = NULL;
ID3D11PixelShader *gpID3D11PixelShader = NULL;
ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Position = NULL;
ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Normal = NULL;
ID3D11InputLayout *gpID3D11InputLayout = NULL;
ID3D11Buffer *gpID3D11Buffer_ConstantBuffer = NULL;
ID3D11RasterizerState *gpID3D11RasterizerState = NULL;

ID3D11DepthStencilView *gpID3D11DepthStencilView = NULL;

ID3D11Buffer *gpID3D11Buffer_IndexBuffer = NULL;
float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];
unsigned int gNumElements;
unsigned int gNumVertices;


float anglePyramid = 0.0f;

bool gbLight = true;
bool gbAnimate = true;

float red_x = 0.0f;
float red_y = 0.0f;
float red_z = 0.0f;

float green_x = 0.0f;
float green_y = 0.0f;
float green_z = 0.0f;

float blue_x = 0.0f;
float blue_y = 0.0f;
float blue_z = 0.0f;

float Xcenter = 0.0f;
float Ycenter = 0.0f;
float Zcenter = 0.0f;

bool Xkey = false;
bool Ykey = false;
bool Zkey = false;

float angleCircle = 0.0f;

struct CBUFFER
{
	XMMATRIX WorldMatrix;
	XMMATRIX ViewMatrix;
	XMMATRIX RotationMatrix;
	XMMATRIX ProjectionMatrix;
	XMVECTOR La0;
	XMVECTOR Ld0;
	XMVECTOR Ls0;
	XMVECTOR La1;
	XMVECTOR Ld1;
	XMVECTOR Ls1;
	XMVECTOR La2;
	XMVECTOR Ld2;
	XMVECTOR Ls2;
	XMVECTOR Ka;
	XMVECTOR Kd;
	XMVECTOR Ks;
	float material_shininess;
	XMVECTOR Light0Position;
	XMVECTOR Light1Position;
	XMVECTOR Light2Position;
	UINT LKeyPressed;
};


float light0Ambient[] = { 0.0f,  0.0f,  0.0f, 1.0f };
float light0Diffuse[] = { 1.0f,  1.0f,  1.0f, 1.0f }; //white
float light0Specular[] = { 1.0f,  1.0f,  1.0f, 1.0f };
float light0Position[] = { 0.0f,  0.0f,  -100.0f, 1.0f };

float light1Ambient[] = { 0.0f,  0.0f,  0.0f, 1.0f };
float light1Diffuse[] = { 1.0f,  1.0f,  1.0f, 1.0f }; //white
float light1Specular[] = { 1.0f,  1.0f,  1.0f, 1.0f };
float light1Position[] = { 0.0f,  0.0f,  -100.0f, 1.0f };

float light2Ambient[] = { 0.0f,  0.0f,  0.0f, 1.0f };
float light2Diffuse[] = { 1.0f,  1.0f,  1.0f, 1.0f }; //white
float light2Specular[] = { 1.0f,  1.0f,  1.0f, 1.0f };
float light2Position[] = { 0.0f,  0.0f,  -100.0f, 1.0f };

//First sphere on first column, emerald
float material1_ambient[] = { 0.0215f, 0.1745f, 0.0215f, 1.0f };
float material1_diffuse[] = { 0.07568f, 0.61424f, 0.07568f, 1.0f };
float material1_specular[] = { 0.633f, 0.727811f, 0.633f, 1.0f };
float material1_shininess[] = { 0.6f * 128 };

//Second sphere on first column, jade
float material2_ambient[] = { 0.135f, 0.2225f, 0.1575f, 1.0f };
float material2_diffuse[] = { 0.54f, 0.89f, 0.63f, 1.0f };
float material2_specular[] = { 0.316228f, 0.316228f, 0.316228f, 1.0f };
float material2_shininess[] = { 0.1f * 128 };

//Third sphere on first column, obsidian
float material3_ambient[] = { 0.05375f, 0.05f, 0.06625f, 1.0f };
float material3_diffuse[] = { 0.18275f, 0.17f, 0.22525f, 1.0f };
float material3_specular[] = { 0.332741f, 0.328634f, 0.346435f, 1.0f };
float material3_shininess[] = { 0.3f * 128 };

//Fourth sphere on first column, pearl
float material4_ambient[] = { 0.25f, 0.20725f, 0.20725f, 1.0f };
float material4_diffuse[] = { 1.0f, 0.829f, 0.829f, 1.0f };
float material4_specular[] = { 0.296648f, 0.296648f, 0.296648f, 1.0f };
float material4_shininess[] = { 0.088f * 128 };

//Fifth sphere on first column, ruby
float material5_ambient[] = { 0.1745f, 0.01175f, 0.01175f, 1.0f };
float material5_diffuse[] = { 0.61424f, 0.04136f, 0.04136f, 1.0f };
float material5_specular[] = { 0.727811f, 0.626959f, 0.626959f, 1.0f };
float material5_shininess[] = { 0.6f * 128 };

//Sixth sphere on first column, turquoise
float material6_ambient[] = { 0.1f, 0.18725f, 0.1745f, 1.0f };
float material6_diffuse[] = { 0.396f, 0.74151f, 0.69102f, 1.0f };
float material6_specular[] = { 0.297254f, 0.30829f, 0.306678f, 1.0f };
float material6_shininess[] = { 1.0f };

//First sphere on second column, brass
float material7_ambient[] = { 0.329412f, 0.223529f, 0.027451f, 1.0f };
float material7_diffuse[] = { 0.780392f, 0.568627f, 0.113725f, 1.0f };
float material7_specular[] = { 0.992157f, 0.941176f, 0.807843f, 1.0f };
float material7_shininess[] = { 0.21794872f * 128 };

//Second sphere on second column, bronze
float material8_ambient[] = { 0.2125f, 0.1275f, 0.054f, 1.0f };
float material8_diffuse[] = { 0.714f, 0.4284f, 0.18144f, 1.0f };
float material8_specular[] = { 0.393548f, 0.211906f, 0.166721f, 1.0f };
float material8_shininess[] = { 0.2f * 128 };

//Third sphere on second column, chrome
float material9_ambient[] = { 0.25f, 0.25f, 0.25f, 1.0f };
float material9_diffuse[] = { 0.4f, 0.4f, 0.4f, 1.0f };
float material9_specular[] = { 0.774597f, 0.774597f, 0.774597f, 1.0f };
float material9_shininess[] = { 0.6f * 128 };

//Fourth sphere on second column, copper
float material10_ambient[] = { 0.19125f, 0.0735f, 0.0225f, 1.0f };
float material10_diffuse[] = { 0.7038f, 0.27048f, 0.0828f, 1.0f };
float material10_specular[] = { 0.276777f, 0.137622f, 0.086014f, 1.0f };
float material10_shininess[] = { 0.1f * 128 };

//Fifth sphere on second column, gold
float material11_ambient[] = { 0.24725f, 0.1995f, 0.07455f, 1.0f };
float material11_diffuse[] = { 0.75164f, 0.60648f, 0.22648f, 1.0f };
float material11_specular[] = { 0.628281f, 0.555802f, 0.366065f, 1.0f };
float material11_shininess[] = { 0.4f * 128 };

//Sixth sphere on second column, silver
float material12_ambient[] = { 0.19225f, 0.19225f, 0.19225f, 1.0f };
float material12_diffuse[] = { 0.50754f, 0.50754f, 0.50754f, 1.0f };
float material12_specular[] = { 0.508273f, 0.508273f, 0.508273f, 1.0f };
float material12_shininess[] = { 0.4f * 128 };

//First sphere on third column, black
float material13_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
float material13_diffuse[] = { 0.01f, 0.01f, 0.01f, 1.0f };
float material13_specular[] = { 0.50f, 0.50f, 0.50f, 1.0f };
float material13_shininess[] = { 0.25f * 128 };

//Second sphere on third column, cyan
float material14_ambient[] = { 0.0f, 0.1f, 0.06f, 1.0f };
float material14_diffuse[] = { 0.0f, 0.50980392f, 0.50980392f, 1.0f };
float material14_specular[] = { 0.50196078f, 0.50196078f, 0.50196078f, 1.0f };
float material14_shininess[] = { 0.25f * 128 };

//Third sphere on third column, green
float material15_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
float material15_diffuse[] = { 0.1f, 0.35f, 0.1f, 1.0f };
float material15_specular[] = { 0.45f, 0.55f, 0.45f, 1.0f };
float material15_shininess[] = { 0.25f * 128 };

//Fourth sphere on third column, red
float material16_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
float material16_diffuse[] = { 0.5f, 0.0f, 0.0f, 1.0f };
float material16_specular[] = { 0.7f, 0.6f, 0.6f, 1.0f };
float material16_shininess[] = { 0.25f * 128 };

//Fifth sphere on third column, white
float material17_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
float material17_diffuse[] = { 0.55f, 0.55f, 0.55f, 1.0f };
float material17_specular[] = { 0.70f, 0.70f, 0.70f, 1.0f };
float material17_shininess[] = { 0.25f * 128 };

//Sixth sphere on third column, yellow  plastic
float material18_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
float material18_diffuse[] = { 0.5f, 0.5f, 0.0f, 1.0f };
float material18_specular[] = { 0.60f, 0.60f, 0.50f, 1.0f };
float material18_shininess[] = { 0.25f * 128 };

//First sphere on fourth column, black
float material19_ambient[] = { 0.02f, 0.02f, 0.02f, 1.0f };
float material19_diffuse[] = { 0.01f, 0.01f, 0.01f, 1.0f };
float material19_specular[] = { 0.4f, 0.4f, 0.4f, 1.0f };
float material19_shininess[] = { 0.078125f * 128 };

//Second sphere on fourth column, cyan
float material20_ambient[] = { 0.0f, 0.05f, 0.05f, 1.0f };
float material20_diffuse[] = { 0.4f, 0.5f, 0.5f, 1.0f };
float material20_specular[] = { 0.04f, 0.7f, 0.7f, 1.0f };
float material20_shininess[] = { 0.078125f * 128 };

//Third sphere on fourth column, green
float material21_ambient[] = { 0.0f, 0.05f, 0.0f, 1.0f };
float material21_diffuse[] = { 0.4f, 0.5f, 0.4f, 1.0f };
float material21_specular[] = { 0.04f, 0.7f, 0.04f, 1.0f };
float material21_shininess[] = { 0.078125f * 128 };

//Fourth sphere on fourth column, red
float material22_ambient[] = { 0.05f, 0.0f, 0.0f, 1.0f };
float material22_diffuse[] = { 0.5f, 0.4f, 0.4f, 1.0f };
float material22_specular[] = { 0.7f, 0.04f, 0.04f, 1.0f };
float material22_shininess[] = { 0.078125f * 128 };

//Fifth sphere on fourth column, white
float material23_ambient[] = { 0.05f, 0.05f, 0.05f, 1.0f };
float material23_diffuse[] = { 0.5f, 0.5f, 0.5f, 1.0f };
float material23_specular[] = { 0.7f, 0.7f, 0.7f, 1.0f };
float material23_shininess[] = { 0.078125f * 128 };

//Fifth sphere on fourth column, yellow rubber
float material24_ambient[] = { 0.05f, 0.05f, 0.0f, 1.0f };
float material24_diffuse[] = { 0.5f, 0.5f, 0.4f, 1.0f };
float material24_specular[] = { 0.7f, 0.7f, 0.04f, 1.0f };
float material24_shininess[] = { 0.078125f * 128 };

XMMATRIX gPerspectiveProjectionMatrix;

float width1, height1;
float viewportX = 0;
float viewportY = 0;
void resize(float, float);

//main
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Function prototype
	HRESULT initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);

	//Variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("Direct3D11");
	bool bDone = false;

	//code

	//Create log file
	if (fopen_s(&gpFile, gszLogFileName, "w") != 0)
	{
		MessageBox(NULL, TEXT("Log file cannot be created\n Exiting....."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf_s(gpFile, "Log file is successfully opened. \n");
		fclose(gpFile);
	}


	//Initialize members of struct WNDCLASS
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//Registering class
	RegisterClassEx(&wndclass);

	//Create Window
	hwnd = CreateWindow(szClassName,
		TEXT("Direct3D11 Window"),
		WS_OVERLAPPEDWINDOW,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;



	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Initialize D3D
	HRESULT hr;
	hr = initialize();
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "initialize() failed, exiting now ......\n");
		fclose(gpFile);
		DestroyWindow(hwnd);
		hwnd = NULL;
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "initialize() succeeded ......\n");
		fclose(gpFile);
	}


	//Messge loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbAnimate == true)
				update();
			display();			 
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
				
			}
		}
	}

	uninitialize();
	return((int)msg.wParam);
}

//WndProc
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//Function prototype
	HRESULT resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	//variable  declaration
	HRESULT hr;


	static bool bIsAKeyPressed = false;
	static bool bIsLKeyPressed = false;

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
		case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		if (gpID3D11DeviceContext)
		{
			hr = resize(LOWORD(lParam), HIWORD(lParam));
			if (FAILED(hr))
			{
				fopen_s(&gpFile, gszLogFileName, "a+");
				fprintf_s(gpFile, "resize() failed......\n");
				fclose(gpFile);
				return(hr);
			}
			else
			{
				fopen_s(&gpFile, gszLogFileName, "a+");
				fprintf_s(gpFile, "resize() succeeded......\n");
				fclose(gpFile);
			}

		}
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			if (gbEscapeKeyIsPressed == false)
			gbEscapeKeyIsPressed = true;
			break;
		case 0x46: //For 'f' or 'F'
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;
		case 0x41: //for 'A' or 'a'
			if (bIsAKeyPressed == false)
			{
				gbAnimate = true;
				bIsAKeyPressed = true;
			}
			else
			{
				gbAnimate = false;
				bIsAKeyPressed = false;
			}
			break;
		case 0x4C: //for 'L' or 'l'
			if (bIsLKeyPressed == false)
			{
				gbLight = true;
				bIsLKeyPressed = true;
			}
			else
			{
				gbLight = false;
				bIsLKeyPressed = false;
			}
			break;
		case 0x58: //For 'x' or 'X'
		{
			Xkey = true;
			Ykey = false;
			Zkey = false;
		}
		break;
		case 0x59: //For 'y' or 'Y'
		{
			Xkey = false;
			Ykey = true;
			Zkey = false;
		}
		break;
		case 0x5A: //For 'z' or 'Z'
		{
			Xkey = false;
			Ykey = false;
			Zkey = true;
		}
		break;
		default:
			break;
		}
	case WM_LBUTTONDOWN:
		break;
	case WM_CLOSE:
		uninitialize();
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));

}

void ToggleFullscreen(void)
{
	//Variable declaration
	MONITORINFO mi;

	//code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);

			}
		}
		ShowCursor(FALSE);

	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

}

HRESULT initialize(void)
{
	//function prototype
	HRESULT resize(int, int);
	void uninitialize(void);

	//variable declaration
	HRESULT hr;
	D3D_DRIVER_TYPE d3dDriverType;
	D3D_DRIVER_TYPE d3dDriverTypes[] = { D3D_DRIVER_TYPE_HARDWARE, D3D_DRIVER_TYPE_WARP, D3D_DRIVER_TYPE_REFERENCE, };
	D3D_FEATURE_LEVEL d3dFeatureLevel_required = D3D_FEATURE_LEVEL_11_0;
	D3D_FEATURE_LEVEL d3dFeatureLevel_acquired = D3D_FEATURE_LEVEL_10_0; // default, lowest
	UINT createDeviceFlags = 0;
	UINT numDriverTypes = 0;
	UINT numFeatureLevels = 1; //based upon d3dFeatureLevel_required

	//code
	numDriverTypes = sizeof(d3dDriverTypes) / sizeof(d3dDriverTypes[0]); // calculating size of array

	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;
	ZeroMemory((void *)&dxgiSwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));
	dxgiSwapChainDesc.BufferCount = 1;
	dxgiSwapChainDesc.BufferDesc.Width = WIN_WIDTH;
	dxgiSwapChainDesc.BufferDesc.Height = WIN_HEIGHT;
	dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	dxgiSwapChainDesc.OutputWindow = ghwnd;
	dxgiSwapChainDesc.SampleDesc.Count = 1;
	dxgiSwapChainDesc.SampleDesc.Quality = 0;									  
	dxgiSwapChainDesc.Windowed = TRUE;

	for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
	{
		d3dDriverType = d3dDriverTypes[driverTypeIndex];
		hr = D3D11CreateDeviceAndSwapChain(
			NULL,							// adapter
			d3dDriverType,					// driver type
			NULL,							// Software
			createDeviceFlags,				// flags
			&d3dFeatureLevel_required,		// feature levels
			numFeatureLevels,				// Num Feature Levels
			D3D11_SDK_VERSION,				// SDK version
			&dxgiSwapChainDesc,				// Swap chain Desc
			&gpIDXGISwapChain,				// Swap chain
			&gpID3D11Device,				// Device
			&d3dFeatureLevel_acquired,		// Feature Level
			&gpID3D11DeviceContext);		// device context
		if (SUCCEEDED(hr))
			break;
	}
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11CreateDeviceAndSwapChain() failed......\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11CreateDeviceAndSwapChain() succeeded......\n");
		fprintf_s(gpFile, "The chosen driver is of ");
		if (d3dDriverType == D3D_DRIVER_TYPE_HARDWARE)
		{
			fprintf_s(gpFile, "Hardware Type. \n");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_WARP)
		{
			fprintf_s(gpFile, "Warp Type. \n");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_REFERENCE)
		{
			fprintf_s(gpFile, "Reference Type. \n");
		}
		else
		{
			fprintf_s(gpFile, "Unknown Type. \n");
		}

		fprintf_s(gpFile, "The supported highest feature level is ");
		if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_11_0)
		{
			fprintf_s(gpFile, "11.0. \n");
		}
		else if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_1)
		{
			fprintf_s(gpFile, "10.1. \n");
		}
		else if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_0)
		{
			fprintf_s(gpFile, "10.0. \n");
		}
		else
		{
			fprintf_s(gpFile, "Unknown  \n");
		}

		fclose(gpFile);
	}

	//initialize shader, input layouts, constant buffers etc.
	const char *vertexShaderSourceCode =
		"cbuffer ConstantBuffer" \
		"{" \
		"float4x4 WorldMatrix; " \
		"float4x4 ViewMatrix; " \
		"float4x4 RotationMatrix; " \
		"float4x4 ProjectionMatrix; " \
		"float4 La0; " \
		"float4 Ld0; " \
		"float4 Ls0; " \
		"float4 La1; " \
		"float4 Ld1; " \
		"float4 Ls1; " \
		"float4 La2; " \
		"float4 Ld2; " \
		"float4 Ls2; " \
		"float4 Ka; " \
		"float4 Kd; " \
		"float4 Ks; " \
		"float material_shininess;" \
		"float4 Light0Position; " \
		"float4 Light1Position; " \
		"float4 Light2Position; " \
		"uint LKeyPressed; " \
		"}" \

		"struct vertex_output" \
		"{" \
		"float4 position : SV_POSITION;" \
		"float3 transformed_normals : NORMAL0;" \
		"float3 light0_direction : NORMAL1;" \
		"float3 light1_direction : NORMAL2;" \
		"float3 light2_direction : NORMAL3;" \
		"float3 viewer_vector : NORMAL4;" \
		"};" \

		"vertex_output main(float4 pos : POSITION, float4 normal : NORMAL)" \
		"{" \
		"vertex_output output;" \
		"if (LKeyPressed == 1)" \
		"{" \
		"float4 eyeCoordinates = mul(WorldMatrix, mul(ViewMatrix, mul(RotationMatrix, pos)));" \
		"output.transformed_normals = mul((float3x3)mul(WorldMatrix, mul(ViewMatrix, RotationMatrix)), (float3)normal);" \
		"output.light0_direction = (float3)Light0Position - eyeCoordinates.xyz;" \
		"output.light1_direction = (float3)Light1Position - eyeCoordinates.xyz;" \
		"output.light2_direction = (float3)Light2Position - eyeCoordinates.xyz;" \
		"output.viewer_vector = -eyeCoordinates.xyz;" \
		"}" \
		"float4 WorldViewPosition = mul(WorldMatrix, mul(ViewMatrix, mul(RotationMatrix, pos)));" \
		"output.position = mul(ProjectionMatrix, WorldViewPosition);" \
		"return(output);" \
		"}";

	ID3DBlob *pID3DBlob_VertexShaderCode = NULL;
	ID3DBlob *pID3DBlob_Error = NULL;

	hr = D3DCompile(vertexShaderSourceCode,
		lstrlenA(vertexShaderSourceCode) + 1,
		"VS",
		NULL,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main",
		"vs_5_0",
		0,
		0,
		&pID3DBlob_VertexShaderCode,
		&pID3DBlob_Error);

	if (FAILED(hr))
	{
		if (pID3DBlob_Error != NULL)
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "D3Dcompile() failed for Vertex Shader : %s\n", (char *)pID3DBlob_Error->GetBufferPointer());
			fclose(gpFile);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return(hr);
		}
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3Dcompile() succeeded for Vertex Shader.\n");
		fclose(gpFile);
	}

	hr = gpID3D11Device->CreateVertexShader(pID3DBlob_VertexShaderCode->GetBufferPointer(), pID3DBlob_VertexShaderCode->GetBufferSize(), NULL, &gpID3D11VertexShader);
	if (FAILED(hr))
	{
	
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "ID3D11Device::CreateVertexShader() failed. \n");
			fclose(gpFile);
			return(hr);
		
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateVertexShader() succeeded..\n");
		fclose(gpFile);
	}
	gpID3D11DeviceContext->VSSetShader(gpID3D11VertexShader, 0, 0);


	//Pixel shader
	const char *pixelShaderSourceCode =
		"cbuffer ConstantBuffer" \
		"{" \
		"float4x4 WorldMatrix; " \
		"float4x4 ViewMatrix; " \
		"float4x4 RotationMatrix; " \
		"float4x4 ProjectionMatrix; " \
		"float4 La0; " \
		"float4 Ld0; " \
		"float4 Ls0; " \
		"float4 La1; " \
		"float4 Ld1; " \
		"float4 Ls1; " \
		"float4 La2; " \
		"float4 Ld2; " \
		"float4 Ls2; " \
		"float4 Ka; " \
		"float4 Kd; " \
		"float4 Ks; " \
		"float material_shininess;" \
		"float4 Light0Position; " \
		"float4 Light1Position; " \
		"float4 Light2Position; " \
		"uint LKeyPressed; " \
		"}" \

		"struct vertex_output" \
		"{" \
		"float4 position : SV_POSITION;" \
		"float3 transformed_normals : NORMAL0;" \
		"float3 light0_direction : NORMAL1;" \
		"float3 light1_direction : NORMAL2;" \
		"float3 light2_direction : NORMAL3;" \
		"float3 viewer_vector : NORMAL4;" \
		"};" \

		"float4 main(vertex_output input) : SV_TARGET" \
		"{" \
		"float4 phong_ads_color;" \
		"if (LKeyPressed == 1)" \
		"{" \
		"float3 normalized_transformed_normals = normalize(input.transformed_normals);" \
		"float3 normalized_light0_direction = normalize(input.light0_direction);" \
		"float3 normalized_light1_direction = normalize(input.light1_direction);" \
		"float3 normalized_light2_direction = normalize(input.light2_direction);" \
		"float3 normalized_viewer_vector = normalize(input.viewer_vector);" \
		"float tn_dot_ld0 = max(dot(normalized_light0_direction, normalized_transformed_normals), 0.0);" \
		"float tn_dot_ld1 = max(dot(normalized_light1_direction, normalized_transformed_normals), 0.0);" \
		"float tn_dot_ld2 = max(dot(normalized_light2_direction, normalized_transformed_normals), 0.0);" \
		"float3 reflection_vector0 = reflect(-normalized_light0_direction, normalized_transformed_normals);" \
		"float3 reflection_vector1 = reflect(-normalized_light1_direction, normalized_transformed_normals);" \
		"float3 reflection_vector2 = reflect(-normalized_light2_direction, normalized_transformed_normals);" \
		"float4 ambient0 = La0 * Ka;" \
		"float4 diffuse0 = Ld0 * Kd * tn_dot_ld0;" \
		"float4 specular0 = Ls0 * Ks * pow(max(dot(reflection_vector0, normalized_viewer_vector), 0.0), material_shininess);" \
		"float4 ambient1 = La1 * Ka;" \
		"float4 diffuse1 = Ld1 * Kd * tn_dot_ld1;" \
		"float4 specular1 = Ls1 * Ks * pow(max(dot(reflection_vector1, normalized_viewer_vector), 0.0), material_shininess);" \
		"float4 ambient2 = La2 * Ka;" \
		"float4 diffuse2 = Ld2 * Kd * tn_dot_ld2;" \
		"float4 specular2 = Ls2 * Ks * pow(max(dot(reflection_vector2, normalized_viewer_vector), 0.0), material_shininess);" \
		"phong_ads_color = ambient0 + diffuse0 + specular0;" \
		"}" \
		"else" \
		"{"\
		"phong_ads_color = float4(1.0, 1.0, 1.0, 1.0);" \
		"}"\
		"float4 phong_ads_color_output;" \
		"phong_ads_color_output = phong_ads_color;" \
		"return(phong_ads_color_output);" \
		"}";

	ID3DBlob *pID3DBlob_PixelShaderCode = NULL;
	pID3DBlob_Error = NULL;

	hr = D3DCompile(pixelShaderSourceCode,
		lstrlenA(pixelShaderSourceCode) + 1,
		"PS",
		NULL,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main",
		"ps_5_0",
		0,
		0,
		&pID3DBlob_PixelShaderCode,
		&pID3DBlob_Error);

	if (FAILED(hr))
	{
		if (pID3DBlob_Error != NULL)
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "D3Dcompile() failed for Pixel Shader : %s\n", (char *)pID3DBlob_Error->GetBufferPointer());
			fclose(gpFile);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return(hr);
		}
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3Dcompile() succeeded for Pixel Shader.\n");
		fclose(gpFile);
	}

	hr = gpID3D11Device->CreatePixelShader(pID3DBlob_PixelShaderCode->GetBufferPointer(), pID3DBlob_PixelShaderCode->GetBufferSize(), NULL, &gpID3D11PixelShader);
	if (FAILED(hr))
	{

		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreatePixelShader() failed. \n");
		fclose(gpFile);
		return(hr);

	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreatePixelShader() succeeded..\n");
		fclose(gpFile);
	}
	gpID3D11DeviceContext->PSSetShader(gpID3D11PixelShader, 0, 0);
	pID3DBlob_PixelShaderCode->Release();
	pID3DBlob_PixelShaderCode = NULL;

	//vertices
	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();


	// *** Sphere***
	//create vertex position buffer
	D3D11_BUFFER_DESC bufferDesc;
	ZeroMemory(&bufferDesc, sizeof(D3D11_BUFFER_DESC));
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc.ByteWidth = sizeof(float) * ARRAYSIZE(sphere_vertices);
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	hr = gpID3D11Device->CreateBuffer(&bufferDesc, NULL, &gpID3D11Buffer_VertexBuffer_Position);
	if (FAILED(hr))
	{

		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() failed for vertex position buffer. \n");
		fclose(gpFile);
		return(hr);

	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() succeeded for vertex position buffer.\n");
		fclose(gpFile);
	}

	//Copy triangle vertices position into above buffer
	D3D11_MAPPED_SUBRESOURCE mappedSubresource;
	ZeroMemory(&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));
	gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer_Position, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &mappedSubresource);
	memcpy(mappedSubresource.pData, sphere_vertices, sizeof(sphere_vertices));
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer_Position, NULL);

	//create triangle vertex color buffer
	ZeroMemory(&bufferDesc, sizeof(D3D11_BUFFER_DESC));
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc.ByteWidth = sizeof(float) * ARRAYSIZE(sphere_normals);
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	hr = gpID3D11Device->CreateBuffer(&bufferDesc, NULL, &gpID3D11Buffer_VertexBuffer_Normal);
	if (FAILED(hr))
	{

		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() failed for vertex color buffer. \n");
		fclose(gpFile);
		return(hr);

	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() succeeded for vertex color buffer.\n");
		fclose(gpFile);
	}



	//Copy triangle vertices color into above buffer
	ZeroMemory(&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));
	gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer_Normal, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &mappedSubresource);
	memcpy(mappedSubresource.pData, sphere_normals, sizeof(sphere_normals));
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer_Normal, NULL);


	//create index buffer
	ZeroMemory(&bufferDesc, sizeof(D3D11_BUFFER_DESC));
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc.ByteWidth = gNumElements * sizeof(short);
	bufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	hr = gpID3D11Device->CreateBuffer(&bufferDesc, NULL, &gpID3D11Buffer_IndexBuffer);
	if (FAILED(hr))
	{

		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() failed for index buffer. \n");
		fclose(gpFile);
		return(hr);

	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() succeeded for index buffer.\n");
		fclose(gpFile);
	}

	// Copy indices into above buffer
	ZeroMemory(&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));
	gpID3D11DeviceContext->Map(gpID3D11Buffer_IndexBuffer, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &mappedSubresource);
	memcpy(mappedSubresource.pData, sphere_elements, gNumElements * sizeof(short));
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_IndexBuffer, NULL);


	//Create and set input layout for  position
	D3D11_INPUT_ELEMENT_DESC inputElementDesc[2];
	inputElementDesc[0].SemanticName = "POSITION";
	inputElementDesc[0].SemanticIndex = 0;
	inputElementDesc[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	inputElementDesc[0].InputSlot = 0;
	inputElementDesc[0].AlignedByteOffset = 0;
	inputElementDesc[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputElementDesc[0].InstanceDataStepRate = 0;

	//Create and set input layout for Normal
	inputElementDesc[1].SemanticName = "NORMAL";
	inputElementDesc[1].SemanticIndex = 0;
	inputElementDesc[1].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	inputElementDesc[1].InputSlot = 1;
	inputElementDesc[1].AlignedByteOffset = 0;
	inputElementDesc[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputElementDesc[1].InstanceDataStepRate = 0;

	hr = gpID3D11Device->CreateInputLayout(inputElementDesc, 2, pID3DBlob_VertexShaderCode->GetBufferPointer(), pID3DBlob_VertexShaderCode->GetBufferSize(), &gpID3D11InputLayout);

	

		if (FAILED(hr))
	{

		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateInputLayout() failed. \n");
		fclose(gpFile);
		return(hr);

	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateInputLayout() succeeded.\n");
		fclose(gpFile);
	}
	gpID3D11DeviceContext->IASetInputLayout(gpID3D11InputLayout);
	pID3DBlob_VertexShaderCode->Release();
	pID3DBlob_VertexShaderCode = NULL;

	//define and set the constant buffer
	D3D11_BUFFER_DESC bufferDesc_ConstantBuffer;
	ZeroMemory(&bufferDesc_ConstantBuffer, sizeof(D3D11_BUFFER_DESC));
	bufferDesc_ConstantBuffer.Usage = D3D11_USAGE_DEFAULT;
	bufferDesc_ConstantBuffer.ByteWidth = sizeof(CBUFFER);
	bufferDesc_ConstantBuffer.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	//bufferDesc_ConstantBuffer.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	hr = gpID3D11Device->CreateBuffer(&bufferDesc_ConstantBuffer, nullptr, &gpID3D11Buffer_ConstantBuffer);
	if (FAILED(hr))
	{

		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() failed for constant buffer. \n");
		fclose(gpFile);
		return(hr);

	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() succeeded for constant buffer.\n");
		fclose(gpFile);
	}
	gpID3D11DeviceContext->VSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer);
	gpID3D11DeviceContext->PSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer);

	//D3D clear color (black)
	gClearColor[0] = 0.25f;
	gClearColor[1] = 0.25f;
	gClearColor[2] = 0.25f;
	gClearColor[3] = 1.0f;

	//Set projection matrix to identity matrix
	gPerspectiveProjectionMatrix = XMMatrixIdentity();

	// Back face culling OFF code
	D3D11_RASTERIZER_DESC rasterizerDesc;
	ZeroMemory(&rasterizerDesc, sizeof(D3D11_RASTERIZER_DESC));
	rasterizerDesc.AntialiasedLineEnable = FALSE;
	rasterizerDesc.MultisampleEnable = FALSE;
	rasterizerDesc.DepthBias = 0;
	rasterizerDesc.DepthBiasClamp = 0.0f;
	rasterizerDesc.SlopeScaledDepthBias = 0.0f;
	rasterizerDesc.CullMode = D3D11_CULL_NONE;
	rasterizerDesc.DepthClipEnable = TRUE;
	rasterizerDesc.FillMode = D3D11_FILL_SOLID;
	rasterizerDesc.FrontCounterClockwise = FALSE;
	rasterizerDesc.ScissorEnable = FALSE;

	gpID3D11Device->CreateRasterizerState(&rasterizerDesc, &gpID3D11RasterizerState);

	gpID3D11DeviceContext->RSSetState(gpID3D11RasterizerState);

	//call resize for first time
	hr = resize(WIN_WIDTH, WIN_HEIGHT);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "resize() failed......\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "resize() succeeded......\n");
		fclose(gpFile);
	}

	return(S_OK);
}

HRESULT resize(int width, int height)
{
	//code
	HRESULT hr = S_OK;

	//free DepthStencilView
	if (gpID3D11DepthStencilView)
	{
		gpID3D11DepthStencilView->Release();
		gpID3D11DepthStencilView = NULL;
	}


	//Free any size dependent resources
	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	//resize swap chain buffers accordingly
	gpIDXGISwapChain->ResizeBuffers(1, width, height, DXGI_FORMAT_R8G8B8A8_UNORM, 0);

	//again set back buffer from swap chain
	ID3D11Texture2D *pID3D11Texture2D_BackBuffer;
	gpIDXGISwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pID3D11Texture2D_BackBuffer);

	//again get render target view from d3d11 device using above back buffer
	hr = gpID3D11Device->CreateRenderTargetView(pID3D11Texture2D_BackBuffer, NULL, &gpID3D11RenderTargetView);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateRenderTargetView() failed......\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateRenderTargetView() succeeded......\n");
		fclose(gpFile);
	}
	pID3D11Texture2D_BackBuffer->Release();
	pID3D11Texture2D_BackBuffer = NULL;

	//Depth Buffer
	D3D11_TEXTURE2D_DESC textureDesc;
	ZeroMemory(&textureDesc, sizeof(D3D11_TEXTURE2D_DESC));
	textureDesc.Width = (UINT)width; // It is depth width
	textureDesc.Height = (UINT)height;
	textureDesc.ArraySize = 1; // No array for depth
	textureDesc.MipLevels = 1;
	textureDesc.SampleDesc.Count = 1;
	textureDesc.SampleDesc.Quality = 0;
	textureDesc.Format = DXGI_FORMAT_D32_FLOAT; //Depth bit
	textureDesc.Usage = D3D11_USAGE_DEFAULT;
	textureDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	textureDesc.CPUAccessFlags = 0;
	textureDesc.MiscFlags = 0;

	ID3D11Texture2D *pID3D11Texture2D_DepthBuffer;
	hr = gpID3D11Device->CreateTexture2D(&textureDesc, NULL, &pID3D11Texture2D_DepthBuffer);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateTexture2D() failed......\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateTexture2D() succeeded......\n");
		fclose(gpFile);
	}


	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
	ZeroMemory(&depthStencilViewDesc, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));
	depthStencilViewDesc.Format = DXGI_FORMAT_D32_FLOAT;
	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;

	hr = gpID3D11Device->CreateDepthStencilView(pID3D11Texture2D_DepthBuffer, &depthStencilViewDesc, &gpID3D11DepthStencilView);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateDepthStencilView() failed......\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateDepthStencilView() succeeded......\n");
		fclose(gpFile);
	}

	if (pID3D11Texture2D_DepthBuffer)
	{
		pID3D11Texture2D_DepthBuffer->Release();
		pID3D11Texture2D_DepthBuffer = NULL;
	}

	//set render target view as render target 
	gpID3D11DeviceContext->OMSetRenderTargets(1, &gpID3D11RenderTargetView, gpID3D11DepthStencilView);

	//Set viewport
	D3D11_VIEWPORT d3dViewPort;
	d3dViewPort.TopLeftX = 0;
	d3dViewPort.TopLeftY = 0;
	d3dViewPort.Width = (float)width;
	d3dViewPort.Height = (float)height;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort);

	//Set perspective matrix

	if (height == 0)
		height = 1;

	width1 = width;
	height1 = height;

	gPerspectiveProjectionMatrix = XMMatrixPerspectiveFovLH(XMConvertToRadians(45.0f), (4.0 / 3.0)* (float)width / (float)height, 0.1f, 100.0f);
	
	return(hr);
}

void display(void)
{
	//code
	//clear render target view to a chosen color
	gpID3D11DeviceContext->ClearRenderTargetView(gpID3D11RenderTargetView, gClearColor);

	gpID3D11DeviceContext->ClearDepthStencilView(gpID3D11DepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);

	//----------- *** Sphere Drawing *** -------------

	// Select which vertex position buffer to display
	UINT stride = sizeof(float) * 3;
	UINT offset = 0;
	gpID3D11DeviceContext->IASetVertexBuffers(0, 1, &gpID3D11Buffer_VertexBuffer_Position, &stride, &offset);

	// Select which vertex color buffer to display
	stride = sizeof(float) * 3;
	offset = 0;
	gpID3D11DeviceContext->IASetVertexBuffers(1, 1, &gpID3D11Buffer_VertexBuffer_Normal, &stride, &offset);

	gpID3D11DeviceContext->IASetIndexBuffer(gpID3D11Buffer_IndexBuffer, DXGI_FORMAT_R16_UINT, 0); //R16 maps with 'short' declaration


	// Select geometry primitive
	gpID3D11DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	CBUFFER constantBuffer;

	if (gbLight == true)
	{
		constantBuffer.La0 = XMVectorSet(light0Ambient[0], light0Ambient[1], light0Ambient[2], light0Ambient[3]);
		constantBuffer.Ld0 = XMVectorSet(light0Diffuse[0], light0Diffuse[1], light0Diffuse[2], light0Diffuse[3]);
		constantBuffer.Ls0 = XMVectorSet(light0Specular[0], light0Specular[1], light0Specular[2], light0Specular[3]);

		constantBuffer.La1 = XMVectorSet(light1Ambient[0], light1Ambient[1], light1Ambient[2], light1Ambient[3]);
		constantBuffer.Ld1 = XMVectorSet(light1Diffuse[0], light1Diffuse[1], light1Diffuse[2], light1Diffuse[3]);
		constantBuffer.Ls1 = XMVectorSet(light1Specular[0], light1Specular[1], light1Specular[2], light1Specular[3]);

		constantBuffer.La2 = XMVectorSet(light2Ambient[0], light2Ambient[1], light2Ambient[2], light2Ambient[3]);
		constantBuffer.Ld2 = XMVectorSet(light2Diffuse[0], light2Diffuse[1], light2Diffuse[2], light2Diffuse[3]);
		constantBuffer.Ls2 = XMVectorSet(light2Specular[0], light2Specular[1], light2Specular[2], light2Specular[3]);

		constantBuffer.Light0Position = XMVectorSet(light0Position[0], light0Position[1], light0Position[2], light0Position[3]);
		constantBuffer.Light1Position = XMVectorSet(light1Position[0], light1Position[1], light1Position[2], light1Position[3]);
		constantBuffer.Light2Position = XMVectorSet(light2Position[0], light2Position[1], light2Position[2], light2Position[3]);
		constantBuffer.LKeyPressed = 1;

		if (Xkey == true)
		{
			//Rotate RED light to Y-direction
			light0Position[0] = red_x;
			light0Position[1] = red_y;
			light0Position[2] = red_z;
		}

		if (Ykey == true)
		{
			//Rotate Blue light to Z-direction
			light0Position[0] = blue_x;
			light0Position[1] = blue_y;
			light0Position[2] = blue_z;
		}

		if (Zkey == true)
		{
			//Rotate GREEN light to X-direction
			light0Position[0] = green_x;
			light0Position[1] = green_y;
			light0Position[2] = green_z;
		}
	}
	else
	{
		constantBuffer.LKeyPressed = 0;
	}

	//translation is concerned with world matrix transformation
	XMMATRIX worldMatrix = XMMatrixIdentity();
	XMMATRIX viewMatrix = XMMatrixIdentity();
	XMMATRIX translateMatrix = XMMatrixIdentity();
	XMMATRIX rotationMatrixX = XMMatrixIdentity();
	XMMATRIX rotationMatrixY = XMMatrixIdentity();
	XMMATRIX rotationMatrixZ = XMMatrixIdentity();
	XMMATRIX rotationMatrixFinal = XMMatrixIdentity();
	
	// translate matrix
	translateMatrix = XMMatrixTranslation(0.0f, 0.0f, 2.0f);

	//Rotate Matrix
	//rotationMatrixY = XMMatrixRotationY(XMConvertToRadians(anglePyramid));
	rotationMatrixFinal = rotationMatrixX * rotationMatrixY * rotationMatrixZ;

	// Apply rotation and translation , * Order is important
	worldMatrix = rotationMatrixFinal * translateMatrix;

	//Load the data into the constant buffer
	constantBuffer.WorldMatrix = worldMatrix;
	constantBuffer.ViewMatrix = viewMatrix;
	constantBuffer.RotationMatrix = rotationMatrixFinal;
	constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix;
	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);

	//Draw vertex buffer to render target
	//gpID3D11DeviceContext->Draw(3, 0);
	//gpID3D11DeviceContext->Draw(3, 3);
	//gpID3D11DeviceContext->Draw(3, 6);
	//gpID3D11DeviceContext->Draw(3, 9);

	//1st Sphere
	D3D11_VIEWPORT d3dViewPort;
	d3dViewPort.TopLeftX = 0;
	d3dViewPort.TopLeftY = 0;
	d3dViewPort.Width = (float)width1 / 4;
	d3dViewPort.Height = (float)height1 / 6;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort);

	constantBuffer.Ka = XMVectorSet(material1_ambient[0], material1_ambient[1], material1_ambient[2], material1_ambient[3]);
	constantBuffer.Kd = XMVectorSet(material1_diffuse[0], material1_diffuse[1], material1_diffuse[2], material1_diffuse[3]);
	constantBuffer.Ks = XMVectorSet(material1_specular[0], material1_specular[1], material1_specular[2], material1_specular[3]);
	constantBuffer.material_shininess = material1_shininess[0];

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);

	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);

	//2nd Sphere
	d3dViewPort.TopLeftX = 0;
	d3dViewPort.TopLeftY = 1 * height1 / 6;
	d3dViewPort.Width = (float)width1 / 4;
	d3dViewPort.Height = (float)height1 / 6;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort);

	constantBuffer.Ka = XMVectorSet(material2_ambient[0], material2_ambient[1], material2_ambient[2], material2_ambient[3]);
	constantBuffer.Kd = XMVectorSet(material2_diffuse[0], material2_diffuse[1], material2_diffuse[2], material2_diffuse[3]);
	constantBuffer.Ks = XMVectorSet(material2_specular[0], material2_specular[1], material2_specular[2], material2_specular[3]);
	constantBuffer.material_shininess = material2_shininess[0];

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);

	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);

	//3rd Sphere
	d3dViewPort.TopLeftX = 0;
	d3dViewPort.TopLeftY = 2 * height1 / 6;
	d3dViewPort.Width = (float)width1 / 4;
	d3dViewPort.Height = (float)height1 / 6;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort);

	constantBuffer.Ka = XMVectorSet(material3_ambient[0], material3_ambient[1], material3_ambient[2], material3_ambient[3]);
	constantBuffer.Kd = XMVectorSet(material3_diffuse[0], material3_diffuse[1], material3_diffuse[2], material3_diffuse[3]);
	constantBuffer.Ks = XMVectorSet(material3_specular[0], material3_specular[1], material3_specular[2], material3_specular[3]);
	constantBuffer.material_shininess = material3_shininess[0];

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);

	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);

	//4th Sphere
	d3dViewPort.TopLeftX = 0;
	d3dViewPort.TopLeftY = 3 * height1 / 6;
	d3dViewPort.Width = (float)width1 / 4;
	d3dViewPort.Height = (float)height1 / 6;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort);

	constantBuffer.Ka = XMVectorSet(material4_ambient[0], material4_ambient[1], material4_ambient[2], material4_ambient[3]);
	constantBuffer.Kd = XMVectorSet(material4_diffuse[0], material4_diffuse[1], material4_diffuse[2], material4_diffuse[3]);
	constantBuffer.Ks = XMVectorSet(material4_specular[0], material4_specular[1], material4_specular[2], material4_specular[3]);
	constantBuffer.material_shininess = material4_shininess[0];

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);

	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);

	//5th Sphere
	d3dViewPort.TopLeftX = 0;
	d3dViewPort.TopLeftY = 4 * height1 / 6;
	d3dViewPort.Width = (float)width1 / 4;
	d3dViewPort.Height = (float)height1 / 6;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort);

	constantBuffer.Ka = XMVectorSet(material5_ambient[0], material5_ambient[1], material5_ambient[2], material5_ambient[3]);
	constantBuffer.Kd = XMVectorSet(material5_diffuse[0], material5_diffuse[1], material5_diffuse[2], material5_diffuse[3]);
	constantBuffer.Ks = XMVectorSet(material5_specular[0], material5_specular[1], material5_specular[2], material5_specular[3]);
	constantBuffer.material_shininess = material5_shininess[0];

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);

	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);

	//6th Sphere
	d3dViewPort.TopLeftX = 0;
	d3dViewPort.TopLeftY = 5 * height1 / 6;
	d3dViewPort.Width = (float)width1 / 4;
	d3dViewPort.Height = (float)height1 / 6;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort);

	constantBuffer.Ka = XMVectorSet(material6_ambient[0], material6_ambient[1], material6_ambient[2], material6_ambient[3]);
	constantBuffer.Kd = XMVectorSet(material6_diffuse[0], material6_diffuse[1], material6_diffuse[2], material6_diffuse[3]);
	constantBuffer.Ks = XMVectorSet(material6_specular[0], material6_specular[1], material6_specular[2], material6_specular[3]);
	constantBuffer.material_shininess = material6_shininess[0];

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);

	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);

	//7th Sphere
	d3dViewPort.TopLeftX = 1 * width1 / 4;;
	d3dViewPort.TopLeftY = 0 * height1 / 6;
	d3dViewPort.Width = (float)width1 / 4;
	d3dViewPort.Height = (float)height1 / 6;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort);

	constantBuffer.Ka = XMVectorSet(material7_ambient[0], material7_ambient[1], material7_ambient[2], material7_ambient[3]);
	constantBuffer.Kd = XMVectorSet(material7_diffuse[0], material7_diffuse[1], material7_diffuse[2], material7_diffuse[3]);
	constantBuffer.Ks = XMVectorSet(material7_specular[0], material7_specular[1], material7_specular[2], material7_specular[3]);
	constantBuffer.material_shininess = material7_shininess[0];

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);

	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);

	//8th Sphere
	d3dViewPort.TopLeftX = 1 * width1 / 4;;
	d3dViewPort.TopLeftY = 1 * height1 / 6;
	d3dViewPort.Width = (float)width1 / 4;
	d3dViewPort.Height = (float)height1 / 6;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort);

	constantBuffer.Ka = XMVectorSet(material8_ambient[0], material8_ambient[1], material8_ambient[2], material8_ambient[3]);
	constantBuffer.Kd = XMVectorSet(material8_diffuse[0], material8_diffuse[1], material8_diffuse[2], material8_diffuse[3]);
	constantBuffer.Ks = XMVectorSet(material8_specular[0], material8_specular[1], material8_specular[2], material8_specular[3]);
	constantBuffer.material_shininess = material8_shininess[0];

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);

	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);

	//9th Sphere
	d3dViewPort.TopLeftX = 1 * width1 / 4;;
	d3dViewPort.TopLeftY = 2 * height1 / 6;
	d3dViewPort.Width = (float)width1 / 4;
	d3dViewPort.Height = (float)height1 / 6;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort);

	constantBuffer.Ka = XMVectorSet(material9_ambient[0], material9_ambient[1], material9_ambient[2], material9_ambient[3]);
	constantBuffer.Kd = XMVectorSet(material9_diffuse[0], material9_diffuse[1], material9_diffuse[2], material9_diffuse[3]);
	constantBuffer.Ks = XMVectorSet(material9_specular[0], material9_specular[1], material9_specular[2], material9_specular[3]);
	constantBuffer.material_shininess = material9_shininess[0];

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);

	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);

	//10th Sphere
	d3dViewPort.TopLeftX = 1 * width1 / 4;;
	d3dViewPort.TopLeftY = 3 * height1 / 6;
	d3dViewPort.Width = (float)width1 / 4;
	d3dViewPort.Height = (float)height1 / 6;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort);

	constantBuffer.Ka = XMVectorSet(material10_ambient[0], material10_ambient[1], material10_ambient[2], material10_ambient[3]);
	constantBuffer.Kd = XMVectorSet(material10_diffuse[0], material10_diffuse[1], material10_diffuse[2], material10_diffuse[3]);
	constantBuffer.Ks = XMVectorSet(material10_specular[0], material10_specular[1], material10_specular[2], material10_specular[3]);
	constantBuffer.material_shininess = material10_shininess[0];

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);

	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);

	//11th Sphere
	d3dViewPort.TopLeftX = 1 * width1 / 4;;
	d3dViewPort.TopLeftY = 4 * height1 / 6;
	d3dViewPort.Width = (float)width1 / 4;
	d3dViewPort.Height = (float)height1 / 6;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort);

	constantBuffer.Ka = XMVectorSet(material11_ambient[0], material11_ambient[1], material11_ambient[2], material11_ambient[3]);
	constantBuffer.Kd = XMVectorSet(material11_diffuse[0], material11_diffuse[1], material11_diffuse[2], material11_diffuse[3]);
	constantBuffer.Ks = XMVectorSet(material11_specular[0], material11_specular[1], material11_specular[2], material11_specular[3]);
	constantBuffer.material_shininess = material11_shininess[0];

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);

	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);

	//12th Sphere
	d3dViewPort.TopLeftX = 1 * width1 / 4;;
	d3dViewPort.TopLeftY = 5 * height1 / 6;
	d3dViewPort.Width = (float)width1 / 4;
	d3dViewPort.Height = (float)height1 / 6;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort);

	constantBuffer.Ka = XMVectorSet(material12_ambient[0], material12_ambient[1], material12_ambient[2], material12_ambient[3]);
	constantBuffer.Kd = XMVectorSet(material12_diffuse[0], material12_diffuse[1], material12_diffuse[2], material12_diffuse[3]);
	constantBuffer.Ks = XMVectorSet(material12_specular[0], material12_specular[1], material12_specular[2], material12_specular[3]);
	constantBuffer.material_shininess = material12_shininess[0];

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);

	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);

	//13th Sphere
	d3dViewPort.TopLeftX = 2 * width1 / 4;;
	d3dViewPort.TopLeftY = 0 * height1 / 6;
	d3dViewPort.Width = (float)width1 / 4;
	d3dViewPort.Height = (float)height1 / 6;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort);

	constantBuffer.Ka = XMVectorSet(material13_ambient[0], material13_ambient[1], material13_ambient[2], material13_ambient[3]);
	constantBuffer.Kd = XMVectorSet(material13_diffuse[0], material13_diffuse[1], material13_diffuse[2], material13_diffuse[3]);
	constantBuffer.Ks = XMVectorSet(material13_specular[0], material13_specular[1], material13_specular[2], material13_specular[3]);
	constantBuffer.material_shininess = material13_shininess[0];

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);

	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);

	//14th Sphere
	d3dViewPort.TopLeftX = 2 * width1 / 4;;
	d3dViewPort.TopLeftY = 1 * height1 / 6;
	d3dViewPort.Width = (float)width1 / 4;
	d3dViewPort.Height = (float)height1 / 6;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort);

	constantBuffer.Ka = XMVectorSet(material14_ambient[0], material14_ambient[1], material14_ambient[2], material14_ambient[3]);
	constantBuffer.Kd = XMVectorSet(material14_diffuse[0], material14_diffuse[1], material14_diffuse[2], material14_diffuse[3]);
	constantBuffer.Ks = XMVectorSet(material14_specular[0], material14_specular[1], material14_specular[2], material14_specular[3]);
	constantBuffer.material_shininess = material14_shininess[0];

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);

	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);


	//15th Sphere
	d3dViewPort.TopLeftX = 2 * width1 / 4;;
	d3dViewPort.TopLeftY = 2 * height1 / 6;
	d3dViewPort.Width = (float)width1 / 4;
	d3dViewPort.Height = (float)height1 / 6;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort);

	constantBuffer.Ka = XMVectorSet(material15_ambient[0], material15_ambient[1], material15_ambient[2], material15_ambient[3]);
	constantBuffer.Kd = XMVectorSet(material15_diffuse[0], material15_diffuse[1], material15_diffuse[2], material15_diffuse[3]);
	constantBuffer.Ks = XMVectorSet(material15_specular[0], material15_specular[1], material15_specular[2], material15_specular[3]);
	constantBuffer.material_shininess = material15_shininess[0];

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);

	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);

	//16th Sphere
	d3dViewPort.TopLeftX = 2 * width1 / 4;;
	d3dViewPort.TopLeftY = 3 * height1 / 6;
	d3dViewPort.Width = (float)width1 / 4;
	d3dViewPort.Height = (float)height1 / 6;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort);

	constantBuffer.Ka = XMVectorSet(material16_ambient[0], material16_ambient[1], material16_ambient[2], material16_ambient[3]);
	constantBuffer.Kd = XMVectorSet(material16_diffuse[0], material16_diffuse[1], material16_diffuse[2], material16_diffuse[3]);
	constantBuffer.Ks = XMVectorSet(material16_specular[0], material16_specular[1], material16_specular[2], material16_specular[3]);
	constantBuffer.material_shininess = material16_shininess[0];

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);

	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);

	//17th Sphere
	d3dViewPort.TopLeftX = 2 * width1 / 4;;
	d3dViewPort.TopLeftY = 4 * height1 / 6;
	d3dViewPort.Width = (float)width1 / 4;
	d3dViewPort.Height = (float)height1 / 6;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort);

	constantBuffer.Ka = XMVectorSet(material17_ambient[0], material17_ambient[1], material17_ambient[2], material17_ambient[3]);
	constantBuffer.Kd = XMVectorSet(material17_diffuse[0], material17_diffuse[1], material17_diffuse[2], material17_diffuse[3]);
	constantBuffer.Ks = XMVectorSet(material17_specular[0], material17_specular[1], material17_specular[2], material17_specular[3]);
	constantBuffer.material_shininess = material17_shininess[0];

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);

	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);

	//18th Sphere
	d3dViewPort.TopLeftX = 2 * width1 / 4;;
	d3dViewPort.TopLeftY = 5 * height1 / 6;
	d3dViewPort.Width = (float)width1 / 4;
	d3dViewPort.Height = (float)height1 / 6;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort);

	constantBuffer.Ka = XMVectorSet(material18_ambient[0], material18_ambient[1], material18_ambient[2], material18_ambient[3]);
	constantBuffer.Kd = XMVectorSet(material18_diffuse[0], material18_diffuse[1], material18_diffuse[2], material18_diffuse[3]);
	constantBuffer.Ks = XMVectorSet(material18_specular[0], material18_specular[1], material18_specular[2], material18_specular[3]);
	constantBuffer.material_shininess = material18_shininess[0];

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);

	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);

	//19th Sphere
	d3dViewPort.TopLeftX = 3 * width1 / 4;;
	d3dViewPort.TopLeftY = 0 * height1 / 6;
	d3dViewPort.Width = (float)width1 / 4;
	d3dViewPort.Height = (float)height1 / 6;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort);

	constantBuffer.Ka = XMVectorSet(material19_ambient[0], material19_ambient[1], material19_ambient[2], material19_ambient[3]);
	constantBuffer.Kd = XMVectorSet(material19_diffuse[0], material19_diffuse[1], material19_diffuse[2], material19_diffuse[3]);
	constantBuffer.Ks = XMVectorSet(material19_specular[0], material19_specular[1], material19_specular[2], material19_specular[3]);
	constantBuffer.material_shininess = material19_shininess[0];

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);

	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);

	//20th Sphere
	d3dViewPort.TopLeftX = 3 * width1 / 4;;
	d3dViewPort.TopLeftY = 1 * height1 / 6;
	d3dViewPort.Width = (float)width1 / 4;
	d3dViewPort.Height = (float)height1 / 6;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort);

	constantBuffer.Ka = XMVectorSet(material20_ambient[0], material20_ambient[1], material20_ambient[2], material20_ambient[3]);
	constantBuffer.Kd = XMVectorSet(material20_diffuse[0], material20_diffuse[1], material20_diffuse[2], material20_diffuse[3]);
	constantBuffer.Ks = XMVectorSet(material20_specular[0], material20_specular[1], material20_specular[2], material20_specular[3]);
	constantBuffer.material_shininess = material20_shininess[0];

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);

	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);


	//21th Sphere
	d3dViewPort.TopLeftX = 3 * width1 / 4;;
	d3dViewPort.TopLeftY = 2 * height1 / 6;
	d3dViewPort.Width = (float)width1 / 4;
	d3dViewPort.Height = (float)height1 / 6;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort);

	constantBuffer.Ka = XMVectorSet(material21_ambient[0], material21_ambient[1], material21_ambient[2], material21_ambient[3]);
	constantBuffer.Kd = XMVectorSet(material21_diffuse[0], material21_diffuse[1], material21_diffuse[2], material21_diffuse[3]);
	constantBuffer.Ks = XMVectorSet(material21_specular[0], material21_specular[1], material21_specular[2], material21_specular[3]);
	constantBuffer.material_shininess = material21_shininess[0];

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);

	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);

	//22th Sphere
	d3dViewPort.TopLeftX = 3 * width1 / 4;;
	d3dViewPort.TopLeftY = 3 * height1 / 6;
	d3dViewPort.Width = (float)width1 / 4;
	d3dViewPort.Height = (float)height1 / 6;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort);

	constantBuffer.Ka = XMVectorSet(material22_ambient[0], material22_ambient[1], material22_ambient[2], material22_ambient[3]);
	constantBuffer.Kd = XMVectorSet(material22_diffuse[0], material22_diffuse[1], material22_diffuse[2], material22_diffuse[3]);
	constantBuffer.Ks = XMVectorSet(material22_specular[0], material22_specular[1], material22_specular[2], material22_specular[3]);
	constantBuffer.material_shininess = material22_shininess[0];

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);

	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);

	//23th Sphere
	d3dViewPort.TopLeftX = 3 * width1 / 4;;
	d3dViewPort.TopLeftY = 4 * height1 / 6;
	d3dViewPort.Width = (float)width1 / 4;
	d3dViewPort.Height = (float)height1 / 6;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort);

	constantBuffer.Ka = XMVectorSet(material23_ambient[0], material23_ambient[1], material23_ambient[2], material23_ambient[3]);
	constantBuffer.Kd = XMVectorSet(material23_diffuse[0], material23_diffuse[1], material23_diffuse[2], material23_diffuse[3]);
	constantBuffer.Ks = XMVectorSet(material23_specular[0], material23_specular[1], material23_specular[2], material23_specular[3]);
	constantBuffer.material_shininess = material23_shininess[0];

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);

	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);

	//24th Sphere
	d3dViewPort.TopLeftX = 3 * width1 / 4;;
	d3dViewPort.TopLeftY = 5 * height1 / 6;
	d3dViewPort.Width = (float)width1 / 4;
	d3dViewPort.Height = (float)height1 / 6;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort);

	constantBuffer.Ka = XMVectorSet(material24_ambient[0], material24_ambient[1], material24_ambient[2], material24_ambient[3]);
	constantBuffer.Kd = XMVectorSet(material24_diffuse[0], material24_diffuse[1], material24_diffuse[2], material24_diffuse[3]);
	constantBuffer.Ks = XMVectorSet(material24_specular[0], material24_specular[1], material24_specular[2], material24_specular[3]);
	constantBuffer.material_shininess = material24_shininess[0];

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);

	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);
	// Switch between front and back buffer
	gpIDXGISwapChain->Present(0, 0);

	
}


void uninitialize(void)
{
	//code

	if (gpID3D11Buffer_ConstantBuffer)
	{
		gpID3D11Buffer_ConstantBuffer->Release();
		gpID3D11Buffer_ConstantBuffer = NULL;
	}

	if (gpID3D11InputLayout)
	{
		gpID3D11InputLayout->Release();
		gpID3D11InputLayout = NULL;
	}

	if (gpID3D11Buffer_VertexBuffer_Position)
	{
		gpID3D11Buffer_VertexBuffer_Position->Release();
		gpID3D11Buffer_VertexBuffer_Position = NULL;
	}

	if (gpID3D11Buffer_VertexBuffer_Normal)
	{
		gpID3D11Buffer_VertexBuffer_Normal->Release();
		gpID3D11Buffer_VertexBuffer_Normal = NULL;
	}

	if (gpID3D11Buffer_IndexBuffer)
	{
		gpID3D11Buffer_IndexBuffer->Release();
		gpID3D11Buffer_IndexBuffer = NULL;
	}


	if (gpID3D11RasterizerState)
	{
		gpID3D11RasterizerState->Release();
		gpID3D11RasterizerState = NULL;
	}

	if (gpID3D11PixelShader)
	{
		gpID3D11PixelShader->Release();
		gpID3D11PixelShader = NULL;
	}

	if (gpID3D11VertexShader)
	{
		gpID3D11VertexShader->Release();
		gpID3D11VertexShader = NULL;
	}

	if (gpID3D11DepthStencilView)
	{
		gpID3D11DepthStencilView->Release();
		gpID3D11DepthStencilView = NULL;
	}

	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	if (gpIDXGISwapChain)
	{
		gpIDXGISwapChain->Release();
		gpIDXGISwapChain = NULL;
	}

	if (gpID3D11DeviceContext)
	{
		gpID3D11DeviceContext->Release();
		gpID3D11DeviceContext = NULL;
	}

	if (gpID3D11Device)
	{
		gpID3D11Device->Release();
		gpID3D11Device = NULL;
	}



	if (gpFile)
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "uninitialize() succeeded.\n");
		fprintf_s(gpFile, "Log File  is successfully closed.\n");
		fclose(gpFile);
	}
}

void update(void)
{
	// Circle equalation  Circle_X= Xcenter + rcosQ , Circle_X = Ycenter + rsinQ, Q = 0 to 360

	angleCircle = angleCircle - 0.001f;
	if (angleCircle >= 360.0f)
		angleCircle = 0.0f;

	red_x = Xcenter + 100.0f * cos(angleCircle);
	red_y = Ycenter;
	red_z = Zcenter + 100.0f * sin(angleCircle);

	green_x = Xcenter + 100.0f * sin(angleCircle);
	green_y = Ycenter + 100.0f * cos(angleCircle);
	green_z = Zcenter;

	blue_x = Xcenter;
	blue_y = Ycenter + 100.0f * sin(angleCircle);
	blue_z = Zcenter + 100.0f * cos(angleCircle);

}
