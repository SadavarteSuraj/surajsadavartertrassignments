//
//  main.m
//  Suraj_13_iOS_TweakSmiley
//
//  Created by user139842 on 8/7/18.
//  Copyright © 2018 user139842. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
