//
//  GLESView.m
//  Suraj_04_TrianglePerspective
//
//  Created by user139842 on 7/30/18.
//  Copyright © 2018 user139842. All rights reserved.
//

#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "vmath.h"
#import "GLESView.h"

enum
{
    VDG_ATTRIBUTE_VERTEX = 0,
    VDG_ATTRIBUTE_COLOR,
    VDG_ATTRIBUTE_NORMAL,
    VDG_ATTRIBUTE_TEXTURE0,
};
@implementation GLESView
{
    EAGLContext *eaglContext;
    
    GLuint defaultFramebuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
    GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;
    
    GLuint gVaoTriangle;
    GLuint gVaoSquare;
    GLuint gVbo_Position;
    GLuint gVbo_Color;
    GLuint gMVPUniform;
    
    vmath::mat4 gPerspectiveProjectionMatrix;

	GLfloat angleTri;
	GLfloat angleRect;

}
-(id)initWithFrame:(CGRect)frame;
{
    self = [super initWithFrame:frame];
    if(self)
    {
        CAEAGLLayer *eaglLayer = (CAEAGLLayer *)super.layer;
        eaglLayer.opaque = YES;
        eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],
                                        kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil];
        
        eaglContext = [[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext == nil)
        {
            [self release];
            return(nil);
        }
        
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenFramebuffers(1, &defaultFramebuffer);
        glGenRenderbuffers(1, &colorRenderbuffer);
        glBindFramebuffer(GL_FRAMEBUFFER,  defaultFramebuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER,  GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER,  GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        glGenRenderbuffers(1, &depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Failed to create complete Framebuffer object %x\n", glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1, &defaultFramebuffer);
            glDeleteRenderbuffers(1, &colorRenderbuffer);
            glDeleteRenderbuffers(1, &depthRenderbuffer);
            
            return(nil);
            
        }
        printf("Renderer : %s | GL version : %s | GLSL version : %s\n", glGetString(GL_RENDERER), glGetString(GL_VERSION), glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        //hard coded initializations
        isAnimating = NO;
        animationFrameInterval = 60; // default since iOS 8.2
        
        //*** Vertex Shader ***
        // Create Shader
        gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        
        //Provide source code to shader
        const GLchar *vertexShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "in vec4 vPosition;" \
        "in vec4 vColor;" \
        "out vec4 out_Color;" \
        "uniform mat4 u_mvp_matrix;" \
        "void main(void)" \
       "{"\
        "gl_Position  = u_mvp_matrix * vPosition;" \
        "out_Color = vColor;" \
        "}";
        
        glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
        
        // compile shader
        glCompileShader(gVertexShaderObject);
        GLint iInfoLogLength = 0;
        GLint iShaderCompiledStatus = 0;
        char *szInfoLog = NULL;
        glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if (iShaderCompiledStatus == GL_FALSE)
        {
            char *szInfoLog = NULL;
            glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("Vertex Shader Compilation Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self  release];
                    
                }
            }
        }
        
        //*** Fragment Shader ***
        //re-initialize
        iInfoLogLength = 0;
        iShaderCompiledStatus = 0;
        szInfoLog = NULL;
        
        // Create Shader
        gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        
        //Provide source code to shader
        const GLchar *fragmentShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "precision highp float;" \
		"in vec4 out_Color;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"FragColor = out_Color;" \
		"}";
        
        glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
        
        // compile shader
        glCompileShader(gFragmentShaderObject);
        glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if (iShaderCompiledStatus == GL_FALSE)
        {
            char *szInfoLog = NULL;
            glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("Fragment Shader Compilation Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self  release];
                    
                }
            }
        }
        
        //Shader program
        //create
        gShaderProgramObject = glCreateProgram();
        
        //attach vertex shader to shader program
        glAttachShader(gShaderProgramObject, gVertexShaderObject);
        
        //attach fragment shader to shader program
        glAttachShader(gShaderProgramObject, gFragmentShaderObject);
        
        //Pre-link binding of shader program object with vertex shader position attribute
        glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");
        
        
        //Pre-link binding of shader program object with vertex shader color attribute
        glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_COLOR, "vColor");
        
        //Link shader
        glLinkProgram(gShaderProgramObject);
        GLint iShaderProgramLinkStatus = 0;
        glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
        if (iShaderProgramLinkStatus == GL_FALSE)
        {
            
            glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
                    printf("Shader Program Link Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self  release];
                }
            }
        }
        
        //get MVP uniform location
        gMVPUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");
        
        // *** vertices, Colors, Shader Attribs, vbo, vao initializations
    const GLfloat triangleVertices[] =
    {
        0.0f, 1.0f, 0.0f, //apex
        -1.0f, -1.0f, 0.0f, // left bottom
        1.0f, -1.0f, 0.0f // right bottom
    };
    
    const GLfloat triangleColor[] =
    {
        1.0f, 0.0f, 0.0f, //Red
        0.0f, 1.0f, 0.0f, //Green
        0.0f, 0.0f, 1.0f, //Blue
    };
    
    const GLfloat SquareVertices[] =
    {
        1.0f, 1.0f, 0.0f,
        -1.0f, 1.0f, 0.0f,
        -1.0f, -1.0f, 0.0f,
        1.0f, -1.0f, 0.0f,
    };
    
    //Vao Triangle
    glGenVertexArrays(1, &gVaoTriangle);  // recording of casatte
    glBindVertexArray(gVaoTriangle);
    
    glGenBuffers(1, &gVbo_Position);
    glBindBuffer(GL_ARRAY_BUFFER, gVbo_Position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(triangleVertices), triangleVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
    glBindBuffer(GL_ARRAY_BUFFER, 0); // passing zero is unbinding
    
    glGenBuffers(1, &gVbo_Color);
    glBindBuffer(GL_ARRAY_BUFFER, gVbo_Color);
    glBufferData(GL_ARRAY_BUFFER, sizeof(triangleColor), triangleColor, GL_STATIC_DRAW);
    glVertexAttribPointer(VDG_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(VDG_ATTRIBUTE_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0); // passing zero is unbinding
    
    glBindVertexArray(0);
    
    //Vao Square
    glGenVertexArrays(1, &gVaoSquare);  // recording of casatte
    glBindVertexArray(gVaoSquare);
    
    glGenBuffers(1, &gVbo_Position);
    glBindBuffer(GL_ARRAY_BUFFER, gVbo_Position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(SquareVertices), SquareVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
    glBindBuffer(GL_ARRAY_BUFFER, 0); // passing zero is unbinding
    glVertexAttrib3f(VDG_ATTRIBUTE_COLOR, 0.39f, 0.53f, 0.96f); // Corn flower color
    glBindVertexArray(0);
        
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
       // glEnable(GL_CULL_FACE);
        
        //set background  color
        glClearColor(0.0f,0.0f, 0.0f, 0.0f); //blue
        
        //set perpective matrix to identity matrix
        gPerspectiveProjectionMatrix = vmath::mat4::identity();
        
        
        
        //GESTURE RECOGNITION
        //Tap gesture here
        UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1]; //touch of one  finger
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1]; //touch of one finger
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        //this will allow to differentiate between single tap and double tap
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        //swipe gesture
        UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        //long press gesture
        UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [self addGestureRecognizer:longPressGestureRecognizer];
        
        
        
    }
    return(self);
    
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 */
- (void)drawRect:(CGRect)rect
{
    //Drawing code
}

+(Class)layerClass
{
    //code
    return([CAEAGLLayer class]);
}

-(void)drawView:(id)sender
{
    //code
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
    
    // Start using OpenGL program object
    glUseProgram(gShaderProgramObject);
    
    // OpenGL Drawing
	//Set model view and modelviewprojection matrices to identity
	vmath::mat4 modelViewMatrix = vmath::mat4::identity();
	vmath::mat4 modelViewProjectionMatrix =vmath::mat4::identity();
    
    //Declare Rotation Matrix to identity
    vmath::mat4 RotationMatrix = vmath::mat4::identity();
    
    //Translate model view matrix
    modelViewMatrix = vmath::translate(-1.5f, 0.0f, -6.0f);
    
    //Rotation matrix for triangle
    RotationMatrix = vmath::rotate(angleTri, 0.0f, 1.0f, 0.0f);
    
    //Multiply model view matrix and Rotation Matrix
    modelViewMatrix = modelViewMatrix * RotationMatrix;
    
    //multiple the modelview and perspective matrix to get modelview projection matrix
    modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix; // ORDER IS IMPORTANT
    
    
    //pass above modelViewProjectionMatrix to the vertexshader in u_mvp_matrix shader variable
    // whose position value we already calculated in initWithFrame() by using glGetUniformLocation()
    glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    
    //*** bind vao ***
    glBindVertexArray(gVaoTriangle);
    
    // *** draw eigther by glDrawTriangles() or glDrawArrays() or glDrawElements()
    glDrawArrays(GL_TRIANGLES, 0, 3); // 3 (each with its x y z) vertices in triangleVertices array
    
    //*** unbind vao ***
    glBindVertexArray(0);
    
    //****Square drawing*******
    
    //Set model view and modelviewprojection matrices to identity
    modelViewMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();
    RotationMatrix = vmath::mat4::identity();

    //Translate model view matrix
    modelViewMatrix = vmath::translate(1.5f, 0.0f, -6.0f);
    
    //Rotation matrix for Square
    RotationMatrix = vmath::rotate(angleRect, 1.0f, 0.0f, 0.0f); //it is giving sytax error if we declare them as interger
    
    //Multiply model view matrix and Rotation Matrix
    modelViewMatrix = modelViewMatrix * RotationMatrix;
    
    //multiple the modelview and perspective matrix to get modelview projection matrix
    modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix; // ORDER IS IMPORTANT
    
    
    //pass above modelViewProjectionMatrix to the vertexshader in u_mvp_matrix shader variable
    // whose position value we already calculated in initWithFrame() by using glGetUniformLocation()
    glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);
    
    //*** bind vao ***
    glBindVertexArray(gVaoSquare);
    
    // *** draw eigther by glDrawTriangles() or glDrawArrays() or glDrawElements()
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    
    //*** unbind vao ***
    glBindVertexArray(0);

	angleTri = angleTri + 0.8f;
    if (angleTri >= 360.0f)
        angleTri = 0.0f;
    
    angleRect = angleRect - 0.8f;
    if (angleRect <= -360.0f)
        angleRect = 0.0f;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
}

-(void)layoutSubviews
{
    //code
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER,  GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER,  GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
    
    glViewport(0, 0, width, height);
    // GLfloat fwidth = (GLfloat)width;
    // GLfloat fheight = (GLfloat)height;
    
    //glOrtho(left, right, bottom, top,  near, far)
    
    //Perspective projection matrix
    gPerspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
    
    /*
     if (width <= height)
     gOrthographicProjectionMatrix = vmath::ortho((-100.0f * (fwidth / fheight)), (100.0f * (fwidth / fheight)), -100.0f, 100.0f, -100.0f, 100.0f);
     else
     gOrthographicProjectionMatrix = vmath::ortho(-100.0f, 100.0f, (-100.0f * (width / height)), (100.0f * (width / height)),
     -100.0f, 100.0f);
     */
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!= GL_FRAMEBUFFER_COMPLETE)
    {
        printf("Failed to create complete framebuffer object %x", glCheckFramebufferStatus(GL_FRAMEBUFFER));
        
    }
    
    [self drawView:nil];
}

-(void)startAnimation
{
    if(!isAnimating)
    {
        displayLink = [NSClassFromString(@"CADisplayLink")displayLinkWithTarget:self selector:@selector(drawView:)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        isAnimating=YES;
    }
}

-(void)stopAnimation
{
    if(isAnimating)
    {
        [displayLink invalidate];
        displayLink = nil;
        
        isAnimating = NO;
        
    }
    
}

// to become first responder
-(BOOL)acceptsFirstResponder
{
    //code
    return(YES);
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}

-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    
}

-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    
}

-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    //code
    [self release];
    exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    
}
- (void)dealloc
{
    // Destroy vao
    if (gVaoTriangle)
    {
        glDeleteVertexArrays(1, &gVaoTriangle);
        gVaoTriangle = 0;
    }
    
    if (gVaoSquare)
    {
        glDeleteVertexArrays(1, &gVaoSquare);
        gVaoSquare = 0;
    }
    
    
    // Destroy vbo_position
    if (gVbo_Position)
    {
        glDeleteVertexArrays(1, &gVbo_Position);
        gVbo_Position = 0;
    }
    
    // Destroy vbo_Color
    if (gVbo_Color)
    {
        glDeleteVertexArrays(1, &gVbo_Color);
        gVbo_Color = 0;
    }

    
    //Detach vertex shader from shader program object
    glDetachShader(gShaderProgramObject, gVertexShaderObject);
    
    //Detach fragment shader from shader program object
    glDetachShader(gShaderProgramObject, gFragmentShaderObject);
    
    
    
    // Delete vertex shader object
    glDeleteShader(gVertexShaderObject);
    gVertexShaderObject = 0;
    
    // Delete fragment shader object
    glDeleteShader(gFragmentShaderObject);
    gFragmentShaderObject = 0;
    
    // Delete shader program object
    glDeleteShader(gShaderProgramObject);
    gShaderProgramObject = 0;
    
    
    if(depthRenderbuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderbuffer);
        depthRenderbuffer =  0;
    }
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderbuffer);
        colorRenderbuffer =  0;
    }
    
    if(defaultFramebuffer)
    {
        glDeleteFramebuffers(1, &defaultFramebuffer);
        defaultFramebuffer =  0;
    }
    
    if([EAGLContext currentContext]==eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    
    [eaglContext release];
    eaglContext =  nil;
    
    [super dealloc];
    
}


@end

