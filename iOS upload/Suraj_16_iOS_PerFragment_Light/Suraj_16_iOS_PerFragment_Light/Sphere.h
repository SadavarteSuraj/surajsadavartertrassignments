//
//  ViewController.h
//  Suraj_15_iOS_PerVertexLight
//
//  Created by user139842 on 8/8/18.
//  Copyright © 2018 user139842. All rights reserved.
//

#include <UIKit/UIKit.h>

@interface Sphere : NSObject
-(void) getSphereVertexData : (float[]) spherePositionCoords : (float[]) sphereNormalCoords : (float[]) sphereTexCoords : (unsigned short[]) sphereElements;
-(int)getNumberOfSphereVertices;
-(int)getNumberOfSphereElements;
@end
