//
//  GLESView.h
//  Suraj_02_BlueScreenWindow
//
//  Created by user139842 on 7/30/18.
//  Copyright © 2018 user139842. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GLESView : UIView <UIGestureRecognizerDelegate>
-(void)startAnimation;
-(void)stopAnimation;

@end
