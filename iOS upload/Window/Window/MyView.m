//
//  MyView.m
//  Window
//
//  Created by user139842 on 5/27/18.
//

#import "MyView.h"

@implementation MyView
{
    NSString *centralText;
}

-(id)initWithFrame:(CGRect)frameRect
{
    self = [super initWithFrame:frameRect];
    if(self)
    {
        //initialization  code  here
        
        //set scene's background color
        [self setBackgroundColor:[UIColor whiteColor]];
        
        centralText =@"Hellow World !!!";
        
        //GESTURE RECOGNITION
        //Tap gesture here
        UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1]; //touch of one  finger
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1]; //touch of one finger
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        //this will allow to differantiate between single tap and double tap
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        //swipe gesture
        UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        //long press gesture
        UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [self addGestureRecognizer:longPressGestureRecognizer];
        
        
                                                    
    }
    return(self);
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
 */
- (void)drawRect:(CGRect)rect
{
    // black background
    UIColor *fillColor = [UIColor blackColor];
    [fillColor set];
    UIRectFill(rect);
    
    NSDictionary *dictionaryForTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                                 [UIFont fontWithName:@"Helvetica" size:24], NSFontAttributeName,
                                                 [UIColor greenColor], NSForegroundColorAttributeName,
                                                 nil];
    
    CGSize textSize = [centralText sizeWithAttributes:dictionaryForTextAttributes];
    CGPoint point;
    point.x = (rect.size.width/2) - (textSize.width/2);
    point.y = (rect.size.height/2) - (textSize.height/2)+12;
    
    [centralText drawAtPoint:point withAttributes:dictionaryForTextAttributes];
    
}

// to become first responder
-(BOOL)acceptFirstResponder
{
    //code
    return(YES);
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    //code
    /* centralText =@"'touchesBegan' event occured";
     [self setNeedsDisplay]; //repainting
     */
}

-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    //code
    centralText =@"'onSingleTap' event occured";
    [self setNeedsDisplay]; //repainting
}

-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    //code
    centralText =@"'onDoubleTap' event occured";
    [self setNeedsDisplay]; //repainting
}

-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    //code
    [self release];
    exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    //code
    centralText =@"'onLongPress' event occured";
    [self setNeedsDisplay]; //repainting
}
- (void)dealloc
{
    //code
    [super dealloc];
    
}


@end













