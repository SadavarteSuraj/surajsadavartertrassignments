//
//  main.m
//  Suraj_24_iOS_24Materials
//
//  Created by user139842 on 8/9/18.
//  Copyright © 2018 user139842. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
