//
//  GLESView.m
//  Suraj_04_TrianglePerspective
//
//  Created by user139842 on 7/30/18.
//  Copyright © 2018 user139842. All rights reserved.
//

#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "vmath.h"
#import "GLESView.h"

#import "Sphere.h"

enum
{
    VDG_ATTRIBUTE_VERTEX = 0,
    VDG_ATTRIBUTE_COLOR,
    VDG_ATTRIBUTE_NORMAL,
    VDG_ATTRIBUTE_TEXTURE0,
};
@implementation GLESView
{
    EAGLContext *eaglContext;
    
    GLuint defaultFramebuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
    GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;
    
    float sphere_vertices[1146];
    float sphere_normals[1146];
    float sphere_textures[764];
    unsigned short sphere_elements[2280];
    
    GLuint gNumVertices;
    GLuint gNumElements;
    
    GLuint gVao_sphere;
    GLuint gVbo_sphere_position;
    GLuint gVbo_sphere_normal;
    GLuint gVbo_sphere_element;
    
    GLuint gMVPUniform;
    
    GLuint model_matrix_uniform, view_matrix_uniform, projection_matrix_uniform, rotation_matrix_uniform;
    
    GLuint L_KeyPressed_uniform;
    
    GLuint La0_uniform;
    GLuint Ld0_uniform;
    GLuint Ls0_uniform;
    GLuint light0_position_uniform;
    
    GLuint La1_uniform;
    GLuint Ld1_uniform;
    GLuint Ls1_uniform;
    GLuint light1_position_uniform;
    
    GLuint La2_uniform;
    GLuint Ld2_uniform;
    GLuint Ls2_uniform;
    GLuint light2_position_uniform;
    
    GLuint Ka_uniform;
    GLuint Kd_uniform;
    GLuint Ks_uniform;
    GLuint material_shininess_uniform;
    
    vmath::mat4 gPerspectiveProjectionMatrix;
    
    GLfloat light0_Ambient[4];
    GLfloat light0_Diffuse[4];
    GLfloat light0_Specular[4];
    GLfloat light0_Position[4];
    
    GLfloat light1_Ambient[4];
    GLfloat light1_Diffuse[4];
    GLfloat light1_Specular[4];
    GLfloat light1_Position[4];
    
    GLfloat light2_Ambient[4];
    GLfloat light2_Diffuse[4];
    GLfloat light2_Specular[4];
    GLfloat light2_Position[4];
    
    GLfloat light_model_ambient[4];
    GLfloat light_model_local_viewer[1];
    
    //First sphere on first column, emerald
    GLfloat material1_ambient[4];
    GLfloat material1_diffuse[4];
    GLfloat material1_specular[4];
    GLfloat material1_shininess[1];
    
    //Second sphere on first column, jade
    GLfloat material2_ambient[4];
    GLfloat material2_diffuse[4];
    GLfloat material2_specular[4];
    GLfloat material2_shininess[1];
    
    //Third sphere on first column, obsidian
    GLfloat material3_ambient[4];
    GLfloat material3_diffuse[4];
    GLfloat material3_specular[4];
    GLfloat material3_shininess[1];
    
    //Fourth sphere on first column, pearl
    GLfloat material4_ambient[4];
    GLfloat material4_diffuse[4];
    GLfloat material4_specular[4];
    GLfloat material4_shininess[1];
    
    GLfloat material5_ambient[4];
    GLfloat material5_diffuse[4];
    GLfloat material5_specular[4];
    GLfloat material5_shininess[1];
    
    GLfloat material6_ambient[4];
    GLfloat material6_diffuse[4];
    GLfloat material6_specular[4];
    GLfloat material6_shininess[1];
    
    GLfloat material7_ambient[4];
    GLfloat material7_diffuse[4];
    GLfloat material7_specular[4];
    GLfloat material7_shininess[1];
    
    
    GLfloat material8_ambient[4];
    GLfloat material8_diffuse[4];
    GLfloat material8_specular[4];
    GLfloat material8_shininess[1];
    
    GLfloat material9_ambient[4];
    GLfloat material9_diffuse[4];
    GLfloat material9_specular[4];
    GLfloat material9_shininess[1];
    
    GLfloat material10_ambient[4];
    GLfloat material10_diffuse[4];
    GLfloat material10_specular[4];
    GLfloat material10_shininess[1];
    
    GLfloat material11_ambient[4];
    GLfloat material11_diffuse[4];
    GLfloat material11_specular[4];
    GLfloat material11_shininess[1];
    
    GLfloat material12_ambient[4];
    GLfloat material12_diffuse[4];
    GLfloat material12_specular[4];
    GLfloat material12_shininess[1];
    
    GLfloat material13_ambient[4];
    GLfloat material13_diffuse[4];
    GLfloat material13_specular[4];
    GLfloat material13_shininess[1];
    
    GLfloat material14_ambient[4];
    GLfloat material14_diffuse[4];
    GLfloat material14_specular[4];
    GLfloat material14_shininess[1];
    
    
    GLfloat material15_ambient[4];
    GLfloat material15_diffuse[4];
    GLfloat material15_specular[4];
    GLfloat material15_shininess[1];
    
    GLfloat material16_ambient[4];
    GLfloat material16_diffuse[4];
    GLfloat material16_specular[4];
    GLfloat material16_shininess[1];
    
    GLfloat material17_ambient[4];
    GLfloat material17_diffuse[4];
    GLfloat material17_specular[4];
    GLfloat material17_shininess[1];
    
    GLfloat material18_ambient[4];
    GLfloat material18_diffuse[4];
    GLfloat material18_specular[4];
    GLfloat material18_shininess[1];
    
    GLfloat material19_ambient[4];
    GLfloat material19_diffuse[4];
    GLfloat material19_specular[4];
    GLfloat material19_shininess[1];
    
    GLfloat material20_ambient[4];
    GLfloat material20_diffuse[4];
    GLfloat material20_specular[4];
    GLfloat material20_shininess[1];
    
    GLfloat material21_ambient[4];
    GLfloat material21_diffuse[4];
    GLfloat material21_specular[4];
    GLfloat material21_shininess[1];
    
    GLfloat material22_ambient[4];
    GLfloat material22_diffuse[4];
    GLfloat material22_specular[4];
    GLfloat material22_shininess[1];
    
    GLfloat material23_ambient[4];
    GLfloat material23_diffuse[4];
    GLfloat material23_specular[4];
    GLfloat material23_shininess[1];
    
    GLfloat material24_ambient[4];
    GLfloat material24_diffuse[4];
    GLfloat material24_specular[4];
    GLfloat material24_shininess[1];
    
    bool gbAnimate;
    bool gbLight;
    
    //variable declaration
    bool bIsAKeyPressed;
    bool bIsLKeyPressed;
    
    GLfloat red_x;
    GLfloat red_y;
    GLfloat red_z;
    
    GLfloat green_x;
    GLfloat green_y;
    GLfloat green_z;
    
    GLfloat blue_x;
    GLfloat blue_y;
    GLfloat blue_z;
    
    GLfloat Xcenter;
    GLfloat Ycenter;
    GLfloat Zcenter;
    
    GLfloat angleCircle;
    
    
    GLfloat anglePyramid;
    GLfloat angleCube;
    
    bool Xkey;
    bool Ykey;
    bool Zkey;
    
    GLfloat width1;
    GLfloat height1;
    
    int singleTap;
    
}
-(id)initWithFrame:(CGRect)frame;
{
    self = [super initWithFrame:frame];
    if(self)
    {
        CAEAGLLayer *eaglLayer = (CAEAGLLayer *)super.layer;
        eaglLayer.opaque = YES;
        eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],
                                        kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil];
        
        eaglContext = [[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext == nil)
        {
            [self release];
            return(nil);
        }
        
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenFramebuffers(1, &defaultFramebuffer);
        glGenRenderbuffers(1, &colorRenderbuffer);
        glBindFramebuffer(GL_FRAMEBUFFER,  defaultFramebuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER,  GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER,  GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        glGenRenderbuffers(1, &depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Failed to create complete Framebuffer object %x\n", glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1, &defaultFramebuffer);
            glDeleteRenderbuffers(1, &colorRenderbuffer);
            glDeleteRenderbuffers(1, &depthRenderbuffer);
            
            return(nil);
            
        }
        printf("Renderer : %s | GL version : %s | GLSL version : %s\n", glGetString(GL_RENDERER), glGetString(GL_VERSION), glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        //hard coded initializations
        isAnimating = NO;
        animationFrameInterval = 60; // default since iOS 8.2
        
        //*** Vertex Shader ***
        // Create Shader
        gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        
        //Provide source code to shader
        const GLchar *vertexShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "in vec4 vPosition;" \
        "in vec3 vNormal;" \
        "uniform mat4 u_model_matrix;" \
        "uniform mat4 u_view_matrix;" \
        "uniform mat4 u_rotation_matrix;" \
        "uniform mat4 u_projection_matrix;" \
        "uniform vec4 u_light0_position;" \
        "uniform vec4 u_light1_position;" \
        "uniform vec4 u_light2_position;" \
        "uniform mediump int u_lighting_enabled;" \
        "out vec3 transformed_normals;" \
        "out vec3 light0_direction;" \
        "out vec3 light1_direction;" \
        "out vec3 light2_direction;" \
        "out vec3 viewer_vector;" \
        "void main(void)" \
        "{" \
        "if (u_lighting_enabled == 1)" \
        "{" \
        "vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;" \
        "transformed_normals =  mat3(u_view_matrix * u_model_matrix * u_rotation_matrix) * vNormal;" \
        "light0_direction = vec3(u_light0_position) - eye_coordinates.xyz;" \
        "light1_direction = vec3(u_light1_position) - eye_coordinates.xyz;" \
        "light2_direction = vec3(u_light2_position) - eye_coordinates.xyz;" \
        "viewer_vector = -eye_coordinates.xyz;" \
        "}" \
        "gl_Position = u_projection_matrix  * u_view_matrix * u_model_matrix * u_rotation_matrix * vPosition;" \
        "}";
        
        glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
        
        // compile shader
        glCompileShader(gVertexShaderObject);
        GLint iInfoLogLength = 0;
        GLint iShaderCompiledStatus = 0;
        char *szInfoLog = NULL;
        glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if (iShaderCompiledStatus == GL_FALSE)
        {
            char *szInfoLog = NULL;
            glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("Vertex Shader Compilation Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self  release];
                    
                }
            }
        }
        
        //*** Fragment Shader ***
        //re-initialize
        iInfoLogLength = 0;
        iShaderCompiledStatus = 0;
        szInfoLog = NULL;
        
        // Create Shader
        gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        
        //Provide source code to shader
        const GLchar *fragmentShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "precision highp float;" \
        "in vec3 transformed_normals; " \
        "in vec3 light0_direction; " \
        "in vec3 light1_direction; " \
        "in vec3 light2_direction; " \
        "in vec3 viewer_vector; " \
        "out vec4 FragColor;" \
        "uniform vec3 u_La0;" \
        "uniform vec3 u_Ld0;" \
        "uniform vec3 u_Ls0;" \
        "uniform vec3 u_La1;" \
        "uniform vec3 u_Ld1;" \
        "uniform vec3 u_Ls1;" \
        "uniform vec3 u_La2;" \
        "uniform vec3 u_Ld2;" \
        "uniform vec3 u_Ls2;" \
        "uniform vec3 u_Ka;" \
        "uniform vec3 u_Kd;" \
        "uniform vec3 u_Ks;" \
        "uniform float u_material_shininess;" \
        "uniform int u_lighting_enabled;" \
        "void main(void)" \
        "{" \
        "vec3 light0;" \
        "vec3 light1;" \
        "vec3 light2;" \
        "vec3 phong_ads_color;" \
        "if (u_lighting_enabled == 1)" \
        "{" \
        "vec3 normalized_transformed_normals = normalize(transformed_normals);" \
        "vec3 normalized_light0_direction = normalize(light0_direction);" \
        "vec3 normalized_light1_direction = normalize(light1_direction);" \
        "vec3 normalized_light2_direction = normalize(light2_direction);" \
        "vec3 normalized_viewer_vector = normalize(viewer_vector);" \
        "float tn_dot_ld0 = max(dot(normalized_transformed_normals, normalized_light0_direction), 0.0);" \
        "vec3 ambient0 = u_La0 * u_Ka;" \
        "vec3 diffuse0 = u_Ld0 * u_Kd * tn_dot_ld0;" \
        "vec3 reflection_vector0 = reflect(-normalized_light0_direction,normalized_transformed_normals);" \
        "vec3 specular0 = u_Ls0 * u_Ks * pow(max(dot(reflection_vector0, normalized_viewer_vector), 0.0), u_material_shininess);" \
        "float tn_dot_ld1 = max(dot(normalized_transformed_normals, normalized_light1_direction), 0.0);" \
        "vec3 ambient1 = u_La1 * u_Ka;" \
        "vec3 diffuse1 = u_Ld1 * u_Kd * tn_dot_ld1;" \
        "vec3 reflection_vector1 = reflect(-normalized_light1_direction,normalized_transformed_normals);" \
        "vec3 specular1 = u_Ls1 * u_Ks * pow(max(dot(reflection_vector1, normalized_viewer_vector), 0.0), u_material_shininess);" \
        "float tn_dot_ld2 = max(dot(normalized_transformed_normals, normalized_light2_direction), 0.0);" \
        "vec3 ambient2 = u_La2 * u_Ka;" \
        "vec3 diffuse2 = u_Ld2 * u_Kd * tn_dot_ld2;" \
        "vec3 reflection_vector2 = reflect(-normalized_light2_direction,normalized_transformed_normals);" \
        "vec3 specular2 = u_Ls2 * u_Ks * pow(max(dot(reflection_vector2, normalized_viewer_vector), 0.0), u_material_shininess);" \
        "light0 = ambient0 + diffuse0 + specular0;" \
        "light1 = ambient1 + diffuse1 + specular1;" \
        "light2 = ambient2 + diffuse2 + specular2;" \
        "phong_ads_color = light0;" \
        "}" \
        "else" \
        "{" \
        "phong_ads_color =  vec3(1.0,  1.0,  1.0);" \
        "}" \
        "FragColor = vec4(phong_ads_color, 1.0); " \
        "}";
        
        glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
        
        // compile shader
        glCompileShader(gFragmentShaderObject);
        glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if (iShaderCompiledStatus == GL_FALSE)
        {
            char *szInfoLog = NULL;
            glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("Fragment Shader Compilation Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self  release];
                    
                }
            }
        }
        
        //Shader program
        //create
        gShaderProgramObject = glCreateProgram();
        
        //attach vertex shader to shader program
        glAttachShader(gShaderProgramObject, gVertexShaderObject);
        
        //attach fragment shader to shader program
        glAttachShader(gShaderProgramObject, gFragmentShaderObject);
        
        //Pre-link binding of shader program object with vertex shader position attribute
        glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");
        
        
        //Pre-link binding of shader program object with vertex shader Normal attribute
        glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");
        
        //Link shader
        glLinkProgram(gShaderProgramObject);
        GLint iShaderProgramLinkStatus = 0;
        glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
        if (iShaderProgramLinkStatus == GL_FALSE)
        {
            
            glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
                    printf("Shader Program Link Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self  release];
                }
            }
        }
        
        //get MVP uniform location
        model_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
        view_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
        projection_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");
        rotation_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_rotation_matrix");
        
        L_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObject, "u_lighting_enabled");
        
        La0_uniform = glGetUniformLocation(gShaderProgramObject, "u_La0");
        Ld0_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld0");
        Ls0_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls0");
        light0_position_uniform = glGetUniformLocation(gShaderProgramObject, "u_light0_position");
        
        La1_uniform = glGetUniformLocation(gShaderProgramObject, "u_La1");
        Ld1_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld1");
        Ls1_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls1");
        light1_position_uniform = glGetUniformLocation(gShaderProgramObject, "u_light1_position");
        
        La2_uniform = glGetUniformLocation(gShaderProgramObject, "u_La2");
        Ld2_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld2");
        Ls2_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls2");
        light2_position_uniform = glGetUniformLocation(gShaderProgramObject, "u_light2_position");
        
        Ka_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ka");
        Kd_uniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");
        Ks_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ks");
        material_shininess_uniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");
        
        // *** vertices, Colors, Shader Attribs, vbo, vao initializations
        
        Sphere *sph = [[Sphere alloc]init];
        [sph getSphereVertexData: sphere_vertices : sphere_normals : sphere_textures : sphere_elements];
        gNumVertices = [sph getNumberOfSphereVertices];
        gNumElements = [sph getNumberOfSphereElements];
        
        
        
        
        //Vao sphere
        glGenVertexArrays(1, &gVao_sphere);  // recording of casatte
        glBindVertexArray(gVao_sphere);
        
        //vbo position
        glGenBuffers(1, &gVbo_sphere_position);
        glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
        glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
        glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
        glBindBuffer(GL_ARRAY_BUFFER, 0); // passing zero is unbinding
        
        //vbo normal
        glGenBuffers(1, &gVbo_sphere_normal);
        glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_normal);
        glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
        glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);
        glBindBuffer(GL_ARRAY_BUFFER, 0); // passing zero is unbinding
        
        //vbo element
        glGenBuffers(1, &gVbo_sphere_element);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0); // passing zero is unbinding
        
        glBindVertexArray(0);
        
        light0_Ambient[0] = 0.0f;
        light0_Ambient[1] = 0.0f;
        light0_Ambient[2] = 0.0f;
        light0_Ambient[3] = 1.0f;
        
        light0_Diffuse[0] = 1.0f;  //White Color
        light0_Diffuse[1] = 1.0f;
        light0_Diffuse[2] = 1.0f;
        light0_Diffuse[3] = 1.0f;
        
        light0_Specular[0] = 1.0f;
        light0_Specular[1] = 1.0f;
        light0_Specular[2] = 1.0f;
        light0_Specular[3] = 1.0f;
        
        light0_Position[0] = 200.0f;
        light0_Position[1] = 100.0f;
        light0_Position[2] = 100.0f;
        light0_Position[3] = 1.0f;
        
        //Light1
        light1_Ambient[0] = 0.0f;
        light1_Ambient[1] = 0.0f;
        light1_Ambient[2] = 0.0f;
        light1_Ambient[3] = 1.0f;
        
        light1_Diffuse[0] = 0.0f;  //Blue Color
        light1_Diffuse[1] = 0.0f;
        light1_Diffuse[2] = 1.0f;
        light1_Diffuse[3] = 0.0f;
        
        light1_Specular[0] = 1.0f;
        light1_Specular[1] = 1.0f;
        light1_Specular[2] = 1.0f;
        light1_Specular[3] = 1.0f;
        
        light1_Position[0] = 200.0f;
        light1_Position[1] = 100.0f;
        light1_Position[2] = 100.0f;
        light1_Position[3] = 1.0f;
        
        //Light2
        light2_Ambient[0] = 0.0f;
        light2_Ambient[1] = 0.0f;
        light2_Ambient[2] = 0.0f;
        light2_Ambient[3] = 1.0f;
        
        light2_Diffuse[0] = 0.0f;  //Green Color
        light2_Diffuse[1] = 1.0f;
        light2_Diffuse[2] = 0.0f;
        light2_Diffuse[3] = 0.0f;
        
        light2_Specular[0] = 1.0f;
        light2_Specular[1] = 1.0f;
        light2_Specular[2] = 1.0f;
        light2_Specular[3] = 1.0f;
        
        light2_Position[0] = 200.0f;
        light2_Position[1] = 100.0f;
        light2_Position[2] = 100.0f;
        light2_Position[3] = 1.0f;
        
        
        light_model_ambient[0] = 0.2f;
        light_model_ambient[1] = 0.2f;
        light_model_ambient[2] = 0.2f;
        light_model_ambient[3] = 0.0f;
        
        light_model_local_viewer[0] = 0.0f;
        
        //First sphere on first column, emerald
        material1_ambient[0] = 0.0215f;
        material1_ambient[1] = 0.1745f;
        material1_ambient[2] = 0.0215f;
        material1_ambient[3] = 1.0f;
        
        material1_diffuse[0] = 0.07568f;
        material1_diffuse[1] = 0.61424f;
        material1_diffuse[2] = 0.07568f;
        material1_diffuse[3] = 1.0f;
        
        material1_specular[0] = 0.633f;
        material1_specular[1] = 0.727811f;
        material1_specular[2] = 0.633f;
        material1_specular[3] = 1.0f;
        
        material1_shininess[0] = (0.6f * 128);
        
        //Second sphere on first column, jade
        material2_ambient[0] = 0.135f;
        material2_ambient[1] = 0.2225f;
        material2_ambient[2] = 0.1575f;
        material2_ambient[3] = 1.0f;
        
        material2_diffuse[0] = 0.54f;
        material2_diffuse[1] = 0.89f;
        material2_diffuse[2] = 0.63f;
        material2_diffuse[3] = 1.0f;
        
        material2_specular[0] = 0.316228f;
        material2_specular[1] = 0.316228f;
        material2_specular[2] = 0.316228f;
        material2_specular[3] = 1.0f;
        
        material2_shininess[0] = (0.1f * 128);
        
        //Third sphere on first column, obsidian
        material3_ambient[0] = 0.05375f;
        material3_ambient[1] = 0.05f;
        material3_ambient[2] = 0.06625f;
        material3_ambient[3] = 1.0f;
        
        material3_diffuse[0] = 0.18275f;
        material3_diffuse[1] = 0.17f;
        material3_diffuse[2] = 0.22525f;
        material3_diffuse[3] = 1.0f;
        
        material3_specular[0] = 0.332741f;
        material3_specular[1] = 0.328634f;
        material3_specular[2] = 0.346435f;
        material3_specular[3] = 1.0f;
        
        material3_shininess[0] = (0.3f * 128);
        
        //Fourth sphere on first column, pearl
        material4_ambient[0] = 0.25f;
        material4_ambient[1] = 0.20725f;
        material4_ambient[2] = 0.20725f;
        material4_ambient[3] = 1.0f;
        
        material4_diffuse[0] = 1.0f;
        material4_diffuse[1] = 0.829f;
        material4_diffuse[2] = 0.829f;
        material4_diffuse[3] = 1.0f;
        
        material4_specular[0] = 0.296648f;
        material4_specular[1] = 0.296648f;
        material4_specular[2] = 0.296648f;
        material4_specular[3] = 1.0f;
        
        material4_shininess[0] = (0.088f * 128);
        
        //Fifth sphere on first column, ruby
        material5_ambient[0] = 0.1745f;
        material5_ambient[1] = 0.01175f;
        material5_ambient[2] = 0.01175f;
        material5_ambient[3] = 1.0f;
        
        material5_diffuse[0] = 0.61424f;
        material5_diffuse[1] = 0.04136f;
        material5_diffuse[2] = 0.04136f;
        material5_diffuse[3] = 1.0f;
        
        material5_specular[0] = 0.727811f;
        material5_specular[1] = 0.626959f;
        material5_specular[2] = 0.626959f;
        material5_specular[3] = 1.0f;
        
        material5_shininess[0] = (0.6f * 128);
        
        
        //Sixth sphere on second column, silver
        material6_ambient[0] = 0.19225f;
        material6_ambient[1] = 0.19225f;
        material6_ambient[2] = 0.19225f;
        material6_ambient[3] = 1.0f;
        
        material6_diffuse[0] = 0.50754f;
        material6_diffuse[1] = 0.50754f;
        material6_diffuse[2] = 0.50754f;
        material6_diffuse[3] = 1.0f;
        
        material6_specular[0] = 0.508273f;
        material6_specular[1] = 0.508273f;
        material6_specular[2] = 0.508273f;
        material6_specular[3] = 1.0f;
        
        material6_shininess[0] = (0.4f * 128);
        
        //First sphere on second column, brass
        material7_ambient[0] = 0.329412f;
        material7_ambient[1] = 0.223529f;
        material7_ambient[2] = 0.027451f;
        material7_ambient[3] = 1.0f;
        
        material7_diffuse[0] = 0.780392f;
        material7_diffuse[1] = 0.568627f;
        material7_diffuse[2] = 0.113725f;
        material7_diffuse[3] = 1.0f;
        
        material7_specular[0] = 0.992157f;
        material7_specular[1] = 0.941176f;
        material7_specular[2] = 0.807843f;
        material7_specular[3] = 1.0f;
        
        material7_shininess[0] = (0.21794872f * 128);
        
        //Second sphere on second column, bronze
        material8_ambient[0] = 0.2125f;
        material8_ambient[1] = 0.1275f;
        material8_ambient[2] = 0.054f;
        material8_ambient[3] = 1.0f;
        
        material8_diffuse[0] = 0.714f;
        material8_diffuse[1] = 0.4284f;
        material8_diffuse[2] = 0.18144f;
        material8_diffuse[3] = 1.0f;
        
        material8_specular[0] = 0.393548f;
        material8_specular[1] = 0.211906f;
        material8_specular[2] = 0.166721f;
        material8_specular[3] = 1.0f;
        
        material8_shininess[0] = (0.2f * 128);
        
        //Third sphere on second column, chrome
        material9_ambient[0] = 0.25f;
        material9_ambient[1] = 0.25f;
        material9_ambient[2] = 0.25f;
        material9_ambient[3] = 1.0f;
        
        material9_diffuse[0] = 0.4f;
        material9_diffuse[1] = 0.4f;
        material9_diffuse[2] = 0.4f;
        material9_diffuse[3] = 1.0f;
        
        material9_specular[0] = 0.774597f;
        material9_specular[1] = 0.774597f;
        material9_specular[2] = 0.774597f;
        material9_specular[3] = 1.0f;
        
        material9_shininess[0] = (0.6f * 128);
        
        //Fourth sphere on second column, copper
        material10_ambient[0] = 0.19125f;
        material10_ambient[1] = 0.0735f;
        material10_ambient[2] = 0.0225f;
        material10_ambient[3] = 1.0f;
        
        material10_diffuse[0] = 0.7038f;
        material10_diffuse[1] = 0.27048f;
        material10_diffuse[2] = 0.0828f;
        material10_diffuse[3] = 1.0f;
        
        material10_specular[0] = 0.276777f;
        material10_specular[1] = 0.137622f;
        material10_specular[2] = 0.086014f;
        material10_specular[3] = 1.0f;
        
        material10_shininess[0] = (0.1f * 128);
        
        //Fifth sphere on second column, gold
        material11_ambient[0] = 0.24725f;
        material11_ambient[1] = 0.1995f;
        material11_ambient[2] = 0.07455f;
        material11_ambient[3] = 1.0f;
        
        material11_diffuse[0] = 0.75164f;
        material11_diffuse[1] = 0.60648f;
        material11_diffuse[2] = 0.22648f;
        material11_diffuse[3] = 1.0f;
        
        material11_specular[0] = 0.628281f;
        material11_specular[1] = 0.555802f;
        material11_specular[2] = 0.366065f;
        material11_specular[3] = 1.0f;
        
        material11_shininess[0] = (0.4f * 128);
        
        //Sixth sphere on second column, silver
        material12_ambient[0] = 0.19225f;
        material12_ambient[1] = 0.19225f;
        material12_ambient[2] = 0.19225f;
        material12_ambient[3] = 1.0f;
        
        material12_diffuse[0] = 0.50754f;
        material12_diffuse[1] = 0.50754f;
        material12_diffuse[2] = 0.50754f;
        material12_diffuse[3] = 1.0f;
        
        material12_specular[0] = 0.508273f;
        material12_specular[1] = 0.508273f;
        material12_specular[2] = 0.508273f;
        material12_specular[3] = 1.0f;
        
        material12_shininess[0] = (0.4f * 128);
        
        //First sphere on third column, black
        material13_ambient[0] = 0.0f;
        material13_ambient[1] = 0.0f;
        material13_ambient[2] = 0.0f;
        material13_ambient[3] = 1.0f;
        
        material13_diffuse[0] = 0.01f;
        material13_diffuse[1] = 0.01f;
        material13_diffuse[2] = 0.01f;
        material13_diffuse[3] = 1.0f;
        
        material13_specular[0] = 0.50f;
        material13_specular[1] = 0.50f;
        material13_specular[2] = 0.50f;
        material13_specular[3] = 1.0f;
        
        material13_shininess[0] = (0.25f * 128);
        
        //Second sphere on third column, cyan
        material14_ambient[0] = 0.0f;
        material14_ambient[1] = 0.1f;
        material14_ambient[2] = 0.06f;
        material14_ambient[3] = 1.0f;
        
        material14_diffuse[0] = 0.0f;
        material14_diffuse[1] = 0.50980392f;
        material14_diffuse[2] = 0.50980392f;
        material14_diffuse[3] = 1.0f;
        
        material14_specular[0] = 0.50196078f;
        material14_specular[1] = 0.50196078f;
        material14_specular[2] = 0.50196078f;
        material14_specular[3] = 1.0f;
        
        material14_shininess[0] = (0.25f * 128);
        
        //Third sphere on third column, green
        material15_ambient[0] = 0.0f;
        material15_ambient[1] = 0.0f;
        material15_ambient[2] = 0.0f;
        material15_ambient[3] = 1.0f;
        
        material15_diffuse[0] = 0.1f;
        material15_diffuse[1] = 0.35f;
        material15_diffuse[2] = 0.1f;
        material15_diffuse[3] = 1.0f;
        
        material15_specular[0] = 0.45f;
        material15_specular[1] = 0.55f;
        material15_specular[2] = 0.45f;
        material15_specular[3] = 1.0f;
        
        material15_shininess[0] = (0.25f * 128);
        
        //Fourth sphere on third column, red
        material16_ambient[0] = 0.0f;
        material16_ambient[1] = 0.0f;
        material16_ambient[2] = 0.0f;
        material16_ambient[3] = 1.0f;
        
        material16_diffuse[0] = 0.5f;
        material16_diffuse[1] = 0.0f;
        material16_diffuse[2] = 0.0f;
        material16_diffuse[3] = 1.0f;
        
        material16_specular[0] = 0.7f;
        material16_specular[1] = 0.6f;
        material16_specular[2] = 0.6f;
        material16_specular[3] = 1.0f;
        
        material16_shininess[0] = (0.25f * 128);
        
        //Fifth sphere on third column, white
        material17_ambient[0] = 0.0f;
        material17_ambient[1] = 0.0f;
        material17_ambient[2] = 0.0f;
        material17_ambient[3] = 1.0f;
        
        material17_diffuse[0] = 0.55f;
        material17_diffuse[1] = 0.55f;
        material17_diffuse[2] = 0.55f;
        material17_diffuse[3] = 0.55f;
        
        material17_specular[0] = 0.70f;
        material17_specular[1] = 0.70f;
        material17_specular[2] = 0.70f;
        material17_specular[3] = 1.0f;
        
        material17_shininess[0] = (0.25f * 128);
        
        //Second sphere on second column, bronze
        material18_ambient[0] = 0.2125f;
        material18_ambient[1] = 0.1275f;
        material18_ambient[2] = 0.054f;
        material18_ambient[3] = 1.0f;
        
        material18_diffuse[0] = 0.714f;
        material18_diffuse[1] = 0.4284f;
        material18_diffuse[2] = 0.18144f;
        material18_diffuse[3] = 1.0f;
        
        material18_specular[0] = 0.393548f;
        material18_specular[1] = 0.211906f;
        material18_specular[2] = 0.166721f;
        material18_specular[3] = 1.0f;
        
        material18_shininess[0] = (0.2f * 128);
        
        //First sphere on fourth column, black
        material19_ambient[0] = 0.02f;
        material19_ambient[1] = 0.02f;
        material19_ambient[2] = 0.02f;
        material19_ambient[3] = 1.0f;
        
        material19_diffuse[0] = 0.01f;
        material19_diffuse[1] = 0.01f;
        material19_diffuse[2] = 0.01f;
        material19_diffuse[3] = 1.0f;
        
        material19_specular[0] = 0.4f;
        material19_specular[1] = 0.4f;
        material19_specular[2] = 0.4f;
        material19_specular[3] = 1.0f;
        
        material19_shininess[0] = (0.078125f * 128);
        
        //Second sphere on fourth column, cyan
        material20_ambient[0] = 0.0f;
        material20_ambient[1] = 0.05f;
        material20_ambient[2] = 0.05f;
        material20_ambient[3] = 1.0f;
        
        material20_diffuse[0] = 0.4f;
        material20_diffuse[1] = 0.5f;
        material20_diffuse[2] = 0.5f;
        material20_diffuse[3] = 1.0f;
        
        material20_specular[0] = 0.04f;
        material20_specular[1] = 0.7f;
        material20_specular[2] = 0.7f;
        material20_specular[3] = 1.0f;
        
        material20_shininess[0] = (0.078125f * 128);
        
        //Third sphere on fourth column, green
        material21_ambient[0] = 0.0f;
        material21_ambient[1] = 0.05f;
        material21_ambient[2] = 0.0f;
        material21_ambient[3] = 1.0f;
        
        material21_diffuse[0] = 0.4f;
        material21_diffuse[1] = 0.5f;
        material21_diffuse[2] = 0.4f;
        material21_diffuse[3] = 1.0f;
        
        material21_specular[0] = 0.04f;
        material21_specular[1] = 0.7f;
        material21_specular[2] = 0.4f;
        material21_specular[3] = 1.0f;
        
        material21_shininess[0] = (0.078125f * 128);
        
        //Fourth sphere on fourth column, red
        material22_ambient[0] = 0.05f;
        material22_ambient[1] = 0.0f;
        material22_ambient[2] = 0.0f;
        material22_ambient[3] = 1.0f;
        
        material22_diffuse[0] = 0.5f;
        material22_diffuse[1] = 0.4f;
        material22_diffuse[2] = 0.4f;
        material22_diffuse[3] = 1.0f;
        
        material22_specular[0] = 0.7f;
        material22_specular[1] = 0.04f;
        material22_specular[2] = 0.04f;
        material22_specular[3] = 1.0f;
        
        material22_shininess[0] = (0.078125f * 128);
        
        //Fifth sphere on fourth column, white
        material23_ambient[0] = 0.05f;
        material23_ambient[1] = 0.05f;
        material23_ambient[2] = 0.05f;
        material23_ambient[3] = 1.0f;
        
        material23_diffuse[0] = 0.5f;
        material23_diffuse[1] = 0.5f;
        material23_diffuse[2] = 0.5f;
        material23_diffuse[3] = 1.0f;
        
        material23_specular[0] = 0.7f;
        material23_specular[1] = 0.7f;
        material23_specular[2] = 0.7f;
        material23_specular[3] = 1.0f;
        
        material23_shininess[0] = (0.078125f * 128);
        
        //Fifth sphere on fourth column, yellow rubber
        material24_ambient[0] = 0.05f;
        material24_ambient[1] = 0.05f;
        material24_ambient[2] = 0.0f;
        material24_ambient[3] = 1.0f;
        
        material24_diffuse[0] = 0.5f;
        material24_diffuse[1] = 0.5f;
        material24_diffuse[2] = 0.4f;
        material24_diffuse[3] = 1.0f;
        
        material24_specular[0] = 0.7f;
        material24_specular[1] = 0.7f;
        material24_specular[2] = 0.04f;
        material24_specular[3] = 1.0f;
        
        material24_shininess[0] = (0.078125f * 128);
        
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        // glEnable(GL_CULL_FACE);
        
        //set background  color
        glClearColor(0.20f,0.20f, 0.20f, 0.0f); //blue
        
        //set perpective matrix to identity matrix
        gPerspectiveProjectionMatrix = vmath::mat4::identity();
        
        red_x = 0.0f;
        red_y = 0.0f;
        red_z = 0.0f;
        
        green_x = 0.0f;
        green_y = 0.0f;
        green_z = 0.0f;
        
        blue_x = 0.0f;
        blue_y = 0.0f;
        blue_z = 0.0f;
        
        Xcenter = 0.0f;
        Ycenter = 0.0f;
        Zcenter = 0.0f;
        
        angleCircle = 0.0f;
        
        Xkey = false;
        Ykey = false;
        Zkey = false;
        
        singleTap = 0;
        
        //GESTURE RECOGNITION
        //Tap gesture here
        UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1]; //touch of one  finger
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1]; //touch of one finger
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        //this will allow to differentiate between single tap and double tap
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        //swipe gesture
        UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        //long press gesture
        UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [self addGestureRecognizer:longPressGestureRecognizer];
        
        
        
    }
    return(self);
    
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 */
- (void)drawRect:(CGRect)rect
{
    //Drawing code
}

+(Class)layerClass
{
    //code
    return([CAEAGLLayer class]);
}

-(void)drawView:(id)sender
{
    //code
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
    
    // Strt using OpenGL program object
    glUseProgram(gShaderProgramObject);
    
    
    
    if (gbLight == true)
    {
        glUniform1i(L_KeyPressed_uniform, 1);
        
        glUniform3fv(La0_uniform, 1, light0_Ambient);
        glUniform3fv(Ld0_uniform, 1, light0_Diffuse);
        glUniform3fv(Ls0_uniform, 1, light0_Specular);
        glUniform4fv(light0_position_uniform, 1, light0_Position);
        
        glUniform3fv(La1_uniform, 1, light1_Ambient);
        glUniform3fv(Ld1_uniform, 1, light1_Diffuse);
        glUniform3fv(Ls1_uniform, 1, light1_Specular);
        glUniform4fv(light1_position_uniform, 1, light1_Position);
        
        glUniform3fv(La2_uniform, 1, light2_Ambient);
        glUniform3fv(Ld2_uniform, 1, light2_Diffuse);
        glUniform3fv(Ls2_uniform, 1, light2_Specular);
        glUniform4fv(light2_position_uniform, 1, light2_Position);
        
        
        if (Xkey == true)
        {
            //Rotate RED light to Y-direction
            light0_Position[0] = red_x;
            light0_Position[1] = red_y;
            light0_Position[2] = red_z;
        }
        
        if (Ykey == true)
        {
            
            //Rotate Blue light to Z-direction
            light0_Position[0] = blue_x;
            light0_Position[1] = blue_y;
            light0_Position[2] = blue_z;
        }
        
        if (Zkey == true)
        {
            //Rotate GREEN light to X-direction
            light0_Position[0] = green_x;
            light0_Position[1] = green_y;
            light0_Position[2] = green_z;
        }
        
    }
    else
    {
        glUniform1i(L_KeyPressed_uniform, 0);
    }
    
    // OpenGL Drawing
    
    
    //Set model view and modelviewprojection matrices to identity
    vmath::mat4 modelMatrix = vmath::mat4::identity();
    vmath::mat4 viewMatrix = vmath::mat4::identity();
    vmath::mat4 RotationMatrix = vmath::mat4::identity();
    
    //****drawing*******
    
    //Translate model view matrix
    modelMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
    
    RotationMatrix = vmath::rotate(angleCube, 0.0f, 0.0f, 0.0f);
    
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(rotation_matrix_uniform, 1, GL_FALSE, RotationMatrix);
    glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);
    
    
    //*** bind vao ***
    glBindVertexArray(gVao_sphere);
    
    // 1st sphere
    glUniform3fv(Ka_uniform, 1, material1_ambient);
    glUniform3fv(Kd_uniform, 1, material1_diffuse);
    glUniform3fv(Ks_uniform, 1, material1_specular);
    glUniform1fv(material_shininess_uniform, 1, material1_shininess);
    
    glViewport(0, 5 * (GLsizei)height1 / 6, (GLsizei)width1 / 4, (GLsizei)height1 / 6);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    
    //2nd sphere
    glUniform3fv(Ka_uniform, 1, material2_ambient);
    glUniform3fv(Kd_uniform, 1, material2_diffuse);
    glUniform3fv(Ks_uniform, 1, material2_specular);
    glUniform1fv(material_shininess_uniform, 1, material2_shininess);
    
    glViewport(0, 4 * (GLsizei)height1 / 6, (GLsizei)width1 / 4, (GLsizei)height1 / 6);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    
    //3rd sphere
    glUniform3fv(Ka_uniform, 1, material3_ambient);
    glUniform3fv(Kd_uniform, 1, material3_diffuse);
    glUniform3fv(Ks_uniform, 1, material3_specular);
    glUniform1fv(material_shininess_uniform, 1, material3_shininess);
    
    glViewport(0, 3 * (GLsizei)height1 / 6, (GLsizei)width1 / 4, (GLsizei)height1 / 6);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    
    //4th sphere
    glUniform3fv(Ka_uniform, 1, material4_ambient);
    glUniform3fv(Kd_uniform, 1, material4_diffuse);
    glUniform3fv(Ks_uniform, 1, material4_specular);
    glUniform1fv(material_shininess_uniform, 1, material4_shininess);
    
    glViewport(0, 2 * (GLsizei)height1 / 6, (GLsizei)width1 / 4, (GLsizei)height1 / 6);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    
    //5th sphere
    glUniform3fv(Ka_uniform, 1, material5_ambient);
    glUniform3fv(Kd_uniform, 1, material5_diffuse);
    glUniform3fv(Ks_uniform, 1, material5_specular);
    glUniform1fv(material_shininess_uniform, 1, material5_shininess);
    
    glViewport(0, 1 * (GLsizei)height1 / 6, (GLsizei)width1 / 4, (GLsizei)height1 / 6);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    
    //6th sphere
    glUniform3fv(Ka_uniform, 1, material6_ambient);
    glUniform3fv(Kd_uniform, 1, material6_diffuse);
    glUniform3fv(Ks_uniform, 1, material6_specular);
    glUniform1fv(material_shininess_uniform, 1, material6_shininess);
    
    glViewport(0, 0 * (GLsizei)height1 / 6, (GLsizei)width1 / 4, (GLsizei)height1 / 6);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    
    //7th sphere
    glUniform3fv(Ka_uniform, 1, material7_ambient);
    glUniform3fv(Kd_uniform, 1, material7_diffuse);
    glUniform3fv(Ks_uniform, 1, material7_specular);
    glUniform1fv(material_shininess_uniform, 1, material7_shininess);
    
    glViewport(1 * (GLsizei)width1 / 4, 5 * (GLsizei)height1 / 6, (GLsizei)width1 / 4, (GLsizei)height1 / 6);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    
    //8th sphere
    glUniform3fv(Ka_uniform, 1, material8_ambient);
    glUniform3fv(Kd_uniform, 1, material8_diffuse);
    glUniform3fv(Ks_uniform, 1, material8_specular);
    glUniform1fv(material_shininess_uniform, 1, material8_shininess);
    
    glViewport(1 * (GLsizei)width1 / 4, 4 * (GLsizei)height1 / 6, (GLsizei)width1 / 4, (GLsizei)height1 / 6);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    
    //9th sphere
    glUniform3fv(Ka_uniform, 1, material9_ambient);
    glUniform3fv(Kd_uniform, 1, material9_diffuse);
    glUniform3fv(Ks_uniform, 1, material9_specular);
    glUniform1fv(material_shininess_uniform, 1, material9_shininess);
    
    glViewport(1 * (GLsizei)width1 / 4, 3 * (GLsizei)height1 / 6, (GLsizei)width1 / 4, (GLsizei)height1 / 6);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    
    //10th sphere
    glUniform3fv(Ka_uniform, 1, material10_ambient);
    glUniform3fv(Kd_uniform, 1, material10_diffuse);
    glUniform3fv(Ks_uniform, 1, material10_specular);
    glUniform1fv(material_shininess_uniform, 1, material10_shininess);
    
    glViewport(1 * (GLsizei)width1 / 4, 2 * (GLsizei)height1 / 6, (GLsizei)width1 / 4, (GLsizei)height1 / 6);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    
    //11th sphere
    glUniform3fv(Ka_uniform, 1, material11_ambient);
    glUniform3fv(Kd_uniform, 1, material11_diffuse);
    glUniform3fv(Ks_uniform, 1, material11_specular);
    glUniform1fv(material_shininess_uniform, 1, material11_shininess);
    
    glViewport(1 * (GLsizei)width1 / 4, 1 * (GLsizei)height1 / 6, (GLsizei)width1 / 4, (GLsizei)height1 / 6);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    
    //12th sphere
    glUniform3fv(Ka_uniform, 1, material12_ambient);
    glUniform3fv(Kd_uniform, 1, material12_diffuse);
    glUniform3fv(Ks_uniform, 1, material12_specular);
    glUniform1fv(material_shininess_uniform, 1, material12_shininess);
    
    glViewport(1 * (GLsizei)width1 / 4, 0 * (GLsizei)height1 / 6, (GLsizei)width1 / 4, (GLsizei)height1 / 6);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    
    //13th sphere
    glUniform3fv(Ka_uniform, 1, material13_ambient);
    glUniform3fv(Kd_uniform, 1, material13_diffuse);
    glUniform3fv(Ks_uniform, 1, material13_specular);
    glUniform1fv(material_shininess_uniform, 1, material13_shininess);
    
    glViewport(2 * (GLsizei)width1 / 4, 5 * (GLsizei)height1 / 6, (GLsizei)width1 / 4, (GLsizei)height1 / 6);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    
    //14th sphere
    glUniform3fv(Ka_uniform, 1, material14_ambient);
    glUniform3fv(Kd_uniform, 1, material14_diffuse);
    glUniform3fv(Ks_uniform, 1, material14_specular);
    glUniform1fv(material_shininess_uniform, 1, material14_shininess);
    
    glViewport(2 * (GLsizei)width1 / 4, 4 * (GLsizei)height1 / 6, (GLsizei)width1 / 4, (GLsizei)height1 / 6);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    
    //15th sphere
    glUniform3fv(Ka_uniform, 1, material15_ambient);
    glUniform3fv(Kd_uniform, 1, material15_diffuse);
    glUniform3fv(Ks_uniform, 1, material15_specular);
    glUniform1fv(material_shininess_uniform, 1, material15_shininess);
    
    glViewport(2 * (GLsizei)width1 / 4, 3 * (GLsizei)height1 / 6, (GLsizei)width1 / 4, (GLsizei)height1 / 6);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    
    
    //16th sphere
    glUniform3fv(Ka_uniform, 1, material16_ambient);
    glUniform3fv(Kd_uniform, 1, material16_diffuse);
    glUniform3fv(Ks_uniform, 1, material16_specular);
    glUniform1fv(material_shininess_uniform, 1, material16_shininess);
    
    glViewport(2 * (GLsizei)width1 / 4, 2 * (GLsizei)height1 / 6, (GLsizei)width1 / 4, (GLsizei)height1 / 6);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    
    //17th sphere
    glUniform3fv(Ka_uniform, 1, material17_ambient);
    glUniform3fv(Kd_uniform, 1, material17_diffuse);
    glUniform3fv(Ks_uniform, 1, material17_specular);
    glUniform1fv(material_shininess_uniform, 1, material17_shininess);
    
    glViewport(2 * (GLsizei)width1 / 4, 1 * (GLsizei)height1 / 6, (GLsizei)width1 / 4, (GLsizei)height1 / 6);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    
    //18th sphere
    glUniform3fv(Ka_uniform, 1, material18_ambient);
    glUniform3fv(Kd_uniform, 1, material18_diffuse);
    glUniform3fv(Ks_uniform, 1, material18_specular);
    glUniform1fv(material_shininess_uniform, 1, material18_shininess);
    
    glViewport(2 * (GLsizei)width1 / 4, 0 * (GLsizei)height1 / 6, (GLsizei)width1 / 4, (GLsizei)height1 / 6);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    
    //19th sphere
    glUniform3fv(Ka_uniform, 1, material19_ambient);
    glUniform3fv(Kd_uniform, 1, material19_diffuse);
    glUniform3fv(Ks_uniform, 1, material19_specular);
    glUniform1fv(material_shininess_uniform, 1, material19_shininess);
    
    glViewport(3 * (GLsizei)width1 / 4, 5 * (GLsizei)height1 / 6, (GLsizei)width1 / 4, (GLsizei)height1 / 6);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    
    //20th sphere
    glUniform3fv(Ka_uniform, 1, material20_ambient);
    glUniform3fv(Kd_uniform, 1, material20_diffuse);
    glUniform3fv(Ks_uniform, 1, material20_specular);
    glUniform1fv(material_shininess_uniform, 1, material20_shininess);
    
    glViewport(3 * (GLsizei)width1 / 4, 4 * (GLsizei)height1 / 6, (GLsizei)width1 / 4, (GLsizei)height1 / 6);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    
    //21st sphere
    glUniform3fv(Ka_uniform, 1, material21_ambient);
    glUniform3fv(Kd_uniform, 1, material21_diffuse);
    glUniform3fv(Ks_uniform, 1, material21_specular);
    glUniform1fv(material_shininess_uniform, 1, material21_shininess);
    
    glViewport(3 * (GLsizei)width1 / 4, 3 * (GLsizei)height1 / 6, (GLsizei)width1 / 4, (GLsizei)height1 / 6);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    
    //22nd sphere
    glUniform3fv(Ka_uniform, 1, material22_ambient);
    glUniform3fv(Kd_uniform, 1, material22_diffuse);
    glUniform3fv(Ks_uniform, 1, material22_specular);
    glUniform1fv(material_shininess_uniform, 1, material22_shininess);
    
    glViewport(3 * (GLsizei)width1 / 4, 2 * (GLsizei)height1 / 6, (GLsizei)width1 / 4, (GLsizei)height1 / 6);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    
    //23rd sphere
    glUniform3fv(Ka_uniform, 1, material23_ambient);
    glUniform3fv(Kd_uniform, 1, material23_diffuse);
    glUniform3fv(Ks_uniform, 1, material23_specular);
    glUniform1fv(material_shininess_uniform, 1, material23_shininess);
    
    glViewport(3 * (GLsizei)width1 / 4, 1 * (GLsizei)height1 / 6, (GLsizei)width1 / 4, (GLsizei)height1 / 6);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    
    //24rd sphere
    glUniform3fv(Ka_uniform, 1, material24_ambient);
    glUniform3fv(Kd_uniform, 1, material24_diffuse);
    glUniform3fv(Ks_uniform, 1, material24_specular);
    glUniform1fv(material_shininess_uniform, 1, material24_shininess);
    
    glViewport(3 * (GLsizei)width1 / 4, 0 * (GLsizei)height1 / 6, (GLsizei)width1 / 4, (GLsizei)height1 / 6);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    
    //*** unbind vao ***
    glBindVertexArray(0);
    
    //Stop using OpenGL program object
    glUseProgram(0);
    
    if (gbAnimate == true)
    {
        // Circle equalation  Circle_X= Xcenter + rcosQ , Circle_X = Ycenter + rsinQ, Q = 0 to 360
        
        angleCircle = angleCircle - 0.01f;
        if (angleCircle >= 360.0f)
            angleCircle = 0.0f;
        
        red_x = Xcenter + 100.0f * cos(angleCircle);
        red_y = Ycenter;
        red_z = Zcenter + 100.0f * sin(angleCircle);
        
        green_x = Xcenter + 100.0f * sin(angleCircle);
        green_y = Ycenter + 100.0f * cos(angleCircle);
        green_z = Zcenter;
        
        blue_x = Xcenter;
        blue_y = Ycenter + 100.0f * sin(angleCircle);
        blue_z = Zcenter + 100.0f * cos(angleCircle);
    }
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
}

-(void)layoutSubviews
{
    //code
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER,  GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER,  GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
    
    
    glViewport(0, 0, width, height);
    // GLfloat fwidth = (GLfloat)width;
    // GLfloat fheight = (GLfloat)height;
    
    //glOrtho(left, right, bottom, top,  near, far)
    
    width1 = width;
    height1 = height;
    
    
    //Perspective projection matrix
    gPerspectiveProjectionMatrix = vmath::perspective(45.0f, (4.0/3.0) *(GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
    /*
     if (width <= height)
     gOrthographicProjectionMatrix = vmath::ortho((-100.0f * (fwidth / fheight)), (100.0f * (fwidth / fheight)), -100.0f, 100.0f, -100.0f, 100.0f);
     else
     gOrthographicProjectionMatrix = vmath::ortho(-100.0f, 100.0f, (-100.0f * (width / height)), (100.0f * (width / height)),
     -100.0f, 100.0f);
     */
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!= GL_FRAMEBUFFER_COMPLETE)
    {
        printf("Failed to create complete framebuffer object %x", glCheckFramebufferStatus(GL_FRAMEBUFFER));
        
    }
    
    [self drawView:nil];
}

-(void)startAnimation
{
    if(!isAnimating)
    {
        displayLink = [NSClassFromString(@"CADisplayLink")displayLinkWithTarget:self selector:@selector(drawView:)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        isAnimating=YES;
    }
}

-(void)stopAnimation
{
    if(isAnimating)
    {
        [displayLink invalidate];
        displayLink = nil;
        
        isAnimating = NO;
        
    }
    
}

// to become first responder
-(BOOL)acceptsFirstResponder
{
    //code
    return(YES);
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}

-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    singleTap++;
    if(singleTap == 1)
    {
        Xkey = true;
        Ykey = false;
        Zkey = false;
    }
    else if (singleTap == 2)
    {
        Xkey = false;
        Ykey = true;
        Zkey = false;
    }
    else if (singleTap == 3)
    {
        Xkey = false;
        Ykey = false;
        Zkey = true;
        
        singleTap = 0;
    }
    
    
    if (bIsAKeyPressed == false)
    {
        gbAnimate = true;
        bIsAKeyPressed = true;
    }
    else
    {
        gbAnimate = false;
        bIsAKeyPressed = false;
    }
    
    
}

-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    
    
    
    if (bIsLKeyPressed == false)
    {
        gbLight = true;
        bIsLKeyPressed = true;
    }
    else
    {
        gbLight = false;
        bIsLKeyPressed = false;
    }
    
}

-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    //code
    [self release];
    exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    
}


- (void)dealloc
{
    
    if (gVao_sphere)
    {
        glDeleteVertexArrays(1, &gVao_sphere);
        gVao_sphere = 0;
    }
    
    
    // Destroy vbo_position
    if (gVbo_sphere_position)
    {
        glDeleteVertexArrays(1, &gVbo_sphere_position);
        gVbo_sphere_position = 0;
    }
    
    // Destroy vbo_normal
    if (gVbo_sphere_normal)
    {
        glDeleteVertexArrays(1, &gVbo_sphere_normal);
        gVbo_sphere_normal = 0;
    }
    
    // Destroy vbo_element
    if (gVbo_sphere_element)
    {
        glDeleteVertexArrays(1, &gVbo_sphere_element);
        gVbo_sphere_element = 0;
    }
    
    //Detach vertex shader from shader program object
    glDetachShader(gShaderProgramObject, gVertexShaderObject);
    
    //Detach fragment shader from shader program object
    glDetachShader(gShaderProgramObject, gFragmentShaderObject);
    
    
    
    // Delete vertex shader object
    glDeleteShader(gVertexShaderObject);
    gVertexShaderObject = 0;
    
    // Delete fragment shader object
    glDeleteShader(gFragmentShaderObject);
    gFragmentShaderObject = 0;
    
    // Delete shader program object
    glDeleteShader(gShaderProgramObject);
    gShaderProgramObject = 0;
    
    
    if(depthRenderbuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderbuffer);
        depthRenderbuffer =  0;
    }
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderbuffer);
        colorRenderbuffer =  0;
    }
    
    if(defaultFramebuffer)
    {
        glDeleteFramebuffers(1, &defaultFramebuffer);
        defaultFramebuffer =  0;
    }
    
    if([EAGLContext currentContext]==eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    
    [eaglContext release];
    eaglContext =  nil;
    
    [super dealloc];
    
}


@end

