//
//  GLESView.m
//  Suraj_04_TrianglePerspective
//
//  Created by user139842 on 7/30/18.
//  Copyright © 2018 user139842. All rights reserved.
//

#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "vmath.h"
#import "GLESView.h"

#import "Sphere.h"

enum
{
    VDG_ATTRIBUTE_VERTEX = 0,
    VDG_ATTRIBUTE_COLOR,
    VDG_ATTRIBUTE_NORMAL,
    VDG_ATTRIBUTE_TEXTURE0,
};
@implementation GLESView
{
    EAGLContext *eaglContext;
    
    GLuint defaultFramebuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimating;
    
    GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;
    
    float sphere_vertices[1146];
    float sphere_normals[1146];
    float sphere_textures[764];
    unsigned short sphere_elements[2280];
    
    GLuint gNumVertices;
    GLuint gNumElements;
    
    GLuint gVao_sphere;
    GLuint gVbo_sphere_position;
    GLuint gVbo_sphere_normal;
    GLuint gVbo_sphere_element;
    
    GLuint gMVPUniform;
    
    GLuint model_matrix_uniform, view_matrix_uniform, projection_matrix_uniform, rotation_matrix_uniform;
    
    GLuint L_KeyPressed_uniform;
    
    GLuint La0_uniform;
    GLuint Ld0_uniform;
    GLuint Ls0_uniform;
    GLuint light0_position_uniform;
    
    GLuint La1_uniform;
    GLuint Ld1_uniform;
    GLuint Ls1_uniform;
    GLuint light1_position_uniform;
    
    GLuint La2_uniform;
    GLuint Ld2_uniform;
    GLuint Ls2_uniform;
    GLuint light2_position_uniform;
    
    GLuint Ka_uniform;
    GLuint Kd_uniform;
    GLuint Ks_uniform;
    GLuint material_shininess_uniform;
    
    vmath::mat4 gPerspectiveProjectionMatrix;
    
    GLfloat light0_Ambient[4];
    GLfloat light0_Diffuse[4];
    GLfloat light0_Specular[4];
    GLfloat light0_Position[4];
    
    GLfloat light1_Ambient[4];
    GLfloat light1_Diffuse[4];
    GLfloat light1_Specular[4];
    GLfloat light1_Position[4];
    
    GLfloat light2_Ambient[4];
    GLfloat light2_Diffuse[4];
    GLfloat light2_Specular[4];
    GLfloat light2_Position[4];
    
    GLfloat material_ambient[4];
    GLfloat material_diffuse[4];
    GLfloat material_specular[4];
    GLfloat material_shininess;
    
    bool gbAnimate;
    bool gbLight;
    
    //variable declaration
    bool bIsAKeyPressed;
    bool bIsLKeyPressed;
    
    GLfloat red_x;
    GLfloat red_y;
    GLfloat red_z;
    
    GLfloat green_x;
    GLfloat green_y;
    GLfloat green_z;
    
    GLfloat blue_x;
    GLfloat blue_y;
    GLfloat blue_z;
    
    GLfloat Xcenter;
    GLfloat Ycenter;
    GLfloat Zcenter;
    
    GLfloat angleCircle;
    
    
    GLfloat anglePyramid;
    GLfloat angleCube;
    
}
-(id)initWithFrame:(CGRect)frame;
{
    self = [super initWithFrame:frame];
    if(self)
    {
        CAEAGLLayer *eaglLayer = (CAEAGLLayer *)super.layer;
        eaglLayer.opaque = YES;
        eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],
                                        kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil];
        
        eaglContext = [[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext == nil)
        {
            [self release];
            return(nil);
        }
        
        [EAGLContext setCurrentContext:eaglContext];
        
        glGenFramebuffers(1, &defaultFramebuffer);
        glGenRenderbuffers(1, &colorRenderbuffer);
        glBindFramebuffer(GL_FRAMEBUFFER,  defaultFramebuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
        
        GLint backingWidth;
        GLint backingHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER,  GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER,  GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        glGenRenderbuffers(1, &depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Failed to create complete Framebuffer object %x\n", glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1, &defaultFramebuffer);
            glDeleteRenderbuffers(1, &colorRenderbuffer);
            glDeleteRenderbuffers(1, &depthRenderbuffer);
            
            return(nil);
            
        }
        printf("Renderer : %s | GL version : %s | GLSL version : %s\n", glGetString(GL_RENDERER), glGetString(GL_VERSION), glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        //hard coded initializations
        isAnimating = NO;
        animationFrameInterval = 60; // default since iOS 8.2
        
        //*** Vertex Shader ***
        // Create Shader
        gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        
        //Provide source code to shader
        const GLchar *vertexShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "in vec4 vPosition;" \
        "in vec3 vNormal;" \
        "uniform mat4 u_model_matrix;" \
        "uniform mat4 u_view_matrix;" \
        "uniform mat4 u_rotation_matrix;" \
        "uniform mat4 u_projection_matrix;" \
        "uniform vec4 u_light0_position;" \
        "uniform vec4 u_light1_position;" \
        "uniform vec4 u_light2_position;" \
        "uniform mediump int u_lighting_enabled;" \
        "out vec3 transformed_normals;" \
        "out vec3 light0_direction;" \
        "out vec3 light1_direction;" \
        "out vec3 light2_direction;" \
        "out vec3 viewer_vector;" \
        "void main(void)" \
        "{" \
        "if (u_lighting_enabled == 1)" \
        "{" \
        "vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;" \
        "transformed_normals =  mat3(u_view_matrix * u_model_matrix * u_rotation_matrix) * vNormal;" \
        "light0_direction = vec3(u_light0_position) - eye_coordinates.xyz;" \
        "light1_direction = vec3(u_light1_position) - eye_coordinates.xyz;" \
        "light2_direction = vec3(u_light2_position) - eye_coordinates.xyz;" \
        "viewer_vector = -eye_coordinates.xyz;" \
        "}" \
        "gl_Position = u_projection_matrix  * u_view_matrix * u_model_matrix * u_rotation_matrix * vPosition;" \
        "}";
        
        glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
        
        // compile shader
        glCompileShader(gVertexShaderObject);
        GLint iInfoLogLength = 0;
        GLint iShaderCompiledStatus = 0;
        char *szInfoLog = NULL;
        glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if (iShaderCompiledStatus == GL_FALSE)
        {
            char *szInfoLog = NULL;
            glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("Vertex Shader Compilation Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self  release];
                    
                }
            }
        }
        
        //*** Fragment Shader ***
        //re-initialize
        iInfoLogLength = 0;
        iShaderCompiledStatus = 0;
        szInfoLog = NULL;
        
        // Create Shader
        gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        
        //Provide source code to shader
        const GLchar *fragmentShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "precision highp float;" \
        "in vec3 transformed_normals; " \
        "in vec3 light0_direction; " \
        "in vec3 light1_direction; " \
        "in vec3 light2_direction; " \
        "in vec3 viewer_vector; " \
        "out vec4 FragColor;" \
        "uniform vec3 u_La0;" \
        "uniform vec3 u_Ld0;" \
        "uniform vec3 u_Ls0;" \
        "uniform vec3 u_La1;" \
        "uniform vec3 u_Ld1;" \
        "uniform vec3 u_Ls1;" \
        "uniform vec3 u_La2;" \
        "uniform vec3 u_Ld2;" \
        "uniform vec3 u_Ls2;" \
        "uniform vec3 u_Ka;" \
        "uniform vec3 u_Kd;" \
        "uniform vec3 u_Ks;" \
        "uniform float u_material_shininess;" \
        "uniform int u_lighting_enabled;" \
        "void main(void)" \
        "{" \
        "vec3 light0;" \
        "vec3 light1;" \
        "vec3 light2;" \
        "vec3 phong_ads_color;" \
        "if (u_lighting_enabled == 1)" \
        "{" \
        "vec3 normalized_transformed_normals = normalize(transformed_normals);" \
        "vec3 normalized_light0_direction = normalize(light0_direction);" \
        "vec3 normalized_light1_direction = normalize(light1_direction);" \
        "vec3 normalized_light2_direction = normalize(light2_direction);" \
        "vec3 normalized_viewer_vector = normalize(viewer_vector);" \
        "float tn_dot_ld0 = max(dot(normalized_transformed_normals, normalized_light0_direction), 0.0);" \
        "vec3 ambient0 = u_La0 * u_Ka;" \
        "vec3 diffuse0 = u_Ld0 * u_Kd * tn_dot_ld0;" \
        "vec3 reflection_vector0 = reflect(-normalized_light0_direction,normalized_transformed_normals);" \
        "vec3 specular0 = u_Ls0 * u_Ks * pow(max(dot(reflection_vector0, normalized_viewer_vector), 0.0), u_material_shininess);" \
        "float tn_dot_ld1 = max(dot(normalized_transformed_normals, normalized_light1_direction), 0.0);" \
        "vec3 ambient1 = u_La1 * u_Ka;" \
        "vec3 diffuse1 = u_Ld1 * u_Kd * tn_dot_ld1;" \
        "vec3 reflection_vector1 = reflect(-normalized_light1_direction,normalized_transformed_normals);" \
        "vec3 specular1 = u_Ls1 * u_Ks * pow(max(dot(reflection_vector1, normalized_viewer_vector), 0.0), u_material_shininess);" \
        "float tn_dot_ld2 = max(dot(normalized_transformed_normals, normalized_light2_direction), 0.0);" \
        "vec3 ambient2 = u_La2 * u_Ka;" \
        "vec3 diffuse2 = u_Ld2 * u_Kd * tn_dot_ld2;" \
        "vec3 reflection_vector2 = reflect(-normalized_light2_direction,normalized_transformed_normals);" \
        "vec3 specular2 = u_Ls2 * u_Ks * pow(max(dot(reflection_vector2, normalized_viewer_vector), 0.0), u_material_shininess);" \
        "light0 = ambient0 + diffuse0 + specular0;" \
        "light1 = ambient1 + diffuse1 + specular1;" \
        "light2 = ambient2 + diffuse2 + specular2;" \
        "phong_ads_color = light0 + light1 + light2;" \
        "}" \
        "else" \
        "{" \
        "phong_ads_color =  vec3(1.0,  1.0,  1.0);" \
        "}" \
        "FragColor = vec4(phong_ads_color, 1.0); " \
        "}";
        
        glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
        
        // compile shader
        glCompileShader(gFragmentShaderObject);
        glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if (iShaderCompiledStatus == GL_FALSE)
        {
            char *szInfoLog = NULL;
            glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("Fragment Shader Compilation Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self  release];
                    
                }
            }
        }
        
        //Shader program
        //create
        gShaderProgramObject = glCreateProgram();
        
        //attach vertex shader to shader program
        glAttachShader(gShaderProgramObject, gVertexShaderObject);
        
        //attach fragment shader to shader program
        glAttachShader(gShaderProgramObject, gFragmentShaderObject);
        
        //Pre-link binding of shader program object with vertex shader position attribute
        glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");
        
        
        //Pre-link binding of shader program object with vertex shader Normal attribute
        glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");
        
        //Link shader
        glLinkProgram(gShaderProgramObject);
        GLint iShaderProgramLinkStatus = 0;
        glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
        if (iShaderProgramLinkStatus == GL_FALSE)
        {
            
            glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if (iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if (szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
                    printf("Shader Program Link Log : %s\n", szInfoLog);
                    free(szInfoLog);
                    [self  release];
                }
            }
        }
        
        //get MVP uniform location
        model_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
        view_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
        projection_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");
        rotation_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_rotation_matrix");
        
        L_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObject, "u_lighting_enabled");
        
        La0_uniform = glGetUniformLocation(gShaderProgramObject, "u_La0");
        Ld0_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld0");
        Ls0_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls0");
        light0_position_uniform = glGetUniformLocation(gShaderProgramObject, "u_light0_position");
        
        La1_uniform = glGetUniformLocation(gShaderProgramObject, "u_La1");
        Ld1_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld1");
        Ls1_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls1");
        light1_position_uniform = glGetUniformLocation(gShaderProgramObject, "u_light1_position");
        
        La2_uniform = glGetUniformLocation(gShaderProgramObject, "u_La2");
        Ld2_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld2");
        Ls2_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls2");
        light2_position_uniform = glGetUniformLocation(gShaderProgramObject, "u_light2_position");
        
        Ka_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ka");
        Kd_uniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");
        Ks_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ks");
        material_shininess_uniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");
        
        // *** vertices, Colors, Shader Attribs, vbo, vao initializations
        
        Sphere *sph = [[Sphere alloc]init];
        [sph getSphereVertexData: sphere_vertices : sphere_normals : sphere_textures : sphere_elements];
        gNumVertices = [sph getNumberOfSphereVertices];
        gNumElements = [sph getNumberOfSphereElements];
        
        
        
        
        //Vao sphere
        glGenVertexArrays(1, &gVao_sphere);  // recording of casatte
        glBindVertexArray(gVao_sphere);
        
        //vbo position
        glGenBuffers(1, &gVbo_sphere_position);
        glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
        glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
        glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);
        glBindBuffer(GL_ARRAY_BUFFER, 0); // passing zero is unbinding
        
        //vbo normal
        glGenBuffers(1, &gVbo_sphere_normal);
        glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_normal);
        glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
        glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);
        glBindBuffer(GL_ARRAY_BUFFER, 0); // passing zero is unbinding
        
        //vbo element
        glGenBuffers(1, &gVbo_sphere_element);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0); // passing zero is unbinding
        
        glBindVertexArray(0);
        
        light0_Ambient[0] = 0.0f;
        light0_Ambient[1] = 0.0f;
        light0_Ambient[2] = 0.0f;
        light0_Ambient[3] = 1.0f;
        
        light0_Diffuse[0] = 1.0f;  //RED Color
        light0_Diffuse[1] = 0.0f;
        light0_Diffuse[2] = 0.0f;
        light0_Diffuse[3] = 0.0f;
        
        light0_Specular[0] = 1.0f;
        light0_Specular[1] = 1.0f;
        light0_Specular[2] = 1.0f;
        light0_Specular[3] = 1.0f;
        
        light0_Position[0] = 200.0f;
        light0_Position[1] = 100.0f;
        light0_Position[2] = 100.0f;
        light0_Position[3] = 1.0f;
        
        //Light1
        light1_Ambient[0] = 0.0f;
        light1_Ambient[1] = 0.0f;
        light1_Ambient[2] = 0.0f;
        light1_Ambient[3] = 1.0f;
        
        light1_Diffuse[0] = 0.0f;  //Blue Color
        light1_Diffuse[1] = 0.0f;
        light1_Diffuse[2] = 1.0f;
        light1_Diffuse[3] = 0.0f;
        
        light1_Specular[0] = 1.0f;
        light1_Specular[1] = 1.0f;
        light1_Specular[2] = 1.0f;
        light1_Specular[3] = 1.0f;
        
        light1_Position[0] = 200.0f;
        light1_Position[1] = 100.0f;
        light1_Position[2] = 100.0f;
        light1_Position[3] = 1.0f;
        
        //Light2
        light2_Ambient[0] = 0.0f;
        light2_Ambient[1] = 0.0f;
        light2_Ambient[2] = 0.0f;
        light2_Ambient[3] = 1.0f;
        
        light2_Diffuse[0] = 0.0f;  //Green Color
        light2_Diffuse[1] = 1.0f;
        light2_Diffuse[2] = 0.0f;
        light2_Diffuse[3] = 0.0f;
        
        light2_Specular[0] = 1.0f;
        light2_Specular[1] = 1.0f;
        light2_Specular[2] = 1.0f;
        light2_Specular[3] = 1.0f;
        
        light2_Position[0] = 200.0f;
        light2_Position[1] = 100.0f;
        light2_Position[2] = 100.0f;
        light2_Position[3] = 1.0f;
        
        material_ambient[0] = 0.0f;
        material_ambient[1] = 0.0f;
        material_ambient[2] = 0.0f;
        material_ambient[3] = 1.0f;
        
        material_diffuse[0] = 1.0f;
        material_diffuse[1] = 1.0f;
        material_diffuse[2] = 1.0f;
        material_diffuse[3] = 1.0f;
        
        material_specular[0] = 1.0f;
        material_specular[1] = 1.0f;
        material_specular[2] = 1.0f;
        material_specular[3] = 1.0f;
        
        material_shininess = 50.0f;
        
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        // glEnable(GL_CULL_FACE);
        
        //set background  color
        glClearColor(0.0f,0.0f, 0.0f, 0.0f); //blue
        
        //set perpective matrix to identity matrix
        gPerspectiveProjectionMatrix = vmath::mat4::identity();
        
        red_x = 0.0f;
        red_y = 0.0f;
        red_z = 0.0f;
        
         green_x = 0.0f;
         green_y = 0.0f;
         green_z = 0.0f;
        
         blue_x = 0.0f;
         blue_y = 0.0f;
         blue_z = 0.0f;
        
         Xcenter = 0.0f;
         Ycenter = 0.0f;
         Zcenter = 0.0f;
        
         angleCircle = 0.0f;
        
        //GESTURE RECOGNITION
        //Tap gesture here
        UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1]; //touch of one  finger
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1]; //touch of one finger
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        //this will allow to differentiate between single tap and double tap
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        //swipe gesture
        UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        //long press gesture
        UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [self addGestureRecognizer:longPressGestureRecognizer];
        
        
        
    }
    return(self);
    
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 */
- (void)drawRect:(CGRect)rect
{
    //Drawing code
}

+(Class)layerClass
{
    //code
    return([CAEAGLLayer class]);
}

-(void)drawView:(id)sender
{
    //code
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
    
    // Strt using OpenGL program object
    glUseProgram(gShaderProgramObject);
    
    
    
    if (gbLight == true)
    {
        glUniform1i(L_KeyPressed_uniform, 1);
        
        glUniform3fv(La0_uniform, 1, light0_Ambient);
        glUniform3fv(Ld0_uniform, 1, light0_Diffuse);
        glUniform3fv(Ls0_uniform, 1, light0_Specular);
        glUniform4fv(light0_position_uniform, 1, light0_Position);
        
        glUniform3fv(La1_uniform, 1, light1_Ambient);
        glUniform3fv(Ld1_uniform, 1, light1_Diffuse);
        glUniform3fv(Ls1_uniform, 1, light1_Specular);
        glUniform4fv(light1_position_uniform, 1, light1_Position);
        
        glUniform3fv(La2_uniform, 1, light2_Ambient);
        glUniform3fv(Ld2_uniform, 1, light2_Diffuse);
        glUniform3fv(Ls2_uniform, 1, light2_Specular);
        glUniform4fv(light2_position_uniform, 1, light2_Position);
        
        glUniform3fv(Ka_uniform, 1, material_ambient);
        glUniform3fv(Kd_uniform, 1, material_diffuse);
        glUniform3fv(Ks_uniform, 1, material_specular);
        glUniform1f(material_shininess_uniform, material_shininess);
        
        //Rotate RED light to Y-direction
        light0_Position[0] = red_x;
        light0_Position[1] = red_y;
        light0_Position[2] = red_z;
        
        //Rotate Blue light to Z-direction
        light1_Position[0] = blue_x;
        light1_Position[1] = blue_y;
        light1_Position[2] = blue_z;
        
        //Rotate GREEN light to X-direction
        light2_Position[0] = green_x;
        light2_Position[1] = green_y;
        light2_Position[2] = green_z;
        
    }
    else
    {
        glUniform1i(L_KeyPressed_uniform, 0);
    }
    
    // OpenGL Drawing
    
    
    //Set model view and modelviewprojection matrices to identity
    vmath::mat4 modelMatrix = vmath::mat4::identity();
    vmath::mat4 viewMatrix = vmath::mat4::identity();
    vmath::mat4 RotationMatrix = vmath::mat4::identity();
    
    //****drawing*******
    
    //Translate model view matrix
    modelMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    
    RotationMatrix = vmath::rotate(angleCube, 0.0f, 0.0f, 0.0f);
    
    glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(rotation_matrix_uniform, 1, GL_FALSE, RotationMatrix);
    glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);
    
    
    //*** bind vao ***
    glBindVertexArray(gVao_sphere);
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    
    
    //*** unbind vao ***
    glBindVertexArray(0);
    
    if (gbAnimate == true)
        {
            // Circle equalation  Circle_X= Xcenter + rcosQ , Circle_X = Ycenter + rsinQ, Q = 0 to 360
            
            angleCircle = angleCircle - 0.1f;
            if (angleCircle >= 360.0f)
                angleCircle = 0.0f;
            
            red_x = Xcenter + 100.0f * cos(angleCircle);
            red_y = Ycenter;
            red_z = Zcenter + 100.0f * sin(angleCircle);
            
            green_x = Xcenter + 100.0f * sin(angleCircle);
            green_y = Ycenter + 100.0f * cos(angleCircle);
            green_z = Zcenter;
            
            blue_x = Xcenter;
            blue_y = Ycenter + 100.0f * sin(angleCircle);
            blue_z = Zcenter + 100.0f * cos(angleCircle);
        }
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
}

-(void)layoutSubviews
{
    //code
    GLint width;
    GLint height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER,  GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER,  GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
    
    glViewport(0, 0, width, height);
    // GLfloat fwidth = (GLfloat)width;
    // GLfloat fheight = (GLfloat)height;
    
    //glOrtho(left, right, bottom, top,  near, far)
    
    //Perspective projection matrix
    gPerspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
    
    /*
     if (width <= height)
     gOrthographicProjectionMatrix = vmath::ortho((-100.0f * (fwidth / fheight)), (100.0f * (fwidth / fheight)), -100.0f, 100.0f, -100.0f, 100.0f);
     else
     gOrthographicProjectionMatrix = vmath::ortho(-100.0f, 100.0f, (-100.0f * (width / height)), (100.0f * (width / height)),
     -100.0f, 100.0f);
     */
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!= GL_FRAMEBUFFER_COMPLETE)
    {
        printf("Failed to create complete framebuffer object %x", glCheckFramebufferStatus(GL_FRAMEBUFFER));
        
    }
    
    [self drawView:nil];
}

-(void)startAnimation
{
    if(!isAnimating)
    {
        displayLink = [NSClassFromString(@"CADisplayLink")displayLinkWithTarget:self selector:@selector(drawView:)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        isAnimating=YES;
    }
}

-(void)stopAnimation
{
    if(isAnimating)
    {
        [displayLink invalidate];
        displayLink = nil;
        
        isAnimating = NO;
        
    }
    
}

// to become first responder
-(BOOL)acceptsFirstResponder
{
    //code
    return(YES);
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}

-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    
    if (bIsAKeyPressed == false)
    {
        gbAnimate = true;
        bIsAKeyPressed = true;
    }
    else
    {
        gbAnimate = false;
        bIsAKeyPressed = false;
    }
    
    
}

-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    
    
    
    if (bIsLKeyPressed == false)
    {
        gbLight = true;
        bIsLKeyPressed = true;
    }
    else
    {
        gbLight = false;
        bIsLKeyPressed = false;
    }
    
}

-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    //code
    [self release];
    exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    
}


- (void)dealloc
{
    
    if (gVao_sphere)
    {
        glDeleteVertexArrays(1, &gVao_sphere);
        gVao_sphere = 0;
    }
    
    
    // Destroy vbo_position
    if (gVbo_sphere_position)
    {
        glDeleteVertexArrays(1, &gVbo_sphere_position);
        gVbo_sphere_position = 0;
    }
    
    // Destroy vbo_normal
    if (gVbo_sphere_normal)
    {
        glDeleteVertexArrays(1, &gVbo_sphere_normal);
        gVbo_sphere_normal = 0;
    }
    
    // Destroy vbo_element
    if (gVbo_sphere_element)
    {
        glDeleteVertexArrays(1, &gVbo_sphere_element);
        gVbo_sphere_element = 0;
    }
    
    //Detach vertex shader from shader program object
    glDetachShader(gShaderProgramObject, gVertexShaderObject);
    
    //Detach fragment shader from shader program object
    glDetachShader(gShaderProgramObject, gFragmentShaderObject);
    
    
    
    // Delete vertex shader object
    glDeleteShader(gVertexShaderObject);
    gVertexShaderObject = 0;
    
    // Delete fragment shader object
    glDeleteShader(gFragmentShaderObject);
    gFragmentShaderObject = 0;
    
    // Delete shader program object
    glDeleteShader(gShaderProgramObject);
    gShaderProgramObject = 0;
    
    
    if(depthRenderbuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderbuffer);
        depthRenderbuffer =  0;
    }
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderbuffer);
        colorRenderbuffer =  0;
    }
    
    if(defaultFramebuffer)
    {
        glDeleteFramebuffers(1, &defaultFramebuffer);
        defaultFramebuffer =  0;
    }
    
    if([EAGLContext currentContext]==eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    
    [eaglContext release];
    eaglContext =  nil;
    
    [super dealloc];
    
}


@end

