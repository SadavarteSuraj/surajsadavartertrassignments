
#include<windows.h>
#include<gl/GL.h>
#include<gl/GLU.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")

//Prototype of wndproc declared globally

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global varible declaration
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

GLboolean bPyramid = GL_TRUE;
GLboolean bCube = GL_FALSE;
GLboolean bSphere = GL_FALSE;


GLfloat angleTri = 0.0f;
GLfloat angleRect = 0.0f;
GLfloat angleSphere = 0.0f;

//Function declaration
void drawPyramid(void);
void drawCube(void);
void drawSphere(void);
void update(void);

GLUquadric *quadric = NULL;

//Array declaration
//Light0
GLfloat light0_ambient[] = { 0.0f, 0.0f, 0.0f, 0.0f };
GLfloat light0_diffuse[] = { 1.0f, 0.0f, 0.0f, 0.0f };
GLfloat light0_specular[] = { 1.0f, 0.0f, 0.0f, 0.0f };
GLfloat light0_position[] = { 2.0f, 1.0f, 1.0f, 0.0f };

//Light1
GLfloat light1_ambient[] = { 0.0f, 0.0f, 0.0f, 0.0f };
GLfloat light1_diffuse[] = { 0.0f, 0.0f, 1.0f, 0.0f };
GLfloat light1_specular[] = { 0.0f, 0.0f, 1.0f, 0.0f };
GLfloat light1_position[] = { -2.0f, 1.0f, 1.0f, 0.0f };

//Material
GLfloat material_ambient[] = { 0.0f, 0.0f, 0.0f, 0.0f };
GLfloat material_diffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat material_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat material_shininess[] = { 50.0f };

//Function declaration

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

bool gLight = false;

//main
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Function prototype
	void initialize(void);
	void uninitialize(void);
	void display();

	//Variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("RTROPENGL");
	bool bDone = false;

	//code
	//Initialize members of struct WNDCLASS
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;

	//Registering class
	RegisterClassEx(&wndclass);

	//Create Window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("MSOGLTEMPLATE"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0,
		0,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	//Initialize
	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Messge loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
				display();
			}
		}
	}

	uninitialize();
	return((int)msg.wParam);
}

//WndProc
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//Function prototype
	//void display(void);
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	/*case WM_PAINT:
		display();
		break;*/
	/*case WM_ERASEBKGND:
		return(0);*/
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x46: //For 'f' or 'F'
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;
		case 0x4C: //For 'l' or 'L'
			if (gLight == false)
			{
				gLight = true;
				glEnable(GL_LIGHTING);
				
			}
			else
			{
				gLight = false;
				glDisable(GL_LIGHTING);
				
			}
			break;
		case 0x50: //For 'p' or 'P' -  Pyramid
			bPyramid = GL_TRUE;
			bCube = GL_FALSE;
			bSphere = GL_FALSE;
			break;
		case 0x43: //For 'c' or 'C' - Cube
			bCube = GL_TRUE;
			bPyramid = GL_FALSE;
			bSphere = GL_FALSE;
		break;
		case 0x53: //For 's' or 'S' - Sphere
			bSphere = GL_TRUE;
			bPyramid = GL_FALSE;
			bCube = GL_FALSE;
		break;
		default:
			break;
		}
	case WM_LBUTTONDOWN:
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));

}

void ToggleFullscreen(void)
{
	//Variable declaration
	MONITORINFO mi;

	//code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);

			}
		}
		ShowCursor(FALSE);

	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

}

void initialize(void)
{
	//function prototype
	void resize(int, int);

	//variable declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	//Initialization of structure PIXELFORMATESCRIPTOR
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL|PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;

	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	glClearDepth(1.0f); // range is 0 to 1
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glLightfv(GL_LIGHT0, GL_AMBIENT, light0_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light0_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light0_specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light0_position);
	

	glLightfv(GL_LIGHT1, GL_AMBIENT, light1_ambient);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, light1_diffuse);
	glLightfv(GL_LIGHT1, GL_SPECULAR, light1_specular);
	glLightfv(GL_LIGHT1, GL_POSITION, light1_position);
	

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	//Enable light
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	
	resize(WIN_WIDTH, WIN_HEIGHT);

}

void display(void)
{
	//code
	if (bPyramid == GL_TRUE)
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		update();
		drawPyramid();
	}
	else if (bCube == GL_TRUE)
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glMatrixMode(GL_MODELVIEW); // Rest Model view matrix
		glLoadIdentity();
		update();
		drawCube();
	}
	else if (bSphere == GL_TRUE)
	{
		
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); 
		glMatrixMode(GL_MODELVIEW); // Rest Model view matrix
		glLoadIdentity();
		update();
		drawSphere();
	}

	//glFlush();
	SwapBuffers(ghdc);
}


void resize(int width, int height)
{
	//code
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width/(GLfloat)height, 0.1f, 100.0f); 
	
}

void uninitialize(void)
{
	//UNINITIALIZE CODE
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;

	if (quadric)
	{
		gluDeleteQuadric(quadric);
	}

}


void drawPyramid(void)
{
	//glTranslatef(-2.0f, 0.0f, -7.0f);
	glTranslatef(0.0f, 0.0f, -5.0f);
	glRotatef(angleTri, 0.0f, 1.0f, 0.0f);
	glBegin(GL_TRIANGLES);

	//Front face
	glNormal3f(0.0f, 0.447214f, 0.894427f);   // glNormal perpendicular to surface
	//glColor3f(1.0f, 0.0f, 0.0f);     // RED
	glVertex3f(0.0f, 1.0f, 0.0f);    // APEX point A
	//glColor3f(0.0f, 1.0f, 0.0f);     //Green
	glVertex3f(-1.0f, -1.0f, 1.0f);  // Point P
	//glColor3f(0.0f, 0.0f, 1.0f);     // Blue
	glVertex3f(1.0f, -1.0f, 1.0f);   //Point Q

	//Right face
	glNormal3f(0.894427f, 0.447214f, 0.0f);
	//glColor3f(1.0f, 0.0f, 0.0f);    // RED
	glVertex3f(0.0f, 1.0f, 0.0f);  // APEX point A
	//glColor3f(0.0f, 0.0f, 1.0f);   // Blue
	glVertex3f(1.0f, -1.0f, 1.0f);  // Point Q
	//glColor3f(0.0f, 1.0f, 0.0f);   // Green
	glVertex3f(1.0f, -1.0f, -1.0f); // Point R

	//Back face
	glNormal3f(0.0f, 0.447214f, -0.894427f);
	//glColor3f(1.0f, 0.0f, 0.0f);  // RED
	glVertex3f(0.0f, 1.0f, 0.0f);  // APEX point A
	//glColor3f(0.0f, 1.0f, 0.0f);  // Green
	glVertex3f(1.0f, -1.0f, -1.0f);  // Point R
	//glColor3f(0.0f, 0.0f, 1.0f); // Blue
	glVertex3f(-1.0f, -1.0f, -1.0f); // Point S

	 //Left face
	glNormal3f(-0.894427f, 0.447214f, 0.0f);
	//glColor3f(1.0f, 0.0f, 0.0f);  // RED
	glVertex3f(0.0f, 1.0f, 0.0f); // APEX point A
	//glColor3f(0.0f, 0.0f, 1.0f);  // Blue
	glVertex3f(-1.0f, -1.0f, -1.0f); // Point S
	//glColor3f(0.0f, 1.0f, 0.0f);  // Green
	glVertex3f(-1.0f, -1.0f, 1.0f); // Point P
	
	glEnd();
}

void drawCube(void)
{
	glTranslatef(0.0f, 0.0f, -4.0f);
	glScalef(0.75f, 0.75f, 0.75f);
	glRotatef(angleRect, 0.0f, 1.0f, 0.0f);

	glBegin(GL_QUADS);

	//Front face
	//glColor3f(1.0f, 0.0f, 0.0f); // Red Color
	glNormal3f(0.0f, 0.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	//Back face
	//glColor3f(0.0f, 1.0f, 0.0f); // Green Color
	glNormal3f(0.0f, 0.0f, -1.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	//Top face
	//glColor3f(0.0f, 0.0f, 1.0f); // Blue Color
	glNormal3f(0.0f, 1.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	//Bottom Face
	//glColor3f(0.0f, 1.0f, 1.0f); // Cyan Color
	glNormal3f(0.0f, -1.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	//Right face
	//glColor3f(1.0f, 0.0f, 1.0f); // Magneta Color
	glNormal3f(1.0f, 0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);


	//Left face
	//glColor3f(1.0f, 1.0f, 0.0f); // Yellow Color
	glNormal3f(-1.0f, 0.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);


	glEnd();
}

void drawSphere(void)
{
	glTranslatef(0.0f, 0.0f, -3.0f);
	glRotatef(angleSphere, 0.0f, 1.0f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	//glColor3f(1.0f, 1.0f, 0.0f);
	gluSphere(quadric, 0.75, 30, 30);
	//gluSphere(quadric, 0.75, 100, 100);
}

void update(void)
{
	angleTri = angleTri + 0.02f;
	if (angleTri >= 360.0f)
		angleTri = 0.0f;

	angleRect = angleRect + 0.02f;
	if (angleRect >= 360.0f)
		angleRect = 0.0f;

	angleSphere = angleSphere + 0.02f;
	if (angleSphere >= 360.0f)
		angleSphere = 0.0f;
}