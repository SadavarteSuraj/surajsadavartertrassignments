Program - Multicolor triangle

Changes from triangle perspective
1. Change vbo to vbo_Position

2. Declare vbo_color

3.  Vertex shader
"in vec4 vColor;" \
"out vec4 out_Color;" \
main {"out_Color = vColor;" \ }

4. Fragment shader
"in vec4 out_Color;" \
main { "FragColor = out_Color;" \ }

5. Bind color atttrib location

6. Declare triangle  color array

7. vbo_position - six statement

8. vbo_Color - six statement

8. uninitialize vbo_color