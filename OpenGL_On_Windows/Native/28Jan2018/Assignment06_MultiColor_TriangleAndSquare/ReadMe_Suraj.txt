Program - Assignment06_MultiColor_TriangleAndSquare

Copy Assignment_BlacknWhite_TriangleAndSquare
1. Change Vbo to vbo_position and declare vbo_color
2. Write color part in vertex shader and fragment shader for triangle 
3. Declare triangle color array
4. bind it to triangle
5. give color to square by using glVertexAttrib3f(VDG_ATTRIBUTE_COLOR, 0.39f, 0.53f, 0.96f); , another method of giving color, no need to declare array here.
6. Display - triangle code 
7. display - square code 
8. uninitialize both vbo_position and vbo_color