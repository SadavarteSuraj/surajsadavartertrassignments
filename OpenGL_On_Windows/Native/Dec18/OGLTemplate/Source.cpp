#include<windows.h>

#include<gl/GL.h>
#include<gl/GLU.h>

#include<stdio.h>
#include<stdlib.h>

#include<vector>

//Symbolic constants
#define TRUE 1
#define FALSE 0

#define BUFFER_SIZE 256		// Maximum length of string in mesh file
#define S_EQUAL 0			//Return value of strcmp when strings are equal

#define WIN_INIT_X 100
#define WIN_INIT_Y 100

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#define VK_F 0x46
#define VK_f 0x60

#define NR_POINT_COORDS 3		// Number of point Coordinates
#define NR_TEXTURE_COORDS 2		// Number of Texture Coordinates
#define NR_NORMAL_COORDS 3		// Number of Normal Coordinates
#define NR_FACE_TOKENS 3		// Minimum number of entries in face data
#define FOY_ANGLE 45			// Field of view in Y direction
#define ZNEAR 45				// Distance from viewer to near plane of viewing volume
#define ZFAR 200				// Distance from viewer to far plane of viewing volume

#define VIEWPORT_BOTTOMLEFT_X 0	// X coordinate of bottom left point of viewport rectangle
#define VIEWPORT_BOTTOMLEFT_Y 0 // Y coordinate of bottom left point of viewport rectangle

#define MONKEYHEAD_X_TRANSLATE 0.0f // X-translation of Monkeyhead
#define MONKEYHEAD_Y_TRANSLATE 0.0f // Y-translation of Monkeyhead
#define MONKEYHEAD_Z_TRANSLATE -5.0f // Z-translation of Monkeyhead

#define MONKEYHEAD_X_SCALE_FACTOR 1.5f // X-scale factor of Monkeyhead
#define MONKEYHEAD_Y_SCALE_FACTOR 1.5f // Y-scale factor of Monkeyhead
#define MONKEYHEAD_Z_SCALE_FACTOR 1.5f // Z-scale factor of Monkeyhead

#define START_ANGLE_POS 0.0f // Marks begining angle position of rotation
#define END_ANGLE_POS 360.0f // Marks terminating angle position of rotation
#define MONKEYHEAD_ANGLE_INCREMENT 1.0f // Increment angle for Monkeyhead

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "kernel32.lib")

//Error handling macros
#define ERRORBOX1(lpszErrorMessage, lpszCaption) {Messagebox(HWND)NULL, TEXT(lpszErrorMessage), TEXT(lpszCaption), MB_ICONERROR, ExitProcess(EXIT_FAILURE);}
#define ERRORBOX2(lpszErrorMessage, lpszCaption) {Messagebox(HWND)NULL, TEXT(lpszErrorMessage), TEXT(lpszCaption), MB_ICONERROR, DestroyWindow(hwnd);}

//Prototype of wndproc declared globally

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global varible declaration
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;


//Function declaration
void LoadMeshData(void);
void update(void);

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

GLfloat g_rotate;

bool gLight = false;

GLfloat light_ambient[] = { 0.25f, 0.25f, 0.25f, 1.0f };
GLfloat light_diffused[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat light_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat light_position[] = { 2.0f, 0.0f, 0.0f, 1.0f };

GLfloat material_diffused[] = { 0.3f, 0.2f, 0.0f, 1.0f };
GLfloat material_specular[] = { 0.4f, 0.2f, 0.0f, 1.0f };
GLfloat material_shininess[] = { 100.0f };


//Vector of vector of floats to hold vertex data
std::vector<std::vector<float>> g_vertices;

//Vector of vector of floats to hold texture data
std::vector<std::vector<float>> g_texture;

//Vector of vector of floats to hold normal data
std::vector<std::vector<float>> g_normals;

//Vector of vector of int to hold index data in g_vertices
std::vector<std::vector<int>> g_face_tri, g_face_texture, g_face_normals;

//Handle to a mesh file
FILE *g_fp_meshfile = NULL;

//Handle to a mesh file
FILE *g_fp_logfile = NULL;

// Hold line in a file
char line[BUFFER_SIZE];


//main
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Function prototype
	void initialize(void);
	void uninitialize(void);
	void display();
	void update(void);

	//Variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("RTROPENGL");
	bool bDone = false;

	//code
	//Initialize members of struct WNDCLASS
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;

	//Registering class
	RegisterClassEx(&wndclass);

	//Create Window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("MSOGLTemplate"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0,
		0,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	//Initialize
	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Messge loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
				display();
			}
		}
	}

	uninitialize();
	return((int)msg.wParam);
}

//WndProc
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//Function prototype
	//void display(void);
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
		/*case WM_PAINT:
		display();
		break;*/
		/*case WM_ERASEBKGND:
		return(0);*/
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x46: //For 'f' or 'F'
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;
		case 0x4C:  //for 'L'
			if (gLight == true)
			{
				glDisable(GL_LIGHTING);
				gLight = false;
			}
			else
			{
				glEnable(GL_LIGHTING);
				gLight = true;
			}
			break;
		default:
			break;
		}
	case WM_LBUTTONDOWN:
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));

}

void ToggleFullscreen(void)
{
	//Variable declaration
	MONITORINFO mi;

	//code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);

			}
		}
		ShowCursor(FALSE);

	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

}

void initialize(void)
{
	//function prototype
	void resize(int, int);
	void uninitialize(void);
	void LoadMeshData(void);

	//CODE
	g_fp_logfile = fopen("MONKEYHEADLOADER.LOG", "w");
	if (!g_fp_logfile)
		uninitialize();

	//variable declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	//Initialization of structure PIXELFORMATESCRIPTOR
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;

	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	glClearDepth(1.0f); // range is 0 to 1
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffused);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);

	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_specular);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	glEnable(GL_LIGHT0);

	LoadMeshData();

	resize(WIN_WIDTH, WIN_HEIGHT);

}

void display(void)
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(MONKEYHEAD_X_TRANSLATE, MONKEYHEAD_Y_TRANSLATE, MONKEYHEAD_Z_TRANSLATE);
	glRotatef(g_rotate, 0.0f, 1.0f, 0.0f);
	glScalef(MONKEYHEAD_X_SCALE_FACTOR, MONKEYHEAD_Y_SCALE_FACTOR, MONKEYHEAD_Z_SCALE_FACTOR);

	// Keep counter closewise winding of vertices of geometry
	glFrontFace(GL_CCW);

	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINES);

	// S1 - For every face index maintained in triangular form in g_face_tri
	// Do following
	// S2 -  Set geometry primitive to GL_TRIANGLES
	//  S3 - Extract triangle from outer loop index
	//  For every point of triangle
	// S4 - calculate the index in g_vertices, in variable vi
	// S5 - calculate X Y Z coordinates of point
	// S6 - send to vertex3f
	// Remark : In S4, we have to substract g_face_tri[i][j] by one because
	// in mesh file indexing starts from 1 and in case of arrays/ vectors indexing start from 0
	for (int i = 0; i != g_face_tri.size(); ++i) //S1
	{
		glBegin(GL_TRIANGLES);  //S2
		for (int j = 0; j != g_face_tri[i].size(); j++)  //S3
		{
			if (g_face_normals.size() > i && g_face_normals[i].size() > j)
			{
				int ni = g_face_normals[i][j] - 1;
				if (g_normals.size() > ni)
				{
					glNormal3f(g_normals[ni][0], g_normals[ni][1], g_normals[ni][2]);
				}
			}
			int vi = g_face_tri[i][j] - 1; //S4
			glVertex3f(g_vertices[vi][0], g_vertices[vi][1], g_vertices[vi][2]); //S5, S6
		}
		glEnd();
	}

	update();

	SwapBuffers(ghdc);
}

void resize(int width, int height)
{
	//code
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

}

void LoadMeshData(void)
{
	void uninitialize(void);

	//Open mesh file, name of mesh file can be parameterized
	g_fp_meshfile = fopen("MonkeyHead.obj", "r");
	if (!g_fp_meshfile)
		uninitialize();

	//Separator strings
	//string holding space separator for strtok
	char *sep_space = " ";
	//String holding forward slash for strtok
	char *sep_fslash = "/";

	//Token pointers
	// character pointer for holding first word in line 
	char *first_token = NULL;

	// character pointer for holding next word separated by specified separator to strtok
	char *token = NULL;

	//Array of character pointer to hold strings of face entries
	//Face entries can be variable. In some files they are three and in some they are four.
	char *face_tokens[NR_FACE_TOKENS];
	//Number of non-null tokens in above vector
	int nr_tokens;

	// character pointer for holding string associatef with vector index
	char *token_vertex_index = NULL;
	// character pointer for holding string associatef with texture index
	char *token_texture_index = NULL;
	// character pointer for holding string associatef with Normal index
	char *token_normal_index = NULL;

	//while there is line in a file
	while (fgets(line, BUFFER_SIZE, g_fp_meshfile) != NULL)
	{
		//bind line to a separator and get first token
		first_token = strtok(line, sep_space);

		//if first token indicates vertex data
		if (strcmp(first_token, "v") == S_EQUAL)
		{
			//Create a vector of NR_POINTS_COORDS of floats to hold the coordinates
			std::vector<float> vec_point_coord(NR_POINT_COORDS);

			//Do following NR_POINT_COORDS times
			//S1. Get next token
			//S2. Feed it to atof to get floating point number out of it
			//S3. Add following point number generated to vector
			// End of loop
			//S4. At the end of loop vector is constructed, add it to global vector of vector of floats, named g_vertices
			for (int i = 0; i != NR_POINT_COORDS; i++)
				vec_point_coord[i] = atof(strtok(NULL, sep_space)); //S1 S2 S3
			g_vertices.push_back(vec_point_coord);  //S4

		}

		else if (strcmp(first_token, "vt") == S_EQUAL)
		{

			std::vector<float> vec_texture_coord(NR_TEXTURE_COORDS);

			//Do following NR_TEXTURE_COORDS times
			//S1. Get next token
			//S2. Feed it to atof to get floating point number out of it
			//S3. Add following point number generated to vector
			// End of loop
			//S4. At the end of loop vector is constructed, add it to global vector of vector of floats, named g_vertices
			for (int i = 0; i != NR_TEXTURE_COORDS; i++)
				vec_texture_coord[i] = atof(strtok(NULL, sep_space)); //S1 S2 S3
			g_texture.push_back(vec_texture_coord);

		}
		//If first token indicates normal data
		else if (strcmp(first_token, "vn") == S_EQUAL)
		{

			std::vector<float> vec_normal_coord(NR_NORMAL_COORDS);

			//Do following NR_NORMAL_COORDS times
			//S1. Get next token
			//S2. Feed it to atof to get floating point number out of it
			//S3. Add following point number generated to vector
			// End of loop
			//S4. At the end of loop vector is constructed, add it to global vector of vector of floats, named g_vertices
			for (int i = 0; i != NR_NORMAL_COORDS; i++)
				vec_normal_coord[i] = atof(strtok(NULL, sep_space)); //S1 S2 S3
			g_normals.push_back(vec_normal_coord);

		}

		//If first token indicates face data
		else if (strcmp(first_token, "f") == S_EQUAL)
		{

			//Define three vector of integers with length 3 to hold indices of 
			//triangle's position coordinates, texture coordinates, and normal coordinates
			//in g_vertices, g_texture and g_normal resp.

			std::vector<int> triangle_vertex_indices(3), texture_vertex_indices(3), normal_vertex_indices(3);

			//Initialize all char pointers in face_tokens to NULL
			memset((void*)face_tokens, 0, NR_FACE_TOKENS);

			//Extract three fields of information in face_tokens
			//and increment nr_tokens accordingly
			nr_tokens = 0;
			while (token = strtok(NULL, sep_space))
			{
				if (strlen(token) < 3)
					break;
				face_tokens[nr_tokens] = token;
				nr_tokens++;
			}

			//Every face data entry is going to have minimum three fields
			// therefore construct a triangle out of it with
			// S1 - triangle coordinate data and 
			// S2 - texture coordinate index data
			// S3 - normal coordinate index data
			// S4 - put that data in triangle_vertex_indices, texture_vertex_indices, normal_vertex_indices. 
			// vectors will be constructed at the end of loop

			for (int i = 0; i != NR_FACE_TOKENS; i++)
			{

				token_vertex_index = strtok(face_tokens[i], sep_fslash); //S1
				token_texture_index = strtok(NULL, sep_fslash);			//S2
				token_normal_index = strtok(NULL, sep_fslash);  //S3
				triangle_vertex_indices[i] = atoi(token_vertex_index);  // S4-1
				texture_vertex_indices[i] = atoi(token_texture_index);  // S4-2
				normal_vertex_indices[i] = atoi(token_normal_index);  // S4-3

			}

			// Add constructed vector to global space vector
			g_face_tri.push_back(triangle_vertex_indices);
			g_face_texture.push_back(texture_vertex_indices);
			g_face_normals.push_back(normal_vertex_indices);

		}
		//Initialize line buffer to NULL
		memset((void*)line, (int)'\0', BUFFER_SIZE);

	}

	//Close mesh file and make file pointer null
	fclose(g_fp_meshfile);
	g_fp_meshfile = NULL;

	//Log vertex texture and face data in log file
	fprintf(g_fp_logfile, "g_vertices:%llu g_texture:%llu g_normals:%llu g_face_tri:%llu\n",
		g_vertices.size(), g_texture.size(), g_normals.size(), g_face_tri.size());

	
}



void uninitialize(void)
{
	//UNINITIALIZE CODE
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;

	//Close log file and make file pointer NULL
	fclose(g_fp_logfile);
	g_fp_logfile = NULL;

}


void update(void)
{
	// Increment monkeyhead rotation angle by MONKEYHEAD_ANGLE_INCREMENT
	g_rotate = g_rotate + MONKEYHEAD_ANGLE_INCREMENT;
	// If rotation angle euqals or exceeds END_ANGLE_POS then
	// reset to START_ANGLE_POS
	if (g_rotate >= END_ANGLE_POS)
		g_rotate = START_ANGLE_POS;

}