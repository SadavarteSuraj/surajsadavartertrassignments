#include<windows.h>
#include<gl/GL.h>
#include<gl/GLU.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")

//Prototype of wndproc declared globally

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global varible declaration 
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

//Function declaration
void object1(void);
void object2(void);
void object3(void);
void object4(void);
void object5(void);
void object6(void);

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

//main
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Function prototype
	void initialize(void);
	void uninitialize(void);
	void display();

	//Variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("RTROPENGL");
	bool bDone = false;

	//code
	//Initialize members of struct WNDCLASS
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;

	//Registering class
	RegisterClassEx(&wndclass);

	//Create Window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("MSOGLTemplate"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0,
		0,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	//Initialize
	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Messge loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
				display();
			}
		}
	}

	uninitialize();
	return((int)msg.wParam);
}

//WndProc
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//Function prototype
	//void display(void);
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	/*case WM_PAINT:
		display();
		break;*/
	/*case WM_ERASEBKGND:
		return(0);*/
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x46: //For 'f' or 'F'
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;
		default:
			break;
		}
	case WM_LBUTTONDOWN:
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));

}

void ToggleFullscreen(void)
{
	//Variable declaration
	MONITORINFO mi;

	//code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);

			}
		}
		ShowCursor(FALSE);

	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

}

void initialize(void)
{
	//function prototype
	void resize(int, int);

	//variable declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	//Initialization of structure PIXELFORMATESCRIPTOR
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL|PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;

	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	resize(WIN_WIDTH, WIN_HEIGHT);

}

void display(void)
{
	//code
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	object1();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	object2();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	object3();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	object4();
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	object5();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	object6();

	SwapBuffers(ghdc);
}

void resize(int width, int height)
{
	//code
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width/(GLfloat)height, 0.1f, 100.0f); 
	
	
}

void uninitialize(void)
{
	//UNINITIALIZE CODE
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;

}

void object1(void)
{
	glTranslatef(-6.0f, 3.0f, -18.0f);
	glPointSize(2);
	glBegin(GL_POINTS);
	glColor3f(1.0f, 1.0f, 1.0f);

	glVertex3f(0.5f, 0.5f, 0.0f); //9
	glVertex3f(1.5f, 0.5f, 0.0f); //13
	glVertex3f(-0.5f, 0.5f, 0.0f); //5
	glVertex3f(-1.5f, 0.5f, 0.0f); //1
	
	glVertex3f(0.5f, -0.5f, 0.0f); //10
	glVertex3f(1.5f, -0.5f, 0.0f); //14
	glVertex3f(-0.5f, -0.5f, 0.0f); //6
	glVertex3f(-1.5f, -0.5f, 0.0f); //2
	
	glVertex3f(0.5f, 1.5f, 0.0f); //8
	glVertex3f(1.5f, 1.5f, 0.0f); //12
	glVertex3f(-0.5f, 1.5f, 0.0f); //4
	glVertex3f(-1.5f, 1.5f, 0.0f); //0

	glVertex3f(0.5f, -1.5f, 0.0f); //11
	glVertex3f(1.5f, -1.5f, 0.0f); //15
	glVertex3f(-0.5f, -1.5f, 0.0f); //7
	glVertex3f(-1.5f, -1.5f, 0.0f); //3

	glEnd();
}

void object2(void)
{
	glTranslatef(0.0f, 3.0f, -18.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);

	glVertex3f(-1.5f, 1.5f, 0.0f); //0
	glVertex3f(1.5f, 1.5f, 0.0f); //12

	glVertex3f(-1.5f, 0.5f, 0.0f); //1
	glVertex3f(1.5f, 0.5f, 0.0f); //13

	glVertex3f(-1.5f, -0.5f, 0.0f); //2
	glVertex3f(1.5f, -0.5f, 0.0f); //14

	glVertex3f(-1.5f, 1.5f, 0.0f); //0
	glVertex3f(-1.5f, -1.5f, 0.0f); //3

	glVertex3f(-0.5f, 1.5f, 0.0f); //4
	glVertex3f(-0.5f, -1.5f, 0.0f); //7

	glVertex3f(0.5f, 1.5f, 0.0f); //8
	glVertex3f(0.5f, -1.5f, 0.0f); //11

	glVertex3f(-1.5f, 0.5f, 0.0f); //1
	glVertex3f(-0.5f, 1.5f, 0.0f); //4

	glVertex3f(-1.5f, -0.5f, 0.0f); //2
	glVertex3f(0.5f, 1.5f, 0.0f); //8

	glVertex3f(-1.5f, -1.5f, 0.0f); //3
	glVertex3f(1.5f, 1.5f, 0.0f); //12

	glVertex3f(-0.5f, -1.5f, 0.0f); //7
	glVertex3f(1.5f, 0.5f, 0.0f); //13

	glVertex3f(0.5f, -1.5f, 0.0f); //11
	glVertex3f(1.5f, -0.5f, 0.0f); //14
		

	glEnd();
}

void object3(void)
{
	glTranslatef(6.0f, 3.0f, -18.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);

	glVertex3f(-1.5f, 1.5f, 0.0f); //0
	glVertex3f(1.5f, 1.5f, 0.0f); //12

	glVertex3f(-1.5f, 0.5f, 0.0f); //1
	glVertex3f(1.5f, 0.5f, 0.0f); //13

	glVertex3f(-1.5f, -0.5f, 0.0f); //2
	glVertex3f(1.5f, -0.5f, 0.0f); //14

	glVertex3f(-1.5f, -1.5f, 0.0f); //3
	glVertex3f(1.5f, -1.5f, 0.0f); //15

	glVertex3f(-1.5f, 1.5f, 0.0f); //0
	glVertex3f(-1.5f, -1.5f, 0.0f); //3

	glVertex3f(-0.5f, 1.5f, 0.0f); //4
	glVertex3f(-0.5f, -1.5f, 0.0f); //7

	glVertex3f(0.5f, 1.5f, 0.0f); //8
	glVertex3f(0.5f, -1.5f, 0.0f); //11

	glVertex3f(1.5f, 1.5f, 0.0f); //12
	glVertex3f(1.5f, -1.5f, 0.0f); //15

	glEnd();
}

void object4(void)
{
	glTranslatef(-6.0f, -3.0f, -18.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);

	glVertex3f(-1.5f, 1.5f, 0.0f); //0
	glVertex3f(1.5f, 1.5f, 0.0f); //12

	glVertex3f(-1.5f, 0.5f, 0.0f); //1
	glVertex3f(1.5f, 0.5f, 0.0f); //13

	glVertex3f(-1.5f, -0.5f, 0.0f); //2
	glVertex3f(1.5f, -0.5f, 0.0f); //14

	glVertex3f(-1.5f, -1.5f, 0.0f); //3
	glVertex3f(1.5f, -1.5f, 0.0f); //15

	glVertex3f(-1.5f, 1.5f, 0.0f); //0
	glVertex3f(-1.5f, -1.5f, 0.0f); //3

	glVertex3f(-0.5f, 1.5f, 0.0f); //4
	glVertex3f(-0.5f, -1.5f, 0.0f); //7

	glVertex3f(0.5f, 1.5f, 0.0f); //8
	glVertex3f(0.5f, -1.5f, 0.0f); //11

	glVertex3f(1.5f, 1.5f, 0.0f); //12
	glVertex3f(1.5f, -1.5f, 0.0f); //15

	glVertex3f(-1.5f, 0.5f, 0.0f); //1
	glVertex3f(-0.5f, 1.5f, 0.0f); //4

	glVertex3f(-1.5f, -0.5f, 0.0f); //2
	glVertex3f(0.5f, 1.5f, 0.0f); //8

	glVertex3f(-1.5f, -1.5f, 0.0f); //3
	glVertex3f(1.5f, 1.5f, 0.0f); //12

	glVertex3f(-0.5f, -1.5f, 0.0f); //7
	glVertex3f(1.5f, 0.5f, 0.0f); //13

	glVertex3f(0.5f, -1.5f, 0.0f); //11
	glVertex3f(1.5f, -0.5f, 0.0f); //14


	glEnd();
}

void object5(void)
{
	glTranslatef(0.0f, -3.0f, -18.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);

	glVertex3f(-1.5f, 1.5f, 0.0f); //0
	glVertex3f(1.5f, 1.5f, 0.0f); //12

	glVertex3f(-1.5f, -1.5f, 0.0f); //3
	glVertex3f(1.5f, -1.5f, 0.0f); //15

	glVertex3f(-1.5f, 1.5f, 0.0f); //0
	glVertex3f(-1.5f, -1.5f, 0.0f); //3

	glVertex3f(1.5f, 1.5f, 0.0f); //12
	glVertex3f(1.5f, -1.5f, 0.0f); //15

	glVertex3f(-1.5f, 1.5f, 0.0f); //0
	glVertex3f(-0.5f, -1.5f, 0.0f); //7

	glVertex3f(-1.5f, 1.5f, 0.0f); //0
	glVertex3f(0.5f, -1.5f, 0.0f); //11

	glVertex3f(-1.5f, 1.5f, 0.0f); //0
	glVertex3f(1.5f, -1.5f, 0.0f); //15

	glVertex3f(-1.5f, 1.5f, 0.0f); //0
	glVertex3f(1.5f, -0.5f, 0.0f); //14

	glVertex3f(-1.5f, 1.5f, 0.0f); //0
	glVertex3f(1.5f, 0.5f, 0.0f); //13

	glEnd();
}

void object6(void)
{
	glTranslatef(6.0f, -3.0f, -18.0f);
	glBegin(GL_QUADS);
	
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-1.5f, 1.5f, 0.0f); //0
	glVertex3f(-1.5f, -1.5f, 0.0f); //3
	glVertex3f(-0.5f, -1.5f, 0.0f); //7
	glVertex3f(-0.5f, 1.5f, 0.0f); //4
	
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-0.5f, 1.5f, 0.0f); //4
	glVertex3f(-0.5f, -1.5f, 0.0f); //7
	glVertex3f(0.5f, -1.5f, 0.0f); //11
	glVertex3f(0.5f, 1.5f, 0.0f); //8

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(0.5f, 1.5f, 0.0f); //8
	glVertex3f(0.5f, -1.5f, 0.0f); //11
	glVertex3f(1.5f, -1.5f, 0.0f); //15
	glVertex3f(1.5f, 1.5f, 0.0f); //12
	
	glEnd();

	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);

	glVertex3f(-1.5f, 0.5f, 0.0f); //1
	glVertex3f(1.5f, 0.5f, 0.0f); //13

	glVertex3f(-1.5f, -0.5f, 0.0f); //2
	glVertex3f(1.5f, -0.5f, 0.0f); //14

	glVertex3f(-0.5f, 1.5f, 0.0f); //4
	glVertex3f(-0.5f, -1.5f, 0.0f); //7

	glVertex3f(0.5f, 1.5f, 0.0f); //8
	glVertex3f(0.5f, -1.5f, 0.0f); //11

	glEnd();
}