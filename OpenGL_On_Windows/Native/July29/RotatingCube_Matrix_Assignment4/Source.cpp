#include<windows.h>
#include<gl/GL.h>
#include<gl/GLU.h>


#define WIN_WIDTH 800
#define WIN_HEIGHT 600
#define _USE_MATH_DEFINES 

#include<Math.h>


#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")

//Prototype of wndproc declared globally

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global varible declaration
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

GLfloat angleRadian = 0.0f;
GLfloat angleDegree = 0.0f;

// Matrix arrays
GLfloat identityMatrix[16];
GLfloat translateMatrix[16];
GLfloat scaleMatrix[16];
GLfloat x_rotationMatrix[16];
GLfloat y_rotationMatrix[16];
GLfloat z_rotationMatrix[16];

//Function declaration
void drawCube(void);
void update(void);

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

//main
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Function prototype
	void initialize(void);
	void uninitialize(void);
	void display();

	//Variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("RTROPENGL");
	bool bDone = false;

	//code
	//Initialize members of struct WNDCLASS
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;

	//Registering class
	RegisterClassEx(&wndclass);

	//Create Window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("MSOGLTemplate"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0,
		0,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	//Initialize
	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Messge loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
				display();
			}
		}
	}

	uninitialize();
	return((int)msg.wParam);
}

//WndProc
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//Function prototype
	//void display(void);
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
		/*case WM_PAINT:
		display();
		break;*/
		/*case WM_ERASEBKGND:
		return(0);*/
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x46: //For 'f' or 'F'
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;
		default:
			break;
		}
	case WM_LBUTTONDOWN:
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));

}

void ToggleFullscreen(void)
{
	//Variable declaration
	MONITORINFO mi;

	//code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);

			}
		}
		ShowCursor(FALSE);

	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

}

void initialize(void)
{
	//function prototype
	void resize(int, int);

	//variable declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	//Initialize Matrix values

	//Identity Matrix
	identityMatrix[0] = 1.0f;
	identityMatrix[1] = 0.0f;
	identityMatrix[2] = 0.0f;
	identityMatrix[3] = 0.0f;
	identityMatrix[4] = 0.0f;
	identityMatrix[5] = 1.0f;
	identityMatrix[6] = 0.0f;
	identityMatrix[7] = 0.0f;
	identityMatrix[8] = 0.0f;
	identityMatrix[9] = 0.0f;
	identityMatrix[10] = 1.0f;
	identityMatrix[11] = 0.0f;
	identityMatrix[12] = 0.0f;
	identityMatrix[13] = 0.0f;
	identityMatrix[14] = 0.0f;
	identityMatrix[15] = 1.0f;

	// Translate Matrix
	translateMatrix[0] = 1.0f;
	translateMatrix[1] = 0.0f;
	translateMatrix[2] = 0.0f;
	translateMatrix[3] = 0.0f;
	translateMatrix[4] = 0.0f;
	translateMatrix[5] = 1.0f;
	translateMatrix[6] = 0.0f;
	translateMatrix[7] = 0.0f;
	translateMatrix[8] = 0.0f;
	translateMatrix[9] = 0.0f;
	translateMatrix[10] = 1.0f;
	translateMatrix[11] = 0.0f;
	translateMatrix[12] = 0.0f;  // Tx
	translateMatrix[13] = 0.0f;  // Ty
	translateMatrix[14] = -5.0f;  // Tz
	translateMatrix[15] = 1.0f;

	//Scale Matrix
	scaleMatrix[0] = 0.75f;  // Sx
	scaleMatrix[1] = 0.0f;
	scaleMatrix[2] = 0.0f;
	scaleMatrix[3] = 0.0f;
	scaleMatrix[4] = 0.0f;
	scaleMatrix[5] = 0.75f; // Sy
	scaleMatrix[6] = 0.0f;
	scaleMatrix[7] = 0.0f;
	scaleMatrix[8] = 0.0f;
	scaleMatrix[9] = 0.0f;
	scaleMatrix[10] = 0.75f; // Sz
	scaleMatrix[11] = 0.0f;
	scaleMatrix[12] = 0.0f;
	scaleMatrix[13] = 0.0f;
	scaleMatrix[14] = 0.0f;
	scaleMatrix[15] = 1.0f;
	

	//Initialization of structure PIXELFORMATESCRIPTOR
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;

	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	glClearDepth(1.0f); // range is 0 to 1
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	
	resize(WIN_WIDTH, WIN_HEIGHT);

}

void display(void)
{
	
	
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	//glLoadIdentity();
	glLoadMatrixf(identityMatrix);
	glMultMatrixf(translateMatrix);
	glMultMatrixf(scaleMatrix);
	glMultMatrixf(x_rotationMatrix);
	glMultMatrixf(y_rotationMatrix);
	glMultMatrixf(z_rotationMatrix);

	update();
	//Initialize rotation matrix

	// x_rotation matrix
	x_rotationMatrix[0] = 1.0f;
	x_rotationMatrix[1] = 0.0f;
	x_rotationMatrix[2] = 0.0f;
	x_rotationMatrix[3] = 0.0f;
	x_rotationMatrix[4] = 0.0f; 
	x_rotationMatrix[5] = cos(angleRadian);
	x_rotationMatrix[6] = sin(angleRadian);
	x_rotationMatrix[7] = 0.0f;
	x_rotationMatrix[8] = 0.0f;
	x_rotationMatrix[9] = -sin(angleRadian);
	x_rotationMatrix[10] = cos(angleRadian);
	x_rotationMatrix[11] = 0.0f;
	x_rotationMatrix[12] = 0.0f;
	x_rotationMatrix[13] = 0.0f;
	x_rotationMatrix[14] = 0.0f;
	x_rotationMatrix[15] = 1.0f;

	// y_rotation matrix
	y_rotationMatrix[0] = cos(angleRadian);
	y_rotationMatrix[1] = 0.0f;
	y_rotationMatrix[2] = -sin(angleRadian);
	y_rotationMatrix[3] = 0.0f;
	y_rotationMatrix[4] = 0.0f;
	y_rotationMatrix[5] = 1.0f;
	y_rotationMatrix[6] = 0.0f;
	y_rotationMatrix[7] = 0.0f;
	y_rotationMatrix[8] = sin(angleRadian);
	y_rotationMatrix[9] = 0.0f;
	y_rotationMatrix[10] = cos(angleRadian);
	y_rotationMatrix[11] = 0.0f;
	y_rotationMatrix[12] = 0.0f;
	y_rotationMatrix[13] = 0.0f;
	y_rotationMatrix[14] = 0.0f;
	y_rotationMatrix[15] = 1.0f;

	// z_rotation matrix
	z_rotationMatrix[0] = cos(angleRadian);
	z_rotationMatrix[1] = sin(angleRadian);
	z_rotationMatrix[2] = 0.0f;
	z_rotationMatrix[3] = 0.0f;
	z_rotationMatrix[4] = -sin(angleRadian);
	z_rotationMatrix[5] = cos(angleRadian);
	z_rotationMatrix[6] = 0.0f;
	z_rotationMatrix[7] = 0.0f;
	z_rotationMatrix[8] = 0.0f;
	z_rotationMatrix[9] = 0.0f;
	z_rotationMatrix[10] = 1.0f;
	z_rotationMatrix[11] = 0.0f;
	z_rotationMatrix[12] = 0.0f;
	z_rotationMatrix[13] = 0.0f;
	z_rotationMatrix[14] = 0.0f;
	z_rotationMatrix[15] = 1.0f;

	drawCube();

	//glFlush();
	SwapBuffers(ghdc);
}

void resize(int width, int height)
{
	//code
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

}

void uninitialize(void)
{
	//UNINITIALIZE CODE
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;

}

void drawCube(void)
{
	//glTranslatef(0.0f, 0.0f, -5.0f);
	//glRotatef(angleRadian, 1.0f, 1.0f, 1.0f);
	
	glBegin(GL_QUADS);

	//Front face
	glColor3f(1.0f, 0.0f, 0.0f); // Red Color
	glVertex3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	//Back face
	glColor3f(0.0f, 1.0f, 0.0f); // Green Color
	glVertex3f(1.0f, 1.0f, -1.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	//Top face
	glColor3f(0.0f, 0.0f, 1.0f); // Blue Color
	glVertex3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);

	//Bottom Face
	glColor3f(0.0f, 1.0f, 1.0f); // Cyan Color
	glVertex3f(1.0f, -1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	//Right face
	glColor3f(1.0f, 0.0f, 1.0f); // Magneta Color
	glVertex3f(1.0f, 1.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	

	//Left face
	glColor3f(1.0f, 1.0f, 0.0f); // Yellow Color
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	
	
	glEnd();
}


void update(void)
{
	angleDegree = angleDegree + 0.01f;
	angleRadian = angleDegree * (M_PI / 180.0f);
		if (angleDegree >= 360.0f)
		angleDegree = 0.0f;

}