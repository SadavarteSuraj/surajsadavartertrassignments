Changes from Orthographic triangle
1. Change orthographic to perspective 
2. Change triangle array from 50 to 1
3. declare perspective matrix
4. Resize -> change ortho to perspective
5. Display -> Translate model view matrix