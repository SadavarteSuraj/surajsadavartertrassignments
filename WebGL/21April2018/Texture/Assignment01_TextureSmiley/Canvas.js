//===3D Rotation ===

//global variable
var canvas = null;
var gl = null; //WebGL context
var bFullscreen=false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros= // when whole 'WebGLMacros' is 'const' , all inside it are automatically const
{
	VDG_ATTRIBUTE_VERTEX:0,
	VDG_ATTRIBUTE_COLOR:1,
	VDG_ATTRIBUTE_NORMAL:2,
	VDG_ATTRIBUTE_TEXTURE0:3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;	
				   
var vao_pyramid;
var vao_cube;
var vbo_position;
var vbo_texture;
var mvpUniform;

var perspectiveProjectionMatrix;

var cube_texture=0;
var uniform_texture0_sampler;

var anglePyramid = 0.0;
var angleCube = 0.0;
var anglePyramidRadian = 0.0;
var angleCubeRadian = 0.0;

var requestAnimationFrame =
window.requestAnimationFrame||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame;

var cancelAnimationFrame = 
window.cancelAnimationFrame||
window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
window.mozCancelRequestAnimationFrame ||window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

//onload function
function main()
{
	//get canvas element
	canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining canvas failed\n");
	else
		console.log("Obtaining canvas successed\n");
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
	
		
	//register keyboard's keydown event handler
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);
	
	//initialize WebGL
	init();
	
	//Start drawing here as warming-up
	resize();
	draw();
	//update();
}

function toggleFullScreen()
{
	//code
	var fullscreen_element =
	document.fullscreenElement ||
	document.webkitFullscreenElement ||
	document.mozFullScreenElement ||
	document.msFullscreenElement ||
	null;
	
	//if not fullscreen
	if(fullscreen_element == null)
	{
	if(canvas.requestFullscreen)
		canvas.requestFullscreen();
	else if(canvas.mozRequestFullScreen)
		canvas.mozRequestFullScreen();
	else if(canvas.webkitRequestFullscreen)
		canvas.webkitRequestFullScreen();
	else if(canvas.msRequestFullscreen)
		canvas.msRequestFullscreen();
	bFullscreen = true;
	}
	else // if already fullscreen
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		bFullscreen = false;
	}
}

function init()
{
	//code
	//get WebGL 2.0 context
	gl = canvas.getContext("webgl2");
	if(gl==null) //failed to get context
	{
		console.log("Failed to get rendering  context for WebGL");
		return;
	}
	gl.viewportWidth =  canvas.width;
	gl.viewportHeight = canvas.height;
	
	//vertex shader
	var vertexShaderSourceCode = 
	"#version 300 es"+
	"\n"+
	"in vec4 vPosition;"+
	"in vec2 vTexture0_Coord;"+
	"out vec2 out_texture0_coord;"+
	"uniform mat4 u_mvp_matrix;"+
	"void main(void)"+
	"{"+
	"gl_Position =  u_mvp_matrix * vPosition;"+
	"out_texture0_coord = vTexture0_Coord;"+
	"}";
	
	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS)==false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	
	var fragmentShaderSourceCode = 
	"#version 300 es"+
	"\n"+
	"precision highp float;"+
	"in vec2 out_texture0_coord;"+
	"uniform  highp sampler2D u_texture0_sampler;"+
	"out vec4 FragColor;"+
	"void main(void)"+
	"{"+
	"FragColor = texture(u_texture0_sampler, out_texture0_coord);"+
	"}";
	
	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS)==false)
	{
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	//Shader program
	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);
	
	//Pre-link binding of shader program object with vertex shader attributes
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.VDG_ATTRIBUTE_TEXTURE0, "vTexture0_Coord");
	
	//linking
	gl.linkProgram(shaderProgramObject);
	if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	

		// Load cube texture
	cube_texture = gl.createTexture();
	cube_texture.image = new Image();
	cube_texture.image.src = "Smiley.png";
	cube_texture.image.onload = function ()
	{
		gl.bindTexture(gl.TEXTURE_2D,  cube_texture);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL,  true);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA,  gl.RGBA,  gl.UNSIGNED_BYTE, cube_texture.image);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		gl.bindTexture(gl.TEXTURE_2D,  null);
	}
	
	//get MVP uniform location
	mvpUniform=gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");
	
	uniform_texture0_sampler = gl.getUniformLocation(shaderProgramObject, "u_texture0_sampler");
	
	// *** Vertices, colors, shader attributes, vbo, vao initializations ***
	var pyramidVertices = new Float32Array([
											//front face
											0.0, 1.0, 0.0,
											-1.0, -1.0, 1.0,
											1.0, -1.0, 1.0,

											//Right face
											0.0, 1.0, 0.0,
											1.0, -1.0, 1.0,
											1.0, -1.0, -1.0,

											//Back face
											0.0, 1.0, 0.0,
											1.0, -1.0, -1.0,
											-1.0, -1.0, -1.0,

											//Let face
											0.0, 1.0, 0.0,
											-1.0, -1.0, -1.0,
											-1.0, -1.0, 1.0,
											]);
											
											
	var pyramidTexcoords = new Float32Array([
											0.5, 1.0,
											0.0, 0.0,
											1.0, 0.0, 

											0.5, 1.0,
											1.0, 0.0,
											0.0, 0.0,
											
											0.5, 1.0,
											1.0, 0.0,
											0.0, 0.0,
											
											0.5, 1.0,
											0.0, 0.0,
											1.0, 0.0,
											]);	
	
	var cubeVertices = new Float32Array([
											1.0, 1.0, 0.0,
											-1.0, 1.0, 0.0,
											-1.0, -1.0, 0.0,
											1.0, -1.0, 0.0,
										]);
											
											
	var cubeTexcoords = new Float32Array([
											
											1.0, 1.0,
											0.0, 1.0,
											0.0, 0.0,
											1.0, 0.0,
										]);
											
	//vao_pyramid										
	vao_pyramid = gl.createVertexArray();
	gl.bindVertexArray(vao_pyramid);
	
	//pyramid vbo position
	vbo_position = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position);
	gl.bufferData(gl.ARRAY_BUFFER, pyramidVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_VERTEX,
							3, //3 is for X,  Y, Z coordinates in our triangleVertices array
							gl.FLOAT,
							false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	//pyramid vbo texture
	vbo_texture = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_texture);
	gl.bufferData(gl.ARRAY_BUFFER, pyramidTexcoords, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_TEXTURE0,
							2, // 2 is for S and T coordinates in our pyramidTexcoords array
							gl.FLOAT,
							false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_TEXTURE0);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	//vao_cube
	vao_cube = gl.createVertexArray();
	gl.bindVertexArray(vao_cube);

	//Cube vbo position
	vbo_position = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position);
	gl.bufferData(gl.ARRAY_BUFFER, cubeVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_VERTEX,
							3, //3 is for X,  Y, Z coordinates in our triangleVertices array
							gl.FLOAT,
							false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	//cube vbo texture
	vbo_texture = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_texture);
	gl.bufferData(gl.ARRAY_BUFFER, cubeTexcoords, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_TEXTURE0,
							2, // 2 is for S and T coordinates in our pyramidTexcoords array
							gl.FLOAT,
							false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_TEXTURE0);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	gl.bindVertexArray(null);
	
		gl.clearDepth(1.0); // range is 0 to 1
		gl.enable(gl.DEPTH_TEST);
		gl.depthFunc(gl.LEQUAL);
		//gl.shadeModel(gl.SMOOTH);
		//gl.hint(gl.PERSPECTIVE_CORRECTION_HINT, gl.NICEST);
	
	//Set clear color
	gl.clearColor(0.0, 0.0, 0.0, 1.0); //Blue
	
	//initialize projection matrix
	perspectiveProjectionMatrix = mat4.create();
	
}

function  resize()
{
	//code
	if(bFullscreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}
	
	//set the viewport to match
	gl.viewport(0, 0, canvas.width, canvas.height);
	
	mat4.perspective(perspectiveProjectionMatrix, 45.0, (parseFloat)(canvas.width)/ (parseFloat)(canvas.height), 0.1, 100.0);
	
}


function draw()
{	
	//code
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	
	gl.useProgram(shaderProgramObject);
	
	var modelViewMatrix = mat4.create();
	var modelViewProjectionMatrix = mat4.create();

	
	
			
	//*** Cube drawing ***
	
	//Set matrix to identity
	mat4.identity(modelViewMatrix);
	mat4.identity(modelViewProjectionMatrix);
	
	//Translate
	mat4.translate(modelViewMatrix, modelViewMatrix, [0,  0.0,  -3.0]);
	
	//mat4.scale(modelViewMatrix, modelViewMatrix, [0.75,  0.75,  0.75]);

	angleCubeRadian = degToRad(angleCube); //converts degree to radian
	
	//mat4.rotateX(modelViewMatrix, modelViewMatrix, angleCubeRadian); // X rotation
	
//	mat4.rotateY(modelViewMatrix, modelViewMatrix, angleCubeRadian); // Y rotation
	
	//mat4.rotateZ(modelViewMatrix, modelViewMatrix, angleCubeRadian); // Z rotation
	
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix,  modelViewMatrix);
	
	gl.uniformMatrix4fv(mvpUniform,  false,  modelViewProjectionMatrix);
	
	gl.bindVertexArray(vao_cube);
	
	//bind with texture
	gl.bindTexture(gl.TEXTURE_2D, cube_texture);
	gl.uniform1i(uniform_texture0_sampler,0);	
	
	gl.drawArrays(gl.TRIANGLE_FAN,  0, 4);
		
	gl.bindVertexArray(null);
	
	gl.useProgram(null);
	
	//update();
	
	//Animation loop
	requestAnimationFrame(draw, canvas);

	
	
}

function uninitialize()
{
	//code
	if(vao_pyramid)
	{
		gl.deleteVertexArray(vao_pyramid);
		vao_pyramid = null;
	}
	
	//code
	if(vao_cube)
	{
		gl.deleteVertexArray(vao_cube);
		vao_cube = null;
	}
	
	if(vbo_position)
	{
		gl.deleteBuffer(vbo_position);
		vbo_position = null;
	}
	
	
	if(vbo_texture)
	{
		gl.deleteBuffer(vbo_texture);
		vbo_texture = null;
	}
	
	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}
		
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}
		
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
		
	}
	
}
function keyDown(event)
{
		//code
		switch(event.keyCode)
		{
			case 27: //Escape
				//uninitialize
				uninitialize();
				//Close our application's tab
				window.close(); //Works is Safari and chrome but not in Firefox
				break;
			case 70: //for 'f' or 'F'
				toggleFullScreen();
				break;
			
						
		}
		
}
	
function mouseDown()
{
		//code
		
}




function update()
{
	anglePyramid = anglePyramid + 0.5;
	if (anglePyramid >= 360.0)
		anglePyramid = 0.0;

	
	
	angleCube = angleCube - 0.5;
	if (angleCube <= -360.0)
		angleCube = 0.0;
	
	

}

function degToRad(degree)
{
	//code
	
	radian =(degree * Math.PI) / 180.0 // PI = 3.14159
	
	return radian;
}	
