//=== PerVertex PerFragment Light ===

//global variable
var canvas = null;
var gl = null; //WebGL context
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

var gbLight = false;

const WebGLMacros= // when whole 'WebGLMacros' is 'const' , all inside it are automatically const
{
	VDG_ATTRIBUTE_VERTEX:0,
	VDG_ATTRIBUTE_COLOR:1,
	VDG_ATTRIBUTE_NORMAL:2,
	VDG_ATTRIBUTE_TEXTURE0:3,
};

var vertexShaderObject1;
var fragmentShaderObject1;
var shaderProgramObject1;

var vertexShaderObject2;
var fragmentShaderObject2;
var shaderProgramObject2;
				   
var vao_pyramid;
var vao_cube;
var vbo_position;
var vbo_normal;
var mvpUniform;

var perspectiveProjectionMatrix;

var modelMatrixUniform, viewMatrixUniform, projectionMatrixUniform;
var laUniform, ldUniform, lsUniform, lightPositionUniform;
var kaUniform, kdUniform, ksUniform, materialshininessUniform;
var LKeyPressedUniform;

var angleCube = 0.0;

var bLKeyPressed = false;

var light_ambient = [0.0,0.0,0.0];
var light_diffuse = [1.0,1.0,1.0];
var light_specular = [1.0,1.0,1.0];
var light_position = [100.0,100.0,100.0,1.0];

var material_ambient = [0.0,0.0,0.0];
var material_diffuse = [1.0,1.0,1.0];
var material_specular = [1.0,1.0,1.0];
var material_shininess = 50.0;

var sphere = null;

var angleCube = 0.0;
var anglePyramidRadian = 0.0;
var angleCubeRadian = 0.0;

var gbPerVertexLight = true;
var gbPerFragmentLight = false;

var requestAnimationFrame =
window.requestAnimationFrame||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame;

var cancelAnimationFrame = 
window.cancelAnimationFrame||
window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
window.mozCancelRequestAnimationFrame ||window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

//onload function
function main()
{
	//get canvas element
	canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining canvas failed\n");
	else
		console.log("Obtaining canvas successed\n");
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
	
		
	//register keyboard's keydown event handler
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);
	
	//initialize WebGL
	init();
	
	//Start drawing here as warming-up
	resize();
	draw();
	//update();
}

function toggleFullScreen()
{
	//code
	var fullscreen_element =
	document.fullscreenElement ||
	document.webkitFullscreenElement ||
	document.mozFullScreenElement ||
	document.msFullscreenElement ||
	null;
	
	//if not fullscreen
	if(fullscreen_element == null)
	{
	if(canvas.requestFullscreen)
		canvas.requestFullscreen();
	else if(canvas.mozRequestFullScreen)
		canvas.mozRequestFullScreen();
	else if(canvas.webkitRequestFullscreen)
		canvas.webkitRequestFullScreen();
	else if(canvas.msRequestFullscreen)
		canvas.msRequestFullscreen();
	bFullscreen = true;
	}
	else // if already fullscreen
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		bFullscreen = false;
	}
}

function init()
{
	//code
	//get WebGL 2.0 context
	gl = canvas.getContext("webgl2");
	if(gl==null) //failed to get context
	{
		console.log("Failed to get rendering  context for WebGL");
		return;
	}
	gl.viewportWidth =  canvas.width;
	gl.viewportHeight = canvas.height;

    //vertex shader PerVertex
	var vertexShaderSourceCode1 =
	        "#version 300 es" +
            "\n" +
		   	"in vec4 vPosition;" +
			"in vec3 vNormal;" +
			"uniform mat4 u_model_matrix;" +
			"uniform mat4 u_view_matrix;" +
			"uniform mat4 u_projection_matrix;" +
			"uniform mediump int u_LKeyPressed;" +
			"uniform vec3 u_La;" +
			"uniform vec3 u_Ld;" +
			"uniform vec3 u_Ls;" +
			"uniform vec4 u_light_position;" +
			"uniform vec3 u_Ka;" +
			"uniform vec3 u_Kd;" +
			"uniform vec3 u_Ks;" +
			"uniform float u_material_shininess;" +
			"out vec3 phong_ads_color;" +
			"void main(void)" +
			"{" +
			"if (u_LKeyPressed == 1)" +
			"{" +
			"vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;" +
			"vec3 transformed_normals =  normalize(mat3(u_view_matrix * u_model_matrix ) * vNormal);" +
			"vec3 light_direction = normalize(vec3(u_light_position) - eye_coordinates.xyz);" +
			"float tn_dot_ld = max(dot(transformed_normals, light_direction), 0.0);" +
			"vec3 ambient = u_La * u_Ka;" +
			"vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;" +
			"vec3 reflection_vector = reflect(-light_direction,transformed_normals);" +
			"vec3 viewer_vector = normalize(-eye_coordinates.xyz);" +
			"vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, viewer_vector), 0.0), u_material_shininess);" +
			"phong_ads_color = ambient + diffuse + specular;" +
			"}" +
			"else" +
			"{" +
			"phong_ads_color = vec3(1.0, 1.0, 1.0);" +
			"}" +
			"gl_Position = u_projection_matrix  * u_view_matrix * u_model_matrix * vPosition;" +
			"}";

	vertexShaderObject1 = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject1, vertexShaderSourceCode1);
	gl.compileShader(vertexShaderObject1);
	if (gl.getShaderParameter(vertexShaderObject1, gl.COMPILE_STATUS) == false) {
	    var error = gl.getShaderInfoLog(vertexShaderObject1);
	    if (error.length > 0) {
	        alert(error);
	        uninitialize();
	    }
	}


	var fragmentShaderSourceCode1 =
	        "#version 300 es" +
	        "\n" +
			"precision highp float;" +
			"in vec3 phong_ads_color;" +
			"out vec4 FragColor;" +
			"void main(void)" +
			"{" +
			"FragColor = vec4(phong_ads_color, 1.0);" +
			"}";

	fragmentShaderObject1 = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject1, fragmentShaderSourceCode1);
	gl.compileShader(fragmentShaderObject1);
	if (gl.getShaderParameter(fragmentShaderObject1, gl.COMPILE_STATUS) == false) {
	    if (error.length > 0) {
	        alert(error);
	        uninitialize();
	    }
	}

    //Shader program
	shaderProgramObject1 = gl.createProgram();
	gl.attachShader(shaderProgramObject1, vertexShaderObject1);
	gl.attachShader(shaderProgramObject1, fragmentShaderObject1);

    //Pre-link binding of shader program object with vertex shader attributes
	gl.bindAttribLocation(shaderProgramObject1, WebGLMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject1, WebGLMacros.VDG_ATTRIBUTE_NORMAL, "vNormal");

    //linking
	gl.linkProgram(shaderProgramObject1);
	if (!gl.getProgramParameter(shaderProgramObject1, gl.LINK_STATUS)) {
	    var error = gl.getProgramInfoLog(shaderProgramObject1);
	    if (error.length > 0) {
	        alert(error);
	        uninitialize();
	    }
	}



    //get MVP uniform location
    //	mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");
	modelMatrixUniform = gl.getUniformLocation(shaderProgramObject1, "u_model_matrix");
	viewMatrixUniform = gl.getUniformLocation(shaderProgramObject1, "u_view_matrix");
	projectionMatrixUniform = gl.getUniformLocation(shaderProgramObject1, "u_projection_matrix");

	LKeyPressedUniform = gl.getUniformLocation(shaderProgramObject1, "u_LKeyPressed");

	laUniform = gl.getUniformLocation(shaderProgramObject1, "u_La");
	ldUniform = gl.getUniformLocation(shaderProgramObject1, "u_Ld");
	lsUniform = gl.getUniformLocation(shaderProgramObject1, "u_Ls");
	lightPositionUniform = gl.getUniformLocation(shaderProgramObject1, "u_light_position");


	kaUniform = gl.getUniformLocation(shaderProgramObject1, "u_Ka");
	kdUniform = gl.getUniformLocation(shaderProgramObject1, "u_Kd");
	ksUniform = gl.getUniformLocation(shaderProgramObject1, "u_Ks");
	materialShininessUniform = gl.getUniformLocation(shaderProgramObject1, "u_material_shininess");
	
	//vertex shader PerFragment
	var vertexShaderSourceCode2 = 
	        "#version 300 es" +
            "\n"+
		   "in vec4 vPosition;" +
			"in vec3 vNormal;" +
			"uniform mat4 u_model_matrix;" +
			"uniform mat4 u_view_matrix;" +
			"uniform mat4 u_projection_matrix;" +
			"uniform mediump int u_LKeyPressed;" +
			"uniform vec4 u_light_position;" +
			"out vec3 transformed_normals;" +
			"out vec3 light_direction;" +
			"out vec3 viewer_vector;" +
			"void main(void)" +
			"{" +
			"if (u_LKeyPressed == 1)" +
			"{" +
			"vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;" +
			"transformed_normals =  normalize(mat3(u_view_matrix * u_model_matrix ) * vNormal);" +
			"light_direction = normalize(vec3(u_light_position) - eye_coordinates.xyz);" +
			"viewer_vector = -eye_coordinates.xyz;" +
			"}" +

			"gl_Position = u_projection_matrix  * u_view_matrix * u_model_matrix * vPosition;" +
			"}";
	
	vertexShaderObject2 = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject2, vertexShaderSourceCode2);
	gl.compileShader(vertexShaderObject2);
	if(gl.getShaderParameter(vertexShaderObject2, gl.COMPILE_STATUS)==false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject2);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	
	var fragmentShaderSourceCode2 = 
	        "#version 300 es"+
	        "\n"+
			"precision highp float;" +
			"in vec3 transformed_normals;" +
			"in vec3 light_direction;" +
			"in vec3 viewer_vector;" +
			"out vec4 FragColor;" +
			"uniform vec3 u_La;" +
			"uniform vec3 u_Ld;" +
			"uniform vec3 u_Ls;" +
			"uniform vec3 u_Ka;" +
			"uniform vec3 u_Kd;" +
			"uniform vec3 u_Ks;" +
			"uniform float u_material_shininess;" +
			"uniform mediump int u_LKeyPressed;" +
			"void main(void)" +
			"{" +
			"vec3 phong_ads_color;" +
			"if(u_LKeyPressed == 1)" +
			"{" +
			"vec3 normalized_transformed_normals = normalize(transformed_normals);" +
			"vec3 normalized_light_direction = normalize(light_direction);" +
			"vec3 normalized_viewer_vector = normalize(viewer_vector);" +
			"float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction), 0.0);" +
			"vec3 ambient = u_La * u_Ka;" +
			"vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;" +
			"vec3 reflection_vector = reflect(-normalized_light_direction,normalized_transformed_normals);" +
			"vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);" +
			"phong_ads_color = ambient + diffuse + specular;" +
			"}" +
			"else" +
			"{" +
			"phong_ads_color =  vec3(1.0,  1.0,  1.0);" +
			"}" +
			"FragColor = vec4(phong_ads_color, 1.0);" +
			"}";
	
	fragmentShaderObject2 = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject2, fragmentShaderSourceCode2);
	gl.compileShader(fragmentShaderObject2);
	if(gl.getShaderParameter(fragmentShaderObject2, gl.COMPILE_STATUS)==false)
	{
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	//Shader program
	shaderProgramObject2 = gl.createProgram();
	gl.attachShader(shaderProgramObject2, vertexShaderObject2);
	gl.attachShader(shaderProgramObject2, fragmentShaderObject2);
	
	//Pre-link binding of shader program object with vertex shader attributes
	gl.bindAttribLocation(shaderProgramObject2, WebGLMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject2, WebGLMacros.VDG_ATTRIBUTE_NORMAL, "vNormal");
	
	//linking
	gl.linkProgram(shaderProgramObject2);
	if(!gl.getProgramParameter(shaderProgramObject2, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject2);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	
	
	//get MVP uniform location
//	mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");
	modelMatrixUniform = gl.getUniformLocation(shaderProgramObject2, "u_model_matrix");
	viewMatrixUniform = gl.getUniformLocation(shaderProgramObject2, "u_view_matrix");
	projectionMatrixUniform = gl.getUniformLocation(shaderProgramObject2, "u_projection_matrix");
    
	LKeyPressedUniform = gl.getUniformLocation(shaderProgramObject2, "u_LKeyPressed");

	laUniform = gl.getUniformLocation(shaderProgramObject2, "u_La");
	ldUniform = gl.getUniformLocation(shaderProgramObject2, "u_Ld");
	lsUniform = gl.getUniformLocation(shaderProgramObject2, "u_Ls");
	lightPositionUniform = gl.getUniformLocation(shaderProgramObject2, "u_light_position");
        

	kaUniform = gl.getUniformLocation(shaderProgramObject2, "u_Ka"); 
	kdUniform = gl.getUniformLocation(shaderProgramObject2, "u_Kd");
	ksUniform = gl.getUniformLocation(shaderProgramObject2, "u_Ks");
	materialShininessUniform = gl.getUniformLocation(shaderProgramObject2, "u_material_shininess");
	
	
	// *** Vertices, colors, shader attributes, vbo, vao initializations ***
	sphere = new Mesh();
	makeSphere(sphere, 2.0, 30, 30);

		gl.clearDepth(1.0); // range is 0 to 1
		gl.enable(gl.DEPTH_TEST);
		gl.depthFunc(gl.LEQUAL);
		//gl.shadeModel(gl.SMOOTH);
		//gl.hint(gl.PERSPECTIVE_CORRECTION_HINT, gl.NICEST);
	
	//Set clear color
	gl.clearColor(0.0, 0.0, 0.0, 1.0); //Black
	
	//initialize projection matrix
	perspectiveProjectionMatrix = mat4.create();
	
}

function  resize()
{
	//code
	if(bFullscreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}
	
	//set the viewport to match
	gl.viewport(0, 0, canvas.width, canvas.height);
	
	mat4.perspective(perspectiveProjectionMatrix, 45.0, (parseFloat)(canvas.width)/ (parseFloat)(canvas.height), 0.1, 100.0);
	
}


function draw()
{	
	//code
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    if (gbPerVertexLight == true) {
        //get MVP uniform location per vertex
        modelMatrixUniform = gl.getUniformLocation(shaderProgramObject1, "u_model_matrix");
        viewMatrixUniform = gl.getUniformLocation(shaderProgramObject1, "u_view_matrix");
        projectionMatrixUniform = gl.getUniformLocation(shaderProgramObject1, "u_projection_matrix");

        LKeyPressedUniform = gl.getUniformLocation(shaderProgramObject1, "u_LKeyPressed");

        laUniform = gl.getUniformLocation(shaderProgramObject1, "u_La");
        ldUniform = gl.getUniformLocation(shaderProgramObject1, "u_Ld");
        lsUniform = gl.getUniformLocation(shaderProgramObject1, "u_Ls");
        lightPositionUniform = gl.getUniformLocation(shaderProgramObject1, "u_light_position");


        kaUniform = gl.getUniformLocation(shaderProgramObject1, "u_Ka");
        kdUniform = gl.getUniformLocation(shaderProgramObject1, "u_Kd");
        ksUniform = gl.getUniformLocation(shaderProgramObject1, "u_Ks");
        materialShininessUniform = gl.getUniformLocation(shaderProgramObject1, "u_material_shininess");

        // Strt using OpenGL program object
        gl.useProgram(shaderProgramObject1);
    }

    if (gbPerFragmentLight == true) {
        //get MVP uniform location per fragment
        modelMatrixUniform = gl.getUniformLocation(shaderProgramObject2, "u_model_matrix");
        viewMatrixUniform = gl.getUniformLocation(shaderProgramObject2, "u_view_matrix");
        projectionMatrixUniform = gl.getUniformLocation(shaderProgramObject2, "u_projection_matrix");

        LKeyPressedUniform = gl.getUniformLocation(shaderProgramObject2, "u_LKeyPressed");

        laUniform = gl.getUniformLocation(shaderProgramObject2, "u_La");
        ldUniform = gl.getUniformLocation(shaderProgramObject2, "u_Ld");
        lsUniform = gl.getUniformLocation(shaderProgramObject2, "u_Ls");
        lightPositionUniform = gl.getUniformLocation(shaderProgramObject2, "u_light_position");


        kaUniform = gl.getUniformLocation(shaderProgramObject2, "u_Ka");
        kdUniform = gl.getUniformLocation(shaderProgramObject2, "u_Kd");
        ksUniform = gl.getUniformLocation(shaderProgramObject2, "u_Ks");
        materialShininessUniform = gl.getUniformLocation(shaderProgramObject2, "u_material_shininess");

        gl.useProgram(shaderProgramObject2);
    }
	
	if (bLKeyPressed == true)
	{
	    gl.uniform1i(LKeyPressedUniform, 1);

        //Set light properties
	    gl.uniform3fv(laUniform, light_ambient);
	    gl.uniform3fv(ldUniform, light_diffuse);
	    gl.uniform3fv(lsUniform, light_specular);
	    gl.uniform4fv(lightPositionUniform, light_position);

	    //set material properties
	    gl.uniform3fv(kaUniform, material_ambient);
	    gl.uniform3fv(kdUniform, material_diffuse);
	    gl.uniform3fv(ksUniform, material_specular);
	    gl.uniform1f(materialShininessUniform, material_shininess);
	}
	else
	{
	    gl.uniform1i(LKeyPressedUniform, 0);
	}
    

    //
	
	var modelMatrix = mat4.create();
	var viewMatrix = mat4.create();
	var modelViewProjectionMatrix = mat4.create();


	//Translate
	mat4.translate(modelMatrix, modelMatrix, [0.0, 0.0, -6.0]);
	
//	mat4.scale(modelViewMatrix, modelViewMatrix, [0.75,  0.75,  0.75]);

	angleCubeRadian = degToRad(angleCube); //converts degree to radian
	
	mat4.rotateX(modelMatrix, modelMatrix, angleCubeRadian); // X rotation
	
	mat4.rotateY(modelMatrix, modelMatrix, angleCubeRadian); // Y rotation
	
	mat4.rotateZ(modelMatrix, modelMatrix, angleCubeRadian); // Z rotation
	
//	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix,  modelViewMatrix);
	
	gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);

	gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);

	gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);
	
	sphere.draw();
	
	gl.useProgram(null);
	
	//update();
	
	//Animation loop
	requestAnimationFrame(draw, canvas);

	
	
}

function uninitialize()
{

	//code
	if(sphere)
	{
	    sphere.deallocate();
		sphere = null;
	}

	if (shaderProgramObject1) {
	    if (fragmentShaderObject1) {
	        gl.detachShader(shaderProgramObject1, fragmentShaderObject1);
	        gl.deleteShader(fragmentShaderObject1);
	        fragmentShaderObject1 = null;
	    }

	    if (vertexShaderObject1) {
	        gl.detachShader(shaderProgramObject1, vertexShaderObject1);
	        gl.deleteShader(vertexShaderObject1);
	        vertexShaderObject1 = null;
	    }

	    gl.deleteProgram(shaderProgramObject1);
	    shaderProgramObject1 = null;

	}
	
	
	
	if(shaderProgramObject2)
	{
		if(fragmentShaderObject2)
		{
			gl.detachShader(shaderProgramObject2, fragmentShaderObject2);
			gl.deleteShader(fragmentShaderObject2);
			fragmentShaderObject2 = null;
		}
		
		if(vertexShaderObject2)
		{
			gl.detachShader(shaderProgramObject2, vertexShaderObject2);
			gl.deleteShader(vertexShaderObject2);
			vertexShaderObject2 = null;
		}
		
		gl.deleteProgram(shaderProgramObject2);
		shaderProgramObject2 = null;
		
	}
	
}
function keyDown(event)
{
		//code
		switch(event.keyCode)
		{
		    case 81: //For 'q' or 'Q'
				//uninitialize
				uninitialize();
				//Close our application's tab
				window.close(); //Works is Safari and chrome but not in Firefox
				break;
		    case 27: //Escape
				toggleFullScreen();
				break;
		    case 76: // For 'l' or 'L'
		        if (bLKeyPressed == false)
		            bLKeyPressed = true;
		        else
		            bLKeyPressed = false;
		        break;
		    case 80: // P - per vertex light
		        if (gbPerVertexLight == false) {
		            gbPerVertexLight = true;
		            gbPerFragmentLight = false;
		        }
		        break;
		    case 70: // F - per fragment light
		        if (gbPerFragmentLight == false) {
		            gbPerFragmentLight = true;
		            gbPerVertexLight = false;
		        }
		        break;
			
						
		}
		
}
	
function mouseDown()
{
		//code
		
}




function update()
{	
	angleCube = angleCube - 0.5;
	if (angleCube <= -360.0)
		angleCube = 0.0;
	
	

}

function degToRad(degree)
{
	//code
	
	radian =(degree * Math.PI) / 180.0 // PI = 3.14159
	
	return radian;
}	
