//=== Three lights and Sphere===

//global variable
var canvas = null;
var gl = null; //WebGL context
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

var gbLight = false;

const WebGLMacros= // when whole 'WebGLMacros' is 'const' , all inside it are automatically const
{
	VDG_ATTRIBUTE_VERTEX:0,
	VDG_ATTRIBUTE_COLOR:1,
	VDG_ATTRIBUTE_NORMAL:2,
	VDG_ATTRIBUTE_TEXTURE0:3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;	
				   
var vao_pyramid;
var vao_cube;
var vbo_position;
var vbo_normal;
var mvpUniform;

var perspectiveProjectionMatrix;

var modelMatrixUniform, viewMatrixUniform, rotationMatrixUniform, projectionMatrixUniform;
var la0Uniform, ld0Uniform, ls0Uniform, light0PositionUniform;
var la1Uniform, ld1Uniform, ls1Uniform, light1PositionUniform;
var la2Uniform, ld2Uniform, ls2Uniform, light2PositionUniform;
var kaUniform, kdUniform, ksUniform, materialshininessUniform;

var LKeyPressedUniform;

var anglePyramid = 0.0;


var gbAnimate = true;
var bLKeyPressed = false;
var bIsAKeyPressed = true;

var red_x = 0.0;
var red_y = 0.0;
var red_z = 0.0;

var green_x = 0.0;
var green_y = 0.0;
var green_z = 0.0;

var blue_x = 0.0;
var blue_y = 0.0;
var blue_z = 0.0;

var Xcenter = 0.0;
var Ycenter = 0.0;
var Zcenter = 0.0;

var angleCircle = 0.0;

var light0_ambient = [0.0,0.0,0.0];
var light0_diffuse = [1.0,0.0,0.0]; //RED
var light0_specular = [1.0,1.0,1.0];
var light0_position = [100.0, 100.0, 100.0, 1.0];

var light1_ambient = [0.0, 0.0, 0.0];
var light1_diffuse = [0.0, 1.0, 0.0]; //GREEN
var light1_specular = [1.0, 1.0, 1.0];
var light1_position = [100.0, 100.0, 100.0, 1.0];

var light2_ambient = [0.0, 0.0, 0.0];
var light2_diffuse = [0.0, 0.0, 1.0]; //BLUE
var light2_specular = [1.0, 1.0, 1.0];
var light2_position = [100.0, 100.0, 100.0, 1.0];

var material_ambient = [0.0,0.0,0.0];
var material_diffuse = [1.0,1.0,1.0];
var material_specular = [1.0,1.0,1.0];
var material_shininess = 50.0;

var anglePyramid = 0.0;
var anglePyramidRadian = 0.0;

var sphere = null;


var requestAnimationFrame =
window.requestAnimationFrame||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame;

var cancelAnimationFrame = 
window.cancelAnimationFrame||
window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
window.mozCancelRequestAnimationFrame ||window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

//onload function
function main()
{
	//get canvas element
	canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining canvas failed\n");
	else
		console.log("Obtaining canvas successed\n");
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
	
		
	//register keyboard's keydown event handler
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);
	
	//initialize WebGL
	init();
	
	//Start drawing here as warming-up
	resize();
	draw();

}

function toggleFullScreen()
{
	//code
	var fullscreen_element =
	document.fullscreenElement ||
	document.webkitFullscreenElement ||
	document.mozFullScreenElement ||
	document.msFullscreenElement ||
	null;
	
	//if not fullscreen
	if(fullscreen_element == null)
	{
	if(canvas.requestFullscreen)
		canvas.requestFullscreen();
	else if(canvas.mozRequestFullScreen)
		canvas.mozRequestFullScreen();
	else if(canvas.webkitRequestFullscreen)
		canvas.webkitRequestFullScreen();
	else if(canvas.msRequestFullscreen)
		canvas.msRequestFullscreen();
	bFullscreen = true;
	}
	else // if already fullscreen
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		bFullscreen = false;
	}
}

function init()
{
	//code
	//get WebGL 2.0 context
	gl = canvas.getContext("webgl2");
	if(gl==null) //failed to get context
	{
		console.log("Failed to get rendering  context for WebGL");
		return;
	}
	gl.viewportWidth =  canvas.width;
	gl.viewportHeight = canvas.height;
	
	//vertex shader
	var vertexShaderSourceCode = 
	        "#version 300 es" +
            "\n" +
			"in vec4 vPosition;" +
			"in vec3 vNormal;" +
			"uniform mat4 u_model_matrix;" +
			"uniform mat4 u_view_matrix;" +
			"uniform mat4 u_projection_matrix;" +
			"uniform mediump int u_LKeyPressed;" +
			"uniform vec4 u_light0_position;" +
			"uniform vec4 u_light1_position;" +
			"uniform vec4 u_light2_position;" +
			"out vec3 transformed_normals;" +
			"out vec3 light0_direction;" +
			"out vec3 light1_direction;" +
			"out vec3 light2_direction;" +
			"out vec3 viewer_vector;" +
			"void main(void)" +
			"{" +
			"if (u_LKeyPressed == 1)" +
			"{" +
			"vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;" +
			"transformed_normals =  normalize(mat3(u_view_matrix * u_model_matrix ) * vNormal);" +
			"light0_direction = normalize(vec3(u_light0_position) - eye_coordinates.xyz);" +
			"light1_direction = normalize(vec3(u_light1_position) - eye_coordinates.xyz);" +
			"light2_direction = normalize(vec3(u_light2_position) - eye_coordinates.xyz);" +
			"viewer_vector = -eye_coordinates.xyz;" +
			"}" +
            "gl_Position = u_projection_matrix  * u_view_matrix * u_model_matrix * vPosition;" +
			"}";

	
	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS)==false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	
	var fragmentShaderSourceCode = 
	        "#version 300 es"+
	        "\n" +
			"precision highp float;" +
			"in vec3 transformed_normals;" +
			"in vec3 light0_direction;" +
			"in vec3 light1_direction;" +
			"in vec3 light2_direction;" +
			"in vec3 viewer_vector;" +
			"out vec4 FragColor;" +
			"uniform vec3 u_La0;" +
			"uniform vec3 u_Ld0;" +
			"uniform vec3 u_Ls0;" +
			"uniform vec3 u_La1;" +
			"uniform vec3 u_Ld1;" +
			"uniform vec3 u_Ls1;" +
			"uniform vec3 u_La2;" +
			"uniform vec3 u_Ld2;" +
			"uniform vec3 u_Ls2;" +
			"uniform vec3 u_Ka;" +
			"uniform vec3 u_Kd;" +
			"uniform vec3 u_Ks;" +
			"uniform float u_material_shininess;" +
			"uniform mediump int u_LKeyPressed;" +
			"void main(void)" +
			"{" +
			"vec3 light0;" +
			"vec3 light1;" +
			"vec3 light2;" +
			"vec3 phong_ads_color;" +
			"if(u_LKeyPressed == 1)" +
			"{" +
			"vec3 normalized_transformed_normals = normalize(transformed_normals);" +
			"vec3 normalized_light0_direction = normalize(light0_direction);" +
			"vec3 normalized_light1_direction = normalize(light1_direction);" +
			"vec3 normalized_light2_direction = normalize(light2_direction);" +
			"vec3 normalized_viewer_vector = normalize(viewer_vector);" +
			"float tn_dot_ld0 = max(dot(normalized_transformed_normals, normalized_light0_direction), 0.0);" +
			"vec3 ambient0 = u_La0 * u_Ka;" +
			"vec3 diffuse0 = u_Ld0 * u_Kd * tn_dot_ld0;" +
			"vec3 reflection_vector0 = reflect(-normalized_light0_direction,normalized_transformed_normals);" +
			"vec3 specular0 = u_Ls0 * u_Ks * pow(max(dot(reflection_vector0, normalized_viewer_vector), 0.0), u_material_shininess);" +
			"float tn_dot_ld1 = max(dot(normalized_transformed_normals, normalized_light1_direction), 0.0);" +
			"vec3 ambient1 = u_La1 * u_Ka;" +
			"vec3 diffuse1 = u_Ld1 * u_Kd * tn_dot_ld1;" +
			"vec3 reflection_vector1 = reflect(-normalized_light1_direction,normalized_transformed_normals);" +
			"vec3 specular1 = u_Ls1 * u_Ks * pow(max(dot(reflection_vector1, normalized_viewer_vector), 0.0), u_material_shininess);" +
			"float tn_dot_ld2 = max(dot(normalized_transformed_normals, normalized_light2_direction), 0.0);" +
			"vec3 ambient2 = u_La2 * u_Ka;" +
			"vec3 diffuse2 = u_Ld2 * u_Kd * tn_dot_ld2;" +
			"vec3 reflection_vector2 = reflect(-normalized_light2_direction,normalized_transformed_normals);" +
			"vec3 specular2 = u_Ls2 * u_Ks * pow(max(dot(reflection_vector2, normalized_viewer_vector), 0.0), u_material_shininess);" +
			"light0 = ambient0 + diffuse0 + specular0;" +
			"light1 = ambient1 + diffuse1 + specular1;" +
			"light2 = ambient2 + diffuse2 + specular2;" +
			"phong_ads_color = light0 + light1 + light2;" +
			"}" +
			"else" +
			"{" +
			"phong_ads_color =  vec3(1.0,  1.0,  1.0);" +
			"}" +
			"FragColor = vec4(phong_ads_color, 1.0);" +
			"}";
	
	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS)==false)
	{
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	//Shader program
	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);
	
	//Pre-link binding of shader program object with vertex shader attributes
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.VDG_ATTRIBUTE_NORMAL, "vNormal");
	
	//linking
	gl.linkProgram(shaderProgramObject);
	if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	
	
	//get MVP uniform location
//	mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");
	modelMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_model_matrix");
	viewMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_view_matrix");
	rotationMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_rotation_matrix");
	projectionMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_projection_matrix");
    
	LKeyPressedUniform = gl.getUniformLocation(shaderProgramObject, "u_LKeyPressed");

	la0Uniform = gl.getUniformLocation(shaderProgramObject, "u_La0");
	ld0Uniform = gl.getUniformLocation(shaderProgramObject, "u_Ld0");
	ls0Uniform = gl.getUniformLocation(shaderProgramObject, "u_Ls0");
	light0PositionUniform = gl.getUniformLocation(shaderProgramObject, "u_light0_position");

	la1Uniform = gl.getUniformLocation(shaderProgramObject, "u_La1");
	ld1Uniform = gl.getUniformLocation(shaderProgramObject, "u_Ld1");
	ls1Uniform = gl.getUniformLocation(shaderProgramObject, "u_Ls1");
	light1PositionUniform = gl.getUniformLocation(shaderProgramObject, "u_light1_position");

	la2Uniform = gl.getUniformLocation(shaderProgramObject, "u_La2");
	ld2Uniform = gl.getUniformLocation(shaderProgramObject, "u_Ld2");
	ls2Uniform = gl.getUniformLocation(shaderProgramObject, "u_Ls2");
	light2PositionUniform = gl.getUniformLocation(shaderProgramObject, "u_light2_position");
        

	kaUniform = gl.getUniformLocation(shaderProgramObject, "u_Ka"); 
	kdUniform = gl.getUniformLocation(shaderProgramObject, "u_Kd");
	ksUniform = gl.getUniformLocation(shaderProgramObject, "u_Ks");
	materialShininessUniform = gl.getUniformLocation(shaderProgramObject, "u_material_shininess");
	
	
    // *** Vertices, colors, shader attributes, vbo, vao initializations ***
    sphere = new Mesh();
	makeSphere(sphere, 2.0, 30, 30);

		gl.clearDepth(1.0); // range is 0 to 1
		gl.enable(gl.DEPTH_TEST);
		gl.depthFunc(gl.LEQUAL);
			
	//Set clear color
	gl.clearColor(0.0, 0.0, 0.0, 1.0); //Black
	
	//initialize projection matrix
	perspectiveProjectionMatrix = mat4.create();
	
}

function  resize()
{
	//code
	if(bFullscreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}
	
	//set the viewport to match
	gl.viewport(0, 0, canvas.width, canvas.height);
	
	mat4.perspective(perspectiveProjectionMatrix, 45.0, (parseFloat)(canvas.width)/ (parseFloat)(canvas.height), 0.1, 100.0);
	
}


function draw()
{	
	//code
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	
	gl.useProgram(shaderProgramObject);

	if (bLKeyPressed == true)
	{
	    gl.uniform1i(LKeyPressedUniform, 1);

        //Set light properties
	    gl.uniform3fv(la0Uniform, light0_ambient);
	    gl.uniform3fv(ld0Uniform, light0_diffuse);
	    gl.uniform3fv(ls0Uniform, light0_specular);
	    gl.uniform4fv(light0PositionUniform, light0_position);

	    gl.uniform3fv(la1Uniform, light1_ambient);
	    gl.uniform3fv(ld1Uniform, light1_diffuse);
	    gl.uniform3fv(ls1Uniform, light1_specular);
	    gl.uniform4fv(light1PositionUniform, light1_position);

	    gl.uniform3fv(la2Uniform, light2_ambient);
	    gl.uniform3fv(ld2Uniform, light2_diffuse);
	    gl.uniform3fv(ls2Uniform, light2_specular);
	    gl.uniform4fv(light2PositionUniform, light2_position);

	    //set material properties
	    gl.uniform3fv(kaUniform, material_ambient);
	    gl.uniform3fv(kdUniform, material_diffuse);
	    gl.uniform3fv(ksUniform, material_specular);
	    gl.uniform1f(materialShininessUniform, material_shininess);

	    //Rotate RED light to Y-direction
	    light0_position[0] = red_x;
	    light0_position[1] = red_y;
	    light0_position[2] = red_z;

	    //Rotate Blue light to Z-direction
	    light1_position[0] = blue_x;
	    light1_position[1] = blue_y;
	    light1_position[2] = blue_z;

	    //Rotate GREEN light to X-direction
	    light2_position[0] = green_x;
	    light2_position[1] = green_y;
	    light2_position[2] = green_z;

	}
	else
	{
	    gl.uniform1i(LKeyPressedUniform, 0);
	}
    

    //
	
	var modelMatrix = mat4.create();
	var viewMatrix = mat4.create();
	var rotationMatrix = mat4.create();
	var modelViewProjectionMatrix = mat4.create();


	//Translate
	mat4.translate(modelMatrix, modelMatrix, [0.0, 0.0, -6.0]);
	
	gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);

	gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);

	gl.uniformMatrix4fv(rotationMatrixUniform, false, rotationMatrix);

	gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);
	
	sphere.draw();
	
	gl.useProgram(null);
	
	if (gbAnimate == true)
	update();
	
	//Animation loop
	requestAnimationFrame(draw, canvas);

	
	
}

function uninitialize()
{

	//code
	if(sphere)
	{
	    sphere.deallocate();
		sphere = null;
	}
	
	
	
	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}
		
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}
		
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
		
	}
	
}
function keyDown(event)
{
		//code
		switch(event.keyCode)
		{
			case 27: //Escape
				//uninitialize
				uninitialize();
				//Close our application's tab
				window.close(); //Works is Safari and chrome but not in Firefox
				break;
			case 70: //for 'f' or 'F'
				toggleFullScreen();
				break;
		    case 76: // For 'l' or 'L'
		        if (bLKeyPressed == false)
		            bLKeyPressed = true;
		        else
		            bLKeyPressed = false;
		        break;
		    case 65: //for 'A' or 'a'
		        if (bIsAKeyPressed == false) {
		            gbAnimate = true;
		            bIsAKeyPressed = true;
		        }
		        else {
		            gbAnimate = false;
		            bIsAKeyPressed = false;
		        }
		        break;
			
						
		}
		
}
	
function mouseDown()
{
		//code
		
}




function update()
{	
    // Circle equalation  Circle_X= Xcenter + rcosQ , Circle_Y = Ycenter + rsinQ, Q = 0 to 2 * Pi
    //Math.cosQ - here Q is in radians
    angleCircle = angleCircle - 0.05;
    if (angleCircle <= -(2.0 * 3.14159))  // minus sign for anti-clockwise  direction
    angleCircle = 0.0;

    red_x = Xcenter + 100.0 * Math.cos(angleCircle);
    red_y = Ycenter;
    red_z = Zcenter + 100.0 * Math.sin(angleCircle);
	
    green_x = Xcenter + 100.0 * Math.sin(angleCircle);
    green_y = Ycenter + 100.0 * Math.cos(angleCircle);
    green_z = Zcenter;
	
    blue_x = Xcenter;
    blue_y = Ycenter + 100.0 * Math.sin(angleCircle);
    blue_z = Zcenter + 100.0 * Math.cos(angleCircle);
	

}

function degToRad(degree)
{
	//code
	
	radian =(degree * Math.PI) / 180.0 // PI = 3.14159
	
	return radian;
}	
