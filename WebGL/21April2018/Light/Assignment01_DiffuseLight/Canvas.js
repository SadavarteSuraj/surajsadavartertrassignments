//===Diffuse Light ===

//global variable
var canvas = null;
var gl = null; //WebGL context
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

var gbLight = false;

const WebGLMacros= // when whole 'WebGLMacros' is 'const' , all inside it are automatically const
{
	VDG_ATTRIBUTE_VERTEX:0,
	VDG_ATTRIBUTE_COLOR:1,
	VDG_ATTRIBUTE_NORMAL:2,
	VDG_ATTRIBUTE_TEXTURE0:3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;	
				   
var vao_pyramid;
var vao_cube;
var vbo_position;
var vbo_normal;
var mvpUniform;

var perspectiveProjectionMatrix;

var modelViewMatrixUniform, projectionMatrixUniform;
var ldUniform, kdUniform, lightPositionUniform;
var LKeyPressedUniform;

var angleCube = 0.0;

var bLKeyPressed = false;


var angleCube = 0.0;
var anglePyramidRadian = 0.0;
var angleCubeRadian = 0.0;

var requestAnimationFrame =
window.requestAnimationFrame||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame;

var cancelAnimationFrame = 
window.cancelAnimationFrame||
window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
window.mozCancelRequestAnimationFrame ||window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

//onload function
function main()
{
	//get canvas element
	canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining canvas failed\n");
	else
		console.log("Obtaining canvas successed\n");
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
	
		
	//register keyboard's keydown event handler
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);
	
	//initialize WebGL
	init();
	
	//Start drawing here as warming-up
	resize();
	draw();
	update();
}

function toggleFullScreen()
{
	//code
	var fullscreen_element =
	document.fullscreenElement ||
	document.webkitFullscreenElement ||
	document.mozFullScreenElement ||
	document.msFullscreenElement ||
	null;
	
	//if not fullscreen
	if(fullscreen_element == null)
	{
	if(canvas.requestFullscreen)
		canvas.requestFullscreen();
	else if(canvas.mozRequestFullScreen)
		canvas.mozRequestFullScreen();
	else if(canvas.webkitRequestFullscreen)
		canvas.webkitRequestFullScreen();
	else if(canvas.msRequestFullscreen)
		canvas.msRequestFullscreen();
	bFullscreen = true;
	}
	else // if already fullscreen
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		bFullscreen = false;
	}
}

function init()
{
	//code
	//get WebGL 2.0 context
	gl = canvas.getContext("webgl2");
	if(gl==null) //failed to get context
	{
		console.log("Failed to get rendering  context for WebGL");
		return;
	}
	gl.viewportWidth =  canvas.width;
	gl.viewportHeight = canvas.height;
	
	//vertex shader
	var vertexShaderSourceCode = 
	        "#version 300 es" +
            "\n"+
		    "in vec4 vPosition;" +
			"in vec3 vNormal;" +
			"uniform mat4 u_model_view_matrix;" +
			"uniform mat4 u_projection_matrix;" +
			"uniform mediump int u_LkeyPressed;" +
			"uniform vec3 u_Ld;" +
			"uniform vec3 u_Kd;" +
			"uniform vec4 u_light_position;" +
			"out vec3 diffuse_light;" +
			"void main(void)" +
			"{" +
			"if (u_LkeyPressed == 1)" +
			"{" +
			"vec4 eyeCoordinates = u_model_view_matrix * vPosition;" +
			"vec3 tnorm =  normalize(mat3(u_model_view_matrix) * vNormal);" +
			"vec3 s = normalize(vec3(u_light_position - eyeCoordinates));" +
			"diffuse_light = u_Ld * u_Kd * max(dot(s, tnorm), 0.0);" +
			"}" +
			"gl_Position  = u_projection_matrix * u_model_view_matrix * vPosition;" +
			"}";
	
	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS)==false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	
	var fragmentShaderSourceCode = 
	        "#version 300 es"+
	        "\n"+
			"precision highp float;" +
			"in vec3 diffuse_light;" +
			"out vec4 FragColor;" +
			"uniform int u_LkeyPressed;" +
			"void main(void)" +
			"{" +
			"vec4 color;" +
			"if (u_LkeyPressed ==1)" +
			"{" +
			"color = vec4(diffuse_light, 1.0);" +
			"}" +
			"else" +
			"{" +
			"color = vec4(1.0, 1.0, 1.0, 1.0);" +
			"}" +
			"FragColor = color;" +
			"}";
	
	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS)==false)
	{
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	//Shader program
	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);
	
	//Pre-link binding of shader program object with vertex shader attributes
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.VDG_ATTRIBUTE_NORMAL, "vNormal");
	
	//linking
	gl.linkProgram(shaderProgramObject);
	if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	
	
	//get MVP uniform location
//	mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");
	modelViewMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_model_view_matrix");
	projectionMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_projection_matrix");
    
	LKeyPressedUniform = gl.getUniformLocation(shaderProgramObject, "u_LkeyPressed");

	ldUniform = gl.getUniformLocation(shaderProgramObject, "u_Ld");
	kdUniform = gl.getUniformLocation(shaderProgramObject, "u_Kd");
	lightPositionUniform = gl.getUniformLocation(shaderProgramObject, "u_light_position");
	
	// *** Vertices, colors, shader attributes, vbo, vao initializations ***
	
	var cubeVertices = new Float32Array([
											1.0, 1.0, -1.0,
											-1.0, 1.0, -1.0,
											-1.0, 1.0, 1.0,
											1.0, 1.0, 1.0,

											1.0, -1.0, 1.0,
											-1.0, -1.0, 1.0,
											-1.0, -1.0, -1.0,
											1.0, -1.0, -1.0,

											1.0, 1.0, 1.0,
											-1.0, 1.0, 1.0,
											-1.0, -1.0, 1.0,
											1.0, -1.0, 1.0,

											1.0, -1.0, -1.0,
											-1.0, -1.0, -1.0,
											-1.0, 1.0, -1.0,
											1.0, 1.0, -1.0,

											-1.0, 1.0, 1.0,
											-1.0, 1.0, -1.0,
											-1.0, -1.0, -1.0,
											-1.0, -1.0, 1.0,

											1.0, 1.0, -1.0,
											1.0, 1.0, 1.0,
											1.0, -1.0, 1.0,
											1.0, -1.0, -1.0,
											]);
											
											
	var cubeNormal = new Float32Array([
                                            0.0, 1.0, 0.0,
		                                    0.0, 1.0, 0.0,
		                                    0.0, 1.0, 0.0,
		                                    0.0, 1.0, 0.0,

		                                    0.0, -1.0, 0.0,
		                                    0.0, -1.0, 0.0,
		                                    0.0, -1.0, 0.0,
		                                    0.0, -1.0, 0.0,

		                                    0.0, 0.0, 1.0,
		                                    0.0, 0.0, 1.0,
		                                    0.0, 0.0, 1.0,
		                                    0.0, 0.0, 1.0,

		                                    0.0, 0.0, -1.0,
		                                    0.0, 0.0, -1.0,
		                                    0.0, 0.0, -1.0,
		                                    0.0, 0.0, -1.0,

		                                    -1.0, 0.0, 0.0,
		                                    -1.0, 0.0, 0.0,
		                                    -1.0, 0.0, 0.0,
		                                    -1.0, 0.0, 0.0,
		
		                                    1.0, 0.0, 0.0,
		                                    1.0, 0.0, 0.0,
		                                    1.0, 0.0, 0.0,
		                                    1.0, 0.0, 0.0
											]);
											
		
	//vao_cube
	vao_cube = gl.createVertexArray();
	gl.bindVertexArray(vao_cube);

	//Cube vbo position
	vbo_position = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position);
	gl.bufferData(gl.ARRAY_BUFFER, cubeVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_VERTEX,
							3, //3 is for X,  Y, Z coordinates in our triangleVertices array
							gl.FLOAT,
							false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	//cube vbo Normal
	vbo_normal = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_normal);
	gl.bufferData(gl.ARRAY_BUFFER, cubeNormal, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.VDG_ATTRIBUTE_NORMAL,
							3, 
							gl.FLOAT,
							false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.VDG_ATTRIBUTE_NORMAL);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	gl.bindVertexArray(null);
	
		gl.clearDepth(1.0); // range is 0 to 1
		gl.enable(gl.DEPTH_TEST);
		gl.depthFunc(gl.LEQUAL);
		//gl.shadeModel(gl.SMOOTH);
		//gl.hint(gl.PERSPECTIVE_CORRECTION_HINT, gl.NICEST);
	
	//Set clear color
	gl.clearColor(0.0, 0.0, 0.0, 1.0); //Blue
	
	//initialize projection matrix
	perspectiveProjectionMatrix = mat4.create();
	
}

function  resize()
{
	//code
	if(bFullscreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}
	
	//set the viewport to match
	gl.viewport(0, 0, canvas.width, canvas.height);
	
	mat4.perspective(perspectiveProjectionMatrix, 45.0, (parseFloat)(canvas.width)/ (parseFloat)(canvas.height), 0.1, 100.0);
	
}


function draw()
{	
	//code
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	
	gl.useProgram(shaderProgramObject);

	if (bLKeyPressed == true)
	{
	    gl.uniform1i(LKeyPressedUniform, 1);

	    gl.uniform3f(ldUniform, 1.0, 1.0, 1.0);
	    gl.uniform3f(kdUniform, 0.5, 0.5, 0.5);

	    var lightPosition = [0.0, 0.0, 2.0, 1.0];
	    gl.uniform4fv(lightPositionUniform, lightPosition);
	}
	else
	{
	    gl.uniform1i(LKeyPressedUniform, 0);
	}
    

    //
	
	var modelViewMatrix = mat4.create();
	var modelViewProjectionMatrix = mat4.create();


	//Translate
	mat4.translate(modelViewMatrix, modelViewMatrix, [0.0,  0.0,  -5.0]);
	
//	mat4.scale(modelViewMatrix, modelViewMatrix, [0.75,  0.75,  0.75]);

	angleCubeRadian = degToRad(angleCube); //converts degree to radian
	
	mat4.rotateX(modelViewMatrix, modelViewMatrix, angleCubeRadian); // X rotation
	
	mat4.rotateY(modelViewMatrix, modelViewMatrix, angleCubeRadian); // Y rotation
	
	mat4.rotateZ(modelViewMatrix, modelViewMatrix, angleCubeRadian); // Z rotation
	
//	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix,  modelViewMatrix);
	
	gl.uniformMatrix4fv(modelViewMatrixUniform, false, modelViewMatrix);

	gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);
	
	gl.bindVertexArray(vao_cube);
	
														   
	gl.drawArrays(gl.TRIANGLE_FAN,  0, 4);
	gl.drawArrays(gl.TRIANGLE_FAN,  4, 4);
	gl.drawArrays(gl.TRIANGLE_FAN,  8, 4);
	gl.drawArrays(gl.TRIANGLE_FAN,  12, 4);
	gl.drawArrays(gl.TRIANGLE_FAN,  16, 4);
	gl.drawArrays(gl.TRIANGLE_FAN,  20, 4);
	
	gl.bindVertexArray(null);
	
	gl.useProgram(null);
	
	update();
	
	//Animation loop
	requestAnimationFrame(draw, canvas);

	
	
}

function uninitialize()
{

	//code
	if(vao_cube)
	{
		gl.deleteVertexArray(vao_cube);
		vao_cube = null;
	}
	
	if(vbo_position)
	{
		gl.deleteBuffer(vbo_position);
		vbo_position = null;
	}
	
	
	if(vbo_normal)
	{
	    gl.deleteBuffer(vbo_normal);
	    vbo_normal = null;
	}
	
	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}
		
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}
		
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
		
	}
	
}
function keyDown(event)
{
		//code
		switch(event.keyCode)
		{
			case 27: //Escape
				//uninitialize
				uninitialize();
				//Close our application's tab
				window.close(); //Works is Safari and chrome but not in Firefox
				break;
			case 70: //for 'f' or 'F'
				toggleFullScreen();
				break;
		    case 76: // For 'l' or 'L'
		        if (bLKeyPressed == false)
		            bLKeyPressed = true;
		        else
		            bLKeyPressed = false;
		        break;
			
						
		}
		
}
	
function mouseDown()
{
		//code
		
}




function update()
{	
	angleCube = angleCube - 0.5;
	if (angleCube <= -360.0)
		angleCube = 0.0;
	
	

}

function degToRad(degree)
{
	//code
	
	radian =(degree * Math.PI) / 180.0 // PI = 3.14159
	
	return radian;
}	
