//=== 24 Materials===

//global variable
var canvas = null;
var gl = null; //WebGL context
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;
var width1;
var height;

var gbLight = false;

const WebGLMacros= // when whole 'WebGLMacros' is 'const' , all inside it are automatically const
{
	VDG_ATTRIBUTE_VERTEX:0,
	VDG_ATTRIBUTE_COLOR:1,
	VDG_ATTRIBUTE_NORMAL:2,
	VDG_ATTRIBUTE_TEXTURE0:3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;	
				   
var vao_pyramid;
var vao_cube;
var vbo_position;
var vbo_normal;
var mvpUniform;

var perspectiveProjectionMatrix;

var modelMatrixUniform, viewMatrixUniform, rotationMatrixUniform, projectionMatrixUniform;
var la0Uniform, ld0Uniform, ls0Uniform, light0PositionUniform;
var la1Uniform, ld1Uniform, ls1Uniform, light1PositionUniform;
var la2Uniform, ld2Uniform, ls2Uniform, light2PositionUniform;
var kaUniform, kdUniform, ksUniform, materialshininessUniform;

var LKeyPressedUniform;

var anglePyramid = 0.0;


var gbAnimate = true;
var bLKeyPressed = false;
var bIsAKeyPressed = true;

var Xkey = false;
var Ykey = false;
var Zkey = false;

var red_x = 0.0;
var red_y = 0.0;
var red_z = 0.0;

var green_x = 0.0;
var green_y = 0.0;
var green_z = 0.0;

var blue_x = 0.0;
var blue_y = 0.0;
var blue_z = 0.0;

var Xcenter = 0.0;
var Ycenter = 0.0;
var Zcenter = 0.0;

var angleCircle = 0.0;

var light0_ambient = [0.0,0.0,0.0];
var light0_diffuse = [1.0,1.0,1.0]; //White
var light0_specular = [1.0,1.0,1.0];
var light0_position = [100.0, 100.0, 100.0, 1.0];

var light1_ambient = [0.0, 0.0, 0.0];
var light1_diffuse = [0.0, 1.0, 0.0]; //GREEN
var light1_specular = [1.0, 1.0, 1.0];
var light1_position = [100.0, 100.0, 100.0, 1.0];

var light2_ambient = [0.0, 0.0, 0.0];
var light2_diffuse = [0.0, 0.0, 1.0]; //BLUE
var light2_specular = [1.0, 1.0, 1.0];
var light2_position = [100.0, 100.0, 100.0, 1.0];

var light_model_ambient = [ 0.2, 0.2, 0.2, 0.0 ];
var light_model_local_viewer = [ 0.0 ];

//first sphere on first column, emerald
var material1_ambient = [ 0.0215, 0.1745, 0.0215, ];
var material1_diffuse = [ 0.07568, 0.61424, 0.07568, ];
var material1_specular = [ 0.633, 0.727811, 0.633, ];
var material1_shininess =  0.6 * 128 ;

//Second sphere on irst column, jade
var material2_ambient = [ 0.135, 0.2225, 0.1575, ];
var material2_diffuse = [ 0.54, 0.89, 0.63, ];
var material2_specular = [ 0.316228, 0.316228, 0.316228, ];
var material2_shininess =  0.1 * 128 ;

//Third sphere on first column, obsidian
var material3_ambient = [ 0.05375, 0.05, 0.06625, ];
var material3_diffuse = [ 0.18275, 0.17, 0.22525, ];
var material3_specular = [ 0.332741, 0.328634, 0.346435, ];
var material3_shininess =  0.3 * 128 ;

//Fourth sphere on first column, pearl
var material4_ambient = [ 0.25, 0.20725, 0.20725, ];
var material4_diffuse = [ 1.0, 0.829, 0.829, ];
var material4_specular = [ 0.296648, 0.296648, 0.296648, ];
var material4_shininess =  0.088 * 128 ;

//Fifth sphere on first column, ruby
var material5_ambient = [ 0.1745, 0.01175, 0.01175, ];
var material5_diffuse = [ 0.61424, 0.04136, 0.04136, ];
var material5_specular = [ 0.727811, 0.626959, 0.626959, ];
var material5_shininess =  0.6 * 128 ;

//Sixth sphere on first column, turquoise
var material6_ambient = [ 0.1, 0.18725, 0.1745, ];
var material6_diffuse = [ 0.396, 0.74151, 0.69102, ];
var material6_specular = [ 0.297254, 0.30829, 0.306678, ];
var material6_shininess =  1.0;

//First sphere on second column, brass
var material7_ambient = [ 0.329412, 0.223529, 0.027451, ];
var material7_diffuse = [ 0.780392, 0.568627, 0.113725, ];
var material7_specular = [ 0.992157, 0.941176, 0.807843, ];
var material7_shininess =  0.21794872 * 128 ;

//Second sphere on second column, bronze
var material8_ambient = [ 0.2125, 0.1275, 0.054, ];
var material8_diffuse = [ 0.714, 0.4284, 0.18144, ];
var material8_specular = [ 0.393548, 0.211906, 0.166721, ];
var material8_shininess =  0.2 * 128 ;

//Third sphere on second column, chrome
var material9_ambient = [ 0.25, 0.25, 0.25, ];
var material9_diffuse = [ 0.4, 0.4, 0.4, ];
var material9_specular = [ 0.774597, 0.774597, 0.774597, ];
var material9_shininess =  0.6 * 128 ;

//Fourth sphere on second column, copper
var material10_ambient = [ 0.19125, 0.0735, 0.0225, ];
var material10_diffuse = [ 0.7038, 0.27048, 0.0828, ];
var material10_specular = [ 0.276777, 0.137622, 0.086014, ];
var material10_shininess =  0.1 * 128 ;

//Fifth sphere on second column, gold
var material11_ambient = [ 0.24725, 0.1995, 0.07455, ];
var material11_diffuse = [ 0.75164, 0.60648, 0.22648, ];
var material11_specular = [ 0.628281, 0.555802, 0.366065, ];
var material11_shininess =  0.4 * 128 ;

//Sixth sphere on second column, silver
var material12_ambient = [ 0.19225, 0.19225, 0.19225, ];
var material12_diffuse = [ 0.50754, 0.50754, 0.50754, ];
var material12_specular = [ 0.508273, 0.508273, 0.508273, ];
var material12_shininess =  0.4 * 128 ;

//First sphere on third column, black
var material13_ambient = [ 0.0, 0.0, 0.0, ];
var material13_diffuse = [ 0.01, 0.01, 0.01, ];
var material13_specular = [ 0.50, 0.50, 0.50, ];
var material13_shininess =  0.25 * 128 ;

//Second sphere on third column, cyan
var material14_ambient = [ 0.0, 0.1, 0.06, ];
var material14_diffuse = [ 0.0, 0.50980392, 0.50980392, ];
var material14_specular = [ 0.50196078, 0.50196078, 0.50196078, ];
var material14_shininess =  0.25 * 128 ;

//Third sphere on third column, green
var material15_ambient = [ 0.0, 0.0, 0.0, ];
var material15_diffuse = [ 0.1, 0.35, 0.1, ];
var material15_specular = [ 0.45, 0.55, 0.45, ];
var material15_shininess =  0.25 * 128 ;

//Fourth sphere on third column, red
var material16_ambient = [ 0.0, 0.0, 0.0, ];
var material16_diffuse = [ 0.5, 0.0, 0.0, ];
var material16_specular = [ 0.7, 0.6, 0.6, ];
var material16_shininess =  0.25 * 128 ;

//Fifth sphere on third column, white
var material17_ambient = [ 0.0, 0.0, 0.0, ];
var material17_diffuse = [ 0.55, 0.55, 0.55, ];
var material17_specular = [ 0.70, 0.70, 0.70, ];
var material17_shininess =  0.25 * 128 ;

//Sixth sphere on third column, yellow  plastic
var material18_ambient = [ 0.0, 0.0, 0.0, ];
var material18_diffuse = [ 0.5, 0.5, 0.0, ];
var material18_specular = [ 0.60, 0.60, 0.50, ];
var material18_shininess =  0.25 * 128 ;

//First sphere on fourth column, black
var material19_ambient = [ 0.02, 0.02, 0.02, ];
var material19_diffuse = [ 0.01, 0.01, 0.01, ];
var material19_specular = [ 0.4, 0.4, 0.4, ];
var material19_shininess =  0.078125 * 128 ;

//Second sphere on fourth column, cyan
var material20_ambient = [ 0.0, 0.05, 0.05, ];
var material20_diffuse = [ 0.4, 0.5, 0.5, ];
var material20_specular = [ 0.04, 0.7, 0.7, ];
var material20_shininess =  0.078125 * 128 ;

//Third sphere on fourth column, green
var material21_ambient = [ 0.0, 0.05, 0.0, ];
var material21_diffuse = [ 0.4, 0.5, 0.4, ];
var material21_specular = [ 0.04, 0.7, 0.04, ];
var material21_shininess =  0.078125 * 128 ;

//Fourth sphere on fourth column, red
var material22_ambient = [ 0.05, 0.0, 0.0, ];
var material22_diffuse = [ 0.5, 0.4, 0.4, ];
var material22_specular = [ 0.7, 0.04, 0.04, ];
var material22_shininess =  0.078125 * 128 ;

//Fifth sphere on fourth column, white
var material23_ambient = [ 0.05, 0.05, 0.05, ];
var material23_diffuse = [ 0.5, 0.5, 0.5, ];
var material23_specular = [ 0.7, 0.7, 0.7, ];
var material23_shininess =  0.078125 * 128 ;

//Fifth sphere on fourth column, yellow rubber
var material24_ambient = [ 0.05, 0.05, 0.0, ];
var material24_diffuse = [ 0.5, 0.5, 0.4, ];
var material24_specular = [ 0.7, 0.7, 0.04, ];
var material24_shininess =  0.078125 * 128 ;


var anglePyramid = 0.0;
var anglePyramidRadian = 0.0;

var sphere = null;


var requestAnimationFrame =
window.requestAnimationFrame||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame;

var cancelAnimationFrame = 
window.cancelAnimationFrame||
window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
window.mozCancelRequestAnimationFrame ||window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

//onload function
function main()
{
	//get canvas element
	canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining canvas failed\n");
	else
		console.log("Obtaining canvas successed\n");
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
	
		
	//register keyboard's keydown event handler
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);
	
	//initialize WebGL
	init();
	
	//Start drawing here as warming-up
	resize();
	draw();

}

function toggleFullScreen()
{
	//code
	var fullscreen_element =
	document.fullscreenElement ||
	document.webkitFullscreenElement ||
	document.mozFullScreenElement ||
	document.msFullscreenElement ||
	null;
	
	//if not fullscreen
	if(fullscreen_element == null)
	{
	if(canvas.requestFullscreen)
		canvas.requestFullscreen();
	else if(canvas.mozRequestFullScreen)
		canvas.mozRequestFullScreen();
	else if(canvas.webkitRequestFullscreen)
		canvas.webkitRequestFullScreen();
	else if(canvas.msRequestFullscreen)
		canvas.msRequestFullscreen();
	bFullscreen = true;
	}
	else // if already fullscreen
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		bFullscreen = false;
	}
}

function init()
{
	//code
	//get WebGL 2.0 context
	gl = canvas.getContext("webgl2");
	if(gl==null) //failed to get context
	{
		console.log("Failed to get rendering  context for WebGL");
		return;
	}
	gl.viewportWidth =  canvas.width;
	gl.viewportHeight = canvas.height;
	
	//vertex shader
	var vertexShaderSourceCode = 
	        "#version 300 es" +
            "\n" +
			"in vec4 vPosition;" +
			"in vec3 vNormal;" +
			"uniform mat4 u_model_matrix;" +
			"uniform mat4 u_view_matrix;" +
			"uniform mat4 u_projection_matrix;" +
			"uniform mediump int u_LKeyPressed;" +
			"uniform vec4 u_light0_position;" +
			"uniform vec4 u_light1_position;" +
			"uniform vec4 u_light2_position;" +
			"out vec3 transformed_normals;" +
			"out vec3 light0_direction;" +
			"out vec3 light1_direction;" +
			"out vec3 light2_direction;" +
			"out vec3 viewer_vector;" +
			"void main(void)" +
			"{" +
			"if (u_LKeyPressed == 1)" +
			"{" +
			"vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;" +
			"transformed_normals =  normalize(mat3(u_view_matrix * u_model_matrix ) * vNormal);" +
			"light0_direction = normalize(vec3(u_light0_position) - eye_coordinates.xyz);" +
			"light1_direction = normalize(vec3(u_light1_position) - eye_coordinates.xyz);" +
			"light2_direction = normalize(vec3(u_light2_position) - eye_coordinates.xyz);" +
			"viewer_vector = -eye_coordinates.xyz;" +
			"}" +
            "gl_Position = u_projection_matrix  * u_view_matrix * u_model_matrix * vPosition;" +
			"}";

	
	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	if(gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS)==false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	
	var fragmentShaderSourceCode = 
	        "#version 300 es"+
	        "\n" +
			"precision highp float;" +
			"in vec3 transformed_normals;" +
			"in vec3 light0_direction;" +
			"in vec3 light1_direction;" +
			"in vec3 light2_direction;" +
			"in vec3 viewer_vector;" +
			"out vec4 FragColor;" +
			"uniform vec3 u_La0;" +
			"uniform vec3 u_Ld0;" +
			"uniform vec3 u_Ls0;" +
			"uniform vec3 u_La1;" +
			"uniform vec3 u_Ld1;" +
			"uniform vec3 u_Ls1;" +
			"uniform vec3 u_La2;" +
			"uniform vec3 u_Ld2;" +
			"uniform vec3 u_Ls2;" +
			"uniform vec3 u_Ka;" +
			"uniform vec3 u_Kd;" +
			"uniform vec3 u_Ks;" +
			"uniform float u_material_shininess;" +
			"uniform mediump int u_LKeyPressed;" +
			"void main(void)" +
			"{" +
			"vec3 light0;" +
			"vec3 light1;" +
			"vec3 light2;" +
			"vec3 phong_ads_color;" +
			"if(u_LKeyPressed == 1)" +
			"{" +
			"vec3 normalized_transformed_normals = normalize(transformed_normals);" +
			"vec3 normalized_light0_direction = normalize(light0_direction);" +
			"vec3 normalized_light1_direction = normalize(light1_direction);" +
			"vec3 normalized_light2_direction = normalize(light2_direction);" +
			"vec3 normalized_viewer_vector = normalize(viewer_vector);" +
			"float tn_dot_ld0 = max(dot(normalized_transformed_normals, normalized_light0_direction), 0.0);" +
			"vec3 ambient0 = u_La0 * u_Ka;" +
			"vec3 diffuse0 = u_Ld0 * u_Kd * tn_dot_ld0;" +
			"vec3 reflection_vector0 = reflect(-normalized_light0_direction,normalized_transformed_normals);" +
			"vec3 specular0 = u_Ls0 * u_Ks * pow(max(dot(reflection_vector0, normalized_viewer_vector), 0.0), u_material_shininess);" +
			"float tn_dot_ld1 = max(dot(normalized_transformed_normals, normalized_light1_direction), 0.0);" +
			"vec3 ambient1 = u_La1 * u_Ka;" +
			"vec3 diffuse1 = u_Ld1 * u_Kd * tn_dot_ld1;" +
			"vec3 reflection_vector1 = reflect(-normalized_light1_direction,normalized_transformed_normals);" +
			"vec3 specular1 = u_Ls1 * u_Ks * pow(max(dot(reflection_vector1, normalized_viewer_vector), 0.0), u_material_shininess);" +
			"float tn_dot_ld2 = max(dot(normalized_transformed_normals, normalized_light2_direction), 0.0);" +
			"vec3 ambient2 = u_La2 * u_Ka;" +
			"vec3 diffuse2 = u_Ld2 * u_Kd * tn_dot_ld2;" +
			"vec3 reflection_vector2 = reflect(-normalized_light2_direction,normalized_transformed_normals);" +
			"vec3 specular2 = u_Ls2 * u_Ks * pow(max(dot(reflection_vector2, normalized_viewer_vector), 0.0), u_material_shininess);" +
			"light0 = ambient0 + diffuse0 + specular0;" +
			"light1 = ambient1 + diffuse1 + specular1;" +
			"light2 = ambient2 + diffuse2 + specular2;" +
			"phong_ads_color = light0;" +
			"}" +
			"else" +
			"{" +
			"phong_ads_color =  vec3(1.0,  1.0,  1.0);" +
			"}" +
			"FragColor = vec4(phong_ads_color, 1.0);" +
			"}";
	
	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	if(gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS)==false)
	{
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
	
	//Shader program
	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);
	
	//Pre-link binding of shader program object with vertex shader attributes
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.VDG_ATTRIBUTE_NORMAL, "vNormal");
	
	//linking
	gl.linkProgram(shaderProgramObject);
	if(!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	
	
	//get MVP uniform location
//	mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");
	modelMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_model_matrix");
	viewMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_view_matrix");
	rotationMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_rotation_matrix");
	projectionMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_projection_matrix");
    
	LKeyPressedUniform = gl.getUniformLocation(shaderProgramObject, "u_LKeyPressed");

	la0Uniform = gl.getUniformLocation(shaderProgramObject, "u_La0");
	ld0Uniform = gl.getUniformLocation(shaderProgramObject, "u_Ld0");
	ls0Uniform = gl.getUniformLocation(shaderProgramObject, "u_Ls0");
	light0PositionUniform = gl.getUniformLocation(shaderProgramObject, "u_light0_position");

	la1Uniform = gl.getUniformLocation(shaderProgramObject, "u_La1");
	ld1Uniform = gl.getUniformLocation(shaderProgramObject, "u_Ld1");
	ls1Uniform = gl.getUniformLocation(shaderProgramObject, "u_Ls1");
	light1PositionUniform = gl.getUniformLocation(shaderProgramObject, "u_light1_position");

	la2Uniform = gl.getUniformLocation(shaderProgramObject, "u_La2");
	ld2Uniform = gl.getUniformLocation(shaderProgramObject, "u_Ld2");
	ls2Uniform = gl.getUniformLocation(shaderProgramObject, "u_Ls2");
	light2PositionUniform = gl.getUniformLocation(shaderProgramObject, "u_light2_position");
        

	kaUniform = gl.getUniformLocation(shaderProgramObject, "u_Ka"); 
	kdUniform = gl.getUniformLocation(shaderProgramObject, "u_Kd");
	ksUniform = gl.getUniformLocation(shaderProgramObject, "u_Ks");
	materialShininessUniform = gl.getUniformLocation(shaderProgramObject, "u_material_shininess");
	
	
    // *** Vertices, colors, shader attributes, vbo, vao initializations ***
    sphere = new Mesh();
	makeSphere(sphere, 2.0, 30, 30);

		gl.clearDepth(1.0); // range is 0 to 1
		gl.enable(gl.DEPTH_TEST);
		gl.depthFunc(gl.LEQUAL);
			
	//Set clear color
	gl.clearColor(0.25, 0.25, 0.25, 1.0); //Gray
	
	//initialize projection matrix
	perspectiveProjectionMatrix = mat4.create();
	
}

function  resize()
{
	//code
	if(bFullscreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}
	
	//set the viewport to match
	gl.viewport(0, 0, canvas.width, canvas.height);
	width1 = canvas.width;
	height1 = canvas.height;
	
	mat4.perspective(perspectiveProjectionMatrix, 45.0, (4.0 / 3.0) * (parseFloat)(canvas.width) / (parseFloat)(canvas.height), 0.1, 100.0);
	
}


function draw()
{	
	//code
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	
	gl.useProgram(shaderProgramObject);

	if (bLKeyPressed == true)
	{
	    gl.uniform1i(LKeyPressedUniform, 1);

        //Set light properties
	    gl.uniform3fv(la0Uniform, light0_ambient);
	    gl.uniform3fv(ld0Uniform, light0_diffuse);
	    gl.uniform3fv(ls0Uniform, light0_specular);
	    gl.uniform4fv(light0PositionUniform, light0_position);

	    gl.uniform3fv(la1Uniform, light1_ambient);
	    gl.uniform3fv(ld1Uniform, light1_diffuse);
	    gl.uniform3fv(ls1Uniform, light1_specular);
	    gl.uniform4fv(light1PositionUniform, light1_position);

	    gl.uniform3fv(la2Uniform, light2_ambient);
	    gl.uniform3fv(ld2Uniform, light2_diffuse);
	    gl.uniform3fv(ls2Uniform, light2_specular);
	    gl.uniform4fv(light2PositionUniform, light2_position);

	   

	    if (Xkey == true)
	    {
	        //Rotate RED light to Y-direction
	        light0_position[0] = red_x;
	        light0_position[1] = red_y;
	        light0_position[2] = red_z;
	    }

	    if (Ykey == true)
	    {
	    //Rotate Blue light to Z-direction
	    light0_position[0] = blue_x;
	    light0_position[1] = blue_y;
	    light0_position[2] = blue_z;
	    }

	    if (Zkey == true)
	    {
	        //Rotate GREEN light to X-direction
	        light0_position[0] = green_x;
	        light0_position[1] = green_y;
	        light0_position[2] = green_z;
	    }

	}
	else
	{
	    gl.uniform1i(LKeyPressedUniform, 0);
	}
    

    //
	
	var modelMatrix = mat4.create();
	var viewMatrix = mat4.create();
	var rotationMatrix = mat4.create();
	var modelViewProjectionMatrix = mat4.create();


	//Translate
	mat4.translate(modelMatrix, modelMatrix, [0.0, 0.0, -6.0]);
	
	gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);

	gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);

	gl.uniformMatrix4fv(rotationMatrixUniform, false, rotationMatrix);

	gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);

    //1st Sphere
    //set material properties
	gl.uniform3fv(kaUniform, material1_ambient);
	gl.uniform3fv(kdUniform, material1_diffuse);
	gl.uniform3fv(ksUniform, material1_specular);
	gl.uniform1f(materialShininessUniform, material1_shininess);

	gl.viewport(0, 5 * height1 / 6, width1 / 4, height1 / 6);
	sphere.draw();

    //2nd Sphere
    //set material properties
	gl.uniform3fv(kaUniform, material2_ambient);
	gl.uniform3fv(kdUniform, material2_diffuse);
	gl.uniform3fv(ksUniform, material2_specular);
	gl.uniform1f(materialShininessUniform, material2_shininess);

   	gl.viewport(0, 4 * height1 / 6, width1 / 4, height1 / 6);
	sphere.draw();

    //3rd Sphere
    //set material properties
	gl.uniform3fv(kaUniform, material3_ambient);
	gl.uniform3fv(kdUniform, material3_diffuse);
	gl.uniform3fv(ksUniform, material3_specular);
	gl.uniform1f(materialShininessUniform, material3_shininess);

   	gl.viewport(0, 3 * height1 / 6, width1 / 4, height1 / 6);
   	sphere.draw();

    //4th Sphere
    //set material properties
	gl.uniform3fv(kaUniform, material4_ambient);
	gl.uniform3fv(kdUniform, material4_diffuse);
	gl.uniform3fv(ksUniform, material4_specular);
	gl.uniform1f(materialShininessUniform, material4_shininess);
       
	gl.viewport(0, 2 * height1 / 6, width1 / 4, height1 / 6);
	sphere.draw();

    //5th Sphere
    //set material properties
	gl.uniform3fv(kaUniform, material5_ambient);
	gl.uniform3fv(kdUniform, material5_diffuse);
	gl.uniform3fv(ksUniform, material5_specular);
	gl.uniform1f(materialShininessUniform, material5_shininess);

	gl.viewport(0, 1 * height1 / 6, width1 / 4, height1 / 6);
	sphere.draw();

    //6th Sphere
    //set material properties
	gl.uniform3fv(kaUniform, material6_ambient);
	gl.uniform3fv(kdUniform, material6_diffuse);
	gl.uniform3fv(ksUniform, material6_specular);
	gl.uniform1f(materialShininessUniform, material6_shininess);

	gl.viewport(0, 0 * height1 / 6, width1 / 4, height1 / 6);
	sphere.draw();

    //7th Sphere
    //set material properties
	gl.uniform3fv(kaUniform, material7_ambient);
	gl.uniform3fv(kdUniform, material7_diffuse);
	gl.uniform3fv(ksUniform, material7_specular);
	gl.uniform1f(materialShininessUniform, material7_shininess);

	gl.viewport(1 * width1 / 4, 5 * height1 / 6, width1 / 4, height1 / 6);
	sphere.draw();

    //8th Sphere
    //set material properties
	gl.uniform3fv(kaUniform, material8_ambient);
	gl.uniform3fv(kdUniform, material8_diffuse);
	gl.uniform3fv(ksUniform, material8_specular);
	gl.uniform1f(materialShininessUniform, material8_shininess);

	gl.viewport(1 * width1 / 4, 4 * height1 / 6, width1 / 4, height1 / 6);
	sphere.draw();

    //9th Sphere
    //set material properties
	gl.uniform3fv(kaUniform, material9_ambient);
	gl.uniform3fv(kdUniform, material9_diffuse);
	gl.uniform3fv(ksUniform, material9_specular);
	gl.uniform1f(materialShininessUniform, material9_shininess);

	gl.viewport(1 * width1 / 4, 3 * height1 / 6, width1 / 4, height1 / 6);
	sphere.draw();

    //10th Sphere
    //set material properties
	gl.uniform3fv(kaUniform, material10_ambient);
	gl.uniform3fv(kdUniform, material10_diffuse);
	gl.uniform3fv(ksUniform, material10_specular);
	gl.uniform1f(materialShininessUniform, material10_shininess);

	gl.viewport(1 * width1 / 4, 2 * height1 / 6, width1 / 4, height1 / 6);
	sphere.draw();

    //11th Sphere
    //set material properties
	gl.uniform3fv(kaUniform, material11_ambient);
	gl.uniform3fv(kdUniform, material11_diffuse);
	gl.uniform3fv(ksUniform, material11_specular);
	gl.uniform1f(materialShininessUniform, material11_shininess);

	gl.viewport(1 * width1 / 4, 1 * height1 / 6, width1 / 4, height1 / 6);
	sphere.draw();

    //12th Sphere
    //set material properties
	gl.uniform3fv(kaUniform, material12_ambient);
	gl.uniform3fv(kdUniform, material12_diffuse);
	gl.uniform3fv(ksUniform, material12_specular);
	gl.uniform1f(materialShininessUniform, material12_shininess);

	gl.viewport(1 * width1 / 4, 0 * height1 / 6, width1 / 4, height1 / 6);
	sphere.draw();

    //13th Sphere
    //set material properties
	gl.uniform3fv(kaUniform, material13_ambient);
	gl.uniform3fv(kdUniform, material13_diffuse);
	gl.uniform3fv(ksUniform, material13_specular);
	gl.uniform1f(materialShininessUniform, material13_shininess);

 	gl.viewport(2 * width1 / 4, 5 * height1 / 6, width1 / 4, height1 / 6);
	sphere.draw();


    //14th Sphere
    //set material properties
	gl.uniform3fv(kaUniform, material14_ambient);
	gl.uniform3fv(kdUniform, material14_diffuse);
	gl.uniform3fv(ksUniform, material14_specular);
	gl.uniform1f(materialShininessUniform, material14_shininess);

	gl.viewport(2 * width1 / 4, 4 * height1 / 6, width1 / 4, height1 / 6);
	sphere.draw();

    //15th Sphere
    //set material properties
	gl.uniform3fv(kaUniform, material15_ambient);
	gl.uniform3fv(kdUniform, material15_diffuse);
	gl.uniform3fv(ksUniform, material15_specular);
	gl.uniform1f(materialShininessUniform, material15_shininess);

	gl.viewport(2 * width1 / 4, 3 * height1 / 6, width1 / 4, height1 / 6);
	sphere.draw();

    //16th Sphere
    //set material properties
	gl.uniform3fv(kaUniform, material16_ambient);
	gl.uniform3fv(kdUniform, material16_diffuse);
	gl.uniform3fv(ksUniform, material16_specular);
	gl.uniform1f(materialShininessUniform, material16_shininess);

	gl.viewport(2 * width1 / 4, 2 * height1 / 6, width1 / 4, height1 / 6);
	sphere.draw();

    //17th Sphere
    //set material properties
	gl.uniform3fv(kaUniform, material17_ambient);
	gl.uniform3fv(kdUniform, material17_diffuse);
	gl.uniform3fv(ksUniform, material17_specular);
	gl.uniform1f(materialShininessUniform, material17_shininess);

	gl.viewport(2 * width1 / 4, 1 * height1 / 6, width1 / 4, height1 / 6);
	sphere.draw();

    //18th Sphere
    //set material properties
	gl.uniform3fv(kaUniform, material18_ambient);
	gl.uniform3fv(kdUniform, material18_diffuse);
	gl.uniform3fv(ksUniform, material18_specular);
	gl.uniform1f(materialShininessUniform, material18_shininess);

	gl.viewport(2 * width1 / 4, 0 * height1 / 6, width1 / 4, height1 / 6);
	sphere.draw();

    //19th Sphere
    //set material properties
	gl.uniform3fv(kaUniform, material19_ambient);
	gl.uniform3fv(kdUniform, material19_diffuse);
	gl.uniform3fv(ksUniform, material19_specular);
	gl.uniform1f(materialShininessUniform, material19_shininess);

	gl.viewport(3 * width1 / 4, 5 * height1 / 6, width1 / 4, height1 / 6);
	sphere.draw();

    //20th Sphere
    //set material properties
	gl.uniform3fv(kaUniform, material20_ambient);
	gl.uniform3fv(kdUniform, material20_diffuse);
	gl.uniform3fv(ksUniform, material20_specular);
	gl.uniform1f(materialShininessUniform, material20_shininess);

	gl.viewport(3 * width1 / 4, 4 * height1 / 6, width1 / 4, height1 / 6);
	sphere.draw();

    //21th Sphere
    //set material properties
	gl.uniform3fv(kaUniform, material21_ambient);
	gl.uniform3fv(kdUniform, material21_diffuse);
	gl.uniform3fv(ksUniform, material21_specular);
	gl.uniform1f(materialShininessUniform, material21_shininess);

	gl.viewport(3 * width1 / 4, 3 * height1 / 6, width1 / 4, height1 / 6);
	sphere.draw();

    //22th Sphere
    //set material properties
	gl.uniform3fv(kaUniform, material22_ambient);
	gl.uniform3fv(kdUniform, material22_diffuse);
	gl.uniform3fv(ksUniform, material22_specular);
	gl.uniform1f(materialShininessUniform, material22_shininess);

	gl.viewport(3 * width1 / 4, 2 * height1 / 6, width1 / 4, height1 / 6);
	sphere.draw();

    //23th Sphere
    //set material properties
	gl.uniform3fv(kaUniform, material23_ambient);
	gl.uniform3fv(kdUniform, material23_diffuse);
	gl.uniform3fv(ksUniform, material23_specular);
	gl.uniform1f(materialShininessUniform, material23_shininess);

	gl.viewport(3 * width1 / 4, 1 * height1 / 6, width1 / 4, height1 / 6);
	sphere.draw();

    //24th Sphere
    //set material properties
	gl.uniform3fv(kaUniform, material24_ambient);
	gl.uniform3fv(kdUniform, material24_diffuse);
	gl.uniform3fv(ksUniform, material24_specular);
	gl.uniform1f(materialShininessUniform, material24_shininess);

	gl.viewport(3 * width1 / 4, 0 * height1 / 6, width1 / 4, height1 / 6);
	sphere.draw();

	
	gl.useProgram(null);
	
	if (gbAnimate == true)
	update();
	
	//Animation loop
	requestAnimationFrame(draw, canvas);

	
	
}

function uninitialize()
{

	//code
	if(sphere)
	{
	    sphere.deallocate();
		sphere = null;
	}
	
	
	
	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}
		
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}
		
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
		
	}
	
}
function keyDown(event)
{
		//code
		switch(event.keyCode)
		{
			case 27: //Escape
				//uninitialize
				uninitialize();
				//Close our application's tab
				window.close(); //Works is Safari and chrome but not in Firefox
				break;
			case 70: //for 'f' or 'F'
				toggleFullScreen();
				break;
		    case 76: // For 'l' or 'L'
		        if (bLKeyPressed == false)
		            bLKeyPressed = true;
		        else
		            bLKeyPressed = false;
		        break;
		    case 65: //for 'A' or 'a'
		        if (bIsAKeyPressed == false) {
		            gbAnimate = true;
		            bIsAKeyPressed = true;
		        }
		        else {
		            gbAnimate = false;
		            bIsAKeyPressed = false;
		        }
		        break;
		    case 88: //For 'x' or 'X'
		        {
		            Xkey = true;
		            Ykey = false;
		            Zkey = false;
		        }
		        break;
		    case 89: //For 'y' or 'Y'
		        {
		            Xkey = false;
		            Ykey = true;
		            Zkey = false;
		        }
		        break;
		    case 90: //For 'z' or 'Z'
		        {
		            Xkey = false;
		            Ykey = false;
		            Zkey = true;
		        }
		        break;
			
						
		}
		
}
	
function mouseDown()
{
		//code
		
}




function update()
{	
    // Circle equalation  Circle_X= Xcenter + rcosQ , Circle_Y = Ycenter + rsinQ, Q = 0 to 2 * Pi
    //Math.cosQ - here Q is in radians
    angleCircle = angleCircle - 0.05;
    if (angleCircle <= -(2.0 * 3.14159))  // minus sign for anti-clockwise  direction
    angleCircle = 0.0;

    red_x = Xcenter + 100.0 * Math.cos(angleCircle);
    red_y = Ycenter;
    red_z = Zcenter + 100.0 * Math.sin(angleCircle);
	
    green_x = Xcenter + 100.0 * Math.sin(angleCircle);
    green_y = Ycenter + 100.0 * Math.cos(angleCircle);
    green_z = Zcenter;
	
    blue_x = Xcenter;
    blue_y = Ycenter + 100.0 * Math.sin(angleCircle);
    blue_z = Zcenter + 100.0 * Math.cos(angleCircle);
	

}

function degToRad(degree)
{
	//code
	
	radian =(degree * Math.PI) / 180.0 // PI = 3.14159
	
	return radian;
}	
