//global variable
var canvas = null;
var context = null;

//onload function
function main()
{
	//get canvas element
	canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining canvas failed\n");
	else
		console.log("Obtaining canvas successed\n");
	
	//print canvas height and width on console
	console.log("Canvas width : "+canvas.width+" and Canvas height : "+canvas.height);
	
	//get 2D context
	 context = canvas.getContext("2d");
	if(!context)
		console.log("Obtaining 2D context failed\n");
	else
		console.log("Obtaining 2D context successed\n");
	
	//fill canvas with black color
	context.fillStyle ="black"; // "#000000"
	context.fillRect(0, 0, canvas.width, canvas.height);
	
	//draw text
	drawText("Hello World !!!");
	
	//register keyboard's keydown event handler
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
}

function drawText(text)
{	
	//code
	//center the text
	context.textAlign = "center"; //center horizontally
	context.textBaseline = "middle"; //center vertically
	
	//text font
	context.font ="48px sans-serif";
	
	//text color
	context.fillStyle ="white"; // "#FFFFFF"
	
	//display the text in center
	context.fillText(text,canvas.width/2, canvas.height/2);
}

function toggleFullScreen()
{
	//code
	var fullscreen_element =
	document.fullscreenElement ||
	document.webkitFullscreenElement ||
	document.mozFullscreenElement ||
	document.msFullScreenElement ||
	null;
	
	//if not fullscreen
	if(fullscreen_element == null)
	{
	if(canvas.requestFullscreen)
		canvas.requestFullscreen();
	else if(canvas.mozRequestFullScreen)
		canvas.mozRequestFullScreen();
	else if(canvas.webkitRequestFullScreen)
		canvas.webkitRequestFullScreen();
	else if(canvas.msRequestFullscreen)
		canvas.msRequestFullscreen();
	}
	else // if already fullscreen
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
	}
}
	
	function keyDown(event)
	{
		//code
		switch(event.keyCode)
		{
			case 70: //for 'f' or 'F'
			toggleFullScreen();
			
			//repaint
			drawText("Hello World !!!");
			break;
		}
		
	}
	
	function mouseDown()
	{
		//code
		
	}
	
	
