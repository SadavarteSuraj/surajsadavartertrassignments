package com.astromedicomp.lightMaterials;

import android.content.Context; // for drawing context related
import android.widget.TextView; // for "TextView" class
import android.graphics.Color; // For Color class
import android.view.Gravity; // For Gravity class
import android.view.MotionEvent; //For MotionEvent
import android.view.GestureDetector; // For Gesturedetector
import android.view.GestureDetector.OnGestureListener; // For OnGestureListener
import android.view.GestureDetector.OnDoubleTapListener; // For  OnDoubleTapListener

import android.opengl.GLSurfaceView; // For OpenGL surface view and all related
import javax.microedition.khronos.opengles.GL10; //For OpenGLES 1.0 needed as param type GL10
import javax.microedition.khronos.egl.EGLConfig; //For EGLConfig needed as param type EGLConfig
import android.opengl.GLES32; // For OpenGLES3.2
import java.nio.ShortBuffer; //

//for vbo
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import  android.opengl.Matrix; // For matrix math

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
	private final Context context;
	private GestureDetector gestureDetector;

	private int vertexShaderObject;
	private int  fragmentShaderObject;
	private int shaderProgramObject;

	public float red_x = 0.0f;
	public float red_y = 0.0f;
	public float red_z = 0.0f;

	public float green_x = 0.0f;
	public float green_y = 0.0f;
	public float green_z = 0.0f;

	public float blue_x = 0.0f;
	public float blue_y = 0.0f;
	public float blue_z = 0.0f;

	public float Xcenter = 0.0f;
	public float Ycenter = 0.0f;
	public float Zcenter = 0.0f;

	public float angleCircle = 0.0f;

	public float width1, height1;

	private int numElements;
	private int numVertices;

	private int[] vao_sphere = new int[1];
	private int[] vbo_sphere_position = new int[1];
	private int[] vbo_sphere_normal = new int[1];
	private int[] vbo_sphere_element = new int[1];

	private float light0_ambient[] = {0.0f, 0.0f, 0.0f, 1.0f};
	private float light0_diffuse[] = {1.0f, 1.0f, 1.0f, 1.0f}; //white
	private float light0_specular[] = {1.0f, 1.0f, 1.0f, 1.0f};
	private float light0_position[] = {200.0f, 100.0f, 100.0f, 1.0f};

	private float light1_ambient[] = {0.0f, 0.0f, 0.0f, 1.0f};
	private float light1_diffuse[] = {0.0f, 1.0f, 0.0f, 1.0f}; //GREEN
	private float light1_specular[] = {1.0f, 1.0f, 1.0f, 1.0f};
	public float light1_position[] = {200.0f, 100.0f, 100.0f, 1.0f};

	private float light2_ambient[] = {0.0f, 0.0f, 0.0f, 1.0f};
	private float light2_diffuse[] = {0.0f, 0.0f, 1.0f, 1.0f}; //BLUE
	private float light2_specular[] = {1.0f, 1.0f, 1.0f, 1.0f};
	public float light2_position[] = {200.0f, 100.0f, 100.0f, 1.0f};

//	private float material_ambient[] = {0.0f, 0.0f, 0.0f, 1.0f};
//	private float material_diffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};
//	private float material_specular[] = {1.0f, 1.0f, 1.0f, 1.0f};
//	public float material_shininess = 50.0f;

	//First sphere on first column, emerald
	private float material1_ambient[] = { 0.0215f, 0.1745f, 0.0215f, 1.0f };
	private float material1_diffuse[] = { 0.07568f, 0.61424f, 0.07568f, 1.0f };
	private float material1_specular[] = { 0.633f, 0.727811f, 0.633f, 1.0f };
	private float material1_shininess = 0.6f * 128.0f;

	//Second sphere on first column, jade
	private float material2_ambient[] = { 0.135f, 0.2225f, 0.1575f, 1.0f };
	private float material2_diffuse[] = { 0.54f, 0.89f, 0.63f, 1.0f };
	private float material2_specular[] = { 0.316228f, 0.316228f, 0.316228f, 1.0f };
	private float material2_shininess = 0.1f * 128.0f;

	//Third sphere on first column, obsidian
	private float material3_ambient[] = { 0.05375f, 0.05f, 0.06625f, 1.0f };
	private float material3_diffuse[] = { 0.18275f, 0.17f, 0.22525f, 1.0f };
	private float material3_specular[] = { 0.332741f, 0.328634f, 0.346435f, 1.0f };
	private float material3_shininess = 0.3f * 128.0f;

	//Fourth sphere on first column, pearl
	private float material4_ambient[] = { 0.25f, 0.20725f, 0.20725f, 1.0f };
	private float material4_diffuse[] = { 1.0f, 0.829f, 0.829f, 1.0f };
	private float material4_specular[] = { 0.296648f, 0.296648f, 0.296648f, 1.0f };
	private float material4_shininess = 0.088f * 128.0f;

	//Fifth sphere on first column, ruby
	private float material5_ambient[] = { 0.1745f, 0.01175f, 0.01175f, 1.0f };
	private float material5_diffuse[] = { 0.61424f, 0.04136f, 0.04136f, 1.0f };
	private float material5_specular[] = { 0.727811f, 0.626959f, 0.626959f, 1.0f };
	private float material5_shininess = 0.6f * 128.0f;

	//Sixth sphere on first column, turquoise
	private float material6_ambient[] = { 0.1f, 0.18725f, 0.1745f, 1.0f };
	private float material6_diffuse[] = { 0.396f, 0.74151f, 0.69102f, 1.0f };
	private float material6_specular[] = { 0.297254f, 0.30829f, 0.306678f, 1.0f };
	private float material6_shininess = 1.0f;

	//First sphere on second column, brass
	private float material7_ambient[] = { 0.329412f, 0.223529f, 0.027451f, 1.0f };
	private float material7_diffuse[] = { 0.780392f, 0.568627f, 0.113725f, 1.0f };
	private float material7_specular[] = { 0.992157f, 0.941176f, 0.807843f, 1.0f };
	private float material7_shininess = 0.21794872f * 128.0f;

	//Second sphere on second column, bronze
	private float material8_ambient[] = { 0.2125f, 0.1275f, 0.054f, 1.0f };
	private float material8_diffuse[] = { 0.714f, 0.4284f, 0.18144f, 1.0f };
	private float material8_specular[] = { 0.393548f, 0.211906f, 0.166721f, 1.0f };
	private float material8_shininess = 0.2f * 128.0f;

	//Third sphere on second column, chrome
	private float material9_ambient[] = { 0.25f, 0.25f, 0.25f, 1.0f };
	private float material9_diffuse[] = { 0.4f, 0.4f, 0.4f, 1.0f };
	private float material9_specular[] = { 0.774597f, 0.774597f, 0.774597f, 1.0f };
	private float material9_shininess = 0.6f * 128.0f;

	//Fourth sphere on second column, copper
	private float material10_ambient[] = { 0.19125f, 0.0735f, 0.0225f, 1.0f };
	private float material10_diffuse[] = { 0.7038f, 0.27048f, 0.0828f, 1.0f };
	private float material10_specular[] = { 0.276777f, 0.137622f, 0.086014f, 1.0f };
	private float material10_shininess = 0.1f * 128.0f;

	//Fifth sphere on second column, gold
	private float material11_ambient[] = { 0.24725f, 0.1995f, 0.07455f, 1.0f };
	private float material11_diffuse[] = { 0.75164f, 0.60648f, 0.22648f, 1.0f };
	private float material11_specular[] = { 0.628281f, 0.555802f, 0.366065f, 1.0f };
	private float material11_shininess = 0.4f * 128.0f;

	//Sixth sphere on second column, silver
	private float material12_ambient[] = { 0.19225f, 0.19225f, 0.19225f, 1.0f };
	private float material12_diffuse[] = { 0.50754f, 0.50754f, 0.50754f, 1.0f };
	private float material12_specular[] = { 0.508273f, 0.508273f, 0.508273f, 1.0f };
	private float material12_shininess = 0.4f * 128.0f;

	//First sphere on third column, black
	private float material13_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
	private float material13_diffuse[] = { 0.01f, 0.01f, 0.01f, 1.0f };
	private float material13_specular[] = { 0.50f, 0.50f, 0.50f, 1.0f };
	private float material13_shininess = 0.25f * 128.0f;

	//Second sphere on third column, cyan
	private float material14_ambient[] = { 0.0f, 0.1f, 0.06f, 1.0f };
	private float material14_diffuse[] = { 0.0f, 0.50980392f, 0.50980392f, 1.0f };
	private float material14_specular[] = { 0.50196078f, 0.50196078f, 0.50196078f, 1.0f };
	private float material14_shininess =  0.25f * 128.0f;

	//Third sphere on third column, green
	private float material15_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
	private float material15_diffuse[] = { 0.1f, 0.35f, 0.1f, 1.0f };
	private float material15_specular[] = { 0.45f, 0.55f, 0.45f, 1.0f };
	private float material15_shininess = 0.25f * 128.0f;

	//Fourth sphere on third column, red
	private float material16_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
	private float material16_diffuse[] = { 0.5f, 0.0f, 0.0f, 1.0f };
	private float material16_specular[] = { 0.7f, 0.6f, 0.6f, 1.0f };
	private float material16_shininess = 0.25f * 128.0f;

	//Fifth sphere on third column, white
	private float material17_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
	private float material17_diffuse[] = { 0.55f, 0.55f, 0.55f, 1.0f };
	private float material17_specular[] = { 0.70f, 0.70f, 0.70f, 1.0f };
	private float material17_shininess = 0.25f * 128.0f;

	//Sixth sphere on third column, yellow  plastic
	private float material18_ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
	private float material18_diffuse[] = { 0.5f, 0.5f, 0.0f, 1.0f };
	private float material18_specular[] = { 0.60f, 0.60f, 0.50f, 1.0f };
	private float material18_shininess = 0.25f * 128.0f;

	//First sphere on fourth column, black
	private float material19_ambient[] = { 0.02f, 0.02f, 0.02f, 1.0f };
	private float material19_diffuse[] = { 0.01f, 0.01f, 0.01f, 1.0f };
	private float material19_specular[] = { 0.4f, 0.4f, 0.4f, 1.0f };
	private float material19_shininess = 0.078125f * 128.0f;

	//Second sphere on fourth column, cyan
	private float material20_ambient[] = { 0.0f, 0.05f, 0.05f, 1.0f };
	private float material20_diffuse[] = { 0.4f, 0.5f, 0.5f, 1.0f };
	private float material20_specular[] = { 0.04f, 0.7f, 0.7f, 1.0f };
	private float material20_shininess = 0.078125f * 128.0f;

	//Third sphere on fourth column, green
	private float material21_ambient[] = { 0.0f, 0.05f, 0.0f, 1.0f };
	private float material21_diffuse[] = { 0.4f, 0.5f, 0.4f, 1.0f };
	private float material21_specular[] = { 0.04f, 0.7f, 0.04f, 1.0f };
	private float material21_shininess = 0.078125f * 128.0f;

	//Fourth sphere on fourth column, red
	private float material22_ambient[] = { 0.05f, 0.0f, 0.0f, 1.0f };
	private float material22_diffuse[] = { 0.5f, 0.4f, 0.4f, 1.0f };
	private float material22_specular[] = { 0.7f, 0.04f, 0.04f, 1.0f };
	private float material22_shininess = 0.078125f * 128.0f;

	//Fifth sphere on fourth column, white
	private float material23_ambient[] = { 0.05f, 0.05f, 0.05f, 1.0f };
	private float material23_diffuse[] = { 0.5f, 0.5f, 0.5f, 1.0f };
	private float material23_specular[] = { 0.7f, 0.7f, 0.7f, 1.0f };
	private float material23_shininess = 0.078125f * 128.0f;

	//Sixth sphere on fourth column, yellow rubber
	private float material24_ambient[] = { 0.05f, 0.05f, 0.0f, 1.0f };
	private float material24_diffuse[] = { 0.5f, 0.5f, 0.4f, 1.0f };
	private float material24_specular[] = { 0.7f, 0.7f, 0.04f, 1.0f };
	private float material24_shininess = 0.078125f * 128.0f;


	private int modelMatrixUniform, viewMatrixUniform, projectionMatrixUniform;
	private int la0Uniform, ld0Uniform, ls0Uniform, lightPosition0Uniform;
	private int la1Uniform, ld1Uniform, ls1Uniform, lightPosition1Uniform;
	private int la2Uniform, ld2Uniform, ls2Uniform, lightPosition2Uniform;
	private int kaUniform, kdUniform, ksUniform, materialShininessUniform;

	private int gLightPositionUniform;
	private int doubleTapUniform;
	
	private float perspectiveProjectionMatrix[] = new float[16];  // 4x4 matrix

	private int singleTap;
	private int doubleTap;
	
	public GLESView(Context drawingContext)
	{	
		super(drawingContext);

		context = drawingContext;

		//Accordingly set EGLContext to current supported version of OpenGL-ES
		setEGLContextClientVersion(3);

		//Set renderer for drawing on the GLSurfaceView
		setRenderer(this);

		//Render the view only when there is change in drawing data
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

		
		gestureDetector = new GestureDetector(context, this, null, false); // This means 'handler' ie who is going to handle
		gestureDetector.setOnDoubleTapListener(this); // This means 'handler' ie who is going to handle

	}

	//overriden method of GLSurfaceView.Renderer (Init mode)
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		//OpenGL ES version check
		String glesVersion = gl.glGetString(GL10.GL_VERSION);
		System.out.println("VDG :  OpenGL-ES Version = "+glesVersion);
		// get GLSL version
		String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("VDG: GLSL Version "+glslVersion);

		initialize(gl);
	}


	//overriden method of GLSurfaceView.Renderer (change size code)
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
		resize(width, height);
	}

	//overriden method of GLSurfaceView.Renderer (Rendering code)
	@Override
	public void onDrawFrame(GL10 unused)
	{
		draw();
		
	}

	//Handling OnTouchEvent is most important
	//Because it triggers all gestures and tap events
	@Override
	public boolean onTouchEvent(MotionEvent  event)
		{
		//code
		int eventaction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		return(true);
	}

	//Abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		//code
		System.out.println("VDG: "+"Double Tap");
		doubleTap++;
		if(doubleTap > 1)
			doubleTap = 0;
		return(true);
	}

	//Abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		// Do not write any code here, because alrady written 'OnDoubleTap'
		return(true);
	}

	//Abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		System.out.println("VDG: "+"Single Tap");
		return(true);
	}

	//Abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDown(MotionEvent e)
	{
		// Do not write any code here, because alrady written 'OnSingleTapConfirmed'
		return(true);
	}

	//Abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
	{
		return(true);
	}
	
	//Abstract method from OnDoubleTapListener so must be implemented
	@Override
	public void onLongPress(MotionEvent e)
	{
		System.out.println("VDG: "+"Long press");
		
	}

	//Abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		System.out.println("VDG: "+"Scroll");
		System.exit(0); // Exit the app at scroll
		return(true);
	}


//Abstract method from OnDoubleTapListener so must be implemented
	@Override
	public void onShowPress(MotionEvent e)
	{
	
	}

	//Abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		//code
		singleTap++;
		if(singleTap > 3)
			singleTap = 0;
		return(true);
	}

	private void initialize(GL10 gl)
	{
		//***********************************
		// Vertex shader
		//***********************************
		// Create shader
		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);

		// vertex shader source code
		final String vertexShaderSourceCode = String.format
		(
			"#version 320 es"+
			"\n"+
			"in vec4 vPosition;"+
			"in vec3 vNormal;"+
			"uniform mat4 u_model_matrix;"+
			"uniform mat4 u_view_matrix;"+
			"uniform mat4 u_projection_matrix;"+
			"uniform mediump int u_double_tap;"+
			"uniform vec4 u_light0_position;"+
			"uniform vec4 u_light1_position;"+
			"uniform vec4 u_light2_position;"+
			"out vec3 transformed_normals;"+
			"out vec3 light0_direction;"+
			"out vec3 light1_direction;"+
			"out vec3 light2_direction;"+
			"out vec3 viewer_vector;"+
			"void main(void)"+
			"{"+
			"if (u_double_tap == 1)"+
			"{"+
			"vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"+
			"transformed_normals =  normalize(mat3(u_view_matrix * u_model_matrix ) * vNormal);"+
			"light0_direction = normalize(vec3(u_light0_position) - eye_coordinates.xyz);"+
			"light1_direction = normalize(vec3(u_light1_position) - eye_coordinates.xyz);"+
			"light2_direction = normalize(vec3(u_light2_position) - eye_coordinates.xyz);"+
			"viewer_vector = -eye_coordinates.xyz;"+
			"}"+
			
			"gl_Position = u_projection_matrix  * u_view_matrix * u_model_matrix * vPosition;"+
			"}"
		);

		//provide source code to shader
		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);

		//compile shader and check for errors
		GLES32.glCompileShader(vertexShaderObject);
		int[] iShaderCompiledStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;
		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);
		if (iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("VDG:  Vertex Shader Compilation log  = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//***********************************
		// Fragment shader
		//***********************************
		// Create shader
		fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

		// vertex shader source code
		final String fragmentShaderSourceCode = String.format
		(
			"#version 320 es"+
			"\n"+
			"precision highp float;"+
			"in vec3 transformed_normals;"+
			"in vec3 light0_direction;"+
			"in vec3 light1_direction;"+
			"in vec3 light2_direction;"+
			"in vec3 viewer_vector;"+
			"out vec4 FragColor;"+
			"uniform vec3 u_La0;"+
			"uniform vec3 u_Ld0;"+
			"uniform vec3 u_Ls0;"+
			"uniform vec3 u_La1;"+
			"uniform vec3 u_Ld1;"+
			"uniform vec3 u_Ls1;"+
			"uniform vec3 u_La2;"+
			"uniform vec3 u_Ld2;"+
			"uniform vec3 u_Ls2;"+
			"uniform vec3 u_Ka;"+
			"uniform vec3 u_Kd;"+
			"uniform vec3 u_Ks;"+
			"uniform float u_material_shininess;"+
			"uniform int u_double_tap;"+
			"void main(void)"+
			"{"+
			"vec3 light0;"+
			"vec3 light1;"+
			"vec3 light2;"+
			"vec3 phong_ads_color;"+
			"if(u_double_tap == 1)"+
			"{"+
			"vec3 normalized_transformed_normals = normalize(transformed_normals);"+
			"vec3 normalized_light0_direction = normalize(light0_direction);"+
			"vec3 normalized_light1_direction = normalize(light1_direction);"+
			"vec3 normalized_light2_direction = normalize(light2_direction);"+
			"vec3 normalized_viewer_vector = normalize(viewer_vector);"+
			"float tn_dot_ld0 = max(dot(normalized_transformed_normals, normalized_light0_direction), 0.0);"+
			"vec3 ambient0 = u_La0 * u_Ka;"+
			"vec3 diffuse0 = u_Ld0 * u_Kd * tn_dot_ld0;"+
			"vec3 reflection_vector0 = reflect(-normalized_light0_direction,normalized_transformed_normals);"+
			"vec3 specular0 = u_Ls0 * u_Ks * pow(max(dot(reflection_vector0, normalized_viewer_vector), 0.0), u_material_shininess);"+
			"float tn_dot_ld1 = max(dot(normalized_transformed_normals, normalized_light1_direction), 0.0);"+
			"vec3 ambient1 = u_La1 * u_Ka;"+
			"vec3 diffuse1 = u_Ld1 * u_Kd * tn_dot_ld1;"+
			"vec3 reflection_vector1 = reflect(-normalized_light1_direction,normalized_transformed_normals);"+
			"vec3 specular1 = u_Ls1 * u_Ks * pow(max(dot(reflection_vector1, normalized_viewer_vector), 0.0), u_material_shininess);"+
			"float tn_dot_ld2 = max(dot(normalized_transformed_normals, normalized_light2_direction), 0.0);"+
			"vec3 ambient2 = u_La2 * u_Ka;"+
			"vec3 diffuse2 = u_Ld2 * u_Kd * tn_dot_ld2;"+
			"vec3 reflection_vector2 = reflect(-normalized_light2_direction,normalized_transformed_normals);"+
			"vec3 specular2 = u_Ls2 * u_Ks * pow(max(dot(reflection_vector2, normalized_viewer_vector), 0.0), u_material_shininess);"+
			"light0 = ambient0 + diffuse0 + specular0;"+
			"light1 = ambient1 + diffuse1 + specular1;"+
			"light2 = ambient2 + diffuse2 + specular2;"+
			"phong_ads_color = light0;"+
			"}"+
			"else"+
			"{"+
			"phong_ads_color =  vec3(1.0,  1.0,  1.0);"+
			"}"+
			"FragColor = vec4(phong_ads_color, 1.0);"+
			"}"
		);

		//provide source code to shader
		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);

		//compile shader and check for errors
		GLES32.glCompileShader(fragmentShaderObject);
		iShaderCompiledStatus[0] = 0;  //reinitialize
		iInfoLogLength[0] = 0;  //reinitialize
		szInfoLog = null; //reinitialize
		GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);
		if (iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("VDG:  Fragment Shader Compilation log  = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

	//create Shader program
	shaderProgramObject = GLES32.glCreateProgram();

	//attach vertex shader to shader program
	GLES32.glAttachShader(shaderProgramObject,  vertexShaderObject);

	//attach vertex shader to shader program
	GLES32.glAttachShader(shaderProgramObject,  fragmentShaderObject);

	//pre-link binding of shader program object with vertex shader attributes
	GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");

	//pre-link binding of shader program object with vertex shader attributes
	GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.VDG_ATTRIBUTE_NORMAL, "vNormal");

	// Link the two  shaders together to shader program object
	GLES32.glLinkProgram(shaderProgramObject);
	int[] iShaderProgramLinkStatus = new int[1];
	iInfoLogLength[0] = 0;
	szInfoLog = null; //reinitialize
		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iShaderProgramLinkStatus, 0);
		if (iShaderProgramLinkStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
				System.out.println("VDG:  Shader Program Link log  = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//get MVP uniform location
	//	mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");

	modelMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_model_matrix");
	viewMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_view_matrix");
	projectionMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_projection_matrix");

	doubleTapUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_double_tap");

	la0Uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_La0");
	ld0Uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ld0");
	ls0Uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ls0");
	lightPosition0Uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_light0_position");

	la1Uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_La1");
	ld1Uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ld1");
	ls1Uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ls1");
	lightPosition1Uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_light1_position");

	la2Uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_La2");
	ld2Uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ld2");
	ls2Uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ls2");
	lightPosition2Uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_light2_position");


	kaUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ka");
	kdUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Kd");
	ksUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ks");
	materialShininessUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_material_shininess");

		//*** vertices, colors, shader attribs, vbo,  vao initializations ***
		Sphere sphere = new Sphere();
		float sphere_vertices[] = new float[1146];
		float sphere_normals[] = new float[1146];
		float sphere_textures[] = new float[764];
		short sphere_elements[] = new short[2280];
		sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
		numVertices = sphere.getNumberOfSphereVertices();
		numElements = sphere.getNumberOfSphereElements();



		GLES32.glGenVertexArrays(1, vao_sphere, 0);
		GLES32.glBindVertexArray(vao_sphere[0]);

		GLES32.glGenBuffers(1, vbo_sphere_position, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_sphere_position[0]);

		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(sphere_vertices.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer verticesBuffer = byteBuffer.asFloatBuffer();
		verticesBuffer.put(sphere_vertices);
		verticesBuffer.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 
								sphere_vertices.length * 4,  
								verticesBuffer,
								GLES32.GL_STATIC_DRAW);

		GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_VERTEX,
									 3,
									 GLES32.GL_FLOAT,
									 false, 0,  0);

		GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_VERTEX);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		
		//Normal vbo
		GLES32.glGenBuffers(1, vbo_sphere_normal, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_sphere_normal[0]);

		byteBuffer = ByteBuffer.allocateDirect(sphere_normals.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		verticesBuffer = byteBuffer.asFloatBuffer();
		verticesBuffer.put(sphere_normals);
		verticesBuffer.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 
								sphere_normals.length * 4,  
								verticesBuffer,
								GLES32.GL_STATIC_DRAW);

		GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_NORMAL,
									 3,
									 GLES32.GL_FLOAT,
									 false, 0,  0);

		GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_NORMAL);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		//Element vbo
		GLES32.glGenBuffers(1, vbo_sphere_element, 0);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		byteBuffer = ByteBuffer.allocateDirect(sphere_elements.length * 2);
		byteBuffer.order(ByteOrder.nativeOrder());
		ShortBuffer elementsBuffer = byteBuffer.asShortBuffer();
		elementsBuffer.put(sphere_elements);
		elementsBuffer.position(0);

		GLES32.glBufferData(GLES32.GL_ELEMENT_ARRAY_BUFFER, 
								sphere_elements.length * 2,  
								elementsBuffer,
								GLES32.GL_STATIC_DRAW);
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, 0);
		
		GLES32.glBindVertexArray(0);

		//Enable depth testing
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);

		//Depth test to do 
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);

		//cull back face for better performance
		//GLES32.glEnable(GLES32.GL_CULL_FACE);
		

		//Set the background frame Color
		GLES32.glClearColor(0.25f, 0.25f, 0.25f, 1.0f);

		//initialize
		doubleTap = 0;

		//Set projection matrix to identity Matrix
		Matrix.setIdentityM(perspectiveProjectionMatrix,  0);

	}

	private void resize(int width, int height)
	{
		//Adjust the viewport based on geometry changes
		//such as screen rotation
		GLES32.glViewport(0, 0, width, height);
		width1 = width;
		height1 = height;

		//perspective matrix

		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (4.0f / 3.0f)  * ((float)width / (float)height), 0.1f, 100.0f);
	}

	public void draw()
	{
		//Draw background Color
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

		//use shader program
		GLES32.glUseProgram(shaderProgramObject);

			if (doubleTap == 1)
			{
				GLES32.glUniform1i(doubleTapUniform, 1);

				GLES32.glUniform3fv(la0Uniform, 1, light0_ambient, 0);
				GLES32.glUniform3fv(ld0Uniform, 1, light0_diffuse, 0);
				GLES32.glUniform3fv(ls0Uniform, 1, light0_specular, 0);
				GLES32.glUniform4fv(lightPosition0Uniform, 1, light0_position, 0);

				GLES32.glUniform3fv(la1Uniform, 1, light1_ambient, 0);
				GLES32.glUniform3fv(ld1Uniform, 1, light1_diffuse, 0);
				GLES32.glUniform3fv(ls1Uniform, 1, light1_specular, 0);
				GLES32.glUniform4fv(lightPosition1Uniform, 1, light1_position, 0);

				GLES32.glUniform3fv(la2Uniform, 1, light2_ambient, 0);
				GLES32.glUniform3fv(ld2Uniform, 1, light2_diffuse, 0);
				GLES32.glUniform3fv(ls2Uniform, 1, light2_specular, 0);
				GLES32.glUniform4fv(lightPosition2Uniform, 1, light2_position, 0);

				

			// Circle equalation  Circle_X= Xcenter + rcosQ , Circle_Y = Ycenter + rsinQ, Q = 0 to 2 * Pi
			//Math.cosQ - here Q is in radians
					angleCircle = angleCircle - 0.05f;
					if (angleCircle <= -(2.0f * 3.14159f))  // minus sign for anti-clockwise  direction
						angleCircle = 0.0f;

			if (singleTap == 1)
			{
					red_x = Xcenter + (float)(100.0f * Math.cos(angleCircle));
					red_y = Ycenter;
					red_z = Zcenter + 100.0f * (float)(Math.sin(angleCircle));
			}

			if (singleTap == 2)
			{
					red_x = Xcenter + 100.0f *(float)(Math.sin(angleCircle));
					red_y = Ycenter + 100.0f * (float)(Math.cos(angleCircle));
					red_z = Zcenter;
			}
			if (singleTap == 3)
			{
					red_x = Xcenter;
					red_y = Ycenter + 100.0f * (float)(Math.sin(angleCircle));
					red_z = Zcenter + 100.0f * (float)(Math.cos(angleCircle));
			}
							
				//Rotate RED light to Y-direction
				light0_position[0] = red_x;
				light0_position[1] = red_y;
				light0_position[2] = red_z;

				//Rotate Blue light to Z-direction
				light1_position[0] = blue_x;
				light1_position[1] = blue_y;
				light1_position[2] = blue_z;

				//Rotate GREEN light to X-direction
				light2_position[0] = green_x;
				light2_position[1] = green_y;
				light2_position[2] = green_z;
			}
			else
			{
				GLES32.glUniform1i(doubleTapUniform, 0);
			}

		//**** Triangle drawing ****
		//OpenGL-ES drawing
		float modelMatrix[] = new float[16];
		float viewMatrix[] = new float[16];

			
		//Set modelview and modelviewprojection matrices to identity
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.setIdentityM(viewMatrix, 0);
	
		Matrix.translateM(modelMatrix, 0, 0.0f,  0.0f, -1.5f);

		//Pass above modelViewProjectionMatrix to the vertex shader in 'u_mvp_matrix' shader variable
		//whose position value we already calculated in initWithFrame() by using glGetUniformLocation
		
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix, 0);
		GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);

		//bind vaoCube
		GLES32.glBindVertexArray(vao_sphere[0]);

		

		//1st sphere
		GLES32.glViewport(0, (int)(5.0f*height1/6.0f), (int)(width1/4.0f), (int)(height1/6.0f));
		GLES32.glUniform3fv(kaUniform, 1, material1_ambient, 0);
		GLES32.glUniform3fv(kdUniform, 1, material1_diffuse, 0);
		GLES32.glUniform3fv(ksUniform, 1, material1_specular, 0);
		GLES32.glUniform1f(materialShininessUniform, material1_shininess);
		//draw , either glDrawTriangles() or glDrawArrays() or glDrawElements()
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);

		//2nd Sphere
		GLES32.glViewport(0, (int)(4.0f*height1/6.0f), (int)(width1/4.0f), (int)(height1/6.0f));
		GLES32.glUniform3fv(kaUniform, 1, material2_ambient, 0);
		GLES32.glUniform3fv(kdUniform, 1, material2_diffuse, 0);
		GLES32.glUniform3fv(ksUniform, 1, material2_specular, 0);
		GLES32.glUniform1f(materialShininessUniform, material2_shininess);
		//draw , either glDrawTriangles() or glDrawArrays() or glDrawElements()
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);

		//3rd sphere
		GLES32.glViewport(0, (int)(3.0f*height1/6.0f), (int)(width1/4.0f), (int)(height1/6.0f));
		GLES32.glUniform3fv(kaUniform, 1, material3_ambient, 0);
		GLES32.glUniform3fv(kdUniform, 1, material3_diffuse, 0);
		GLES32.glUniform3fv(ksUniform, 1, material3_specular, 0);
		GLES32.glUniform1f(materialShininessUniform, material3_shininess);
		//draw , either glDrawTriangles() or glDrawArrays() or glDrawElements()
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);

		//4th sphere
		GLES32.glViewport(0, (int)(2.0f*height1/6.0f), (int)(width1/4.0f), (int)(height1/6.0f));
		GLES32.glUniform3fv(kaUniform, 1, material4_ambient, 0);
		GLES32.glUniform3fv(kdUniform, 1, material4_diffuse, 0);
		GLES32.glUniform3fv(ksUniform, 1, material4_specular, 0);
		GLES32.glUniform1f(materialShininessUniform, material4_shininess);
		//draw , either glDrawTriangles() or glDrawArrays() or glDrawElements()
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);

		//5th sphere
		GLES32.glViewport(0, (int)(1.0f*height1/6.0f), (int)(width1/4.0f), (int)(height1/6.0f));
		GLES32.glUniform3fv(kaUniform, 1, material5_ambient, 0);
		GLES32.glUniform3fv(kdUniform, 1, material5_diffuse, 0);
		GLES32.glUniform3fv(ksUniform, 1, material5_specular, 0);
		GLES32.glUniform1f(materialShininessUniform, material5_shininess);
		//draw , either glDrawTriangles() or glDrawArrays() or glDrawElements()
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);

		//6th sphere
		GLES32.glViewport(0, (int)(0.0f*height1/6.0f), (int)(width1/4.0f), (int)(height1/6.0f));
		GLES32.glUniform3fv(kaUniform, 1, material6_ambient, 0);
		GLES32.glUniform3fv(kdUniform, 1, material6_diffuse, 0);
		GLES32.glUniform3fv(ksUniform, 1, material6_specular, 0);
		GLES32.glUniform1f(materialShininessUniform, material6_shininess);
		//draw , either glDrawTriangles() or glDrawArrays() or glDrawElements()
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);

		//7th sphere
		GLES32.glViewport((int)(1.0f*width1/4.0f), (int)(5.0f*height1/6.0f), (int)(width1/4.0f), (int)(height1/6.0f));
		GLES32.glUniform3fv(kaUniform, 1, material7_ambient, 0);
		GLES32.glUniform3fv(kdUniform, 1, material7_diffuse, 0);
		GLES32.glUniform3fv(ksUniform, 1, material7_specular, 0);
		GLES32.glUniform1f(materialShininessUniform, material7_shininess);
		//draw , either glDrawTriangles() or glDrawArrays() or glDrawElements()
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);

		//8th sphere
		GLES32.glViewport((int)(1.0f*width1/4.0f), (int)(4.0f*height1/6.0f), (int)(width1/4.0f), (int)(height1/6.0f));
		GLES32.glUniform3fv(kaUniform, 1, material8_ambient, 0);
		GLES32.glUniform3fv(kdUniform, 1, material8_diffuse, 0);
		GLES32.glUniform3fv(ksUniform, 1, material8_specular, 0);
		GLES32.glUniform1f(materialShininessUniform, material8_shininess);
		//draw , either glDrawTriangles() or glDrawArrays() or glDrawElements()
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);

		//9th sphere
		GLES32.glViewport((int)(1.0f*width1/4.0f), (int)(3.0f*height1/6.0f), (int)(width1/4.0f), (int)(height1/6.0f));
		GLES32.glUniform3fv(kaUniform, 1, material9_ambient, 0);
		GLES32.glUniform3fv(kdUniform, 1, material9_diffuse, 0);
		GLES32.glUniform3fv(ksUniform, 1, material9_specular, 0);
		GLES32.glUniform1f(materialShininessUniform, material9_shininess);
		//draw , either glDrawTriangles() or glDrawArrays() or glDrawElements()
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);

		//10th sphere
		GLES32.glViewport((int)(1.0f*width1/4.0f), (int)(2.0f*height1/6.0f), (int)(width1/4.0f), (int)(height1/6.0f));
		GLES32.glUniform3fv(kaUniform, 1, material10_ambient, 0);
		GLES32.glUniform3fv(kdUniform, 1, material10_diffuse, 0);
		GLES32.glUniform3fv(ksUniform, 1, material10_specular, 0);
		GLES32.glUniform1f(materialShininessUniform, material10_shininess);
		//draw , either glDrawTriangles() or glDrawArrays() or glDrawElements()
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);

		//11th sphere
		GLES32.glViewport((int)(1.0f*width1/4.0f), (int)(1.0f*height1/6.0f), (int)(width1/4.0f), (int)(height1/6.0f));
		GLES32.glUniform3fv(kaUniform, 1, material11_ambient, 0);
		GLES32.glUniform3fv(kdUniform, 1, material11_diffuse, 0);
		GLES32.glUniform3fv(ksUniform, 1, material11_specular, 0);
		GLES32.glUniform1f(materialShininessUniform, material11_shininess);
		//draw , either glDrawTriangles() or glDrawArrays() or glDrawElements()
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);

		//12th sphere
		GLES32.glViewport((int)(1.0f*width1/4.0f), (int)(0.0f*height1/6.0f), (int)(width1/4.0f), (int)(height1/6.0f));
		GLES32.glUniform3fv(kaUniform, 1, material12_ambient, 0);
		GLES32.glUniform3fv(kdUniform, 1, material12_diffuse, 0);
		GLES32.glUniform3fv(ksUniform, 1, material12_specular, 0);
		GLES32.glUniform1f(materialShininessUniform, material12_shininess);
		//draw , either glDrawTriangles() or glDrawArrays() or glDrawElements()
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);

		//13th sphere
		GLES32.glViewport((int)(2.0f*width1/4.0f), (int)(5.0f*height1/6.0f), (int)(width1/4.0f), (int)(height1/6.0f));
		GLES32.glUniform3fv(kaUniform, 1, material13_ambient, 0);
		GLES32.glUniform3fv(kdUniform, 1, material13_diffuse, 0);
		GLES32.glUniform3fv(ksUniform, 1, material13_specular, 0);
		GLES32.glUniform1f(materialShininessUniform, material13_shininess);
		//draw , either glDrawTriangles() or glDrawArrays() or glDrawElements()
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);


		//14th sphere
		GLES32.glViewport((int)(2.0f*width1/4.0f), (int)(4.0f*height1/6.0f), (int)(width1/4.0f), (int)(height1/6.0f));
		GLES32.glUniform3fv(kaUniform, 1, material14_ambient, 0);
		GLES32.glUniform3fv(kdUniform, 1, material14_diffuse, 0);
		GLES32.glUniform3fv(ksUniform, 1, material14_specular, 0);
		GLES32.glUniform1f(materialShininessUniform, material14_shininess);
		//draw , either glDrawTriangles() or glDrawArrays() or glDrawElements()
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);

		//15th sphere
		GLES32.glViewport((int)(2.0f*width1/4.0f), (int)(3.0f*height1/6.0f), (int)(width1/4.0f), (int)(height1/6.0f));
		GLES32.glUniform3fv(kaUniform, 1, material15_ambient, 0);
		GLES32.glUniform3fv(kdUniform, 1, material15_diffuse, 0);
		GLES32.glUniform3fv(ksUniform, 1, material15_specular, 0);
		GLES32.glUniform1f(materialShininessUniform, material15_shininess);
		//draw , either glDrawTriangles() or glDrawArrays() or glDrawElements()
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);

		//16th sphere
		GLES32.glViewport((int)(2.0f*width1/4.0f), (int)(2.0f*height1/6.0f), (int)(width1/4.0f), (int)(height1/6.0f));
		GLES32.glUniform3fv(kaUniform, 1, material16_ambient, 0);
		GLES32.glUniform3fv(kdUniform, 1, material16_diffuse, 0);
		GLES32.glUniform3fv(ksUniform, 1, material16_specular, 0);
		GLES32.glUniform1f(materialShininessUniform, material16_shininess);
		//draw , either glDrawTriangles() or glDrawArrays() or glDrawElements()
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);

		//17th sphere
		GLES32.glViewport((int)(2.0f*width1/4.0f), (int)(1.0f*height1/6.0f), (int)(width1/4.0f), (int)(height1/6.0f));
		GLES32.glUniform3fv(kaUniform, 1, material17_ambient, 0);
		GLES32.glUniform3fv(kdUniform, 1, material17_diffuse, 0);
		GLES32.glUniform3fv(ksUniform, 1, material17_specular, 0);
		GLES32.glUniform1f(materialShininessUniform, material17_shininess);
		//draw , either glDrawTriangles() or glDrawArrays() or glDrawElements()
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);


		//18th sphere
		GLES32.glViewport((int)(2.0f*width1/4.0f), (int)(0.0f*height1/6.0f), (int)(width1/4.0f), (int)(height1/6.0f));
		GLES32.glUniform3fv(kaUniform, 1, material18_ambient, 0);
		GLES32.glUniform3fv(kdUniform, 1, material18_diffuse, 0);
		GLES32.glUniform3fv(ksUniform, 1, material18_specular, 0);
		GLES32.glUniform1f(materialShininessUniform, material18_shininess);
		//draw , either glDrawTriangles() or glDrawArrays() or glDrawElements()
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);

		//19th sphere
		GLES32.glViewport((int)(3.0f*width1/4.0f), (int)(5.0f*height1/6.0f), (int)(width1/4.0f), (int)(height1/6.0f));
		GLES32.glUniform3fv(kaUniform, 1, material19_ambient, 0);
		GLES32.glUniform3fv(kdUniform, 1, material19_diffuse, 0);
		GLES32.glUniform3fv(ksUniform, 1, material19_specular, 0);
		GLES32.glUniform1f(materialShininessUniform, material19_shininess);
		//draw , either glDrawTriangles() or glDrawArrays() or glDrawElements()
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);

		//20th sphere
		GLES32.glViewport((int)(3.0f*width1/4.0f), (int)(4.0f*height1/6.0f), (int)(width1/4.0f), (int)(height1/6.0f));
		GLES32.glUniform3fv(kaUniform, 1, material20_ambient, 0);
		GLES32.glUniform3fv(kdUniform, 1, material20_diffuse, 0);
		GLES32.glUniform3fv(ksUniform, 1, material20_specular, 0);
		GLES32.glUniform1f(materialShininessUniform, material20_shininess);
		//draw , either glDrawTriangles() or glDrawArrays() or glDrawElements()
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);

		//21st sphere
		GLES32.glViewport((int)(3.0f*width1/4.0f), (int)(3.0f*height1/6.0f), (int)(width1/4.0f), (int)(height1/6.0f));
		GLES32.glUniform3fv(kaUniform, 1, material21_ambient, 0);
		GLES32.glUniform3fv(kdUniform, 1, material21_diffuse, 0);
		GLES32.glUniform3fv(ksUniform, 1, material21_specular, 0);
		GLES32.glUniform1f(materialShininessUniform, material21_shininess);
		//draw , either glDrawTriangles() or glDrawArrays() or glDrawElements()
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);

		//22nd sphere
		GLES32.glViewport((int)(3.0f*width1/4.0f), (int)(2.0f*height1/6.0f), (int)(width1/4.0f), (int)(height1/6.0f));
		GLES32.glUniform3fv(kaUniform, 1, material22_ambient, 0);
		GLES32.glUniform3fv(kdUniform, 1, material22_diffuse, 0);
		GLES32.glUniform3fv(ksUniform, 1, material22_specular, 0);
		GLES32.glUniform1f(materialShininessUniform, material22_shininess);
		//draw , either glDrawTriangles() or glDrawArrays() or glDrawElements()
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);

		//23rd sphere
		GLES32.glViewport((int)(3.0f*width1/4.0f), (int)(1.0f*height1/6.0f), (int)(width1/4.0f), (int)(height1/6.0f));
		GLES32.glUniform3fv(kaUniform, 1, material23_ambient, 0);
		GLES32.glUniform3fv(kdUniform, 1, material23_diffuse, 0);
		GLES32.glUniform3fv(ksUniform, 1, material23_specular, 0);
		GLES32.glUniform1f(materialShininessUniform, material23_shininess);
		//draw , either glDrawTriangles() or glDrawArrays() or glDrawElements()
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);

		//24th sphere
		GLES32.glViewport((int)(3.0f*width1/4.0f), (int)(0.0f*height1/6.0f), (int)(width1/4.0f), (int)(height1/6.0f));
		GLES32.glUniform3fv(kaUniform, 1, material24_ambient, 0);
		GLES32.glUniform3fv(kdUniform, 1, material24_diffuse, 0);
		GLES32.glUniform3fv(ksUniform, 1, material24_specular, 0);
		GLES32.glUniform1f(materialShininessUniform, material24_shininess);
		//draw , either glDrawTriangles() or glDrawArrays() or glDrawElements()
		GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
		GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);

		//undind vao
		GLES32.glBindVertexArray(0);

		//un-use shader program
		GLES32.glUseProgram(0);

		//Render
		requestRender(); // Equivalent to double buffer
		
	}

		

	void uninitialize()
	{
		//code
		// destroy vao_sphere
		if(vao_sphere[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1, vao_sphere, 0);
			vao_sphere[0] = 0;

		}

		// destroy vaoCube
		if(vbo_sphere_position[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_sphere_position, 0);
			vbo_sphere_position[0] = 0;

		}

		// destroy vaoCube
		if(vbo_sphere_normal[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_sphere_normal, 0);
			vbo_sphere_normal[0] = 0;

		}

		// destroy vbo_sphere_normal
		if(vbo_sphere_element[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_sphere_element, 0);
			vbo_sphere_element[0] = 0;

		}

		if(shaderProgramObject != 0)
		{
			if(vertexShaderObject != 0)
			{
				//detach vertex shader from shader program object
				GLES32.glDetachShader(shaderProgramObject, vertexShaderObject);
				//delete vertex shader object
				GLES32.glDeleteShader(vertexShaderObject);
				vertexShaderObject = 0;
			}

			if(fragmentShaderObject != 0)
			{
				//detach fragment shader from shader program object
				GLES32.glDetachShader(shaderProgramObject, fragmentShaderObject);
				//delete fragment shader object
				GLES32.glDeleteShader(fragmentShaderObject);
				fragmentShaderObject = 0;
			}
		}

		//delete shader program object
		if(shaderProgramObject != 0)
		{
		GLES32.glDeleteProgram(shaderProgramObject);
		shaderProgramObject = 0;
		}
	}

}

