package com.astromedicomp.lightstwo;

import android.content.Context; // for drawing context related
import android.widget.TextView; // for "TextView" class
import android.graphics.Color; // For Color class
import android.view.Gravity; // For Gravity class
import android.view.MotionEvent; //For MotionEvent
import android.view.GestureDetector; // For Gesturedetector
import android.view.GestureDetector.OnGestureListener; // For OnGestureListener
import android.view.GestureDetector.OnDoubleTapListener; // For  OnDoubleTapListener

import android.opengl.GLSurfaceView; // For OpenGL surface view and all related
import javax.microedition.khronos.opengles.GL10; //For OpenGLES 1.0 needed as param type GL10
import javax.microedition.khronos.egl.EGLConfig; //For EGLConfig needed as param type EGLConfig
import android.opengl.GLES32; // For OpenGLES3.2
import java.nio.ShortBuffer; //

//for vbo
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;



import  android.opengl.Matrix; // For matrix math

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
	private final Context context;
	private GestureDetector gestureDetector;

	private int vertexShaderObject;
	private int  fragmentShaderObject;
	private int shaderProgramObject;

	private int[] vaoPyramid = new int[1];
	private int[] vbo_Position = new int[1];
	private int[] vbo_Normal = new int[1];

	private int numElements;
	private int numVertices;

	private int[] vao_sphere = new int[1];
	private int[] vbo_sphere_position = new int[1];
	private int[] vbo_sphere_normal = new int[1];
	private int[] vbo_sphere_element = new int[1];

	private float light0_ambient[] = {0.0f, 0.0f, 0.0f, 1.0f};
	private float light0_diffuse[] = {1.0f, 0.0f, 0.0f, 1.0f}; //RED color
	private float light0_specular[] = {1.0f, 1.0f, 1.0f, 1.0f};
	private float light0_position[] = {200.0f, 100.0f, 100.0f, 1.0f};

	private float light1_ambient[] = {0.0f, 0.0f, 0.0f, 1.0f};
	private float light1_diffuse[] = {0.0f, 0.0f, 1.0f, 1.0f}; //Blue color
	private float light1_specular[] = {1.0f, 1.0f, 1.0f, 1.0f};
	private float light1_position[] = {-200.0f, 100.0f, 100.0f, 1.0f};

	private float material_ambient[] = {0.0f, 0.0f, 0.0f, 1.0f};
	private float material_diffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};
	private float material_specular[] = {1.0f, 1.0f, 1.0f, 1.0f};
	private float material_shininess = 50.0f;


	private int modelMatrixUniform, viewMatrixUniform, projectionMatrixUniform, rotationMatrixUniform;
	private int la0Uniform, ld0Uniform, ls0Uniform, lightPosition0Uniform;
	private int la1Uniform, ld1Uniform, ls1Uniform, lightPosition1Uniform;
	private int kaUniform, kdUniform, ksUniform, materialShininessUniform;

	private int gLightPositionUniform;
	private int doubleTapUniform;
	
	private float perspectiveProjectionMatrix[] = new float[16];  // 4x4 matrix

	float anglePyramid = 0.0f;

	private int singleTap;
	private int doubleTap;
	
	public GLESView(Context drawingContext)
	{	
		super(drawingContext);

		context = drawingContext;

		//Accordingly set EGLContext to current supported version of OpenGL-ES
		setEGLContextClientVersion(3);

		//Set renderer for drawing on the GLSurfaceView
		setRenderer(this);

		//Render the view only when there is change in drawing data
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

		
		gestureDetector = new GestureDetector(context, this, null, false); // This means 'handler' ie who is going to handle
		gestureDetector.setOnDoubleTapListener(this); // This means 'handler' ie who is going to handle

	}

	//overriden method of GLSurfaceView.Renderer (Init mode)
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		//OpenGL ES version check
		String glesVersion = gl.glGetString(GL10.GL_VERSION);
		System.out.println("VDG :  OpenGL-ES Version = "+glesVersion);
		// get GLSL version
		String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("VDG: GLSL Version "+glslVersion);

		initialize(gl);
	}


	//overriden method of GLSurfaceView.Renderer (change size code)
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
		resize(width, height);
	}

	//overriden method of GLSurfaceView.Renderer (Rendering code)
	@Override
	public void onDrawFrame(GL10 unused)
	{
		draw();
	}

	//Handling OnTouchEvent is most important
	//Because it triggers all gestures and tap events
	@Override
	public boolean onTouchEvent(MotionEvent  event)
		{
		//code
		int eventaction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		return(true);
	}

	//Abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		//code
		System.out.println("VDG: "+"Double Tap");
		doubleTap++;
		if(doubleTap > 1)
			doubleTap = 0;
		return(true);
	}

	//Abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		// Do not write any code here, because alrady written 'OnDoubleTap'
		return(true);
	}

	//Abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		System.out.println("VDG: "+"Single Tap");
		return(true);
	}

	//Abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDown(MotionEvent e)
	{
		// Do not write any code here, because alrady written 'OnSingleTapConfirmed'
		return(true);
	}

	//Abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
	{
		return(true);
	}
	
	//Abstract method from OnDoubleTapListener so must be implemented
	@Override
	public void onLongPress(MotionEvent e)
	{
		System.out.println("VDG: "+"Long press");
		
	}

	//Abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		System.out.println("VDG: "+"Scroll");
		System.exit(0); // Exit the app at scroll
		return(true);
	}


//Abstract method from OnDoubleTapListener so must be implemented
	@Override
	public void onShowPress(MotionEvent e)
	{
	
	}

	//Abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		//code
		singleTap++;
		if(singleTap > 1)
			singleTap = 0;
		return(true);
	}

	private void initialize(GL10 gl)
	{
		//***********************************
		// Vertex shader
		//***********************************
		// Create shader
		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);

		// vertex shader source code
		final String vertexShaderSourceCode = String.format
		(
			"#version 320 es"+
			"\n"+
			"in vec4 vPosition;"+
			"in vec3 vNormal;"+
			"uniform mat4 u_model_matrix;"+
			"uniform mat4 u_view_matrix;"+
			"uniform mat4 u_rotation_matrix;"+
			"uniform mat4 u_projection_matrix;"+
			"uniform mediump int u_double_tap;"+
			"uniform vec4 u_light0_position;"+
			"uniform vec4 u_light1_position;"+
			"out vec3 transformed_normals;"+
			"out vec3 light0_direction;"+
			"out vec3 light1_direction;"+
			"out vec3 viewer_vector;"+
			"void main(void)"+
			"{"+
			"if (u_double_tap == 1)"+
			"{"+
			"vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"+
			"transformed_normals =  normalize(mat3(u_view_matrix * u_model_matrix * u_rotation_matrix ) * vNormal);"+
			"light0_direction = normalize(vec3(u_light0_position) - eye_coordinates.xyz);"+
			"light1_direction = normalize(vec3(u_light1_position) - eye_coordinates.xyz);"+
			"viewer_vector = -eye_coordinates.xyz;"+
			"}"+
			
			"gl_Position = u_projection_matrix  * u_view_matrix * u_model_matrix * u_rotation_matrix * vPosition;"+
			"}"
		);

		//provide source code to shader
		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);

		//compile shader and check for errors
		GLES32.glCompileShader(vertexShaderObject);
		int[] iShaderCompiledStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;
		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);
		if (iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("VDG:  Vertex Shader Compilation log  = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//***********************************
		// Fragment shader
		//***********************************
		// Create shader
		fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

		// vertex shader source code
		final String fragmentShaderSourceCode = String.format
		(
			"#version 320 es"+
			"\n"+
			"precision highp float;"+
			"in vec3 transformed_normals;"+
			"in vec3 light0_direction;"+
			"in vec3 light1_direction;"+
			"in vec3 viewer_vector;"+
			"out vec4 FragColor;"+
			"uniform vec3 u_La0;"+
			"uniform vec3 u_Ld0;"+
			"uniform vec3 u_Ls0;"+
			"uniform vec3 u_La1;"+
			"uniform vec3 u_Ld1;"+
			"uniform vec3 u_Ls1;"+
			"uniform vec3 u_Ka;"+
			"uniform vec3 u_Kd;"+
			"uniform vec3 u_Ks;"+
			"uniform float u_material_shininess;"+
			"uniform int u_double_tap;"+
			"void main(void)"+
			"{"+
			"vec3 light0;"+
			"vec3 light1;"+
			"vec3 phong_ads_color;"+
			"if(u_double_tap == 1)"+
			"{"+
			"vec3 normalized_transformed_normals = normalize(transformed_normals);"+
			"vec3 normalized_light0_direction = normalize(light0_direction);"+
			"vec3 normalized_light1_direction = normalize(light1_direction);"+
			"vec3 normalized_viewer_vector = normalize(viewer_vector);"+
			"float tn_dot_ld0 = max(dot(normalized_transformed_normals, normalized_light0_direction), 0.0);"+
			"vec3 ambient0 = u_La0 * u_Ka;"+
			"vec3 diffuse0 = u_Ld0 * u_Kd * tn_dot_ld0;"+
			"vec3 reflection_vector0 = reflect(-normalized_light0_direction,normalized_transformed_normals);"+
			"vec3 specular0 = u_Ls0 * u_Ks * pow(max(dot(reflection_vector0, normalized_viewer_vector), 0.0), u_material_shininess);"+
			"float tn_dot_ld1 = max(dot(normalized_transformed_normals, normalized_light1_direction), 0.0);"+
			"vec3 ambient1 = u_La1 * u_Ka;"+
			"vec3 diffuse1 = u_Ld1 * u_Kd * tn_dot_ld1;"+
			"vec3 reflection_vector1 = reflect(-normalized_light1_direction,normalized_transformed_normals);"+
			"vec3 specular1 = u_Ls1 * u_Ks * pow(max(dot(reflection_vector1, normalized_viewer_vector), 0.0), u_material_shininess);"+
			"light0 = ambient0 + diffuse0 + specular0;"+
			"light1 = ambient1 + diffuse1 + specular1;"+
			"phong_ads_color = light0 + light1;"+
			"}"+
			"else"+
			"{"+
			"phong_ads_color =  vec3(1.0,  1.0,  1.0);"+
			"}"+
			"FragColor = vec4(phong_ads_color, 1.0);"+
			"}"
		);

		//provide source code to shader
		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);

		//compile shader and check for errors
		GLES32.glCompileShader(fragmentShaderObject);
		iShaderCompiledStatus[0] = 0;  //reinitialize
		iInfoLogLength[0] = 0;  //reinitialize
		szInfoLog = null; //reinitialize
		GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);
		if (iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("VDG:  Fragment Shader Compilation log  = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

	//create Shader program
	shaderProgramObject = GLES32.glCreateProgram();

	//attach vertex shader to shader program
	GLES32.glAttachShader(shaderProgramObject,  vertexShaderObject);

	//attach vertex shader to shader program
	GLES32.glAttachShader(shaderProgramObject,  fragmentShaderObject);

	//pre-link binding of shader program object with vertex shader attributes
	GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");

	//pre-link binding of shader program object with vertex shader attributes
	GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.VDG_ATTRIBUTE_NORMAL, "vNormal");

	// Link the two  shaders together to shader program object
	GLES32.glLinkProgram(shaderProgramObject);
	int[] iShaderProgramLinkStatus = new int[1];
	iInfoLogLength[0] = 0;
	szInfoLog = null; //reinitialize
		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iShaderProgramLinkStatus, 0);
		if (iShaderProgramLinkStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
				System.out.println("VDG:  Shader Program Link log  = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//get MVP uniform location
	//	mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");

	modelMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_model_matrix");
	viewMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_view_matrix");
	projectionMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_projection_matrix");
	rotationMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_rotation_matrix");

	doubleTapUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_double_tap");

	la0Uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_La0");
	ld0Uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ld0");
	ls0Uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ls0");
	lightPosition0Uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_light0_position");

	la1Uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_La1");
	ld1Uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ld1");
	ls1Uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ls1");
	lightPosition1Uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_light1_position");


	kaUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ka");
	kdUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Kd");
	ksUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_Ks");
	materialShininessUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_material_shininess");

		//*** vertices, colors, shader attribs, vbo,  vao initializations ***
		final float pyramidVertices[] = new float[]
		{
			//Front face
		0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,

		//Right face
		0.0f, 1.0f, 0.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, -1.0f,

		//Back face
		0.0f, 1.0f, 0.0f,
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,

		//Left face
		0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f,
		};

			final float pyramidNormals[] = new float[]
		{
		0.0f, 0.447214f, 0.894427f,
		0.0f, 0.447214f, 0.894427f,
		0.0f, 0.447214f, 0.894427f,
	
		0.894427f, 0.447214f, 0.0f,
		0.894427f, 0.447214f, 0.0f,
		0.894427f, 0.447214f, 0.0f,
		
		0.0f, 0.447214f, -0.894427f,
		0.0f, 0.447214f, -0.894427f,
		0.0f, 0.447214f, -0.894427f,

		-0.894427f, 0.447214f, 0.0f,
		-0.894427f, 0.447214f, 0.0f,
		-0.894427f, 0.447214f, 0.0f
		};

			

		//vaoPyramid
		GLES32.glGenVertexArrays(1, vaoPyramid, 0);
		GLES32.glBindVertexArray(vaoPyramid[0]);

		GLES32.glGenBuffers(1, vbo_Position, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Position[0]);

		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(pyramidVertices.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer verticesBuffer = byteBuffer.asFloatBuffer();
		verticesBuffer.put(pyramidVertices);
		verticesBuffer.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 
								pyramidVertices.length * 4,  
								verticesBuffer,
								GLES32.GL_STATIC_DRAW);

		GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_VERTEX,
									 3,
									 GLES32.GL_FLOAT,
									 false, 0,  0);

		GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_VERTEX);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		
		
		
		//vbo_cube normal
		GLES32.glGenBuffers(1, vbo_Normal, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Normal[0]);

		byteBuffer = ByteBuffer.allocateDirect(pyramidNormals.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		verticesBuffer = byteBuffer.asFloatBuffer();
		verticesBuffer.put(pyramidNormals);
		verticesBuffer.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 
								pyramidNormals.length * 4,  
								verticesBuffer,
								GLES32.GL_STATIC_DRAW);

		GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_NORMAL,
									 3,
									 GLES32.GL_FLOAT,
									 false, 0,  0);

		GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_NORMAL);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		
		GLES32.glBindVertexArray(0);

		//Enable depth testing
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);

		//Depth test to do 
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);

		//cull back face for better performance
		//GLES32.glEnable(GLES32.GL_CULL_FACE);
		

		//Set the background frame Color
		GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

		//Set projection matrix to identity Matrix
		Matrix.setIdentityM(perspectiveProjectionMatrix,  0);

	}

	private void resize(int width, int height)
	{
		//Adjust the viewport based on geometry changes
		//such as screen rotation
		GLES32.glViewport(0, 0, width, height);

		//perspective matrix

		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)width / (float)height, 0.1f, 100.0f);
	}

	public void draw()
	{
		//Draw background Color
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

		//use shader program
		GLES32.glUseProgram(shaderProgramObject);

			if (doubleTap == 1)
			{
				GLES32.glUniform1i(doubleTapUniform, 1);

				GLES32.glUniform3fv(la0Uniform, 1, light0_ambient, 0);
				GLES32.glUniform3fv(ld0Uniform, 1, light0_diffuse, 0);
				GLES32.glUniform3fv(ls0Uniform, 1, light0_specular, 0);
				GLES32.glUniform4fv(lightPosition0Uniform, 1, light0_position, 0);

				GLES32.glUniform3fv(la1Uniform, 1, light1_ambient, 0);
				GLES32.glUniform3fv(ld1Uniform, 1, light1_diffuse, 0);
				GLES32.glUniform3fv(ls1Uniform, 1, light1_specular, 0);
				GLES32.glUniform4fv(lightPosition1Uniform, 1, light1_position, 0);

				GLES32.glUniform3fv(kaUniform, 1, material_ambient, 0);
				GLES32.glUniform3fv(kdUniform, 1, material_diffuse, 0);
				GLES32.glUniform3fv(ksUniform, 1, material_specular, 0);
				GLES32.glUniform1f(materialShininessUniform, material_shininess);
			}
			else
			{
				GLES32.glUniform1i(doubleTapUniform, 0);
			}

		//**** Triangle drawing ****
		//OpenGL-ES drawing
		float modelMatrix[] = new float[16];
		float viewMatrix[] = new float[16];
		float rotationMatrix[] = new float[16];
		float scaleMatrix[] = new float[16];
		
		//Set modelview and modelviewprojection matrices to identity
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.setIdentityM(viewMatrix, 0);
		Matrix.setIdentityM(rotationMatrix, 0);
		Matrix.setIdentityM(scaleMatrix, 0);

		//Translate model view matrix
		Matrix.translateM(modelMatrix, 0, 0.0f, 0.0f, -4.0f);
		

		//Scale model view Matrix
		//Matrix.scaleM(modelViewMatrix, 0, 0.75f, 0.75f, 0.75f);

		//Rotation matrix for Square
		Matrix.setRotateM(rotationMatrix, 0, anglePyramid, 0.0f, 1.0f, 0.0f);

		//Multiply the modelview and rotation matrix
		//Matrix.multiplyMM(modelMatrix, 0, modelMatrix, 0,rotationMatrix, 0);

		//Pass above modelViewProjectionMatrix to the vertex shader in 'u_mvp_matrix' shader variable
		//whose position value we already calculated in initWithFrame() by using glGetUniformLocation
	

		//Pass above modelViewProjectionMatrix to the vertex shader in 'u_mvp_matrix' shader variable
		//whose position value we already calculated in initWithFrame() by using glGetUniformLocation
		GLES32.glUniformMatrix4fv(modelMatrixUniform, 1, false, modelMatrix, 0);
		GLES32.glUniformMatrix4fv(viewMatrixUniform, 1, false, viewMatrix, 0);
		GLES32.glUniformMatrix4fv(rotationMatrixUniform, 1, false, rotationMatrix, 0);
		GLES32.glUniformMatrix4fv(projectionMatrixUniform, 1, false, perspectiveProjectionMatrix, 0);

		//bind vaoCube
		GLES32.glBindVertexArray(vaoPyramid[0]);

		//draw , either glDrawTriangles() or glDrawArrays() or glDrawElements()
		GLES32.glDrawArrays(GLES32.GL_TRIANGLES,0,3); 
		GLES32.glDrawArrays(GLES32.GL_TRIANGLES,3,3);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLES,6,3);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLES,9,3);
		
		//undind vao
		GLES32.glBindVertexArray(0);

		//un-use shader program
		GLES32.glUseProgram(0);

		//angleCube = angleCube - 0.4f;

		if(singleTap == 1)
		{
		anglePyramid = anglePyramid + 0.4f;
		if (anglePyramid >= 360.0f)
			anglePyramid = 0.0f;

		}

		//Render
		requestRender(); // Equivalent to double buffer
		
	}

		

	void uninitialize()
	{
		//code
		// destroy vaoPyramid
		if(vaoPyramid[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1, vaoPyramid, 0);
			vaoPyramid[0] = 0;

		}


		
		// destroy vbo_Position
		if(vbo_Position[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1, vbo_Position, 0);
			vbo_Position[0] = 0;

		}

		if(shaderProgramObject != 0)
		{
			if(vertexShaderObject != 0)
			{
				//detach vertex shader from shader program object
				GLES32.glDetachShader(shaderProgramObject, vertexShaderObject);
				//delete vertex shader object
				GLES32.glDeleteShader(vertexShaderObject);
				vertexShaderObject = 0;
			}

			if(fragmentShaderObject != 0)
			{
				//detach fragment shader from shader program object
				GLES32.glDetachShader(shaderProgramObject, fragmentShaderObject);
				//delete fragment shader object
				GLES32.glDeleteShader(fragmentShaderObject);
				fragmentShaderObject = 0;
			}
		}

		//delete shader program object
		if(shaderProgramObject != 0)
		{
		GLES32.glDeleteProgram(shaderProgramObject);
		shaderProgramObject = 0;
		}
	}

}

