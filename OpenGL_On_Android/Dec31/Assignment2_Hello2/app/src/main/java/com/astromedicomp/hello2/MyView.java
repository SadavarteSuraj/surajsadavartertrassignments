package com.astromedicomp.hello2;

import android.content.Context; // for drawing context related
import android.widget.TextView; // for "TextView" class
import android.graphics.Color; // For Color class
import android.view.Gravity; // For Gravity class

public class MyView extends TextView
{
MyView(Context context)
	{
		super(context);
		setText("Hello World Galaxy !!!");
		setTextSize(60);
		setTextColor(Color.GREEN);
		setGravity(Gravity.CENTER);


	}
}


