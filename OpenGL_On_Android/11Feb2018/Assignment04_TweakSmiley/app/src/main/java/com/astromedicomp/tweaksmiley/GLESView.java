package com.astromedicomp.tweaksmiley;

import android.content.Context; // for drawing context related
import android.widget.TextView; // for "TextView" class
import android.graphics.Color; // For Color class
import android.view.Gravity; // For Gravity class
import android.view.MotionEvent; //For MotionEvent
import android.view.GestureDetector; // For Gesturedetector
import android.view.GestureDetector.OnGestureListener; // For OnGestureListener
import android.view.GestureDetector.OnDoubleTapListener; // For  OnDoubleTapListener

import android.opengl.GLSurfaceView; // For OpenGL surface view and all related
import javax.microedition.khronos.opengles.GL10; //For OpenGLES 1.0 needed as param type GL10
import javax.microedition.khronos.egl.EGLConfig; //For EGLConfig needed as param type EGLConfig
import android.opengl.GLES32; // For OpenGLES3.2


//for vbo
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.Buffer; // for  Buffer function

//import com.sun.opengl.util.*; //For BufferUtil 


import  android.opengl.Matrix; // For matrix math

import android.graphics.BitmapFactory; // texture factory
import android.graphics.Bitmap; //for PNG file
import android.opengl.GLUtils; // for texImage2D



//texture related



public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
	private final Context context;
	private GestureDetector gestureDetector;

	private int vertexShaderObject;
	private int  fragmentShaderObject;
	private int shaderProgramObject;

	private int[] vaoSquare = new int[1];
	private int[] vbo_square_Position = new int[1];
	private int[] vbo_square_texture = new int[1];

	private int[] textureCheckerboard =  new int[1]; 

	private static final int checkImageWidth = 64;
	private static final int checkImageHeight = 64;
	
	private byte checkImage[]= new byte[64 * 64 * 4];

	//private ByteBuffer checkImageBuf = BufferUtil.newByteBuffer(checkImageHeight * checkImageWidth * 4);
	
	private int mvpUniform;

	private int texture0_sampler_uniform;

	private int[] texture_smiley =  new int[1];

	private float perspectiveProjectionMatrix[] = new float[16];  // 4x4 matrix

	private int singleTap = 0;

	int[] texture = new int[1];

	public GLESView(Context drawingContext)
	{	
		super(drawingContext);

		context = drawingContext;

		//Accordingly set EGLContext to current supported version of OpenGL-ES
		setEGLContextClientVersion(3);

		//Set renderer for drawing on the GLSurfaceView
		setRenderer(this);

		//Render the view only when there is change in drawing data
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

		
		gestureDetector = new GestureDetector(context, this, null, false); // This means 'handler' ie who is going to handle
		gestureDetector.setOnDoubleTapListener(this); // This means 'handler' ie who is going to handle

	}

	//overriden method of GLSurfaceView.Renderer (Init mode)
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		//OpenGL ES version check
		String glesVersion = gl.glGetString(GL10.GL_VERSION);
		System.out.println("VDG :  OpenGL-ES Version = "+glesVersion);
		// get GLSL version
		String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("VDG: GLSL Version "+glslVersion);

		initialize(gl);
	}


	//overriden method of GLSurfaceView.Renderer (change size code)
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
		resize(width, height);
	}

	//overriden method of GLSurfaceView.Renderer (Rendering code)
	@Override
	public void onDrawFrame(GL10 unused)
	{
		draw();
	}

	//Handling OnTouchEvent is most important
	//Because it triggers all gestures and tap events
	@Override
	public boolean onTouchEvent(MotionEvent  event)
		{
		//code
		int eventaction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		return(true);
	}

	//Abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		System.out.println("VDG: "+"Double Tap");
		return(true);
	}

	//Abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		// Do not write any code here, because alrady written 'OnDoubleTap'
		return(true);
	}

	//Abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		System.out.println("VDG: "+"Single Tap");
		return(true);
	}

	//Abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDown(MotionEvent e)
	{
		// Do not write any code here, because alrady written 'OnSingleTapConfirmed'
		return(true);
	}

	//Abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
	{
		return(true);
	}
	
	//Abstract method from OnDoubleTapListener so must be implemented
	@Override
	public void onLongPress(MotionEvent e)
	{
		System.out.println("VDG: "+"Long press");
		
	}

	//Abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		System.out.println("VDG: "+"Scroll");
		System.exit(0); // Exit the app at scroll
		return(true);
	}


//Abstract method from OnDoubleTapListener so must be implemented
	@Override
	public void onShowPress(MotionEvent e)
	{
	
	}

	//Abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
	//code
		singleTap++;
		if(singleTap > 4)
			singleTap = 0;
		return(true);
	}

	private void initialize(GL10 gl)
	{
		//***********************************
		// Vertex shader
		//***********************************
		// Create shader
		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);

		// vertex shader source code
		final String vertexShaderSourceCode = String.format
		(
			"#version 320 es"+
			"\n"+
			"in vec4 vPosition;"+
			"in vec2 vTexture0_Coord;"+
			"out vec2 out_texture0_Coord;"+
			"uniform mat4 u_mvp_matrix;"+
			"void main(void)"+
			"{"+
			"gl_Position =  u_mvp_matrix * vPosition;"+
			"out_texture0_Coord = vTexture0_Coord;"+
			"}"
		);

		//provide source code to shader
		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);

		//compile shader and check for errors
		GLES32.glCompileShader(vertexShaderObject);
		int[] iShaderCompiledStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;
		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);
		if (iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("VDG:  Vertex Shader Compilation log  = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//***********************************
		// Fragment shader
		//***********************************
		// Create shader
		fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

		// vertex shader source code
		final String fragmentShaderSourceCode = String.format
		(
			"#version 320 es"+
			"\n"+
			"precision highp float;"+
			"in vec2 out_texture0_Coord;"+
			"uniform highp sampler2D u_texture0_sampler;"+
			"out vec4 FragColor;"+
			"void main(void)"+
			"{"+
			"FragColor = texture(u_texture0_sampler,  out_texture0_Coord);"+
			"}"
		);

		//provide source code to shader
		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);

		//compile shader and check for errors
		GLES32.glCompileShader(fragmentShaderObject);
		iShaderCompiledStatus[0] = 0;  //reinitialize
		iInfoLogLength[0] = 0;  //reinitialize
		szInfoLog = null; //reinitialize
		GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompiledStatus, 0);
		if (iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("VDG:  Fragment Shader Compilation log  = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

	//create Shader program
	shaderProgramObject = GLES32.glCreateProgram();

	//attach vertex shader to shader program
	GLES32.glAttachShader(shaderProgramObject,  vertexShaderObject);

	//attach vertex shader to shader program
	GLES32.glAttachShader(shaderProgramObject,  fragmentShaderObject);

	//pre-link binding of shader program object with vertex shader attributes
	GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.VDG_ATTRIBUTE_VERTEX, "vPosition");
	GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.VDG_ATTRIBUTE_TEXTURE0, "vTexture0_Coord");

	// Link the two  shaders together to shader program object
	GLES32.glLinkProgram(shaderProgramObject);
	int[] iShaderProgramLinkStatus = new int[1];
	iInfoLogLength[0] = 0;
	szInfoLog = null; //reinitialize
		GLES32.glGetShaderiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iShaderProgramLinkStatus, 0);
		if (iShaderProgramLinkStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
				System.out.println("VDG:  Shader Program Link log  = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//get MVP uniform location
		mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");

		//get texture sampler uniform location
		texture0_sampler_uniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_texture0_sampler");

		// Load textures
		texture_smiley[0] = loadGLTexture(R.raw.smiley);
		
		//*** vertices, colors, shader attribs, vbo,  vao initializations ***
		final float squareVertices[] = new float[]
		{
			//***Anti-clockwise direction of vertices is important***

		1.0f, 1.0f, 0.0f,
		-1.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 0.0f,
		1.0f, -1.0f, 0.0f,
				
		};

			final float squareTexCoords[] = new float[]
		{
		1.0f, 1.0f,
		0.0f, 1.0f,
		0.0f, 0.0f,
		1.0f, 0.0f,
				
		};

		
		

		//vaoPyramid
		GLES32.glGenVertexArrays(1, vaoSquare, 0);
		GLES32.glBindVertexArray(vaoSquare[0]);

		//vbo position
		GLES32.glGenBuffers(1, vbo_square_Position, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_square_Position[0]);

		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(squareVertices.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer verticesBuffer = byteBuffer.asFloatBuffer();
		verticesBuffer.put(squareVertices);
		verticesBuffer.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 
								squareVertices.length * 4,  
								verticesBuffer,
								GLES32.GL_STATIC_DRAW);

		GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_VERTEX,
									 3,
									 GLES32.GL_FLOAT,
									 false, 0,  0);

		GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_VERTEX);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		
		//vbo_pyramid_texture
		GLES32.glGenBuffers(1, vbo_square_texture, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_square_texture[0]);

		byteBuffer = ByteBuffer.allocateDirect(squareTexCoords.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer colorBuffer = byteBuffer.asFloatBuffer();
		colorBuffer.put(squareTexCoords);
		colorBuffer.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 
								squareTexCoords.length * 4,  
								colorBuffer,
								GLES32.GL_DYNAMIC_DRAW);

		GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_TEXTURE0,
									 2,
									 GLES32.GL_FLOAT,
									 false, 0,  0);

		GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_TEXTURE0);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		GLES32.glBindVertexArray(0);

		//Enable depth testing
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);

		//Depth test to do 
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);

		//cull back face for better performance
		//GLES32.glEnable(GLES32.GL_CULL_FACE);
		

		//Set the background frame Color
		GLES32.glClearColor(0.20f, 0.20f, 0.20f, 1.0f);

		LoadGLProcedureTexture();

		//Set projection matrix to identity Matrix
		Matrix.setIdentityM(perspectiveProjectionMatrix,  0);

	}

	private void resize(int width, int height)
	{
		//Adjust the viewport based on geometry changes
		//such as screen rotation
		GLES32.glViewport(0, 0, width, height);

		//perspective matrix

		Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)width / (float)height, 0.1f, 100.0f);
	}

	private int loadGLTexture(int imageFileResourceID)
	{
		//code
		BitmapFactory.Options options =  new BitmapFactory.Options();
		options.inScaled = false;

		//read in the resource
		Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), imageFileResourceID, options);

		int[] texture = new int[1];

		//create a texture object to apply to model
		GLES32.glGenTextures(1, texture, 0);

		//indicate that pixel rows are tightly packed (default to stride of 4 which is kind of only good for RGBA or FLOAT data types)
		GLES32.glPixelStorei(GLES32.GL_UNPACK_ALIGNMENT, 1);

		//bind with the texture
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture[0]);

		//set up filter and wrap mode for this texture object
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MAG_FILTER, GLES32.GL_LINEAR);
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MIN_FILTER, GLES32.GL_LINEAR_MIPMAP_LINEAR);

		//load bitmap into bound texture
		GLUtils.texImage2D(GLES32.GL_TEXTURE_2D, 0, bitmap, 0);

		//generate mipmap
		GLES32.glGenerateMipmap(GLES32.GL_TEXTURE_2D);

		return(texture[0]);
	}


	void LoadGLProcedureTexture()
{
	
	//code
	MakeCheckImage();


	GLES32.glPixelStorei(GLES32.GL_UNPACK_ALIGNMENT, 1); //pixel storage mode (word alignment/1 bytes)
	GLES32.glGenTextures(1, textureCheckerboard, 0);  //1 image
	GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, textureCheckerboard[0]); //bind texture
	GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_WRAP_S, GLES32.GL_REPEAT);
	GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_WRAP_T, GLES32.GL_REPEAT);
	GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MAG_FILTER, GLES32.GL_NEAREST);
	GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MIN_FILTER, GLES32.GL_NEAREST);
	
	Buffer textureBuffer = ByteBuffer.wrap(checkImage); ////Wraps a byte array into a buffer.

	GLES32.glTexImage2D(GLES32.GL_TEXTURE_2D, 0, GLES32.GL_RGBA, 64, 64, 0, GLES32.GL_RGBA, GLES32.GL_UNSIGNED_BYTE, textureBuffer);

	//GLES32.glTexEnvf(GLES32.GL_TEXTURE_ENV, GLES32.GL_TEXTURE_ENV_MODE, GLES32.GL_REPLACE);
	GLES32.glGenerateMipmap(GLES32.GL_TEXTURE_2D);
}


void MakeCheckImage()
{
	//code
	int i, j, c;
	for (i = 0; i < 64; i++)
	{
		for (j = 0; j < 64; j++)
		{
			if(((i/8)%2 == 0) ^ ((j/8)%2 == 0))
				c =  0;
				else
				c  =  255;
			checkImage[i*256 + j*4] = (byte) 255;
			checkImage[i*256 + j*4 + 1] = (byte) 255;
			checkImage[i*256 + j*4 + 2] = (byte) 255;
			checkImage[i*256 + j*4 + 3] = (byte) 255;
			
		}
	}
		
}
	public void draw()
	{
		//Draw background Color
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

		//use shader program
		GLES32.glUseProgram(shaderProgramObject);

		//**** Triangle drawing ****
		//OpenGL-ES drawing
		float modelViewMatrix[] = new float[16];
		float modelViewProjectionMatrix[] = new float[16];
		float rotationMatrix[] = new float[16];

		//Set modelview and modelviewprojection matrices to identity
		Matrix.setIdentityM(modelViewMatrix, 0);
		Matrix.setIdentityM(modelViewProjectionMatrix, 0);
		Matrix.setIdentityM(rotationMatrix, 0);

		//Translate model view matrix
		Matrix.translateM(modelViewMatrix, 0, 0.0f, 0.0f, -5.0f);

		//Multiply the modelview and projection  matrix to get the modelViewProjectionMatrix
		Matrix.multiplyMM(modelViewProjectionMatrix, 0, perspectiveProjectionMatrix, 0,modelViewMatrix, 0);

		//Pass above modelViewProjectionMatrix to the vertex shader in 'u_mvp_matrix' shader variable
		//whose position value we already calculated in initWithFrame() by using glGetUniformLocation
		GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

		//bind vaoPyramid
		GLES32.glBindVertexArray(vaoSquare[0]);

		final float[] quadTexture = new float[8];

	if (singleTap == 0) 
	{
		quadTexture[0] = 1.0f;
		quadTexture[1] = 1.0f;
		quadTexture[2] = 0.0f;
		quadTexture[3] = 1.0f;
		quadTexture[4] = 0.0f;
		quadTexture[5] = 0.0f;
		quadTexture[6] = 1.0f;
		quadTexture[7] = 0.0f;
			
	}

	else if (singleTap == 1)
	{
		quadTexture[0] = 0.5f;
		quadTexture[1] = 0.5f;
		quadTexture[2] = 0.0f;
		quadTexture[3] = 0.5f;
		quadTexture[4] = 0.0f;
		quadTexture[5] = 0.0f;
		quadTexture[6] = 0.5f;
		quadTexture[7] = 0.0f;
	}
	else if (singleTap == 2)
	{
		quadTexture[0] = 1.0f;
		quadTexture[1] = 1.0f;
		quadTexture[2] = 0.0f;
		quadTexture[3] = 1.0f;
		quadTexture[4] = 0.0f;
		quadTexture[5] = 0.0f;
		quadTexture[6] = 1.0f;
		quadTexture[7] = 0.0f;
	}
	else if (singleTap == 3)
	{
		quadTexture[0] = 2.0f;
		quadTexture[1] = 2.0f;
		quadTexture[2] = 0.0f;
		quadTexture[3] = 2.0f;
		quadTexture[4] = 0.0f;
		quadTexture[5] = 0.0f;
		quadTexture[6] = 2.0f;
		quadTexture[7] = 0.0f;
	}
	else if (singleTap == 4)
	{
		quadTexture[0] = 0.5f;
		quadTexture[1] = 0.5f;
		quadTexture[2] = 0.5f;
		quadTexture[3] = 0.5f;
		quadTexture[4] = 0.5f;
		quadTexture[5] = 0.5f;
		quadTexture[6] = 0.5f;
		quadTexture[7] = 0.5f;
	}


		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_square_texture[0]);

		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(quadTexture.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer colorBuffer = byteBuffer.asFloatBuffer();
		colorBuffer.put(quadTexture);
		colorBuffer.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 
								quadTexture.length * 4,  
								colorBuffer,
								GLES32.GL_DYNAMIC_DRAW);

		GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_TEXTURE0,
									 2,
									 GLES32.GL_FLOAT,
									 false, 0,  0);

		GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_TEXTURE0);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		

	if (singleTap == 0)
	{
		//bind with smiley texture
		GLES32.glActiveTexture(GLES32.GL_TEXTURE0); //0th texture corresponds to VDG_ATTRIBUTE_TEXTURE0
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, textureCheckerboard[0]);
		GLES32.glUniform1i(texture0_sampler_uniform, 0);  // 0th sampler enable as we have only 1 texture sampler in fragment shader
	}
	else
	{
		//bind with smiley texture
		GLES32.glActiveTexture(GLES32.GL_TEXTURE0); //0th texture corresponds to VDG_ATTRIBUTE_TEXTURE0
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_smiley[0]);
		GLES32.glUniform1i(texture0_sampler_uniform, 0);  // 0th sampler enable as we have only 1 texture sampler in fragment shader
	}

	//	GLES32.glUniform1i(texture0_sampler_uniform, 0);

		//draw , either glDrawTriangles() or glDrawArrays() or glDrawElements()
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4); 
	//	GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 4, 4); 

		//undind vao
		GLES32.glBindVertexArray(0);

	
		//un-use shader program
		GLES32.glUseProgram(0);

		//Render
		requestRender(); // Equivalent to double buffer
		
	}

	

	void uninitialize()
	{
		//code
		// destroy vaoSquare
		if(vaoSquare[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1, vaoSquare, 0);
			vaoSquare[0] = 0;

		}


		// destroy vbo_square_Position
		if(vbo_square_Position[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1, vbo_square_Position, 0);
			vbo_square_Position[0] = 0;

		}

		// destroy vbo_square_texture
		if(vbo_square_texture[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1, vbo_square_texture, 0);
			vbo_square_texture[0] = 0;

		}



		if(shaderProgramObject != 0)
		{
			if(vertexShaderObject != 0)
			{
				//detach vertex shader from shader program object
				GLES32.glDetachShader(shaderProgramObject, vertexShaderObject);
				//delete vertex shader object
				GLES32.glDeleteShader(vertexShaderObject);
				vertexShaderObject = 0;
			}

			if(fragmentShaderObject != 0)
			{
				//detach fragment shader from shader program object
				GLES32.glDetachShader(shaderProgramObject, fragmentShaderObject);
				//delete fragment shader object
				GLES32.glDeleteShader(fragmentShaderObject);
				fragmentShaderObject = 0;
			}
		}

		//delete shader program object
		if(shaderProgramObject != 0)
		{
		GLES32.glDeleteProgram(shaderProgramObject);
		shaderProgramObject = 0;
		}
	}

}

