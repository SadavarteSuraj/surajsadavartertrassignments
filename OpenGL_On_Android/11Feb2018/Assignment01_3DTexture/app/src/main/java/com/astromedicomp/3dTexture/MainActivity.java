package com.astromedicomp.threedTexture;

import android.app.Activity;
import android.os.Bundle;

import android.view.Window; // For Window class
import android.view.WindowManager; // For WindowManager class
import android.content.pm.ActivityInfo; // For ActivityInfo , pm - package Manager
import android.graphics.Color; // For Color class

public class MainActivity extends Activity 
{
	private GLESView glesView;

    @Override
    protected void onCreate(Bundle savedInstanceState) 
	{

	
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);

		// Remove Titlebar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);


		//Make fullscreen
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
		WindowManager.LayoutParams.FLAG_FULLSCREEN );

		//Set oritentation landscape
		MainActivity.this.setRequestedOrientation(
		ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

		// Set background color black
		getWindow().getDecorView().setBackgroundColor(Color.rgb(0,0,0));

		glesView = new GLESView(this);
		setContentView(glesView);
    }

	@Override
	protected void onPause()
	{
		super.onPause();
	}

	@Override
	protected void onResume()
	{
		super.onResume();
	}

}
