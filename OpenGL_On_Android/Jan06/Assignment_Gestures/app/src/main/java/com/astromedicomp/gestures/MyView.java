package com.astromedicomp.gestures;

import android.content.Context; // for drawing context related
import android.widget.TextView; // for "TextView" class
import android.graphics.Color; // For Color class
import android.view.Gravity; // For Gravity class
import android.view.MotionEvent; //For MotionEvent
import android.view.GestureDetector; // For Gesturedetector
import android.view.GestureDetector.OnGestureListener; // For OnGestureListener
import android.view.GestureDetector.OnDoubleTapListener; // For  OnDoubleTapListener

public class MyView extends TextView implements OnGestureListener, OnDoubleTapListener
{

private GestureDetector gestureDetector;

MyView(Context context)
	{	
		super(context);
		setText("Hello World !!!");
		setTextSize(60);
		setTextColor(Color.GREEN);
		setGravity(Gravity.CENTER);

		gestureDetector = new GestureDetector(context, this, null, false); // This means 'handler' ie who is going to handle
		gestureDetector.setOnDoubleTapListener(this); // This means 'handler' ie who is going to handle

	}

	//Handling OnTouchEvent is most important
	//Because it triggers all gestures and tap events
	@Override
	public boolean onTouchEvent(MotionEvent  event)
		{
		//code
		int eventaction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		return(true);
	}

	//Abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		setText("Double Tap");
		return(true);
	}

	//Abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		// Do not write any code here, because alrady written 'OnDoubleTap'
		return(true);
	}

	//Abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		setText("Single Tap");
		return(true);
	}

	//Abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onDown(MotionEvent e)
	{
		// Do not write any code here, because alrady written 'OnSingleTapConfirmed'
		return(true);
	}

	//Abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
	{
		return(true);
	}
	
	//Abstract method from OnDoubleTapListener so must be implemented
	@Override
	public void onLongPress(MotionEvent e)
	{
		setText("Long Press");
		
	}

	//Abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		setText("Scroll");
		return(true);
	}


//Abstract method from OnDoubleTapListener so must be implemented
	@Override
	public void onShowPress(MotionEvent e)
	{
	
	}

	//Abstract method from OnDoubleTapListener so must be implemented
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}

}